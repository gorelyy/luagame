#pragma once
#include <algorithm>

class FColor{
public:
    FColor();
    FColor(float red, float green, float blue, float alpha);
    FColor operator+(const FColor& right);
    FColor operator-(const FColor& right);
    FColor operator*(float right);
    float r,
          g,
          b,
          a;
};
