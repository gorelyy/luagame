
#include "luagame/components/QuadC.h"

QuadC::QuadC()
{
}

void QuadC::setSize(const sf::Vector2f& size)
{
    size_ = size;
}

void QuadC::setTextureRect(const sf::IntRect& rect)
{
    tex_coord_ = sf::Vector2f((float)rect.left,
                              (float)rect.top);
    tex_size_  = sf::Vector2f((float)rect.width,
                              (float)rect.height);
}

sf::Vector2f QuadC::getVertexCoord(uint index,
                                   const sf::Vector2f& scrolloffset,
                                   const sf::Vector2f& layercoord,
                                   const sf::Vector2f& layeroffset,
                                   const sf::Vector2f& layerscale,
                                   float layerangle,
                                   const sf::Vector2f& layerpivot)
{
    float angle = (angle_ + offset_angle_) *  utils::constants::RAD;
    sf::Vector2f scale(scale_ * offset_scale_);
    sf::Vector2f a(layercoord + layeroffset + (coord_ + offset_coord_  * scale + scrolloffset * scroll_factor_) * layerscale);
    sf::Vector2f pivot = a + pivot_ * layerscale * scale;
    switch(index){
        case 0:
            if (angle)
                a = sf::Vector2f(cosf(angle) * (a.x - pivot.x) - sinf(angle) * (a.y - pivot.y) + pivot.x,
                                 sinf(angle) * (a.x - pivot.x) + cosf(angle) * (a.y - pivot.y) + pivot.y);
            if(layerangle){
                angle = layerangle * utils::constants::RAD;
                pivot = layerpivot + layercoord + layeroffset;
                a = sf::Vector2f(cosf(angle) * (a.x - pivot.x) - sinf(angle) * (a.y - pivot.y) + pivot.x,
                                 sinf(angle) * (a.x - pivot.x) + cosf(angle) * (a.y - pivot.y) + pivot.y);
            }
            return a;
        case 3:
            a = a + sf::Vector2f(size_.x,0) * layerscale * scale;
            if(angle)
                a = sf::Vector2f(cosf(angle) * (a.x - pivot.x) - sinf(angle) * (a.y - pivot.y) + pivot.x,
                                 sinf(angle) * (a.x - pivot.x) + cosf(angle) * (a.y - pivot.y) + pivot.y);
            if(layerangle){
                angle = layerangle * utils::constants::RAD;
                pivot = layerpivot + layercoord + layeroffset;
                a = sf::Vector2f(cosf(angle) * (a.x - pivot.x) - sinf(angle) * (a.y - pivot.y) + pivot.x,
                                 sinf(angle) * (a.x - pivot.x) + cosf(angle) * (a.y - pivot.y) + pivot.y);
            }
            return a;
        case 2:
            a = a + size_ * layerscale * scale;
            if (angle)
                a = sf::Vector2f(cosf(angle) * (a.x - pivot.x) - sinf(angle) * (a.y - pivot.y) + pivot.x,
                                 sinf(angle) * (a.x - pivot.x) + cosf(angle) * (a.y - pivot.y) + pivot.y);
            if(layerangle){
                angle = layerangle * utils::constants::RAD;
                pivot = layerpivot + layercoord + layeroffset;
                a = sf::Vector2f(cosf(angle) * (a.x - pivot.x) - sinf(angle) * (a.y - pivot.y) + pivot.x,
                                 sinf(angle) * (a.x - pivot.x) + cosf(angle) * (a.y - pivot.y) + pivot.y);
            }
            return a;
        case 1:
            a = a + sf::Vector2f(0,size_.y) * layerscale * scale;
            if(angle)
                a = sf::Vector2f(cosf(angle) * (a.x - pivot.x) - sinf(angle) * (a.y - pivot.y) + pivot.x,
                                 sinf(angle) * (a.x - pivot.x) + cosf(angle) * (a.y - pivot.y) + pivot.y);
            if(layerangle){
                angle = layerangle * utils::constants::RAD;
                pivot = layerpivot + layercoord + layeroffset;
                a = sf::Vector2f(cosf(angle) * (a.x - pivot.x) - sinf(angle) * (a.y - pivot.y) + pivot.x,
                                 sinf(angle) * (a.x - pivot.x) + cosf(angle) * (a.y - pivot.y) + pivot.y);
            }
            return a;
    }
}

sf::Vector2f QuadC::getVertexTextureCoord(uint index)
{
    uint w = index / 2;
    uint h = (bool)((index + 3)%3);
    return sf::Vector2f(tex_coord_.x + w * tex_size_.x, tex_coord_.y + h * tex_size_.y);
}

const sf::Vector2f& QuadC::size() const
{
    return size_;
}

sf::IntRect QuadC::textureRect() const
{
    return sf::IntRect((int)tex_coord_.x,(int)tex_coord_.y,
                       (int)tex_size_.x, (int)tex_size_.y);
}

