TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
DEFINES += LUABIND_DYNAMIC_LINK
QMAKE_CXXFLAGS_RELEASE += /Ox

LIBS += -L"C:\lib\lua5.1\lib"
LIBS += -L"C:\lib\luabind-0.9.1\lib"
LIBS += -L"C:\lib\SFML\lib"

CONFIG(release, debug|release): LIBS += -lsfml-audio -lsfml-graphics -lsfml-network -lsfml-window -lsfml-system -lluabind -llua5.1
CONFIG(debug, debug|release): LIBS += -lsfml-audio-d -lsfml-graphics-d -lsfml-network-d -lsfml-window-d -lsfml-system-d -lluabindd -llua5.1

INCLUDEPATH += "C:\lib\lua5.1\include"
DEPENDPATH += "C:\lib\lua5.1\include"
INCLUDEPATH += "C:\lib\boost_1_55_0\include"
DEPENDPATH += "C:\lib\boost_1_55_0\include"
INCLUDEPATH += "C:\lib\SFML\include"
DEPENDPATH += "C:\lib\SFML\include"
INCLUDEPATH += "C:\lib\luabind-0.9.1\include"
DEPENDPATH += "C:\lib\luabind-0.9.1\include"
INCLUDEPATH += "E:\luagame\luagame\include"
DEPENDPATH += "E:\luagame\luagame\include"

SOURCES += \
    src/components/AnimC.cpp \
    src/components/BoxC.cpp \
    src/components/CameraC.cpp \
    src/components/CollisionCheckC.cpp \
    src/components/LayerC.cpp \
    src/components/LineC.cpp \
    src/components/MoveC.cpp \
    src/components/PointC.cpp \
    src/components/QuadC.cpp \
    src/components/ScriptC.cpp \
    src/systems/Collision.cpp \
    src/systems/Motion.cpp \
    src/systems/Renderer.cpp \
    src/systems/Scripting.cpp \
    src/AABB.cpp \
    src/Component.cpp \
    src/Core.cpp \
    src/Entity.cpp \
    src/FColor.cpp \
    src/Grid.cpp \
    src/Group.cpp \
    src/IdManager.cpp \
    src/ImgCache.cpp \
    src/Input.cpp \
    src/main.cpp \
    src/QTree.cpp \
    src/Scene.cpp \
    src/Utils.cpp \
    src/Pool.cpp \
    src/QTreeNode.cpp \
    src/Logger.cpp \
    src/components/TweenAngleC.cpp \
    src/components/TweenScaleC.cpp \
    src/components/TweenCoordC.cpp \
    src/Drawable.cpp \
    src/Audio.cpp \
    src/components/SoundC.cpp \
    src/SoundCache.cpp \
    src/systems/SoundMixer.cpp \
    src/FontCache.cpp \
    src/components/TextC.cpp \
    src/systems/Typewriter.cpp \
    src/ComponentContainer.cpp \
    src/EntityGroupRegistry.cpp \
    src/GroupContainer.cpp \
    src/VertexArrayContainer.cpp \
    src/components/TimerC.cpp \
    src/SceneBuilder.cpp \
    src/PointConnection.cpp \
    src/systems/PointGraph.cpp

HEADERS += \
    include/luagame/components/AnimC.h \
    include/luagame/components/BoxC.h \
    include/luagame/components/CameraC.h \
    include/luagame/components/CollisionCheckC.h \
    include/luagame/components/LayerC.h \
    include/luagame/components/LineC.h \
    include/luagame/components/MoveC.h \
    include/luagame/components/PointC.h \
    include/luagame/components/QuadC.h \
    include/luagame/components/ScriptC.h \
    include/luagame/systems/Collision.h \
    include/luagame/systems/Motion.h \
    include/luagame/systems/Renderer.h \
    include/luagame/systems/Scripting.h \
    include/luagame/AABB.h \
    include/luagame/BroadPhase.h \
    include/luagame/Component.h \
    include/luagame/ComponentContainer.h \
    include/luagame/Core.h \
    include/luagame/Entity.h \
    include/luagame/FColor.h \
    include/luagame/Grid.h \
    include/luagame/Group.h \
    include/luagame/GroupContainer.h \
    include/luagame/IdManager.h \
    include/luagame/ImgCache.h \
    include/luagame/Input.h \
    include/luagame/QTree.h \
    include/luagame/Scene.h \
    include/luagame/Utils.h \
    include/luagame/Pool.h \
    include/luagame/VertexArrayContainer.h \
    include/luagame/QTreeNode.h \
    include/luagame/Logger.h \
    include/luagame/Tweenable.h \
    include/luagame/globals.h \
    include/luagame/components/TweenScaleC.h \
    include/luagame/components/TweenAngleC.h \
    include/luagame/components/TweenCoordC.h \
    include/luagame/Drawable.h \
    include/luagame/Audio.h \
    include/luagame/components/SoundC.h \
    include/luagame/SoundCache.h \
    include/luagame/systems/SoundMixer.h \
    include/luagame/FontCache.h \
    include/luagame/components/TextC.h \
    include/luagame/systems/Typewriter.h \
    include/luagame/EntityGroupRegistry.h \
    include/luagame/luainterface.h \
    include/luagame/components/TimerC.h \
    include/luagame/SceneBuilder.h \
    include/luagame/PointConnection.h \
    include/luagame/systems/PointGraph.h

OTHER_FILES += \
    main.lua \
    settings.lua \
    scenes/pong.lua \
    fonts/square.ttf


