#pragma once
#include <SFML/Window.hpp>
#include <map>
#include <iostream>
#include <unordered_map>
#include <string>
#include <set>

class Input {
public:
    Input();
    sf::Vector2i mouseCoord()           const;
    sf::Vector2i mouseWindowCoord()     const;
    bool pressed(const std::string& name);
    bool justPressed(const std::string& name);
    bool justReleased(const std::string& name);
    void    update(const sf::Event& event);
    void    clear();
    void    setWindow(sf::Window* window);
private:    
    std::unordered_map<std::string, sf::Keyboard::Key> keymap_;
    std::map<std::string, sf::Mouse::Button> mousemap_;
    std::vector<sf::Event> current_events_;
    sf::Window* window_;
};
