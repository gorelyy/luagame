
#include "luagame/systems/Scripting.h"

Scripting::Scripting() 
{
}

void Scripting::update(Scene* scene)
{
    scene->eachC<ScriptC>(scene->queue(Scene::SCRIPTING),[this](ScriptC* script)
    {
        script->update();
    });
}

void Scripting::postUpdate(Scene* scene)
{
    scene->eachC<ScriptC>(scene->queue(Scene::SCRIPTING),[this](ScriptC* script)
    {
        script->postUpdate();
    });
}
