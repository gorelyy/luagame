
#include "luagame/components/CollisionCheckC.h"

CollisionCheckC::CollisionCheckC() :
    collision_area_(sf::IntRect(0,0,0,0)),
    collision_response_status_(true),
    broad_phase_type_(BROADPHASE::OFF),
    draw_debug_mode_(DRAWDEBUGMODE::NONE),
    groupBstatic_(false),
    aabbdebugcolor_(sf::Color::White),
    broadphasedebugcolor_(sf::Color::White)
{
}

void CollisionCheckC::setCollisionGroups(const std::string& groupA, const std::string& groupB)
{
    groupA_ = groupA;
    groupB_ = groupB;
}

void CollisionCheckC::setCollisionArea(int left, int top, int width, int height)
{
     collision_area_ = sf::IntRect(left, top, width, height);
}

void CollisionCheckC::setTreeDepth(uint depth)
{
     treedepth_ = depth;
}

void CollisionCheckC::setCollisionResponse(bool status)
{
    collision_response_status_ = status;
}

bool CollisionCheckC::collisionResponseEnabled() const
{
    return collision_response_status_;
}

void CollisionCheckC::setTilemapGroup(const std::string& group)
{
    groupA_ = group;
}

const std::string& CollisionCheckC::groupA() const
{
    return groupA_;
}

const std::string& CollisionCheckC::groupB() const
{
    return groupB_;
}

const sf::Color& CollisionCheckC::aabbColor() const
{
    return aabbdebugcolor_;
}

const sf::Color& CollisionCheckC::broadphaseColor() const
{
    return broadphasedebugcolor_;
}

void CollisionCheckC::registerCollision(const componentID& boxA)
{
    collisions_list_.push_back(boxA);
}

void CollisionCheckC::clearCollisions()
{
    collisions_list_.clear();
}

const sf::IntRect& CollisionCheckC::collisionArea() const
{
    return collision_area_;
}

void CollisionCheckC::eachBoxA(const luabind::object& luafunction)
{
    luafunction_stack_.push_back(luafunction);
    for(auto& each : collisions_list_)
        luafunction_stack_.back()(&each);
    luafunction_stack_.pop_back();
}

void CollisionCheckC::setGridCellSize(const sf::Vector2f& size)
{
    cell_size_ = size;
}

const sf::Vector2f& CollisionCheckC::cellSize() const
{
    return cell_size_;
}

void CollisionCheckC::setBroadPhaseType(CollisionCheckC::BROADPHASE type)
{
    broad_phase_type_ = type;
}

void CollisionCheckC::setDrawDebugMode(CollisionCheckC::DRAWDEBUGMODE mode)
{
    draw_debug_mode_ = mode;
}

CollisionCheckC::BROADPHASE CollisionCheckC::broadPhaseType() const
{
    return broad_phase_type_;
}

CollisionCheckC::DRAWDEBUGMODE CollisionCheckC::drawDebugMode() const
{
    return draw_debug_mode_;
}

bool CollisionCheckC::isGroupBStatic() const
{
    return groupBstatic_;
}

uint CollisionCheckC::treeDepth() const
{
    return treedepth_;
}

void CollisionCheckC::setAABBDebugColor(const sf::Color &color)
{
    aabbdebugcolor_ = color;
}

void CollisionCheckC::setBroadPhaseDebugColor(const sf::Color &color)
{
    broadphasedebugcolor_ = color;
}

void CollisionCheckC::setDebugColor(const sf::Color &color)
{
    aabbdebugcolor_ = color;
    broadphasedebugcolor_ = color;
}
