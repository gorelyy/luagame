#include "luagame/components/TweenAngleC.h"

TweenAngleC::TweenAngleC()
{
}

void TweenAngleC::set(float angle, float time)
{
    tween_float_.set(angle,time);
}

void TweenAngleC::set(float angle, float time, Ease::FUNCTION f, Ease::TYPE type)
{
    tween_float_.set(angle,time,f,type);
}

void TweenAngleC::set(float angle, float time, Ease::FUNCTION f, Ease::TYPE type, int repeat, unsigned char mode)
{
    tween_float_.set(angle,time,f,type,repeat, mode);
}

void TweenAngleC::addCallbackOnFinish(const luabind::object &callback)
{
    tween_float_.addCallbackOnFinish(callback);
}

void TweenAngleC::addCallbackOnRepeat(const luabind::object &callback)
{
    tween_float_.addCallbackOnRepeat(callback);
}

void TweenAngleC::addCustomEasing(const luabind::object &f)
{
    tween_float_.addCustomEasing(f);
}

void TweenAngleC::update(double dt)
{
    tween_float_.update(dt);
}

float TweenAngleC::angle() const
{
    return tween_float_.value();
}

bool TweenAngleC::isEnabled() const
{
    return tween_float_.isEnabled();
}

