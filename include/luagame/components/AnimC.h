#pragma once
#include "../Component.h"
#include <SFML/Graphics/Rect.hpp>
#include <SFML/System/Vector2.hpp>
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <luabind/luabind.hpp>

class AnimC: public Component {
public:
    AnimC();
    enum REPEAT_MODE{
        LOOP,
        PENDULUM
    };
    uint    currentFrame()              const;
    const sf::IntRect&  textureRect()   const;
    const sf::IntRect&  textureBounds() const;
    const sf::Vector2f& frameSize()     const;
    bool    update(double dt);
    void    addAnimation(const std::string& name, const sf::IntRect &texture_bounds, const luabind::object& sequence);
    void    addAnimation(const std::string& name, const sf::IntRect &texture_bounds, uint i_start, uint i_end);
    void    addAnimation(const std::string& name, const luabind::object& sequence);
    void    addAnimation(const std::string& name, uint i_start, uint i_end);
    void    addCallbackOnFinish(const std::string& name, const luabind::object& f);
    void    addCallbackOnFrame(const std::string& name, int frame, const luabind::object& f);
    void    play(const std::string& name, double fps);
    void    play(const std::string& name, double fps, int repeat);
    void    play(const std::string& name, double fps, int repeat, REPEAT_MODE repeatmode);
    void    playNext(const std::string& name, double fps);
    void    playNext(const std::string& name, double fps, int repeat);
    void    playNext(const std::string& name, double fps, int repeat, REPEAT_MODE repeatmode);
    void    playNext(uint start_frame, const std::string& name, double fps);
    void    playNext(uint start_frame, const std::string& name, double fps, int repeat);
    void    playNext(uint start_frame, const std::string& name, double fps, int repeat, REPEAT_MODE repeatmode);
    void    setFrameSize(const sf::Vector2f& size);
    void    setTextureBounds(const sf::IntRect &texture_bounds);
private:
    struct AnimCallback{
        AnimCallback() : frame(-2)
        {
        }
        luabind::object onfinish;
        luabind::object onframe;
        int frame;
    };

    bool        start_;
    bool        play_;
    bool        nextframenewanim_;
    double      looptime_;
    double      playbackspeed_;
    double      next_playbackspeed_;
    int         max_loop_;
    int         next_max_loop_;
    int         loop_counter_;
    int         callbackframe_;
    REPEAT_MODE repeat_mode_;
    REPEAT_MODE next_repeat_mode_;
    sf::IntRect frame_texrect_;
    sf::IntRect texture_bounds_;
    sf::Vector2f frame_size_;
    std::string currentplay_;
    std::string next_play_;
    int next_play_start_frame_;
    std::vector<uint> currentseq_;
    int currentframe_;
    std::map<std::string, std::vector<uint> > animlist_;
    std::map<std::string,AnimCallback> callbacklist_;
};



