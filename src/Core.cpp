#include "luagame/Core.h"
#include <iostream>
#include "luagame/luainterface.h"

Core::Core() :
    exit_(false),
	firstloop_(true)
{
}

void Core::run() 
{
    core_lua_state_ = luaL_newstate();
    luainterface::initLuaState(core_lua_state_);
    luabind::globals(core_lua_state_)["_core"] = this;
    luabind::globals(core_lua_state_)["_input"] = &input_;
    luabind::globals(core_lua_state_)["_audio"] = &audio_;

    sf::Vector2f window_resolution;
    std::string title;
    uint screenmode;
    uint colordepth;
    sf::Color windowcolor;
    double updaterate;

    try{
        luabind::object require = luabind::globals(core_lua_state_)["require"];
        luabind::object settings = require("settings");
        luabind::object window_settings = settings["window"];
        title =  luabind::object_cast<std::string>(window_settings["title"]);
        window_resolution = luabind::object_cast<sf::Vector2f>(window_settings["resolution"]);
        colordepth = luabind::object_cast<uint>(window_settings["colordepth"]);
        screenmode = (luabind::object_cast<bool>(window_settings["fullscreen"])) ? sf::Style::Fullscreen : sf::Style::Default;
        vsync_ = luabind::object_cast<bool>(window_settings["vsync"]);
        windowcolor = luabind::object_cast<sf::Color>(window_settings["color"]);
        framerate_ = luabind::object_cast<uint>(window_settings["framerate"]);
        if (vsync_ && !framerate_)
            framerate_ = 60;
        updaterate = luabind::object_cast<double>(window_settings["updaterate"]);
    }
    catch(const luabind::error &e){
        std::cerr<<"luagame_error reading window settings: "<<e.what();
        luabind::object error_msg(luabind::from_stack(e.state(), -1));
        std::cerr<<error_msg<<std::endl;
        exit_ = true;
    }

    framerate_cap_ = (framerate_ > 0)? 1.0 / (double)framerate_ : 0;
    dt_ = (updaterate > 0)? 1.0 / updaterate : 1.0 / 30.0;

    window_.create(sf::VideoMode((uint)window_resolution.x, (uint)window_resolution.y, colordepth),title, screenmode);
    window_.setVerticalSyncEnabled(vsync_);
    window_.setKeyRepeatEnabled(false);
    renderer_.setWindow(&window_,windowcolor);

    try{
        luabind::object callCreate, callInit;
        luabind::object require = luabind::globals(core_lua_state_)["require"];
        luabind::object main = require("main");
        callCreate = main["create"];
        callInit = main["init"];
        update_function_ = main["update"];
        callCreate();
        if (callInit.is_valid() && luabind::type(callInit) != LUA_TNIL)
            callInit();
    }
    catch(const luabind::error &e){
        std::cerr<<"luagame_error while initialization: "<<e.what();
        luabind::object error_msg(luabind::from_stack(e.state(), -1));
        std::cerr<<error_msg<<std::endl;
        exit_ = true;
    }
    sound_mixer_.bindAudio(&audio_);
    mainLoop();
    window_.close();    
}

void Core::mainLoop()
{    

    sf::Event event;
    double currenttime = 0.0;
    double accumulator = 0.0;
    double frametimebuffer = 0;
    while (!exit_){
        double newtime = firstloop_? dt_ : clock_.getElapsedTime().asSeconds();
        firstloop_ = false;
        double frametime = newtime - currenttime;

        if (vsync_){
            frametime += frametimebuffer;
            int framecount = (int)(frametime * framerate_ + 1);
            if (framecount <= 0) framecount = 1;
            double oldframetime = frametime;
            frametime = framecount / (double)framerate_;
            frametimebuffer = oldframetime - frametime;
        }
        currenttime = newtime;

        if (frametime > 5 * dt_)
            frametime = 5 * dt_;

        accumulator += frametime ;

        while(accumulator >= dt_){
            accumulator -= dt_;
            while (window_.pollEvent(event)){
                if (event.type == sf::Event::Closed)
                    exit_ = true;
                input_.update(event);    
            }

            try{
                current_scene_->eachC<TimerC>([&](TimerC* timer){timer->update(dt_);});
                scripting_.update(current_scene_);
                current_scene_->updateScript();
                if (update_function_.is_valid() && luabind::type(update_function_) != LUA_TNIL)
                    update_function_(current_scene_);
                input_.clear();
            }
            catch(const luabind::error &e){
                std::cerr<<"luagame_error while updating scripts: "<<e.what();
                luabind::object error_msg(luabind::from_stack(e.state(), -1));
                std::cerr<<error_msg<<std::endl;
                exit_ = true;
            }
            typewriter_.update(current_scene_,dt_);
            motion_.update(current_scene_,dt_);
            collision_.update(current_scene_);
            pointgraph_.update(current_scene_,dt_);
            try{
                scripting_.postUpdate(current_scene_);
                current_scene_->postUpdate();
            }
            catch(const luabind::error &e){
                std::cerr<<"luagame_error while postupdating scripts: "<<e.what();
                luabind::object error_msg(luabind::from_stack(e.state(), -1));
                std::cerr<<error_msg<<std::endl;
                exit_ = true;
            }
            for (lua_State* state : deleted_lua_states_)
                lua_close(state);
            deleted_lua_states_.clear();
            logger_.update();
        }

        double interpolation = accumulator/dt_;
        renderer_.update(current_scene_,dt_,interpolation,frametime);
        sound_mixer_.update(current_scene_,interpolation);

        newtime = clock_.getElapsedTime().asSeconds();
        while (!vsync_ && framerate_cap_ > 0 && newtime - currenttime < framerate_cap_)
            newtime = clock_.getElapsedTime().asSeconds();
    }
}

void Core::addScene(const std::string& name, const std::string& script_name)
{
    if (scene_.count(name) > 0)
        return;
            
    Scene& scene = scene_[name];
    scene.setName(name);
    lua_State * scene_lua_state = luaL_newstate(); 
    lua_state_map_[name] = scene_lua_state;
    luainterface::initLuaState(scene_lua_state);
    luabind::globals(scene_lua_state)["_core"] = this;
    luabind::globals(scene_lua_state)["_input"] = &input_;
    luabind::globals(scene_lua_state)["_audio"] = &audio_;
    scene.bindLuaState(scene_lua_state);
    scene.bindLogger(&logger_);
    scene.addStartScript(script_name);
    scene.setImageCache(&image_cache_);
    scene.setSoundCache(&sound_cache_);
    scene.setFontCache(&font_cache_);
    scene.setWindowSize(window_.getSize());
    scene.setUpdateRate(dt_);
    scene.createDefaultGroups();
    Entity* audiofocus = scene.addEntity("_audiofocus");
    audio_.bindFocus(audiofocus);
}

void  Core::buildScene(Scene *scene)
{
    scenebuilder_.build(scene);
}

void  Core::asyncBuildScene(Scene *scene)
{
    scenebuilder_.asyncBuild(scene);
}

void Core::deleteScene(const std::string &name)
{
    if (!scene_.size() || !scene_.count(name))
        return;
    if (current_scene_ == &scene_[name]){
        luabind::globals(core_lua_state_)["_scene"] = luabind::nil;
        current_scene_ = nullptr;
    }
    scene_.erase(name);
    deleted_lua_states_.push_back(lua_state_map_[name]);
    lua_state_map_.erase(name);
}

void Core::deleteScene(Scene *scene)
{
    deleteScene(scene->name());
}

void Core::resetScene(std::string name)
{
    if (!scene_.size() || !scene_.count(name))
        return;
    std::string scriptname = scene_[name].startScriptName();
    deleteScene(name);
    addScene(name, scriptname);
    selectScene(name);
    scenebuilder_.build(&scene_[name]);
}

void Core::resetScene(Scene *scene)
{
    resetScene(scene->name());
}

Scene* Core::scene(const std::string& name)
{
    if(scene_.count(name) > 0)
        return &(scene_[name]);
    else
        return nullptr;
}

void Core::selectScene(const std::string& name)
{    
    if(!scene_.count(name))
        return;
    current_scene_ = &(scene_[name]);
    luabind::globals(core_lua_state_)["_scene"] = current_scene_;
    luabind::globals(lua_state_map_[name])["_scene"] = current_scene_;
}

void Core::exitNow()
{
    exit_ = true;
}

const sf::Texture* Core::getTexture(const std::string& name)
{
    return image_cache_.getTexture(name);
}

const sf::SoundBuffer* Core::getSound(const std::string &name)
{
    return sound_cache_.getSound(name);
}

sf::Music* Core::getSoundStream(const std::string &name)
{
    return sound_cache_.getStream(name);
}

const sf::Font* Core::getFont(const std::string &name)
{
    return font_cache_.getFont(name);
}

void Core::asyncLoadTexture(const std::string& name)
{
    image_cache_.asyncLoadTexture(name);
}

void Core::asyncLoadSound(const std::string &name)
{
    sound_cache_.asyncLoadSound(name);
}

void Core::resetLogger()
{
    logger_.reset();
}

