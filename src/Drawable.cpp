#include "luagame/Drawable.h"

Drawable::Drawable() :
    scale_(sf::Vector2f(1.f,1.f)),
    offset_scale_(sf::Vector2f(1.f,1.f)),
    angle_(0.f),
    offset_angle_(0.f),
    color_(sf::Color::White),
    visible_(true)
{
}

bool Drawable::isVisible() const
{
    return visible_;
}

void Drawable::setCoord(const sf::Vector2f& coord)
{
    coord_ = coord;
}

void Drawable::setOffsetCoord(const sf::Vector2f &offset)
{
    offset_coord_ = offset;
}

void Drawable::setScale(const sf::Vector2f &scale)
{
    scale_ = scale;
}

void Drawable::setOffsetScale(const sf::Vector2f &scale)
{
    offset_scale_ = scale;
}

void Drawable::setAngle(float angle)
{
    angle_ = (float)(fmod(angle, 360));
    if (angle_ < 0)
        angle_ += 360.f;
}

void Drawable::setOffsetAngle(float angle)
{
    offset_angle_ = (float)(fmod(angle, 360));
    if (offset_angle_ < 0)
        offset_angle_ += 360.f;
}

void Drawable::setPivot(const sf::Vector2f& pivot)
{
    pivot_ = pivot;
}

void Drawable::setScrollFactor(const sf::Vector2f &scrollfactor)
{
    scroll_factor_ = scrollfactor;
}

void Drawable::setColor(const sf::Color &color)
{
    color_ = color;
}

void Drawable::setVisibility(bool value)
{
    visible_ = value;
}

const sf::Vector2f& Drawable::coord() const
{
    return coord_;
}

const sf::Vector2f& Drawable::offsetCoord() const
{
    return offset_coord_;
}

const sf::Vector2f& Drawable::scale() const
{
    return scale_;
}

const sf::Vector2f& Drawable::offsetScale() const
{
    return offset_scale_;
}


float Drawable::angle() const
{
    return angle_;
}

float Drawable::offsetAngle() const
{
    return offset_angle_;
}

const sf::Vector2f& Drawable::pivot() const
{
    return pivot_;
}

const sf::Color& Drawable::color() const
{
    return color_;
}

const sf::Vector2f& Drawable::scrollFactor() const
{
    return scroll_factor_;
}

