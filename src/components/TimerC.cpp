#include "luagame/components/TimerC.h"

TimerC::TimerC() :
    goal_(0.f),
    elapsed_(0.f),
    loop_(0),
    loopsnum_(0)
{
}

void TimerC::set(float goal, int loop)
{
    if (loop_)
        return;
    goal_ = goal;
    elapsed_ = 0.f;
    loop_ = loop;
    loopsnum_ = 0;
}

void TimerC::set(float goal, int loop, const luabind::object &callback)
{
    if (loop_)
        return;
    set(goal, loop);
    if(callback.is_valid() && luabind::type(callback) == LUA_TFUNCTION)
        callback_.push_front(callback);
}

void TimerC::set(float goal)
{
    if (loop_)
        return;
    set(goal,1);
}

void TimerC::update(float dt)
{
    if (!loop_)
        return;
    elapsed_ += dt;
    if (elapsed_ >= goal_){
        loopsnum_++;
        elapsed_ = 0.f;
        loop_--;
        bool pop = false;
        if (!loop_)
            pop = true;
        if (!callback_.empty())
            callback_.back()();
        if (pop)
            callback_.pop_back();
    }
}

float TimerC::remains() const
{
    return goal_ - elapsed_;
}

float TimerC::elapsed() const
{
    return elapsed_;
}

int TimerC::loopNumber() const
{
    return loopsnum_;
}
