#include "luagame/Scene.h"
#include "luagame/ImgCache.h"
#include "luagame/FontCache.h"
#include "luagame/SoundCache.h"
#include "luagame/Logger.h"
#include "luagame/SceneBuilder.h"

Scene::Scene() :
    constructed_(false),
    async_build_started_(false)
{
}

Entity* Scene::addEntity(const std::string& name, const sf::Vector2f& coord)
{
    ID id  =  id_manager_.getNewID();
    Entity& entity = entity_container_[id];
    entitynames_[&entity] = name;
    entity.bindComponentContainer(&component_container_);
    entity.bindPointConnection(&pointconnection_);
    entity.bindLua(scene_lua_state_);
    entity.setID(id);
    entity.addComponent<PointC>();
    PointC* point = entity.getComponent<PointC>();
    point->set(coord);
    luabind::globals(scene_lua_state_)["_e"][name] = &entity;
    Pool& pool = pool_container_[id] ;
    pool.setID(id);
	pool.bindComponentContainer(&component_container_);
    luabind::globals(scene_lua_state_)["_pool"][name] = &pool;
    group_container_.group(Group::BUILD)->add(id);
    return &entity;
}

Entity* Scene::addEntity(const std::string& name)
{
    luabind::object e =  luabind::globals(scene_lua_state_)["_e"][name];
    if (luabind::type(e) != LUA_TNIL)
        return luabind::object_cast<Entity*>(e);
    return addEntity(name, sf::Vector2f());
}

Entity* Scene::addEntity(const std::string &name, const luabind::object &buildfunction)
{
    luabind::object entity = luabind::globals(scene_lua_state_)["_e"][name];
    if (luabind::type(entity) != LUA_TNIL)
        return luabind::object_cast<Entity*>(entity);

    Entity* e = addEntity(name, sf::Vector2f());
    if (buildfunction.is_valid() && luabind::type(buildfunction) == LUA_TFUNCTION){
        try{
            luabind::object f = buildfunction;
            f(e);
        }
        catch(const luabind::error &e){
            std::cerr<<"luagame_error while adding new entity:"<<e.what();
            luabind::object error_msg(luabind::from_stack(e.state(), -1));
            std::cerr<<error_msg<<std::endl;
        }
    }
    scenebuilder_->postBuild(this);
    return e;
}

Entity* Scene::addEntity(const std::string &name, const sf::Vector2f &coord, const luabind::object &buildfunction)
{
    luabind::object entity = luabind::globals(scene_lua_state_)["_e"][name];
    if (luabind::type(entity) != LUA_TNIL)
        return luabind::object_cast<Entity*>(entity);

    Entity* e = addEntity(name, coord);
    if (buildfunction.is_valid() && luabind::type(buildfunction) == LUA_TFUNCTION){
        try{
            luabind::object f = buildfunction;
            f(e);
        }
        catch(const luabind::error &e){
            std::cerr<<"luagame_error while adding new entity:"<<e.what();
            luabind::object error_msg(luabind::from_stack(e.state(), -1));
            std::cerr<<error_msg<<std::endl;
        }
    }
    scenebuilder_->postBuild(this);
    return e;
}

void Scene::deleteEntity(Entity* entity)
{
    if (!entitynames_.count(entity))
        return;
    std::string name = entitynames_[entity];
    ID id = entity->id();

    entity->each<QuadC>([&](QuadC* quad)
    {
        if (quad->poolID() != id){
            vertex_array_container_.deleteVertices(quad->poolID(),quad->poolIndex(),4);
            size_t deletedquadpoolindex = quad->poolIndex();
            eachC<QuadC>(quad->poolID(),[deletedquadpoolindex](QuadC* q)
            {
                if (q->poolIndex() >= deletedquadpoolindex)
                    q->setPoolIndex(q->poolIndex() - 1);
            });
            quad->setEnabled(false);
        }
    });

    entity->each<LineC>([&](LineC* line)
    {
        if (line->poolID() != id){
            vertex_array_container_.deleteVertices(line->poolID(),line->poolIndex(),2);
            size_t deletedlinepoolindex = line->poolIndex();
            eachC<LineC>(line->poolID(),[deletedlinepoolindex](LineC* l)
            {
                if (l->poolIndex() >= deletedlinepoolindex)
                    l->setPoolIndex(l->poolIndex() - 1);
            });
            line->setEnabled(false);
         }
    });



    for(Group* group : entity_group_registry_.entityGroups(id))
         group->remove(id);

	luabind::globals(scene_lua_state_)["_e"][name] = luabind::nil;
    luabind::globals(scene_lua_state_)["_pool"][name] = luabind::nil;
    id_manager_.removeID(id);
    pool_container_.erase(id);
    entity_container_.erase(id);
    component_container_.deleteEntity(id);
    vertex_array_container_.deleteVertexArray(id);
    if (pointconnection_.parentRegistered(componentID(id,0))){
        auto children = pointconnection_.children(componentID(id,0));
        pointconnection_.removeParentingNode(componentID(id,0));
        for (componentID child : children)
            deleteEntity(&entity_container_[child.first]);
    }
    if (pointconnection_.leaderRegistered(componentID(id,0))){
        pointconnection_.removeLeadingNode(componentID(id,0));
    }
}

Entity* Scene::getEntity(ID id)
{
    if (entity_container_.count(id))
        return &entity_container_[id];
    else return nullptr;
}

void Scene::bindLogger(Logger *logger)
{
    logger_ = logger;
}

void Scene::log(const std::string &name, const std::string &value)
{
    logger_->log(name,value);
}

Pool* Scene::addPool(const std::string &name)
{
    luabind::object e =  luabind::globals(scene_lua_state_)["_pool"][name];
    if (luabind::type(e) != LUA_TNIL)
        return luabind::object_cast<Pool*>(e);
    ID id  =  id_manager_.getNewID();
    Pool& pool = pool_container_[id] ;
    pool.setID(id);
	pool.bindComponentContainer(&component_container_);
    pool.setCapacity<LayerC>(1);
    LayerC layer;
    layer.setID(0);
    layer.setPointIndex(0);
    layer.setLuaState(scene_lua_state_);
    auto address = component_container_.addComponent(layer,id);
    pool.bindLayer(component_container_.getComponent<LayerC>(address));
    luabind::globals(scene_lua_state_)["_pool"][name] = &pool;
    group_container_.group(Group::BUILD)->add(id);
    return &pool;
}

void Scene::addCamera(const std::string& name, CameraC::MODE mode, const sf::Vector2f& size, const sf::Vector2f& coord,
                          const sf::IntRect& scrollarea, const sf::Color& color )
{
    Entity*  screen = addEntity("_camera_" +name + "_screen",sf::Vector2f());
    CameraC* camera = screen->addComponent<CameraC>();
    camera->queue()->bindEntityContainer(&entity_container_);
    camera->queue()->bindEntityGroupRegistry(&entity_group_registry_);
    camera->set(mode,size);
    if (mode == CameraC::WINDOW){
        camera->setWindowSize(window_size_);
        camera->setWindowOffset(coord);
    }
    else{
        PointC*  screenpoint = screen->getComponent<PointC>();
        LayerC*  layer = screen->addComponent<LayerC>();
        QuadC*   quad = screen->addComponent<QuadC>();
        group_container_.defaultGroup<LayerC>()->remove(layer->entityID());
        if (camera->texture())
            layer->bindTexture(camera->texture());
        quad->setSize(size);
        quad->setTextureRect(sf::IntRect(0,0,size.x,size.y));
        screenpoint->set(coord);
        if (mode == CameraC::WINDOWTEXTURE){
            if (coord.x == -1 && coord.y == -1)
                screenpoint->set((window_size_.x - size.x) / 2, (window_size_.y - size.y) / 2);
            group_container_.group(Group::RENDER_TO_WINDOW)->add(screen->id());
            group_container_.defaultGroup<CameraC>()->setPriority(screen->id(),255);
        }
    }
    camera->setBackgroundColor(color);
    camera->setScrollArea(scrollarea);
    Entity* focus = addEntity("_camera_" + name + "_focus",sf::Vector2f());
    PointC* focuspoint = focus->getComponent<PointC>();
    focus->addComponent<MoveC>();
    focus->addComponent<LayerC>();
    focuspoint->set(sf::Vector2f(coord.x + size.x / 2,coord.y + size.y / 2));
    camera->setFocus(focus->id());
    std::string addtable = "_camera[\""+name+"\"] = {}\n";
    luaL_dostring(scene_lua_state_,addtable.c_str());
    luabind::globals(scene_lua_state_)["_camera"][name]["screen"] = screen;
    luabind::globals(scene_lua_state_)["_camera"][name]["focus"] = focus;
    screen->lock();
    focus->lock();
}

void Scene::addCamera(const std::string& name, CameraC::MODE mode, const sf::Vector2f& size,
                      const sf::Vector2f& coord, const sf::IntRect& scrollborders)
{
    addCamera(name, mode, size, coord, scrollborders, (mode == CameraC::TEXTURE) ? sf::Color::Transparent : sf::Color::Black);
}

void Scene::addCamera(const std::string& name, CameraC::MODE mode, const sf::Vector2f& size, const sf::Vector2f& coord)
{
    addCamera(name, mode, size, coord, sf::IntRect(-32000,-32000,64000,64000), (mode == CameraC::TEXTURE) ? sf::Color::Transparent : sf::Color::Black);
}

void Scene::addCamera(const std::string& name, CameraC::MODE mode, const sf::Vector2f& size)
{
    addCamera(name, mode, size, sf::Vector2f(), sf::IntRect(-32000,-32000,64000,64000), (mode == CameraC::TEXTURE) ? sf::Color::Transparent : sf::Color::Black);
}

void Scene::addText(const std::string &name, const std::string& textstring,int maxsize, const std::string &fontname,
                    int fontsize, const sf::Color& textcolor, TextC::TEXTTWEEN tweentype, const sf::IntRect &textarea, const sf::Color& textareacolor)
{
    luabind::object e = luabind::globals(scene_lua_state_)["_e"][name];
    if (luabind::type(e) != LUA_TNIL)
        return;
    maxsize = maxsize + 1; //Последний символ новой строки, на случай если его нет
    Entity* entity = addEntity(name,sf::Vector2f());
    LayerC* layer = entity->addComponent<LayerC>();
    TextC* text = entity->addComponent<TextC>();
    const sf::Font* font = font_cache_->getFont(fontname);
    text->bindFont(font);
    text->setFontSize(fontsize);
    text->setTextTween(tweentype);
    text->setCapacity(maxsize);
    text->setColor(textcolor);

    layer->bindTexture(text->texture());
    for (int i = 0; i < maxsize; ++i){
        entity->addComponent<PointC>();
        QuadC* quad = entity->addComponent<QuadC>();
        quad->setColor(sf::Color::Transparent);
        if (tweentype == TextC::ALL){
            entity->addComponent<TweenCoordC>();
            entity->addComponent<TweenScaleC>();
            entity->addComponent<TweenAngleC>();
        }
        else if(tweentype == TextC::NONE){}
        else{
            if(tweentype == TextC::COORD)
                entity->addComponent<TweenCoordC>();
            if(tweentype == TextC::SCALE)
                entity->addComponent<TweenScaleC>();
            if(tweentype == TextC::ANGLE)
                entity->addComponent<TweenAngleC>();
        }
    }
    if (textstring.size()){
        text->setText(textstring);
        const std::wstring& str = text->textString();
        for(int i = 0, limit = std::min(text->capacity(), str.size()); i < limit; ++i){
            PointC* point = entity->getComponent<PointC>(i + 1);
            point->set(text->iterationCharacterCoord(i));
            QuadC* quad = entity->getComponent<QuadC>(i);
            if (!text->characterIsValid(i)){
                quad->setTextureRect(sf::IntRect());
                quad->setSize(sf::Vector2f());
                quad->setColor(sf::Color::Transparent);
            }
            else{
                quad->setTextureRect(text->characterTextureRect(i));
                quad->setSize(text->characterSize(i));
                quad->setColor(text->color());
            }
        }
        text->updated();
    }
    entity->lock();
    if (!textarea.width){
        return;
    }

    addCamera(name + "_camera",CameraC::TEXTURE,
              sf::Vector2f(textarea.width,textarea.height),
              sf::Vector2f(textarea.left,textarea.top),sf::IntRect(),textareacolor);

    Entity* screen = luabind::object_cast<Entity*>(luabind::globals(scene_lua_state_)["_camera"][name + "_camera"]["screen"]);
    Entity* focus = luabind::object_cast<Entity*>(luabind::globals(scene_lua_state_)["_camera"][name + "_camera"]["focus"]);

    entity->addChild(screen);
    screen->getComponent<PointC>()->set(sf::Vector2f(textarea.left,textarea.top));
    entity->addChild(focus);
    focus->getComponent<PointC>()->set(sf::Vector2f(textarea.left + textarea.width / 2, textarea.top + textarea.height / 2));

    CameraC* camera = screen->getComponent<CameraC>();
    camera->queue()->add(layer->entityID());
    if (textareacolor == sf::Color::Transparent)
        camera->setBlendMode(sf::BlendNone);
    group_container_.defaultGroup<LayerC>()->remove(layer->entityID());
    group_container_.defaultGroup<LayerC>()->add(screen->id());
}

void Scene::addText(const std::string &name, const std::string& textstring,int maxsize, const std::string &fontname,
                    int fontsize, const sf::Color& textcolor, TextC::TEXTTWEEN tweentype, const sf::IntRect &textarea)
{
    addText(name,textstring, maxsize, fontname, fontsize, textcolor, tweentype, textarea, sf::Color::Transparent);
}


void Scene::addText(const std::string &name, const std::string& textstring,int maxsize, const std::string &fontname,
                    int fontsize, const sf::Color& textcolor, TextC::TEXTTWEEN tweentype)
{
    addText(name,textstring, maxsize, fontname, fontsize, textcolor, tweentype, sf::IntRect(), sf::Color::Transparent);
}

void Scene::addText(const std::string &name, const std::string& textstring,int maxsize, const std::string &fontname,
                    int fontsize, const sf::Color& textcolor)
{
    addText(name,textstring, maxsize, fontname, fontsize, textcolor, TextC::NONE, sf::IntRect(), sf::Color::Transparent);
}

void Scene::addText(const std::string &name, const std::string& textstring,int maxsize, const std::string &fontname,
                    int fontsize)
{
    addText(name,textstring, maxsize, fontname, fontsize, sf::Color::White, TextC::NONE, sf::IntRect(), sf::Color::Transparent);
}



void Scene::addText(const std::string &name, const std::string& textstring, const std::string &fontname,int fontsize)
{
    addText(name,textstring, textstring.size(), fontname, fontsize, sf::Color::White, TextC::NONE, sf::IntRect(), sf::Color::Transparent);
}

void Scene::addText(const std::string &name, const std::string& textstring, const std::string &fontname,
                    int fontsize, const sf::Color& textcolor)
{
    addText(name,textstring, textstring.size(), fontname, fontsize, textcolor, TextC::NONE, sf::IntRect(), sf::Color::Transparent);
}

void Scene::addText(const std::string &name, const std::string& textstring, const std::string &fontname,
                    int fontsize, const sf::Color& textcolor, TextC::TEXTTWEEN tweentype)
{
    addText(name,textstring, textstring.size(), fontname, fontsize, textcolor, tweentype, sf::IntRect(), sf::Color::Transparent);
}

void Scene::addText(const std::string &name, const std::string& textstring, const std::string &fontname,
                    int fontsize, const sf::Color& textcolor, TextC::TEXTTWEEN tweentype, const sf::IntRect &textarea)
{
    addText(name,textstring, textstring.size(), fontname, fontsize, textcolor, tweentype, textarea, sf::Color::Transparent);
}

void Scene::addText(const std::string &name, const std::string& textstring, const std::string &fontname,
                    int fontsize, const sf::Color& textcolor, TextC::TEXTTWEEN tweentype, const sf::IntRect &textarea, const sf::Color& textareacolor)
{
    addText(name,textstring, textstring.size(), fontname, fontsize, textcolor, tweentype, textarea, textareacolor);
}

void Scene::addText(const std::string &name, int maxsize, const std::string &fontname,int fontsize)
{
    addText(name, "", maxsize, fontname, fontsize, sf::Color::White, TextC::NONE, sf::IntRect(), sf::Color::Transparent);
}


void Scene::addText(const std::string &name, int maxsize, const std::string &fontname,
                    int fontsize, const sf::Color& textcolor)
{
    addText(name, "", maxsize, fontname, fontsize, textcolor, TextC::NONE, sf::IntRect(), sf::Color::Transparent);
}

void Scene::addText(const std::string &name, int maxsize, const std::string &fontname,
                    int fontsize, const sf::Color& textcolor, TextC::TEXTTWEEN tweentype)
{
    addText(name, "", maxsize, fontname, fontsize, textcolor, tweentype, sf::IntRect(), sf::Color::Transparent);
}

void Scene::addText(const std::string &name, int maxsize, const std::string &fontname,
                    int fontsize, const sf::Color& textcolor, TextC::TEXTTWEEN tweentype, const sf::IntRect &textarea)
{
    addText(name, "", maxsize, fontname, fontsize, textcolor, tweentype, textarea, sf::Color::Transparent);
}

void Scene::addText(const std::string &name, int maxsize, const std::string &fontname,
                    int fontsize, const sf::Color& textcolor, TextC::TEXTTWEEN tweentype, const sf::IntRect &textarea, const sf::Color &textareacolor)
{
    addText(name, "", maxsize, fontname, fontsize, textcolor, tweentype, textarea, textareacolor);
}

void Scene::createDefaultGroups()
{
    group_container_.bindEntityGroupRegistry(&entity_group_registry_);
    group_container_.build();
    component_container_.bindGroupContainer(&group_container_);
    //NEW_COMPONENT_TYPE
    addDefaultGroup<QuadC>("quad");
    addDefaultGroup<LineC>("line");
    addDefaultGroup<MoveC>("motion");
    addDefaultGroup<ScriptC>("script");
    addDefaultGroup<TweenCoordC>("tweencoord");
    addDefaultGroup<TweenScaleC>("tweenscale");
    addDefaultGroup<TweenAngleC>("tweenangle");
    addDefaultGroup<AnimC>("animation");
    addDefaultGroup<BoxC>("body");
    addDefaultGroup<CollisionCheckC>("collision_check");
    addDefaultGroup<PointC>("point");
    addDefaultGroup<LayerC>("layer");
    addDefaultGroup<CameraC>("camera");
    addDefaultGroup<SoundC>("sound");
    addDefaultGroup<TextC>("text");
    addDefaultGroup<TimerC>("timer");


    queue_[COLLISION].add(group_container_.defaultGroup<CollisionCheckC>());
    queue_[MOTION].add(group_container_.defaultGroup<MoveC>());
    queue_[RENDER_CAMERA].add(group_container_.defaultGroup<CameraC>());
    queue_[SCRIPTING].add(group_container_.defaultGroup<ScriptC>());
    queue_[SOUNDMIXER].add(group_container_.defaultGroup<SoundC>());
    queue_[TYPEWRITER].add(group_container_.defaultGroup<TextC>());
}

void Scene::setConstructed(bool value)
{
    constructed_ = value;
    component_container_.setSceneConstructed(value);
}

void Scene::setAsyncBuildStarted(bool value)
{
    async_build_started_ = value;
}

bool Scene::isConstructed()
{
    return constructed_;
}

bool Scene::isAsyncBuildStarted()
{
    return async_build_started_;
}

void Scene::updateScript()
{
    if (update_function_.is_valid() && luabind::type(update_function_) != LUA_TNIL)
        update_function_();
}

void Scene::updateTextureSize(ID id)
{
    if (!constructed_)
        return;
    Entity* entity = &entity_container_[id];
    if (!entity->isLocked())
        return;
    eachC<AnimC>(id,[&](AnimC* animation)
    {
        if (animation->textureBounds().width)
            return;
        QuadC* quad = entity->getComponentByPointIndex<QuadC>(animation->pointIndex());
        if (!quad){
            animation->setEnabled(false);
            return;
        }
        LayerC* layer = entity->getComponent<LayerC>();
        if (!layer || !layer->texture()){
            animation->setEnabled(false);
            return;
        }
        animation->setTextureBounds(sf::IntRect(0,0,layer->texture()->getSize().x,layer->texture()->getSize().y));
        quad->setTextureRect(animation->textureRect());
    });

    eachC<LayerC>(id,[&](LayerC* layer)
    {
        sf::Vector2f texturesize;
        if (layer->texture())
            texturesize = sf::Vector2f((float)layer->texture()->getSize().x,
                                       (float)layer->texture()->getSize().y);

        eachC<QuadC>(id,[texturesize](QuadC* quad)
        {
            if (!quad->size().x){
                quad->setSize(texturesize);
                quad->setTextureRect(sf::IntRect(0,0,(int)texturesize.x,(int)texturesize.y));
            }
            else if (!quad->textureRect().width){
                quad->setTextureRect(sf::IntRect(0,0,(int)quad->size().x,(int)quad->size().y));
            }
        });
    });

}
void Scene::setName(const std::string &name)
{
    name_ = name;
}

void Scene::addStartScript(const std::string& name)
{
    start_script_name_ = name;
}

void Scene::setUpdateLuaFunction(const luabind::object& f)
{
    update_function_ = f;
}

void Scene::setPostUpdateLuaFunction(const luabind::object& f)
{
    postupdate_function_ = f;
}

std::vector<sf::Vertex>& Scene::getVertexArray(ID id)
{
    return vertex_array_container_.getVertexArray(id);
}

void Scene::setImageCache(ImgCache* imagecache)
{
    image_cache_ = imagecache;
}

void Scene::setSoundCache(SoundCache* soundcache)
{
    sound_cache_ = soundcache;
}

void Scene::setFontCache(FontCache* fontcache)
{
    font_cache_ = fontcache;
}

void Scene::setWindowSize(const sf::Vector2u &window_size)
{
    window_size_ = sf::Vector2f((float)window_size.x,(float)window_size.y);
}

void Scene::setUpdateRate(double dt)
{
    dt_ = dt;
}

lua_State * Scene::luaState() const
{
    return scene_lua_state_;
}

void Scene::bindLuaState(lua_State *state)
{
    scene_lua_state_ = state;
}

void Scene::postUpdate()
{
    if (postupdate_function_.is_valid() && luabind::type(postupdate_function_) != LUA_TNIL)
        postupdate_function_();
}

BroadPhase& Scene::broadPhase()
{
    return broadphase_;
}

void Scene::sortPointConnections()
{
    pointconnection_.sort();
}

void Scene::storeParentCoord(componentID parent, const sf::Vector2f &coord, const sf::Vector2f& prevcoord)
{
    pointconnection_.setParentCoord(parent, coord, prevcoord);
}

const ParentCoord& Scene::parentCoord(componentID parent)
{
    return pointconnection_.parentCoord(parent);
}

void Scene::storeParentOffset(componentID parent, const sf::Vector2f &coord)
{
    pointconnection_.setParentOffset(parent,coord);
}

const sf::Vector2f& Scene::parentOffset(componentID parent)
{
    return pointconnection_.parentOffset(parent);
}

Group* Scene::queue(QUEUETYPE id)
{
    return &queue_[id];
}
const std::string& Scene::name() const
{
    return name_;
}

const std::string& Scene::startScriptName() const
{
    return start_script_name_;
}

