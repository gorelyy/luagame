#include "luagame/components/AnimC.h"

AnimC::AnimC():
    looptime_(0),
    play_(false),
    next_play_start_frame_(-2),
    callbackframe_(-2),
    repeat_mode_(LOOP),
    max_loop_(1),
    loop_counter_(0)
{
}

void AnimC::addAnimation(const std::string& name, const sf::IntRect& texture_bounds, const luabind::object& sequence)
{
    texture_bounds_ = texture_bounds;
    std::vector<uint> frames;
    for(luabind::iterator frame(sequence),end; frame != end; ++frame)
        frames.push_back(luabind::object_cast<uint>(*frame));
    animlist_[name] = frames;
}

void AnimC::addAnimation(const std::string& name, const sf::IntRect& texture_bounds, uint i_start, uint i_end)
{
    texture_bounds_ = texture_bounds;
    std::vector<uint> frames;
    for(uint i_frame = i_start; i_frame != i_end + 1; ++i_frame)
        frames.push_back(i_frame);
    animlist_[name] = frames;
}

void AnimC::addAnimation(const std::string& name, const luabind::object& sequence)
{
    addAnimation(name, sf::IntRect(0,0,0,0), sequence);
}

void AnimC::addAnimation(const std::string& name, uint i_start, uint i_end)
{
     addAnimation(name, sf::IntRect(0,0,0,0), i_start, i_end);
}

void AnimC::play(const std::string& name, double fps, int repeat, REPEAT_MODE repeatmode)
{
    playbackspeed_ = 1.0 / fps;
    if (name != currentplay_ && animlist_.count(name) > 0){
        nextframenewanim_= false;
        currentseq_ = animlist_[name];
        currentframe_ = 0;
        start_ = true;
        currentplay_ = name;
        play_ = true;
        looptime_ = 0;
        repeat_mode_ = repeatmode;
        max_loop_ = (repeat >= 0) ? repeat : std::numeric_limits<int>::max();
        loop_counter_ = 0;
        if (callbacklist_.count(name)){
            callbackframe_ = callbacklist_[name].frame;
        }
    }
}

void AnimC::play(const std::string& name, double fps, int repeat)
{
    play(name,fps,repeat,LOOP);
}

void AnimC::play(const std::string& name, double fps)
{
    play(name,fps,1,LOOP);
}

void AnimC::addCallbackOnFinish(const std::string &name, const luabind::object& f)
{
    callbacklist_[name].onfinish = f;
}

void AnimC::addCallbackOnFrame(const std::string &name, int frame, const luabind::object& f)
{
    callbacklist_[name].onframe = f;
    callbacklist_[name].frame = frame + 1;
}

void AnimC::playNext(uint start_frame, const std::string &name, double fps, int repeat, REPEAT_MODE repeatmode)
{
    next_play_start_frame_ = start_frame;
    next_play_ = name;
    next_playbackspeed_ = fps;
    next_max_loop_ = repeat;
    next_repeat_mode_ = repeatmode;
}

void AnimC::playNext(uint start_frame, const std::string &name, double fps, int repeat)
{
    playNext(start_frame,name,fps,repeat,LOOP);
}

void AnimC::playNext(uint start_frame, const std::string &name, double fps)
{
    playNext(start_frame,name,fps,1,LOOP);
}

void AnimC::playNext(const std::string &name, double fps, int repeat, REPEAT_MODE repeatmode)
{
    if (currentplay_.empty())
        play(name,fps,repeat,repeatmode);
    else
        playNext(animlist_[currentplay_].size() - 1,name,fps,repeat,repeatmode);
}

void AnimC::playNext(const std::string &name, double fps, int repeat)
{
    if (currentplay_.empty())
        play(name, fps, repeat, LOOP);
    else
        playNext(animlist_[currentplay_].size() - 1,name,fps,repeat,LOOP);
}

void AnimC::playNext(const std::string &name, double fps)
{
    if (currentplay_.empty())
        play(name, fps, 1, LOOP);
    else
        playNext(animlist_[currentplay_].size() - 1,name,fps,1,LOOP);
}

bool AnimC::update(double dt)
{
    if(!play_)
        return false;
    looptime_+= dt;
    if (looptime_ >= playbackspeed_ || start_){
        int frame = currentFrame();
        if(nextframenewanim_){
            play(next_play_, next_playbackspeed_);
            return false;
        }

        if(frame == next_play_start_frame_){
            nextframenewanim_ = true;
            next_play_start_frame_ = -2;
            looptime_= 0;
        }

        looptime_ = 0;
        start_ = false;
        int i_frame, i_row, i_column;
        i_frame = currentseq_[frame];
        uint texture_width_in_frames = texture_bounds_.width / frame_size_.x;
        i_row = i_frame / texture_width_in_frames;
        i_column = i_frame % texture_width_in_frames;

        frame_texrect_.left = texture_bounds_.left + frame_size_.x * i_column;
        frame_texrect_.top = texture_bounds_.top + frame_size_.y * i_row;
        frame_texrect_.width = frame_size_.x;
        frame_texrect_.height = frame_size_.y;

        if (currentseq_.size() > 1 && !nextframenewanim_){
            if (currentframe_ >= currentseq_.size() - 1){
                if(repeat_mode_ == LOOP){
                    currentframe_= -1;
                    loop_counter_++;
                }
                else{
                    if (currentframe_ == currentseq_.size() - 1)
                        loop_counter_++;
                    if (currentframe_ == currentseq_.size() * 2 - 2){
                        currentframe_ = 0;
                        loop_counter_++;
                    }
                }
                if (loop_counter_ >= max_loop_){

                    luabind::object& callback = callbacklist_[currentplay_].onfinish;
                    if (callback.is_valid()){
                        luabind::object f = callback;
                        try{
                            f();
                        }
                        catch(const luabind::error &e){
                            std::cerr<<"luagame_error in animation callback on finish: "<<e.what();
                            luabind::object error_msg(luabind::from_stack(e.state(), -1));
                            std::cerr<<error_msg<<std::endl;
                        }
                    }
                    loop_counter_= 0;
                    play_ = false;
                }
            }

            currentframe_++;

            if (frame + 1 == callbackframe_){
                luabind::object& callback = callbacklist_[currentplay_].onframe;
                if (callback.is_valid()){
                    luabind::object f = callback;
                    try{
                        f();
                    }
                    catch(const luabind::error &e){
                        std::cerr<<"luagame_error in animation callback on frame: "<<e.what();
                        luabind::object error_msg(luabind::from_stack(e.state(), -1));
                        std::cerr<<error_msg<<std::endl;
                    }
                }
            }

            if (!play_)
                currentplay_.clear();

        }

        return true;
    }
    return false;
}

uint AnimC::currentFrame() const
{
    //currentframe_: 0,1,2,3,4,5,6 -> result: 0,1,2,3,2,1,0
    return std::abs( (int)((int)repeat_mode_ * (loop_counter_ % 2) * (currentseq_.size() - 2) - currentframe_ % currentseq_.size() ));
}

void AnimC::setFrameSize(const sf::Vector2f& size)
{
    frame_size_ = size;
}

const sf::Vector2f& AnimC::frameSize() const
{
    return frame_size_;
}

const sf::IntRect& AnimC::textureRect() const
{
    return frame_texrect_;
}

void AnimC::setTextureBounds(const sf::IntRect& texture_bounds)
{
    texture_bounds_ = texture_bounds;
    frame_texrect_.left = texture_bounds_.left;
    frame_texrect_.top = texture_bounds_.top;
    frame_texrect_.width = frame_size_.x ? frame_size_.x : texture_bounds_.width;
    frame_texrect_.height = frame_size_.y ? frame_size_.y : texture_bounds_.height;
}

const sf::IntRect &AnimC::textureBounds() const
{
    return texture_bounds_;
}

