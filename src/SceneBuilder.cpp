#include "luagame/SceneBuilder.h"

SceneBuilder::SceneBuilder()
{
}
void SceneBuilder::build(Scene* scene)
{
    if(scene->isConstructed())
        return;

    //Create default camera
    CameraC::MODE mode;
    sf::Vector2f size;
    sf::Vector2f window_offset;
    sf::IntRect scroll_area;
    sf::Color color;
    try{
        luabind::object require = luabind::globals(scene->luaState())["require"];
        luabind::object settings = require("settings");
        luabind::object camera_settings = settings["default_camera"];
        mode = (CameraC::MODE)luabind::object_cast<int>(camera_settings["mode"]);
        size = luabind::object_cast<sf::Vector2f>(camera_settings["size"]);
        window_offset = luabind::object_cast<sf::Vector2f>(camera_settings["window_offset"]);
        scroll_area = luabind::object_cast<sf::IntRect>(camera_settings["scroll_area"]);
        color = luabind::object_cast<sf::Color>(camera_settings["color"]);
    }
    catch(const luabind::error &e){
        std::cerr<<"luagame_error while reading default camera settings:"<<e.what();
        luabind::object error_msg(luabind::from_stack(e.state(), -1));
        std::cerr<<error_msg<<std::endl;
    }
    scene->addCamera("default",mode, size, window_offset,scroll_area,color);
    Entity* screen = scene->getEntity(2); //first is audio focus
    CameraC* camera = screen->getComponent<CameraC>();
    camera->queue()->add(scene->defaultGroup<LayerC>());

    try{
        luabind::object require = luabind::globals(scene->luaState())["require"];
        luabind::object startscript = require(scene->startScriptName());
        luabind::object create = startscript["create"];
        scene->setUpdateLuaFunction(startscript["update"]);
        scene->setPostUpdateLuaFunction(startscript["postupdate"]);
        luabind::globals(scene->luaState())["_scene"] = scene;
        create();
        postBuild(scene);
        scene->setConstructed(true);
    }
    catch(const luabind::error &e){
        std::cerr<<"luagame_error while building scene:"<<e.what();
        luabind::object error_msg(luabind::from_stack(e.state(), -1));
        std::cerr<<error_msg<<std::endl;
    }
}

void SceneBuilder::postBuild(Scene* scene)
{
    bool requires_iteration = true;
    std::set<ID> idlist;
    std::vector<ScriptC*> scripts;
    while (requires_iteration){
        scene->eachExistingC<ScriptC>(scene->group(Group::BUILD),[&](ScriptC* script){scripts.push_back(script);});
        for (ScriptC* script : scripts)
            script->create();
        requires_iteration = false;
        scene->group(Group::BUILD)->each([&](ID id)
        {
            if (!idlist.count(id)){
                requires_iteration = true;
                idlist.insert(id);
            }
        });
    }

    //SET RESOURCES
    scene->eachExistingC<LayerC>(scene->group(Group::BUILD),[&](LayerC* layer)
    {
        sf::Vector2f texturesize;
        if (layer->texture())
            texturesize = sf::Vector2f((float)layer->texture()->getSize().x,
                                       (float)layer->texture()->getSize().y);
        scene->eachExistingC<QuadC>(layer->poolID(),[texturesize](QuadC* quad)
        {
            if (!quad->size().x){
                quad->setSize(texturesize);
                quad->setTextureRect(sf::IntRect(0,0,(int)texturesize.x,(int)texturesize.y));
            }
            else if (!quad->textureRect().width){
                quad->setTextureRect(sf::IntRect(0,0,(int)quad->size().x,(int)quad->size().y));
            }
        });
        if (!layer->entityID())
            return;
        Entity* entity = scene->getEntity(layer->entityID());
        PointC* point = entity->getComponent<PointC>(layer->pointIndex());
        layer->setCoord(point->coord());
        createTilemap(scene,layer,entity);
    });


    //Set box coordinates and create collision broad phase structures
    scene->eachExistingC<CollisionCheckC>(scene->group(Group::BUILD),[&](CollisionCheckC* check)
    {
        createCollisionBroadPhase(scene,check);
    });

    //set AnimC texture rectangle
    scene->eachExistingC<AnimC>(scene->group(Group::BUILD),[&](AnimC* animation)
    {
        if (animation->textureBounds().width)
            return;
        Entity* entity = scene->getEntity(animation->entityID());
        QuadC* quad = entity->getComponentByPointIndex<QuadC>(animation->pointIndex());
        //ERROR-MESSAGE:QUAD IS NOT SET
        if (!quad){
            animation->setEnabled(false);
            return;
        }
        if (!animation->frameSize().x)
            animation->setFrameSize(quad->size());
        LayerC* layer = scene->getComponent<LayerC>(std::make_pair(quad->poolID(),0));
        //ERROR-MESSAGE:LAYER IS NOT SET
        if (!layer || !layer->texture() || !layer->texture()->getSize().x){
            animation->setEnabled(false);
            return;
        }
        animation->setTextureBounds(sf::IntRect(0,0,layer->texture()->getSize().x,layer->texture()->getSize().y));
        quad->setTextureRect(animation->textureRect());
    });

    //Create vertex arrays, and drawing primitives coordinates
    scene->eachExistingC<LayerC>(scene->group(Group::BUILD),[&](LayerC* layer)
    {
        createVertexArray(scene,layer);
    });

    //Update pool vertex array on runtime
    if (scene->isConstructed()){
        std::set<ID> batchpools;
        scene->group(Group::BUILD)->each([&](ID id)
        {
            Entity* entity = scene->getEntity(id);
            entity->each<QuadC>([&](QuadC* quad)
            {
                if (quad->poolID() != entity->id())
                    batchpools.insert(quad->poolID());
            });
            entity->each<LineC>([&](LineC* line)
            {
                if (line->poolID() != entity->id())
                    batchpools.insert(line->poolID());
            });
        });
        for (ID id:batchpools){
            scene->eachExistingC<LayerC>(id,[&](LayerC* layer){
                createVertexArray(scene,layer);
            });
        }

    }

    scene->group(Group::BUILD)->each([scene](ID id)
    {
        Entity* e = scene->getEntity(id);
        if (e){
            e->lock();
            e->finalizeAllComponentPools();
        }
    });
    scene->group(Group::BUILD)->clear();
}

void SceneBuilder::asyncBuild(Scene* scene)
{
    if(!scene->isConstructed() && !scene->isAsyncBuildStarted()){
        scene->setAsyncBuildStarted(true);
        build_thread_ = std::thread(&SceneBuilder::build, this,scene);
        build_thread_.detach();
    }
}

void SceneBuilder::createVertexArray(Scene* scene, LayerC* layer)
{
    Entity* e = scene->getEntity(layer->entityID());
    if (!layer->isDynamic())
        layer->setCoord(e->getComponent<PointC>(0)->coord());

    std::vector<sf::Vertex>& vertex_array = scene->getVertexArray(layer->poolID());
    size_t quadpoolsize = scene->componentPoolSize<QuadC>(layer->poolID());
    if (quadpoolsize)
        vertex_array.resize(quadpoolsize * 4);
    size_t linepoolsize = scene->componentPoolSize<LineC>(layer->poolID());
    if (linepoolsize)
        vertex_array.resize(linepoolsize * 2);
    scene->eachExistingC<QuadC>(layer->poolID(),[&](QuadC* quad)
    {
        Entity* entity = scene->getEntity(quad->entityID());
        if (quad->pointIndex()){
            PointC* point = entity->getComponent<PointC>(quad->pointIndex());
            quad->setCoord(point->coord());
            quad->setAngle(point->angle());
        }
        if (layer->isDynamic()){
            for(int i = 0; i != 4; ++i){
                vertex_array[quad->poolIndex() * 4 + i].position = quad->getVertexCoord(i,sf::Vector2f(),sf::Vector2f(),sf::Vector2f(),layer->scale() * layer->offsetScale(),layer->angle() + layer->offsetAngle(),layer->pivot());
                vertex_array[quad->poolIndex() * 4 + i].texCoords = quad->getVertexTextureCoord(i);
                vertex_array[quad->poolIndex() * 4 + i].color = quad->isVisible()?quad->color() : sf::Color::Transparent;
            }
        }
        else{
            for(int i = 0; i != 4; ++i){
                vertex_array[quad->poolIndex() * 4 + i].position = quad->getVertexCoord(i,sf::Vector2f(),sf::Vector2f(),sf::Vector2f(),sf::Vector2f(1.f,1.f),0.f,sf::Vector2f());
                vertex_array[quad->poolIndex() * 4 + i].texCoords = quad->getVertexTextureCoord(i);
                vertex_array[quad->poolIndex() * 4 + i].color = quad->isVisible()?quad->color() : sf::Color::Transparent;
            }
        }


     });

    scene->eachExistingC<LineC>(layer->poolID(),[&](LineC* line)
    {
        Entity* entity = scene->getEntity(line->entityID());
        if (line->pointIndex()){
            PointC* point = entity->getComponent<PointC>(line->pointIndex());
            line->setCoord(point->coord());
            line->setAngle(point->angle());
        }
        if (layer->isDynamic()){
            for(int i = 0; i != 2; ++i){
                vertex_array[line->poolIndex() * 2 + i].position = line->getVertexCoord(i,sf::Vector2f(),sf::Vector2f(),sf::Vector2f(),layer->scale() * layer->offsetScale(),layer->angle() + layer->offsetAngle(),layer->pivot());
                vertex_array[line->poolIndex() * 2 + i].texCoords = line->getVertexTextureCoord(i);
                vertex_array[line->poolIndex() * 2 + i].color = line->isVisible()?line->color() : sf::Color::Transparent;
            }
        }
        else{
            for(int i = 0; i != 2; ++i){
                vertex_array[line->poolIndex() * 2 + i].position = line->getVertexCoord(i,sf::Vector2f(),sf::Vector2f(),sf::Vector2f(),sf::Vector2f(1.f,1.f),0.f,sf::Vector2f());
                vertex_array[line->poolIndex() * 2 + i].texCoords = line->getVertexTextureCoord(i);
                vertex_array[line->poolIndex() * 2 + i].color = line->isVisible()?line->color() : sf::Color::Transparent;
            }
        }
    });

}

void SceneBuilder::createCollisionBroadPhase(Scene* scene, CollisionCheckC *check)
{
    if (check->drawDebugMode() == CollisionCheckC::AABB || check->drawDebugMode() == CollisionCheckC::ALL){
        std::set<Entity*> modified_entities;
        auto createdebugbox = [&](BoxC* box)
        {
            Entity* e = scene->getEntity(box->entityID());
            e->unlock();
            modified_entities.insert(e);
            e->addComponent<LayerC>();
            std::string name = "debug_ABBB_entity" + std::to_string(e->id());
            scene->addGroup<LayerC>("layer",name);
            Group* group = scene->group<LayerC>(name);
            if (!group->size()){
                Entity* debugentity = scene->addEntity(name);
                LayerC* layer = debugentity->addComponent<LayerC>();
                layer->setType(sf::Lines);
                scene->defaultGroup<LayerC>()->setPriority(debugentity->id(),255);
                group->add(debugentity->id());
                e->addChild(debugentity);

            }
            Pool* pool = luabind::object_cast<Pool*>(luabind::globals(scene->luaState())["_pool"][name]);

            if (box->pointIndex() > 0){
                box->setCoord(e->getComponent<PointC>(0)->coord() + e->getComponent<PointC>(box->pointIndex())->coord());
                box->setPrevCoord(e->getComponent<PointC>(0)->prevCoord() + e->getComponent<PointC>(box->pointIndex())->prevCoord());
            }
            else{
                box->setCoord(e->getComponent<PointC>(0)->coord());
                box->setPrevCoord(e->getComponent<PointC>(0)->prevCoord());
            }

            LineC* line = e->addComponentWithPointIndex<LineC>(pool, box->pointIndex(), false);
            line->setLength(box->width());
            line->setColor(check->aabbColor());

            line = e->addComponentWithPointIndex<LineC>(pool, box->pointIndex(), false);
            line->setLength(box->height());
            line->setOffsetAngle(90);
            line->setOffsetCoord(sf::Vector2f(box->width(),0));
            line->setColor(check->aabbColor());

            line = e->addComponentWithPointIndex<LineC>(pool, box->pointIndex(), false);
            line->setLength(box->width());
            line->setOffsetAngle(180);
            line->setOffsetCoord(sf::Vector2f(box->width(),box->height()));
            line->setColor(check->aabbColor());

            line = e->addComponentWithPointIndex<LineC>(pool, box->pointIndex(), false);
            line->setLength(box->height());
            line->setOffsetAngle(270);
            line->setOffsetCoord(sf::Vector2f(0,box->height()));
            line->setColor(check->aabbColor());
        };

        scene->eachExistingC<BoxC>(check->groupA(),createdebugbox);
        scene->eachExistingC<BoxC>(check->groupB(),createdebugbox);
    }
    else{
        scene->eachExistingC<BoxC>(check->groupA(),[&](BoxC* box)
        {
            Entity* e = scene->getEntity(box->entityID());
            if (box->pointIndex() > 0){
                box->setCoord(e->getComponent<PointC>(0)->coord() + e->getComponent<PointC>(box->pointIndex())->coord());
                box->setPrevCoord(e->getComponent<PointC>(0)->prevCoord() + e->getComponent<PointC>(box->pointIndex())->prevCoord());
            }
            else{
                box->setCoord(e->getComponent<PointC>(0)->coord());
                box->setPrevCoord(e->getComponent<PointC>(0)->prevCoord());
            }
        });

        scene->eachExistingC<BoxC>(check->groupB(),[&](BoxC* box)
        {
            Entity* e = scene->getEntity(box->entityID());
            if (box->pointIndex() > 0){
                box->setCoord(e->getComponent<PointC>(0)->coord() + e->getComponent<PointC>(box->pointIndex())->coord());
                box->setPrevCoord(e->getComponent<PointC>(0)->prevCoord() + e->getComponent<PointC>(box->pointIndex())->prevCoord());
            }
            else{
                box->setCoord(e->getComponent<PointC>(0)->coord());
                box->setPrevCoord(e->getComponent<PointC>(0)->prevCoord());
            }
        });
    }
    if (check->drawDebugMode() == CollisionCheckC::BROAD || check->drawDebugMode() == CollisionCheckC::ALL){
        Grid* grid = nullptr;
        QTree* tree = nullptr;
        switch(check->broadPhaseType()){
            case CollisionCheckC::STATICGRID:
            {
                grid = scene->broadPhase().add<Grid>();
                grid->setDynamic(false);
            }
            case CollisionCheckC::GRID :
            {
                if (!grid)
                    grid = scene->broadPhase().add<Grid>();
                grid->load(check->groupA(), check->groupB(),
                           check->collisionArea(), check->cellSize(), check);
                if (!grid->isDynamic())
                    scene->eachExistingC<BoxC>(grid->groupB(), [grid](BoxC* box)
                    {
                        grid->updateBoxBPosition(box);
                    });

                const std::vector<sf::IntRect>& cells = grid->cells();
                std::string name = "debug_broadphase_" + std::to_string(check->id().first) + std::to_string(check->id().second);
                Entity* entity = scene->addEntity(name,sf::Vector2f(check->collisionArea().left, check->collisionArea().top));
                LayerC* layer = entity->addComponent<LayerC>();
                layer->setType(sf::Lines);
                layer->setDynamic(false);
                scene->defaultGroup<LayerC>()->setPriority(entity->id(),255);
                for (auto& cell:cells){
                    PointC* point = entity->addComponent<PointC>();
                    point->setCoord(cell.left,cell.top);
                    LineC* line = entity->addComponent<LineC>();
                    line->setLength(cell.width);
                    line->setColor(check->broadphaseColor());
                    //
                    line = entity->addComponent<LineC>();
                    line->setLength(cell.height);
                    line->setOffsetAngle(90);
                    line->setOffsetCoord(sf::Vector2f(cell.width,0));
                    line->setColor(check->broadphaseColor());
                    //
                    line = entity->addComponent<LineC>();
                    line->setLength(cell.width);
                    line->setOffsetAngle(180);
                    line->setOffsetCoord(sf::Vector2f(cell.width,cell.height));
                    line->setColor(check->broadphaseColor());
                    //
                    line = entity->addComponent<LineC>();
                    line->setLength(cell.height);
                    line->setOffsetAngle(270);
                    line->setOffsetCoord(sf::Vector2f(0,cell.height));
                    line->setColor(check->broadphaseColor());
                }
            }
                break;
            case CollisionCheckC::STATICQUADTREE :
            {
                tree = scene->broadPhase().add<QTree>();
                tree->setDynamic(false);
                tree->load(check->groupA(), check->groupB(),check->collisionArea(),check->treeDepth(), check);
                scene->eachExistingC<BoxC>(tree->groupB(),[tree](BoxC* boxB)
                {
                    QTreeNode* node = tree->findBoxNode(boxB);
                    boxB->setQTreeNode(node);
                    node->addBoxB(boxB);
                    if (node->level() < tree->maxLevel())
                        tree->addBoxBNotLeafeNode(node);
                });
            }
            case CollisionCheckC::QUADTREE :
            {
                if (!tree){
                    tree = scene->broadPhase().add<QTree>();
                    tree->load(check->groupA(), check->groupB(),check->collisionArea(),check->treeDepth(), check);
                    scene->eachExistingC<BoxC>(tree->groupB(),[tree](BoxC* boxB)
                    {
                        QTreeNode* node = tree->findBoxNode(boxB);
                        boxB->setQTreeNode(node);
                        node->addBoxB(boxB);
                    });
                }
                scene->eachExistingC<BoxC>(tree->groupA(),[tree](BoxC* boxA)
                {
                    QTreeNode* node = tree->findBoxNode(boxA);
                    boxA->setQTreeNode(node);
                    node->addBoxA(boxA);
                });

                const std::vector<sf::IntRect>& cells = tree->cells();
                std::string name = "debug_broadphase" + std::to_string(check->id().first) + std::to_string(check->id().second);
                Entity* entity = scene->addEntity(name,sf::Vector2f(check->collisionArea().left, check->collisionArea().top));
                LayerC* layer = entity->addComponent<LayerC>();
                layer->setType(sf::Lines);
                layer->setDynamic(false);
                for (auto& cell:cells){
                    PointC* point = entity->addComponent<PointC>();
                    point->setCoord(cell.left,cell.top);
                    LineC* line = entity->addComponent<LineC>();
                    line->setLength(cell.width);
                    line->setColor(check->broadphaseColor());
                    //
                    point = entity->addComponent<PointC>();
                    point->setCoord(cell.left + cell.width,cell.top);
                    line = entity->addComponent<LineC>();
                    line->setLength(cell.height);
                    line->setOffsetAngle(90);
                    line->setColor(check->broadphaseColor());
                    //
                    point = entity->addComponent<PointC>();
                    point->setCoord(cell.left + cell.width,cell.top + cell.height);
                    line = entity->addComponent<LineC>();
                    line->setLength(cell.width);
                    line->setOffsetAngle(180);
                    line->setColor(check->broadphaseColor());
                    //
                    point = entity->addComponent<PointC>();
                    point->setCoord(cell.left,cell.top + cell.height);
                    line = entity->addComponent<LineC>();
                    line->setLength(cell.height);
                    line->setOffsetAngle(270);
                    line->setColor(check->broadphaseColor());
                }
            }
                break;
        }
    }
    else{
        Grid* grid = nullptr;
        QTree* tree = nullptr;
        switch(check->broadPhaseType()){
            case CollisionCheckC::STATICGRID :
            {
                grid = scene->broadPhase().add<Grid>();
                grid->setDynamic(false);
            }
            case CollisionCheckC::GRID :
            {
                if(!grid)
                    grid = scene->broadPhase().add<Grid>();
                grid->load(check->groupA(), check->groupB(),
                           check->collisionArea(), check->cellSize(), check);
                if (!grid->isDynamic())
                    scene->eachExistingC<BoxC>(grid->groupB(), [grid](BoxC* box)
                    {
                        grid->updateBoxBPosition(box);
                    });
            }
            break;
            case CollisionCheckC::STATICQUADTREE :
            {
                tree = scene->broadPhase().add<QTree>();
                tree->setDynamic(false);
                tree->load(check->groupA(), check->groupB(), check->collisionArea(),check->treeDepth(), check);
                scene->eachExistingC<BoxC>(tree->groupB(),[tree](BoxC* boxB)
                {
                    QTreeNode* node = tree->findBoxNode(boxB);
                    boxB->setQTreeNode(node);
                    node->addBoxB(boxB);
                    tree->addBoxBNotLeafeNode(node);
                });
            }
            case CollisionCheckC::QUADTREE :
            {
                if (!tree){
                    tree = scene->broadPhase().add<QTree>();
                    tree->load(check->groupA(), check->groupB(), check->collisionArea(),check->treeDepth(), check);
                    scene->eachExistingC<BoxC>(tree->groupB(),[tree](BoxC* boxB)
                    {
                        QTreeNode* node = tree->findBoxNode(boxB);
                        boxB->setQTreeNode(node);
                        node->addBoxB(boxB);
                    });
                }
                scene->eachExistingC<BoxC>(tree->groupA(),[tree](BoxC* boxA)
                {
                    QTreeNode* node = tree->findBoxNode(boxA);
                    boxA->setQTreeNode(node);
                    node->addBoxA(boxA);
                });
            }
            break;
        }

    }
}

void SceneBuilder::createTilemap(Scene* scene, LayerC* layer, Entity* entity)
{
    const std::vector<int>& tilemapdata = layer->tilemapData();
    const sf::Texture* texture = layer->texture();
    if (tilemapdata.empty() || !texture)
        return;
    uint tilemap_width = layer->tilemapWidth();
    uint tilemap_height = layer->tilemapHeight();
    uint tilesize = layer->tilemapTileSize();
    sf::IntRect tex_bounds = (layer->tilemapTextureBounds().width)?layer->tilemapTextureBounds() : sf::IntRect(0,0,texture->getSize().x, texture->getSize().y);
    int x, y;
    int texture_width = tex_bounds.width / tilesize;
    int i_row, i_column;
    for (y = 0; y < tilemap_height; ++y){
        for (x = 0; x < tilemap_width; ++x){
            int index = (x + y * tilemap_width);
            int value = tilemapdata[index];
            if(value){
                i_row = (value - 1) / texture_width;
                i_column = (value - 1) % texture_width;
                PointC* point = entity->addComponent<PointC>();
                point->set(x*tilesize,y*tilesize);
                QuadC* quad = entity->addComponent<QuadC>();
                quad->setSize(sf::Vector2f(tilesize,tilesize));
                quad->setTextureRect(sf::IntRect(tex_bounds.left + i_column*tilesize,tex_bounds.top + i_row*tilesize, tilesize,tilesize));
            }
        }
    }
    const std::string& collisiongroup = layer->tilemapCollisionGroup();
    if (!collisiongroup.empty())
    {
        uint width = layer->tilemapWidth();
        uint height = layer->tilemapHeight();
        std::vector<std::vector<uint> > a;
        a.resize(height + 2);
        for(auto &each:a)
            each.resize(width + 2);
        uint x, y;
        uint boxsize = layer->tilemapTileSize();
        for(y = 0; y < height + 2; ++y){
            for (x = 0; x < width + 2; ++x){
                if (y == 0 || y == height + 1 || x == 0 || x == width + 1)
                    a[y][x] = 0;
                else
                    a[y][x] = tilemapdata[(y - 1) * width + (x-1)];
            }
        }
        bool putbox;
        int i;
        for (y = 1; y < height + 1; ++y){
            i = 0;
            for (x = 1; x < width + 2; ++x){
                if (a[y][x] > 0){
                    if (!a[y-1][x]   || !a[y+1][x]   || !a[y][x+1]   || !a[y][x-1] ||
                        !a[y-1][x-1] || !a[y-1][x+1] || !a[y+1][x-1] || !a[y+1][x+1])
                        putbox = true;
                }
                if (putbox){
                    PointC* offsetpoint = entity->addComponent<PointC>();
                    offsetpoint->set(boxsize * (x-1),boxsize * (y-1));
                    BoxC* newbox = entity->addComponent<BoxC>();
                    scene->group<BoxC>(collisiongroup)->add(entity->id());
                    newbox->setSize(sf::Vector2f(boxsize,boxsize));
                    if (!a[y-1][x])
                        newbox->setSolidSide(Component::UP);
                    if(!a[y+1][x])
                        newbox->setSolidSide(Component::DOWN);
                    if(!a[y][x-1])
                        newbox->setSolidSide(Component::LEFT);
                    if(!a[y][x+1])
                        newbox->setSolidSide(Component::RIGHT);
                    i=0;
                    putbox = false;
                }

            }
        }
    }
}
