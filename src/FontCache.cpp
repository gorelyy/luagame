#include "luagame/FontCache.h"

FontCache::FontCache():
    enabled_(true)
{
    launchLoadingThread();
}

const sf::Font* FontCache::getFont(const std::string &filename)
{
    if (!enabled_)
        return nullptr;
    auto it = font_container_.find(filename);
    if (it != font_container_.end())
        return &(it->second);
    font_container_[filename].loadFromFile(filename);
    return &font_container_[filename];
}

void FontCache::asyncLoadFont(const std::string& filename)
{
    if (!enabled_){
        if (std::find(loading_queue_.begin(),
                      loading_queue_.end(),filename) == loading_queue_.end())
            loading_queue_.push_back(filename);
        return;
    }
    auto it = font_container_.find(filename);
    if (it == font_container_.end()){
        enabled_ = false;
        if (std::find(loading_queue_.begin(),
                      loading_queue_.end(),filename) == loading_queue_.end())
            loading_queue_.push_back(filename);
    }
}

void FontCache::loadingThread(){
    while(true){
        while(loading_queue_.empty()){
            enabled_ = true;
            sf::sleep(sf::milliseconds(16));
        }
        enabled_ = false;
        std::string& filename = loading_queue_.back();
        font_container_[filename].loadFromFile(filename);
        loading_queue_.pop_back();
    }

}

void FontCache::launchLoadingThread()
{
    loading_thread_ = std::thread(&FontCache::loadingThread, this);
    loading_thread_.detach();
}
