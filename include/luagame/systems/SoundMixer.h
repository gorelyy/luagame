#pragma once
class Scene;
class Audio;

class SoundMixer {
public:
    SoundMixer();
    void update(Scene* scene, float dt);
    void bindAudio(Audio* audio);
private:
    Audio* audio_;
};
