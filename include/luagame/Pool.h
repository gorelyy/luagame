#pragma once
class ComponentContainer;
//NEW_COMPONENT_TYPE
class QuadC;
class LineC;
class MoveC;
class ScriptC;
class AnimC;
class BoxC;
class CollisionCheckC;
class PointC;
class LayerC;
class CameraC;
class TweenCoordC;
class TweenScaleC;
class TweenAngleC;
class SoundC;
class TextC;
class TimerC;
#include <luabind/luabind.hpp>
#include "globals.h"

template <typename cClass>
class BasePool{
public:
    BasePool() : capacity_(0)
    {
    }

    void setCapacity(size_t capacity)
    {
        capacity_ = capacity;
    }

    size_t getCapacity()
    {
        return capacity_;
    }

private:
    size_t capacity_;

};

//NEW_COMPONENT_TYPE
class Pool:     private BasePool<QuadC>,
                private BasePool<LineC>,
                private BasePool<MoveC>,
                private BasePool<ScriptC>,
                private BasePool<AnimC>,
                private BasePool<BoxC>,
                private BasePool<CollisionCheckC>,
                private BasePool<PointC>,
                private BasePool<LayerC>,
                private BasePool<CameraC>,
                private BasePool<TweenCoordC>,
                private BasePool<TweenScaleC>,
                private BasePool<TweenAngleC>,
                private BasePool<SoundC>,
                private BasePool<TextC>,
                private BasePool<TimerC>{
public:
    Pool();
    ID id() const;
    LayerC* layer();
    void    bindComponentContainer(ComponentContainer* cc);
    void    bindLayer(LayerC* layer);
    void    setID(ID id);
    template <typename cClass>
    void each(const luabind::object& f)
    {
        component_container_->eachC<cClass>(id_,f);
    }
    template <typename cClass>
    void eachDisabled(const luabind::object& f)
    {
        component_container_->eachDisabledC<cClass>(id_,f);
    }

    template <typename cClass>
    size_t capacity()
    {
        return BasePool<cClass>::getCapacity();
    }

    template <typename cClass>
    void setCapacity(size_t capacity)
    {
        if (BasePool<cClass>::getCapacity())
            return;
        BasePool<cClass>::setCapacity(capacity);
        component_container_->reserveComponentPool<cClass>(id_, capacity);

    }

    template <typename cClass>
    bool hasFreeSpace()
    {
        size_t capacity = BasePool<cClass>::getCapacity();
        if (!capacity)
            return false;
        if (component_container_->poolSize<cClass>(id_) < capacity)
            return true;
        else
            return false;
    }
private:
    ID id_;
    ComponentContainer* component_container_;
    LayerC* layer_;
};
