#include "luagame/systems/PointGraph.h"

PointGraph::PointGraph()
{
}

void PointGraph::update(Scene* scene, float dt)
{
    scene->eachParentingConnection([&](componentID parent, componentID child)
    {
        Entity* parententity = scene->getEntity(parent.first);
        PointC* parentpoint = parententity->getComponent<PointC>(parent.second);
        Entity* childentity = scene->getEntity(child.first);
        PointC* childpoint =  childentity->getComponent<PointC>(child.second);
        childpoint->setCoord(childpoint->coord() + parentpoint->coord() - scene->parentCoord(parent).current);
        childpoint->setPrev(childpoint->prevCoord() + parentpoint->prevCoord() - scene->parentCoord(parent).prev);
        childpoint->setScale(parentpoint->scale());
        childpoint->setPrevScale(parentpoint->prevScale());
        childpoint->setAngle(parentpoint->angle());
        childpoint->setPrevAngle(parentpoint->prevAngle());
    });

    scene->eachParentingConnection([&](componentID parent, componentID child)
    {
        Entity* parententity = scene->getEntity(parent.first);
        PointC* parentpoint = parententity->getComponent<PointC>(parent.second);
        scene->storeParentCoord(parent,parentpoint->coord(),parentpoint->prevCoord());
    });

    scene->sortPointConnections();
    scene->eachFollowingConnection([&](componentID leader, componentID follower, FollowData& followdata)
    {
        Entity* leaderentity = scene->getEntity(leader.first);
        PointC* leaderpoint = leaderentity->getComponent<PointC>(leader.second);
        Entity* followentity = scene->getEntity(follower.first);
        PointC* followpoint = followentity->getComponent<PointC>(follower.second);
        followpoint->savePrevCoord();
        if (followdata.mode == FollowData::DISTANCE)
            followpoint->setCoord(leaderpoint->coord()  - followdata.distance);
        else if (followdata.mode == FollowData::DEADZONE){
            const sf::Vector2f& distance = followdata.distance;
            sf::Vector2f rf = leaderpoint->coord() - followpoint->coord();
            float rdistance = sqrt(distance.x*distance.x + distance.y*distance.y);
            float rfollow = sqrt(rf.x*rf.x + rf.y*rf.y);
            float easing = (followdata.easing) ? followdata.easing /dt : 1;
            if (rfollow > rdistance)
                followpoint->setCoord(utils::round(followpoint->coord() + rf * (1 - rdistance / rfollow) / easing));
        }

        if (followdata.callback.is_valid())
            try{
                followdata.callback(followpoint,leaderpoint,&followdata, followentity,leaderentity);
            }
            catch(const luabind::error &e){
                std::cerr<<"Lua callback function error: "<<e.what();
                luabind::object error_msg(luabind::from_stack(e.state(), -1));
                std::cerr<<error_msg<<std::endl;
            }

    });

}
