 
#include "luagame/systems/Motion.h"

Motion::Motion() 
{    
}

void Motion::update(Scene* scene, float dt)
{
    dt_ = dt;
    scene_ = scene;

    scene->eachC<MoveC>(scene->queue(Scene::MOTION),[this](MoveC* move)
    {
        loop(move);
    });
}

void Motion::loop(MoveC* motion)
{   
    Entity* entity = scene_->getEntity(motion->entityID());
    PointC* point = entity->getComponent<PointC>(motion->pointIndex());
    const sf::Vector2f& velocity     = motion->velocity();
    const sf::Vector2f& acceleration = motion->acceleration();
    const sf::Vector2f& scalevelocity = motion->scaleVelocity();
    const sf::Vector2f& scaleacceleration = motion->scaleAcceleration();
    
    motion->setVelocity(velocity + acceleration * dt_);
    motion->setScaleVelocity(scalevelocity + scaleacceleration * dt_);
    motion->setAngularVelocity(motion->angularVelocity() + motion->angularAcceleration() * dt_);
    motion->setCircularVelocity(motion->circularVelocity() + motion->circularAcceleration() * dt_);
    motion->setDirectionalVelocity(motion->directionalVelocity() + motion->directionalAcceleration() * dt_);
    motion->setRotationVelocity(motion->rotationVelocity() + motion->rotationAcceleration() * dt_);
    motion->setAngle(motion->angle() + motion->rotationVelocity() * dt_);

    point->savePrevCoord();
    float directionalvelocity = motion->directionalVelocity();
    if (directionalvelocity){
       float motionangle = motion->angle();
        point->setCoord(point->coord() + sf::Vector2f(directionalvelocity * cosf(motionangle * utils::constants::RAD),
                                                      directionalvelocity * sinf(motionangle * utils::constants::RAD)) * dt_);
    }
    point->setCoord(point->coord() + velocity * dt_);

    motion->setMotionCenter(motion->motionCenter() + velocity * dt_);
    if (motion->circularVelocity()){
        const sf::Vector2f& coord = point->coord();
        const sf::Vector2f& pivot = motion->motionCenter();
        float angle = motion->circularVelocity() * dt_ *  utils::constants::RAD;
        sf::Vector2f newcoord = sf::Vector2f(cosf(angle) * (coord.x - pivot.x) - sinf(angle) * (coord.y - pivot.y) + pivot.x,
                                             sinf(angle) * (coord.x - pivot.x) + cosf(angle) * (coord.y - pivot.y) + pivot.y);
        point->setCoord(newcoord);
    }

    point->savePrevScale();
    point->setScale(point->scale() + motion->scaleVelocity() * dt_);

    point->savePrevAngle();
    point->setAngle(point->angle() + motion->angularVelocity() * dt_);
}
