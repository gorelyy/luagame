 
#include "luagame/IdManager.h"

IdManager::IdManager() : uniqueID_(1), groupID_(1){}

ID IdManager::getNewID()
{
    ID result;
    if (!removedID_.empty()){
        result = removedID_.back();
        removedID_.pop_back();
    }
    else
        result = uniqueID_++;
    return result;
}

ID IdManager::getGroupID()
{
    ID result;
    if (!removed_groupID_.empty()){
        result = removed_groupID_.back();
        removed_groupID_.pop_back();
    }
    else
        result = groupID_++;
    return result;
}

void IdManager::removeID(ID id)
{
    removedID_.push_back(id);
}
