 
#include "luagame/components/ScriptC.h"

ScriptC::ScriptC() : created_(false)
{
}

void ScriptC::load(const std::string& script_name)
{       
    luabind::object dofile = luabind::globals(luaState())["dofile"];
    script_ = dofile(script_name);
}

void ScriptC::load(const std::string& script_name,const luabind::object &args)
{
    luabind::object dofile = luabind::globals(luaState())["dofile"];
    script_ = dofile(script_name);
    create_args_ = args;
}

void ScriptC::create()
{
    if(created_ || !script_.is_valid())
        return;
    created_ = true;
    luabind::object callCreate = script_["create"];
    if (callCreate.is_valid() && luabind::type(callCreate) != LUA_TNIL)
        callCreate(this, create_args_);
}

void ScriptC::update()
{
    if(!script_.is_valid())
        return;
    luabind::object callUpdate = script_["update"];
    if (callUpdate.is_valid() && luabind::type(callUpdate) != LUA_TNIL)
        callUpdate();
}

void ScriptC::postUpdate()
{
    if(!script_.is_valid())
        return;
    luabind::object callUpdate = script_["postupdate"];
    if (callUpdate.is_valid() && luabind::type(callUpdate) != LUA_TNIL)
        callUpdate();
}

luabind::object ScriptC::get(const std::string &name)
{
    if(!script_.is_valid())
        return luabind::object();
    return script_[name];
}

void ScriptC::call(const std::string &name)
{
    if(!script_.is_valid())
        return;
    luabind::object f = script_[name];
    if(f.is_valid() && luabind::type(f) == LUA_TFUNCTION)
        f();
}

void ScriptC::call(const std::string &name, const luabind::object& args)
{
    if(!script_.is_valid())
        return;
    luabind::object f = script_[name];
    if(f.is_valid() && luabind::type(f) == LUA_TFUNCTION)
        f(args);
}
