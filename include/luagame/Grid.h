#pragma once
class CollisionCheckC;
class BoxC;
#include <SFML/Graphics/Rect.hpp>
#include <SFML/System/Vector2.hpp>
#include <unordered_map>
#include <unordered_set>
#include <set>
#include <vector>

class Grid{
typedef std::pair<BoxC*, BoxC*> collisionPair;
public:
    Grid();
    bool isDynamic()                                const;
    CollisionCheckC*   collisionData()              const;
    const std::string& groupA()                     const;
    const std::string& groupB()                     const;
    const std::vector<collisionPair>& collisions()  const;
    const std::vector<sf::IntRect>& cells()         const;
    void    update();
    void    updateBoxBPosition(BoxC* box);
    void    updateBoxAPosition(BoxC* box);
    void    findCollisions(BoxC* box);
    void    clearCollisions();
    void    optimizeCollisions();
    void    setDynamic(bool value);
    void    load(const std::string& A,
                 const std::string& B,
                 const sf::IntRect& collision_area,
                 const sf::Vector2f& cell_size,
                 CollisionCheckC* collision_data);
private:
    bool dynamic_;
    CollisionCheckC* collision_data_;
    sf::IntRect collision_area_;
    sf::Vector2f cell_size_;
    std::string groupA_,
                groupB_;
    std::vector<collisionPair> collisions_;
    std::vector<sf::IntRect> cell_rectangles_;
    std::unordered_map<int, std::vector<BoxC*> > cells_;
    std::unordered_set<BoxC*> box_registry_;
};



