#include "luagame/components/LayerC.h"

LayerC::LayerC() :
    texture_(nullptr),
    dynamic_(true),
    type_(sf::Quads)
{
}

const sf::Texture* LayerC::texture() const
{
    return texture_;
}

void LayerC::bindTexture(const sf::Texture* t)
{
    texture_ = t;
}

void LayerC::setDynamic(bool value)
{
    dynamic_ = value;
}

bool LayerC::isDynamic() const
{
    return dynamic_;
}

void LayerC::setTilemap(uint width_in_tiles, uint height_in_tiles, uint size, const sf::IntRect& texture_bounds, const luabind::object& data)
{
    tilemap_texture_bounds_= texture_bounds;
    width_in_tiles_ = width_in_tiles;
    height_in_tiles_ = height_in_tiles;
    tilesize_ = size;
    for(luabind::iterator value(data), end; value != end; ++value)
        tilemap_data_.push_back(luabind::object_cast<int>(*value));
}

const std::vector<int>& LayerC::tilemapData() const
{
    return tilemap_data_;
}

uint LayerC::tilemapWidth() const
{
    return width_in_tiles_;
}

uint LayerC::tilemapHeight() const
{
    return height_in_tiles_;
}

uint LayerC::tilemapTileSize() const
{
    return tilesize_;
}

const sf::IntRect& LayerC::tilemapTextureBounds() const
{
    return tilemap_texture_bounds_;
}

void LayerC::setType(sf::PrimitiveType type)
{
    type_ = type;
}

sf::PrimitiveType LayerC::type() const
{
    return type_;
}

void LayerC::setTilemapCollision(const std::string &groupname)
{
    tilemap_collision_group_ = groupname;
}

const std::string& LayerC::tilemapCollisionGroup() const
{
    return tilemap_collision_group_;
}
