#include "luagame/EntityGroupRegistry.h"

EntityGroupRegistry::EntityGroupRegistry()
{
}

void EntityGroupRegistry::registerGroup(ID entity, Group *group)
{
    auto& groups = registry_[entity];
    groups.push_back(group);
}

void EntityGroupRegistry::deregisterGroup(ID entity, Group *group)
{
    auto& groups = registry_[entity];
    auto it = std::find(groups.begin(), groups.end(), group);
    if (it != groups.end()){
        std::swap(*it,groups.back());
        groups.pop_back();
    }
}

const std::vector<Group*>& EntityGroupRegistry::entityGroups(ID entity) const
{
    return registry_.at(entity);
}
