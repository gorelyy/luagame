#pragma once
#include"../Component.h"
#include <string>
#include <vector>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <locale>
#include <codecvt>

class TextC : public Component{
public:
    TextC();
    enum TEXTTWEEN {
        NONE,
        COORD,
        SCALE,
        ANGLE,
        COLOR,
        ALL
    };
    TEXTTWEEN textTween()                       const;
    sf::Vector2f iterationCharacterCoord(size_t index);
    const sf::Texture* texture()                const;
    const std::wstring& textString()            const;
    const sf::Color& color()                    const;
    sf::Vector2f characterSize(int index)       const;
    sf::Vector2f characterOffset(int index)     const;
    sf::Vector2f characterCoord(size_t index)   const;
    sf::IntRect characterTextureRect(int index) const;
    size_t capacity()                           const;
    size_t prevTextSize()                       const;
    int  currentTypingChar()                    const;
    bool requiresUpdate()                       const;
    bool requiresHide()                         const;
    bool isDynamic()                            const;
    bool characterIsValid(int index)            const;
    void    setTextTween(TEXTTWEEN type);
    void    bindFont(const sf::Font* font);
    void    setText(std::string text);
    void    setFontSize(int size);
    void    setTypingCallback(const luabind::object& callback);
    void    setCapacity(size_t size);
    void    setColor(const sf::Color& color);
    void    hide();
    void    hidden();
    void    updated();
    void    updateTime(float time);
    void    type(int lines, float speed);
    void    setDynamic(bool value);
private:

    void    calculateLines(int index);
    sf::Color color_;
    TEXTTWEEN tweentype_;
    const sf::Font* font_;
    std::wstring text_;
    sf::IntRect text_area_;
    int fontsize_;
    int current_typing_char_;
    float typing_speed_;
    float hspace_,
          vspace_;
    float time_;
    bool bold_;
    bool requires_update_;
    bool requires_hide_;
    bool dynamic_;
    uint prevchar_;
    uint currentline_;
    uint max_typing_char_;
    size_t capacity_;
    size_t prevtextsize_;
    sf::Vector2f curchar_coord_;
    std::vector<std::pair<uint, uint> > lines_;
    luabind::object typing_callback_;
};
