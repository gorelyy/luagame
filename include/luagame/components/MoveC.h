#pragma once
#include "../Component.h"
#include <SFML/System/Vector2.hpp>
#include <cmath>

class MoveC:public Component {
public:
    MoveC();
    const sf::Vector2f&    velocity()       const;
    const sf::Vector2f&    acceleration()   const;
    const sf::Vector2f&    maxVelocity()    const;
    const sf::Vector2f&   minVelocity()     const;
    float    rotationVelocity()             const;
    float    rotationAcceleration()         const;
    float    maxRotationVelocity()          const;
    float    minRotationVelocity()          const;
    float    angle()                        const;
    float    directionalVelocity()          const;
    float    directionalAcceleration()      const;
    float    maxDirectionalVelocity()       const;
    float    minDirectionalVelocity()       const;
    float    angularVelocity()              const;
    float    angularAcceleration()          const;
    float    maxAngularVelocity()           const;
    float    minAngularVelocity()           const;
    const sf::Vector2f& scaleVelocity()     const;
    const sf::Vector2f& scaleAcceleration() const;
    const sf::Vector2f& maxScaleVelocity()  const;
    const sf::Vector2f& minScaleVelocity()  const;
    float    circularVelocity()             const;
    float    circularAcceleration()         const;
    const sf::Vector2f&    motionCenter()   const;
    void    setVelocity(const sf::Vector2f& v);
    void    setVelocityX(float vx);
    void    setVelocityY(float vy);
    void    setAcceleration(const sf::Vector2f& acceleration);
    void    setAccelerationX(float ax);
    void    setAccelerationY(float ay);
    void    setMaxVelocity(const sf::Vector2f& v);
    void    setMinVelocity(const sf::Vector2f& v);
    void    capVelocity(const sf::Vector2f& v);
    void    setRotationVelocity(float v);
    void    setRotationAcceleration(float a);
    void    setAngle(float angle);
    void    setMaxRotationVelocity(float vr);
    void    setMinRotationVelocity(float vr);
    void    capRotationVelocity(float vr);
    void    setDirectionalVelocity(float v);
    void    setDirectionalAcceleration(float a);
    void    setMaxDirectionalVelocity(float vr);
    void    setMinDirectionalVelocity(float vr);
    void    capDirectionalVelocity(float vr);

    void    setAngularVelocity(float v);
    void    setAngularAcceleration(float a);
    void    setMaxAngularVelocity(float vr);
    void    setMinAngularVelocity(float vr);
    void    capAngularVelocity(float vr);
    void    setScaleVelocity(const sf::Vector2f&v);
    void    setScaleVelocityX(float vx);
    void    setScaleVelocityY(float vy);
    void    setScaleAcceleration(const sf::Vector2f& a);
    void    setScaleAccelerationX(float ax);
    void    setScaleAccelerationY(float ay);
    void    setMaxScaleVelocity(const sf::Vector2f& v);
    void    setMinScaleVelocity(const sf::Vector2f& v);
    void    capScaleVelocity(const sf::Vector2f& v);
    void    setMotionCenter(const sf::Vector2f& coord);
    void    setCircularVelocity(float vr);
    void    setCircularAcceleration(float ar);
private:    
    sf::Vector2f velocity_;
    sf::Vector2f acceleration_;
    sf::Vector2f maxvelocity_;
    sf::Vector2f minvelocity_;

    float rotationvelocity_;
    float rotationacceleration_;
    float maxrotationvelocity_;
    float minrotationvelocity_;
    float angle_;
    float directionalvelocity_;
    float directionalacceleration_;
    float maxdirectionalvelocity_;
    float mindirectionalvelocity_;

    float angularvelocity_;
    float angularacceleration_;
    float maxangularvelocity_;
    float minangularvelocity_;

    float circularvelocity_;
    float circularacceleration_;
    sf::Vector2f motion_center_;

    sf::Vector2f scalevelocity_;
    sf::Vector2f scaleacceleration_;
    sf::Vector2f maxscalevelocity_;
    sf::Vector2f minscalevelocity_;
};


