#pragma once
#include <SFML/Graphics/VertexArray.hpp>
#include <unordered_map>
#include <vector>
#include "globals.h"

class VertexArrayContainer
{
public:
    VertexArrayContainer();
    std::vector<sf::Vertex>& getVertexArray(ID key);
    void    deleteVertexArray(ID key);
    void    deleteVertices(ID key, size_t index, size_t size);
private:
    std::unordered_map<ID, std::vector<sf::Vertex> > layers_;
};
