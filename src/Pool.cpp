#include "luagame/Pool.h"
Pool::Pool() :
    layer_(nullptr)
{
}

void Pool::bindComponentContainer(ComponentContainer *cc)
{
    component_container_ = cc;
}

void Pool::bindLayer(LayerC *layer)
{
    layer_ = layer;
}

ID Pool::id() const
{
    return id_;
}

void Pool::setID(ID id)
{
    id_ = id;
}

LayerC* Pool::layer()
{
    return layer_;
}
