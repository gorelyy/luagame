#include "luagame/GroupContainer.h"

GroupContainer::GroupContainer()
{
}

Group* GroupContainer::group(Group::BUILTIN id)
{
    return &builtingroups_[id];
}

void GroupContainer::bindEntityGroupRegistry(EntityGroupRegistry *egr)
{
    entitygroupregistry_ = egr;
}

void GroupContainer::build()
{
    //NEW_COMPONENT_TYPE
    BaseGroupContainer<QuadC>::getDefaultGroup()->bindEntityGroupRegistry(entitygroupregistry_);
    BaseGroupContainer<LineC>::getDefaultGroup()->bindEntityGroupRegistry(entitygroupregistry_);
    BaseGroupContainer<MoveC>::getDefaultGroup()->bindEntityGroupRegistry(entitygroupregistry_);
    BaseGroupContainer<ScriptC>::getDefaultGroup()->bindEntityGroupRegistry(entitygroupregistry_);
    BaseGroupContainer<AnimC>::getDefaultGroup()->bindEntityGroupRegistry(entitygroupregistry_);
    BaseGroupContainer<BoxC>::getDefaultGroup()->bindEntityGroupRegistry(entitygroupregistry_);
    BaseGroupContainer<CollisionCheckC>::getDefaultGroup()->bindEntityGroupRegistry(entitygroupregistry_);
    BaseGroupContainer<PointC>::getDefaultGroup()->bindEntityGroupRegistry(entitygroupregistry_);
    BaseGroupContainer<LayerC>::getDefaultGroup()->bindEntityGroupRegistry(entitygroupregistry_);
    BaseGroupContainer<CameraC>::getDefaultGroup()->bindEntityGroupRegistry(entitygroupregistry_);
    BaseGroupContainer<TweenCoordC>::getDefaultGroup()->bindEntityGroupRegistry(entitygroupregistry_);
    BaseGroupContainer<TweenScaleC>::getDefaultGroup()->bindEntityGroupRegistry(entitygroupregistry_);
    BaseGroupContainer<TweenAngleC>::getDefaultGroup()->bindEntityGroupRegistry(entitygroupregistry_);
    BaseGroupContainer<SoundC>::getDefaultGroup()->bindEntityGroupRegistry(entitygroupregistry_);
    BaseGroupContainer<TextC>::getDefaultGroup()->bindEntityGroupRegistry(entitygroupregistry_);
    BaseGroupContainer<TimerC>::getDefaultGroup()->bindEntityGroupRegistry(entitygroupregistry_);


    for (auto& each : builtingroups_)
        each.bindEntityGroupRegistry(entitygroupregistry_);
}
