#pragma once
#include<SFML/System/Vector2.hpp>
#include<SFML/Graphics/Color.hpp>
#include<math.h>
#include"Utils.h"

class Drawable {
public:
    Drawable();
    const sf::Vector2f& coord()         const;
    const sf::Vector2f& offsetCoord()   const;
    const sf::Vector2f& scale()         const;
    const sf::Vector2f& offsetScale()   const;
    float angle()                       const;
    float offsetAngle()                 const;
    const sf::Vector2f& pivot()         const;
    const sf::Color &color()            const;
    const sf::Vector2f& scrollFactor()  const;
    bool isVisible()                    const;
    void    setCoord(const sf::Vector2f& coord);
    void    setOffsetCoord(const sf::Vector2f& offset);
    void    setScale(const sf::Vector2f& scale);
    void    setOffsetScale(const sf::Vector2f& scale);
    void    setAngle(float angle);
    void    setOffsetAngle(float angle);
    void    setPivot(const sf::Vector2f& pivot);
    void    setColor(const sf::Color& color);
    void    setScrollFactor(const sf::Vector2f& scrollfactor);
    void    setVisibility(bool value);
protected:
    bool visible_;
    float angle_;
    float offset_angle_;
    sf::Vector2f coord_;
    sf::Vector2f offset_coord_;
    sf::Vector2f scale_;
    sf::Vector2f offset_scale_;
    sf::Vector2f pivot_;
    sf::Vector2f scroll_factor_;
    sf::Color color_;

};
