#pragma once
class Group;
#include <unordered_map>
#include "globals.h"

class EntityGroupRegistry{
public:
    EntityGroupRegistry();
    const std::vector<Group*>& entityGroups(ID entity) const;
    void    registerGroup(ID entity, Group* group);
    void    deregisterGroup(ID entity, Group* group);
private:
    std::unordered_map<ID,std::vector<Group*> > registry_;
};
