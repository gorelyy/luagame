#pragma once
#include <SFML/Graphics.hpp>
#include <map>
#include <string>
#include <thread>
class FontCache{
public:
    FontCache();
    const sf::Font* getFont(const std::string& filename);
    void    asyncLoadFont(const std::string& filename);
    void    launchLoadingThread();
private:
    void    loadingThread();

    bool enabled_;
    std::map<std::string, sf::Font> font_container_;
    std::thread loading_thread_;
    std::vector<std::string> loading_queue_;

};
