return {
  window = {
    title = "luagame",
    resolution = _v2(640,480),
    colordepth = 32,
    vsync = true,
    fullscreen = false,
    updaterate = 30,
    framerate = 59,     
    color = _color(50,50,50,255)
  },
  
  default_camera = { 
    mode =          _CAMERA.WINDOW,
    size =          _v2(640,480),--_v2(1024,896),
    window_offset = _v2(-1,-1),  -- (-1,-1) to center
    scroll_area =   _rect(0,0,640,480),
    color =         _color(0,0,0,255)
  }
}
--[[
return {
  window = {
    title = "Shooting Demo",
    resolution = _v2(1600,900),
    colordepth = 32,
    vsync = true,
    fullscreen = false,
    updaterate = 30,
    framerate = 58,            -- <= 0 for unlimited
    color = _color(50,50,50,255)
  },
  
  default_camera = { 
    mode =          _CAMERA.WINDOW,
    size =          _v2(1600,900),--_v2(1024,896),
    window_offset = _v2(-1,-1),  -- (-1,-1) to center
    scroll_area =   _rect(-1000,-1000,2640,2640),
    color =         _color(0,0,0,255)
  }
}
]]