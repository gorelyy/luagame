#pragma once
#include <vector>
#include "globals.h"

class IdManager {
public:
    IdManager();
    uint getNewID();
    uint getGroupID();
    void    removeID(ID id);
private:

    ID uniqueID_;
    ID groupID_;
    std::vector <ID > removedID_;
    std::vector <ID > removed_groupID_;
};
