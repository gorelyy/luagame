#pragma once
extern "C"
{
    #include <lua.h>
    #include <lualib.h>
    #include <lauxlib.h>
}
#include <luabind/luabind.hpp>
#include "globals.h"
#include <SFML/System/Vector2.hpp>

inline sf::Vector2f operator*(const sf::Vector2f& left, int right)
{
    return sf::Vector2f(left.x * right, left.y * right);
}

inline sf::Vector2f operator*(const sf::Vector2f& left, const sf::Vector2f& right)
{
    return sf::Vector2f(left.x*right.x, left.y * right.y);
}

inline sf::Vector2f operator*(const sf::Vector2f& left, float right)
{
    return sf::Vector2f(left.x * right, left.y * right);
}

class Component {
public:  
    enum SIDE {
        UP,
        RIGHT,
        DOWN,
        LEFT,
        ALL,
        NONE,
        ANY
    };
    enum AXIS {X,Y};
    Component();
    std::string key()           const;
    const componentID&  id()    const;
    ID      entityID()          const;
    uint    componentIndex()    const;
    int     pointIndex()        const;
    bool    enabled()           const;
    ID      poolID()            const;
    uint    poolIndex()         const;
    void    setID(ID e);
    void    setIndex(uint index);
    void    setPointIndex(int index);
    void    setEnabled(bool status);
    void    setLuaState(lua_State* L);
    void    setPoolID(ID p);
    void    setPoolIndex(uint index);
    void    setDisabledCounter(uint* counter);
protected:

    lua_State * luaState() const;
    int         point_index_;
    componentID id_;
    componentID pool_id_;
private:
    lua_State* L_;
    bool enabled_;
    uint* disabledcounter_;

};



