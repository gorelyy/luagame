#pragma once
#include "../Component.h"
#include "../Drawable.h"
#include <SFML/Graphics.hpp>
#include <SFML/System/Vector2.hpp>
#include <vector>
#include <string>

class LayerC:public Component, public Drawable {
public:
    LayerC();
    bool isDynamic()                            const;
    uint tilemapWidth()                         const;
    uint tilemapHeight()                        const;
    uint tilemapTileSize()                      const;
    const sf::Texture* texture()                const;
    const sf::Vector2f& scrollFactor()          const;
    const sf::IntRect& tilemapTextureBounds()   const;
    const std::string& tilemapCollisionGroup()  const;
    sf::PrimitiveType type()                    const;
    const std::vector<int>& tilemapData()       const;
    void    bindTexture(const sf::Texture* t);
    void    setDynamic(bool value);
    void    setTilemap(uint width_in_tiles,
                       uint height_in_tiles,
                       uint size,
                       const sf::IntRect& texture_bounds,
                       const luabind::object& data);
    void    setTilemapCollision(const std::string&  groupname);
    void    setType(sf::PrimitiveType type);
private:
    const sf::Texture* texture_;
    bool dynamic_;
    sf::PrimitiveType type_;
    std::vector<int> tilemap_data_;
    sf::IntRect tilemap_texture_bounds_;
    std::string tilemap_collision_group_;
    uint width_in_tiles_;
    uint height_in_tiles_;
    uint tilesize_;
};
