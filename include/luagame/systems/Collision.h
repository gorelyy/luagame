#pragma once
class BoxC;
class Scene;
class Grid;
class BroadPhase;
class QTree;
class CollisionCheckC;
class AABB;
#include "../Utils.h"
#include <string>
#include <vector>
#include <set>
#include <iostream>
#include <memory>
#include "../globals.h"

class Collision{
typedef std::pair<BoxC*, BoxC*> collisionPair; 
public:
    Collision();  
    void    update(Scene* scene);
private:
    bool    overlap(BoxC* boxA, BoxC* boxB, sf::Vector2f* separation, sf::Vector2f* translation);
    void    loop(CollisionCheckC* component);
    void    loopBox(BoxC* box);
    void    collideAll(std::vector<collisionPair>::const_iterator pairs_begin,
                       std::vector<collisionPair>::const_iterator pairs_end,
                       CollisionCheckC* collision_data);
    void    collidePair(BoxC* boxA, BoxC* boxB,CollisionCheckC* collision_data);

    CollisionCheckC* test_collision_data_;
    Scene* scene_;
    collisionPair temppair_;
    std::string tempname_;
    std::vector<std::shared_ptr<BroadPhase> >  broad_phase_checks_;
    std::vector<collisionPair> collisions_;
    std::set<componentID> checked_to_save_state_boxesA_;
};

