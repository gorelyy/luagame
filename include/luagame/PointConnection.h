#pragma once
class Group;
#include <unordered_map>
#include <vector>
#include <algorithm>
#include <SFML/System/Vector2.hpp>
#include "globals.h"
#include <luabind/luabind.hpp>

struct FollowData
{
    enum FollowType {
        DISTANCE,
        DEADZONE
    };
    FollowData() :
        mode(FollowType::DISTANCE), easing(1.f)
    {
    }

    FollowData(const sf::Vector2f& d, FollowType m, float s, const luabind::object& c) :
        distance(d), mode(m), easing(s), callback(c)
    {
    }

    sf::Vector2f distance;
    float easing;
    FollowType mode;
    luabind::object callback;
};

struct ParentCoord
{
    sf::Vector2f current;
    sf::Vector2f prev;
};

struct componentIDHasher
{
  std::size_t operator()(const componentID& k) const
  {
    using std::size_t;
    using std::hash;
    using std::string;

    return ((hash<ID>()(k.first)
             ^ (hash<uint>()(k.second) << 1)) >> 1);
  }
};


class PointConnection{
public:
    PointConnection();
    bool parentRegistered(componentID parent)            const;
    bool leaderRegistered(componentID leader)            const;
    const std::vector<componentID>& parents()            const;
    const std::vector<componentID>& leaders()            const;
    const std::vector<componentID>& children(componentID parent)  const;
    const std::vector<componentID>& followers(componentID leader) const;
    const ParentCoord& parentCoord(componentID parent);
    const sf::Vector2f& parentOffset(componentID parent);
    FollowData& followData(componentID follower);
    void    connectFollowing(componentID leader,componentID follower, const FollowData& followdata);
    void    connectFollowing(componentID leader,Group* group, const FollowData& followdata);
    void    connectParenting(componentID parent,componentID child);
    void    connectParenting(componentID parent,Group* group);
    void    removeFollower(componentID leader,componentID follower);
    void    removeLeadingNode(componentID node);
    void    removeChild(componentID parent,componentID child);
    void    removeParentingNode(componentID node);
    void    sort();
    void    setParentCoord(componentID parent, const sf::Vector2f& coord, const sf::Vector2f& prevcoord);
    void    setParentOffset(componentID parent, const sf::Vector2f& offset);
private:
    componentID findLeader(componentID root, componentID id) const;
    componentID findParent(componentID node, componentID id) const;
    unsigned short findParentingLevel(componentID node, componentID id, unsigned short level) const;
    unsigned short findFollowingLevel(componentID node, componentID id, unsigned short level) const;
    void    setParentingLevel(componentID id,unsigned short level);
    void    setFollowingLevel(componentID id,unsigned short level);
    void    incrementFollowingLevel(componentID id);

    std::unordered_map<componentID,std::vector<componentID>,componentIDHasher > parent_;
    std::unordered_map<componentID,std::vector<componentID>,componentIDHasher > leader_;
    std::unordered_map<componentID,FollowData,componentIDHasher> follow_data_;
    std::unordered_map<componentID,ParentCoord,componentIDHasher> parent_data_coord_;
    std::unordered_map<componentID,sf::Vector2f,componentIDHasher> parent_data_offset_;
    std::unordered_map<componentID,unsigned short,componentIDHasher> parent_level_;
    std::vector<componentID> parent_level_root_;
    std::unordered_map<componentID,unsigned short,componentIDHasher> leader_level_;
    std::vector<componentID> leader_level_root_;
    std::vector<componentID> parent_update_order_; //sort by child number
    std::vector<componentID> leader_update_order_;
    bool need_to_sort_;
};
