#include "luagame/QTree.h"

QTree::QTree() : dynamic_(true)
{
}

void QTree::load(const std::string& A, const std::string& B, const sf::IntRect& area, uint maxlevel, CollisionCheckC* collision_data)
{
    groupA_ = A;
    groupB_ = B;
    radius_ = sf::Vector2f(area.width / 2, area.height/2);
    invsize_ = sf::Vector2f(255.f / area.width, 255.f / area.height);
    center_ = sf::Vector2f(area.left,area.top) + radius_;
    maxlevel_ = utils::clip(maxlevel,1u,6u);
    collision_data_ = collision_data;

    uint nodesnum = 1;
    for (int i = 1; i <= maxlevel_; ++i)
        nodesnum += (1<<i)*(1<<i);
    nodes_.resize(nodesnum);

    QTreeNode* node = &nodes_.front();
    QTreeNode* last = nullptr;
    float width = area.width;
    float height = area.height;
    int index = 0;
    for (uint i = 0; i <= maxlevel_; ++i){
        levels_[i] = node;
        for (uint y = (1<<i); y--;){
            QTreeNode* row = &node[y*(1<<i)];
            float ycoord = (float)y / (float)(1<<i) * area.height;
            for (uint x = (1<<i); x--;){
                row[x].set(sf::Vector2f((float)x / (float)(1<<i) * area.width,ycoord),sf::Vector2f(width,height),i,index++);
                cell_rectangles_.push_back(sf::IntRect((float)x / (float)(1<<i) * area.width,ycoord,width,height));
                if (last){
                    uint parentx = x >> 1;
                    uint parenty = y >> 1;
                    uint parentindex = (parenty * (1<<(i-1))) + parentx;
                    uint childx = x & 1;
                    uint childy = y & 1;
                    last[parentindex].setChild((childy<<1) + childx,&row[x]);
                }
            }
        }

        last = node;
        node += (1<<i)*(1<<i);
        width = width / 2;
        height = height / 2;

    }
}

QTreeNode* QTree::findBoxNode(BoxC *box)
{
    AABB aabb(box->coord(), box->size() * box->scale());
    if (aabb.min.x < center_.x - radius_.x || aabb.max.x > center_.x + radius_.x ||
        aabb.min.y < center_.y - radius_.y || aabb.max.y > center_.y + radius_.y)
        return &nodes_[0];

    AABB aabbshifted(box->coord() - center_ + radius_,box->size() * box->scale());

    aabbshifted.min.x *= invsize_.x;
    aabbshifted.min.y *= invsize_.y;
    aabbshifted.max.x *= invsize_.x;
    aabbshifted.max.y *= invsize_.y;

    uint minx = (uint)utils::clip(floorf(aabbshifted.min.x),0.f, 255.f);
    uint maxx = (uint)utils::clip(ceilf(aabbshifted.max.x),0.f, 255.f);
    uint miny = (uint)utils::clip(floorf(aabbshifted.min.y),0.f, 255.f);
    uint maxy = (uint)utils::clip(ceilf(aabbshifted.max.y),0.f, 255.f);

    uint x = minx ^ maxx;
    x = x? 7 - utils::highestBit(x) : maxlevel_;
    uint y = miny ^ maxy;
    y = y? 7 - utils::highestBit(y) : maxlevel_;

    uint level = std::min(maxlevel_,std::min(x,y));

    x = minx >> (8 - level);
    y = miny >> (8 - level);

    return &levels_[level][y * (1<<level) +x];
}

void QTree::updateBoxA(BoxC *boxA)
{
    if (boxA->prevCoord() == boxA->coord())
        return;
    QTreeNode* newnode = findBoxNode(boxA);
    QTreeNode* node = boxA->getQTreeNode();
    if (node != newnode){
        node->removeBoxA(boxA);
        boxA->setQTreeNode(newnode);
        newnode->addBoxA(boxA);
    }
}

void QTree::updateBoxB(BoxC *boxB)
{
    QTreeNode* node = boxB->getQTreeNode();
    if (boxB->prevCoord() == boxB->coord()){
        if (node->level() < maxlevel_)
            lookDownForCollisionsWithB(boxB,node);
    }
    else{
        QTreeNode* newnode = findBoxNode(boxB);
        if (node != newnode){
            node->removeBoxB(boxB);
            boxB->setQTreeNode(newnode);
            newnode->addBoxB(boxB);
        }
        if (newnode->level() < maxlevel_)
            lookDownForCollisionsWithB(boxB,newnode);
    }
}

void QTree::updateStaticBoxesB()
{
    for (QTreeNode* node : boxB_not_leaves_nodes_){
        for (BoxC* boxB : node->boxesB())
            lookDownForCollisionsWithB(boxB,node);
    }
}

void QTree::findCollisions(BoxC *boxA)
{
    QTreeNode* node = findBoxNode(boxA);
    lookDownForCollisionsWithA(boxA,node);
}

void QTree::lookDownForCollisionsWithB(BoxC *boxB, QTreeNode *node)
{
    for (auto boxA : node->boxesA())
        collisions_.push_back(std::make_pair(boxA,boxB));
    auto& kids = node->kids();
    if(kids[0] == nullptr)
        return;
    for(uint i = 0; i < 4; ++i)
        lookDownForCollisionsWithB(boxB,kids[i]);
}

void QTree::lookDownForCollisionsWithA(BoxC* boxA, QTreeNode* node)
{
    for (auto boxB : node->boxesB())
        collisions_.push_back(std::make_pair(boxA,boxB));
    auto& kids = node->kids();
    if(kids[0] == nullptr)
        return;
    for(uint i = 0; i < 4; ++i)
        lookDownForCollisionsWithA(boxA,kids[i]);
}

const std::vector<QTree::collisionPair>& QTree::collisions() const
{
    return collisions_;
}

const std::vector<sf::IntRect>& QTree::cells() const
{
    return cell_rectangles_;
}

const std::string& QTree::groupA() const
{
    return groupA_;
}

const std::string& QTree::groupB() const
{
    return groupB_;
}

void QTree::clearCollisions()
{
    collisions_.clear();
}

CollisionCheckC* QTree::collisionData()
{
    return collision_data_;
}

const std::vector<QTreeNode*>& QTree::boxBNotLeavesNodes() const
{
    return boxB_not_leaves_nodes_;
}

void QTree::addBoxBNotLeafeNode(QTreeNode *node)
{
    auto it = std::find(boxB_not_leaves_nodes_.begin(),
                        boxB_not_leaves_nodes_.end(),node);
    if (it == boxB_not_leaves_nodes_.end())
        boxB_not_leaves_nodes_.push_back(node);
}

void QTree::setDynamic(bool value)
{
    dynamic_ = value;
}

bool QTree::isDynamic() const
{
    return dynamic_;
}

uint QTree::maxLevel() const
{
    return maxlevel_;
}
