 
#include "luagame/Group.h"
#include "luagame/Entity.h"
#include "luagame/EntityGroupRegistry.h"

Group::Group() : size_(0), priority_(0), need_to_sort_(false)
{
}

void Group::add(ID id)
{
    if(std::find(id_list_.begin(),id_list_.end(),id) != id_list_.end())
        return;
    size_++;
    id_list_.push_back(id);
    group_list_.resize(size_,nullptr);
    priority_container_[id] = 0;
    need_to_sort_ = true;
    entitygroupregistry_->registerGroup(id,this);
}

void Group::add(Entity *entity)
{
    add(entity->id());
}

void Group::add(ID id, size_t index)
{
    if(std::find(id_list_.begin(),id_list_.end(),id) != id_list_.end())
        return;
    size_++;
    id_list_.insert(id_list_.begin() + index, id);
    group_list_.insert(group_list_.begin() + index, nullptr);
    priority_container_[id] = 0;
    need_to_sort_ = true;
    entitygroupregistry_->registerGroup(id,this);
}

void Group::add(Entity* entity, size_t index)
{
    add(entity->id(),index);
}

void Group::add(Group* group)
{
    if(std::find(group_list_.begin(),group_list_.end(), group) != group_list_.end())
        return;
    size_++;
    group_list_.push_back(group);
    id_list_.resize(size_, -1);
    need_to_sort_ = true;
}

void Group::add(Group* group, size_t index)
{
    if(std::find(group_list_.begin(),group_list_.end(), group) != group_list_.end())
        return;
    size_++;
    group_list_.insert(group_list_.begin() + index,group);
    id_list_.insert(id_list_.begin() + index, -1);
    need_to_sort_ = true;
}

void Group::clear()
{
    size_ = 0;
    priority_ = 0;
    need_to_sort_ = false;
    id_list_.clear();
    group_list_.clear();
    priority_container_.clear();
}

size_t Group::size() const
{
    return size_;
}

void Group::sort()
{
    if (!need_to_sort_)
        return;
    std::sort(id_list_.begin(),id_list_.end(),[&](ID a, ID b)
    {
        return priority_container_[a] < priority_container_[b];
    });

    while(id_list_.back() < 0)
        id_list_.pop_back();

    //Поместить действительные указатели в начало
    std::sort(group_list_.begin(), group_list_.end(),[&](Group* a, Group* b){
        if (!a)
            return false;
        else if(!b)
            return true;
        else
            return a->priority() < b->priority();
    });


    std::vector<Group*> tmp_group;
    for(size_t i = 0; i != group_list_.size(); ++i)
        if (!group_list_[i])
            break;
        else
            tmp_group.push_back(group_list_[i]);
    group_list_.assign(group_list_.size(), nullptr);
	
	if (tmp_group.empty())
		return;
    size_t group_index = 0;
    for (size_t i = 0; i != id_list_.size(); ++i)
        if (priority_container_[id_list_[i]] >= tmp_group[group_index]->priority()){
            id_list_.insert(id_list_.begin() + i,-1);
            group_list_[i] = tmp_group[group_index];
            if(++group_index == tmp_group.size())
                break;
        }

    need_to_sort_ = false;
}

void Group::deepSort()
{
    sort();
    for (Group* group : group_list_)
        if(group)
            group->deepSort();
}

unsigned char Group::priority() const
{
    return priority_;
}

void Group::setPriority(unsigned char priority)
{
    priority_ = priority;
}

void Group::setPriority(ID id, unsigned char priority)
{
    priority_container_[id] = priority;
    need_to_sort_ = true;
}

void Group::setPriority(Entity *entity, unsigned char priority)
{
    setPriority(entity->id(),priority);
}

void Group::remove(ID id)
{
    auto it = std::find(id_list_.begin(), id_list_.end(), id);
	if (it == id_list_.end())
		return;
	size_t index = it - id_list_.begin();
    id_list_.erase(it);
    group_list_.erase(group_list_.begin() + index);
	size_--;
    priority_container_.erase(id);
    entitygroupregistry_->deregisterGroup(id,this);
}

void Group::remove(Entity *entity)
{
    remove(entity->id());
}

void Group::remove(Group* group)
{
    auto it = std::find(group_list_.begin(), group_list_.end(), group);
	if (it == group_list_.end())
		return;
	size_t index = it - group_list_.begin();
    group_list_.erase(it);
    id_list_.erase(id_list_.begin() + index);
	size_--;
}

bool Group::has(ID id)
{
     if(std::find(id_list_.begin(),id_list_.end(),id) != id_list_.end())
         return true;
     for (auto group:group_list_)
        if (group)
            if (group->has(id))
                return true;
            else continue;
     return false;

}

bool Group::has(Entity *entity)
{
    return has(entity->id());
}

bool Group::hasInRoot(ID id)
{
    if(std::find(id_list_.begin(),id_list_.end(),id) != id_list_.end())
        return true;
    return false;
}

bool Group::hasInRoot(Entity *entity)
{
    return hasInRoot(entity->id());
}

void Group::bindEntityGroupRegistry(EntityGroupRegistry *egr)
{
    entitygroupregistry_ = egr;
}

void Group::bindEntityContainer(std::unordered_map<ID, Entity> *ec)
{
    entity_container_ = ec;
}

void Group::each(const luabind::object &f)
{
    luafunction_stack_.push_back(f);
    for (size_t i = 0; i != size_; ++i){
        if (group_list_[i])
            group_list_[i]->each(f);
        else
            luafunction_stack_.back()(&entity_container_->at(id_list_[i]));
    }
    luafunction_stack_.pop_back();
}
