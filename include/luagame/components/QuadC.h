#pragma once
#include "../Component.h"
#include "../Drawable.h"
#include "../Utils.h"
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <iostream>

class QuadC: public Component, public Drawable{
public:
   QuadC();
   const sf::Vector2f& size() const;
   sf::IntRect  textureRect() const;
   sf::Vector2f getVertexTextureCoord(uint index);
   sf::Vector2f getVertexCoord(uint index,
                               const sf::Vector2f &scrolloffset,
                               const sf::Vector2f& layercoord,
                               const sf::Vector2f& layeroffset,
                               const sf::Vector2f& layerscale,
                               float layerangle, const sf::Vector2f& layerpivot);
   void    setSize(const sf::Vector2f& size);
   void    setTextureRect(const sf::IntRect &rect);
private:
    sf::Vector2f size_;
    sf::Vector2f tex_size_;
    sf::Vector2f tex_coord_;
};
