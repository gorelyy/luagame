#include "luagame/components/TweenCoordC.h"


TweenCoordC::TweenCoordC()
{
}
void TweenCoordC::set(const sf::Vector2f &coord, float time)
{
    tween_vector_.set(coord,time);
}

void TweenCoordC::set(const sf::Vector2f &coord, float time, Ease::FUNCTION f, Ease::TYPE type)
{
    tween_vector_.set(coord,time,f,type);
}

void TweenCoordC::set(const sf::Vector2f &coord, float time, Ease::FUNCTION f, Ease::TYPE type, int repeat, unsigned char mode)
{
    tween_vector_.set(coord,time,f,type,repeat, mode);
}

void TweenCoordC::addCallbackOnFinish(const luabind::object &callback)
{
    tween_vector_.addCallbackOnFinish(callback);
}

void TweenCoordC::addCallbackOnRepeat(const luabind::object &callback)
{
    tween_vector_.addCallbackOnRepeat(callback);
}

void TweenCoordC::addCustomEasing(const luabind::object &f)
{
    tween_vector_.addCustomEasing(f);
}

void TweenCoordC::update(double dt)
{
    tween_vector_.update(dt);
}

sf::Vector2f TweenCoordC::offset() const
{
    return tween_vector_.value();
}

bool TweenCoordC::isEnabled() const
{
    return tween_vector_.isEnabled();
}
