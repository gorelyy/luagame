#include "luagame/components/TweenScaleC.h"

TweenScaleC::TweenScaleC()
{
}

void TweenScaleC::set(const sf::Vector2f &scale, float time)
{
    tween_vector_.set(scale,time);
}

void TweenScaleC::set(const sf::Vector2f &scale, float time, Ease::FUNCTION f, Ease::TYPE type)
{
    tween_vector_.set(scale,time,f,type);
}

void TweenScaleC::set(const sf::Vector2f &scale, float time, Ease::FUNCTION f, Ease::TYPE type, int repeat, unsigned char mode)
{
    tween_vector_.set(scale,time,f,type,repeat, mode);
}

void TweenScaleC::addCallbackOnFinish(const luabind::object &callback)
{
    tween_vector_.addCallbackOnFinish(callback);
}

void TweenScaleC::addCallbackOnRepeat(const luabind::object &callback)
{
    tween_vector_.addCallbackOnRepeat(callback);
}

void TweenScaleC::addCustomEasing(const luabind::object &f)
{
    tween_vector_.addCustomEasing(f);
}

void TweenScaleC::update(double dt)
{
    tween_vector_.update(dt);
}

sf::Vector2f TweenScaleC::scale() const
{
    return tween_vector_.value();
}

bool TweenScaleC::isEnabled() const
{
    return tween_vector_.isEnabled();
}

