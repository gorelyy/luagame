#include "luagame/Input.h"

Input::Input() 
{
    keymap_["up"]        = sf::Keyboard::Up;
    keymap_["down"]      = sf::Keyboard::Down;
    keymap_["right"]     = sf::Keyboard::Right;
    keymap_["left"]      = sf::Keyboard::Left;
    keymap_["esc"]       = sf::Keyboard::Escape;
    keymap_["f1"]        = sf::Keyboard::F1;
    keymap_["f2"]        = sf::Keyboard::F2;
    keymap_["f3"]        = sf::Keyboard::F3;
    keymap_["f4"]        = sf::Keyboard::F4;
    keymap_["f5"]        = sf::Keyboard::F5;
    keymap_["f6"]        = sf::Keyboard::F6;
    keymap_["f7"]        = sf::Keyboard::F7;
    keymap_["f8"]        = sf::Keyboard::F8;
    keymap_["f9"]        = sf::Keyboard::F9;
    keymap_["f10"]       = sf::Keyboard::F10;
    keymap_["f11"]       = sf::Keyboard::F11;
    keymap_["f12"]       = sf::Keyboard::F12;
    keymap_["1"]         = sf::Keyboard::Num1;
    keymap_["2"]         = sf::Keyboard::Num2;
    keymap_["3"]         = sf::Keyboard::Num3;
    keymap_["4"]         = sf::Keyboard::Num4;
    keymap_["5"]         = sf::Keyboard::Num5;
    keymap_["6"]         = sf::Keyboard::Num6;
    keymap_["7"]         = sf::Keyboard::Num7;
    keymap_["8"]         = sf::Keyboard::Num8;
    keymap_["9"]         = sf::Keyboard::Num9;
    keymap_["0"]         = sf::Keyboard::Num0;
    keymap_["-"]         = sf::Keyboard::Subtract;
    keymap_["+"]         = sf::Keyboard::Equal;
    keymap_["backspace"] = sf::Keyboard::BackSpace;
    keymap_["q"]         = sf::Keyboard::Q;
    keymap_["w"]         = sf::Keyboard::W;
    keymap_["e"]         = sf::Keyboard::E;
    keymap_["r"]         = sf::Keyboard::R;
    keymap_["t"]         = sf::Keyboard::T;
    keymap_["y"]         = sf::Keyboard::Y;
    keymap_["u"]         = sf::Keyboard::U;
    keymap_["i"]         = sf::Keyboard::I;
    keymap_["o"]         = sf::Keyboard::O;
    keymap_["p"]         = sf::Keyboard::P;
    keymap_["["]         = sf::Keyboard::LBracket;
    keymap_["]"]         = sf::Keyboard::RBracket;
    keymap_["enter"]     = sf::Keyboard::Return;
    keymap_["a"]         = sf::Keyboard::A;
    keymap_["s"]         = sf::Keyboard::S;
    keymap_["d"]         = sf::Keyboard::D;
    keymap_["f"]         = sf::Keyboard::F;
    keymap_["g"]         = sf::Keyboard::G;
    keymap_["h"]         = sf::Keyboard::H;
    keymap_["j"]         = sf::Keyboard::J;
    keymap_["k"]         = sf::Keyboard::K;
    keymap_["l"]         = sf::Keyboard::L;
    keymap_[";"]         = sf::Keyboard::SemiColon;
    keymap_["'"]         = sf::Keyboard::Quote;
    keymap_["backslash"] = sf::Keyboard::BackSlash;
    keymap_["L_shift"]   = sf::Keyboard::LShift;
    keymap_["z"]         = sf::Keyboard::Z;
    keymap_["x"]         = sf::Keyboard::X;
    keymap_["c"]         = sf::Keyboard::C;
    keymap_["v"]         = sf::Keyboard::V;
    keymap_["b"]         = sf::Keyboard::B;
    keymap_["n"]         = sf::Keyboard::N;
    keymap_["m"]         = sf::Keyboard::M;
    keymap_[","]         = sf::Keyboard::Comma;
    keymap_["."]         = sf::Keyboard::Period;
    keymap_["/"]         = sf::Keyboard::Slash;
    keymap_["R_shift"]   = sf::Keyboard::RShift;
    keymap_["L_ctrl"]    = sf::Keyboard::LControl;
    keymap_["L_alt"]     = sf::Keyboard::LAlt;
    keymap_["spacebar"]  = sf::Keyboard::Space;
    keymap_["R_alt"]     = sf::Keyboard::RAlt;
    keymap_["R_ctrl"]    = sf::Keyboard::RControl;
    
    mousemap_["mouse-left"]   = sf::Mouse::Left;
    mousemap_["mouse-right"]  = sf::Mouse::Right;
    mousemap_["mouse-middle"] = sf::Mouse::Middle;
}

void Input::setWindow(sf::Window* window)
{
    window_ = window;
}

void Input::update(const sf::Event& event)
{
    current_events_.push_back(event);
}

void Input::clear()
{
    current_events_.clear();
}

bool Input::pressed(const std::string& name)
{
    if(keymap_.count(name))
        return sf::Keyboard::isKeyPressed(keymap_[name]);
    if(mousemap_.count(name))
        return sf::Mouse::isButtonPressed(mousemap_[name]);
    return false;
}

bool Input::justPressed(const std::string& name)
{   
    if (keymap_.count(name))
        for(auto each = current_events_.cbegin(), end = current_events_.cend(); each != end; ++each)
            if(each->type == sf::Event::KeyPressed && keymap_[name] == each->key.code)
                return true;
    if (mousemap_.count(name))
        for(auto each = current_events_.cbegin(), end = current_events_.cend(); each != end; ++each)
            if(each->type == sf::Event::MouseButtonPressed && mousemap_[name] == each->mouseButton.button)
                return true;
    return false;
}

bool Input::justReleased(const std::string& name)
{
    if (keymap_.count(name))
        for(auto each = current_events_.cbegin(), end = current_events_.cend(); each != end; ++each) 
            if (each->type == sf::Event::KeyReleased && keymap_[name] == each->key.code)
                return true;
    if (mousemap_.count(name))
        for(auto each = current_events_.cbegin(), end = current_events_.cend(); each != end; ++each)
            if(each->type == sf::Event::MouseButtonReleased && mousemap_[name] == each->mouseButton.button)
                return true;
    return false;
}

sf::Vector2i Input::mouseCoord() const
{
    return sf::Mouse::getPosition();
}

sf::Vector2i Input::mouseWindowCoord() const
{
    return sf::Mouse::getPosition(*window_);
}
