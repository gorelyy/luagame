#pragma once
class Entity;
class EntityGroupRegistry;
#include <vector>
#include <deque>
#include <unordered_map>
#include <algorithm>
#include <luabind/luabind.hpp>
#include "globals.h"

class Group
{
public:
    Group();
    enum BUILTIN{
        RENDER_TO_WINDOW,
        BUILD
    };
    size_t size()               const;
    unsigned char priority()    const;
    bool has(ID id);
    bool has(Entity* entity);
    bool hasInRoot(ID id);
    bool hasInRoot(Entity* entity);
    void    add(ID id);
    void    add(Entity* entity);
    void    add(ID id, size_t index);
    void    add(Entity*entity, size_t index);
    void    add(Group* group);
    void    add(Group* group, size_t index);
    void    remove(ID id);
    void    remove(Group* group);
    void    remove(Entity* entity);
    void    clear();
    void    sort();
    void    deepSort();
    void    setPriority(unsigned char priority);
    void    setPriority(ID id, unsigned char priority);
    void    setPriority(Entity* entity, unsigned char priority);
    void    bindEntityGroupRegistry(EntityGroupRegistry* egr);
    void    bindEntityContainer(std::unordered_map<ID,Entity>* ec);
    void    each(const luabind::object& f);
    template <typename Functor>
    void each(Functor& f)
    {
		for (size_t i = 0; i != size_; ++i){
			if (group_list_[i])
				group_list_[i]->each(f);
			else
				f(id_list_[i]);
		}
    }
private:
    size_t size_;
    unsigned char priority_;
    bool need_to_sort_;
    EntityGroupRegistry* entitygroupregistry_;
    std::vector<ID> id_list_;
    std::vector<Group*> group_list_;
    std::unordered_map<ID,unsigned char> priority_container_;
    std::unordered_map<ID,Entity>* entity_container_;
    std::deque<luabind::object> luafunction_stack_;
};
