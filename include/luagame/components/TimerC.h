#pragma once
#include "luagame/Component.h"
#include <deque>

class TimerC: public Component{
public:
    TimerC();
    float remains()    const;
    float elapsed()    const;
    int loopNumber()   const;
    void    set(float goal);
    void    set(float goal, int loop);
    void    set(float goal, int loop, const luabind::object& callback);
    void    update(float dt);
private:
    float elapsed_;
    float goal_;
    int loop_;
    int loopsnum_;
    std::deque<luabind::object> callback_;
};
