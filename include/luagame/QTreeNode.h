#pragma once
class QTree;
#include "luagame/components/BoxC.h"
#include <vector>
#include <iostream>
#include "globals.h"

class QTreeNode{
public:
    QTreeNode();
    const std::vector<BoxC*>& boxesA()      const;
    const std::vector<BoxC*>& boxesB()      const;
    const std::array<QTreeNode*, 4>& kids() const;
    uint level()                            const;
    uint index()                            const;
    void setChild(uint index, QTreeNode* ptr);
    void addBoxA(BoxC* box);
    void addBoxB(BoxC* box);
    void removeBoxA(BoxC* box);
    void removeBoxB(BoxC *box);
    void set(const sf::Vector2f& coord,
             const sf::Vector2f& size,
             uint level,
             uint index);
private:
    uint level_,
         index_;
    bool boxA_inside_;
    sf::Vector2f coord_;
    sf::Vector2f size_;
    std::array<QTreeNode*,4> kids_;
    std::vector<BoxC*> boxesA_,
                       boxesB_;
};
