 
#include "luagame/components/MoveC.h"

MoveC::MoveC() :
    maxvelocity_(sf::Vector2f(std::numeric_limits<float>::max(), std::numeric_limits<float>::max())),
    minvelocity_(sf::Vector2f(- std::numeric_limits<float>::max(),- std::numeric_limits<float>::max())),
    maxscalevelocity_(sf::Vector2f(std::numeric_limits<float>::max(), std::numeric_limits<float>::max())),
    minscalevelocity_(sf::Vector2f(- std::numeric_limits<float>::max(), - std::numeric_limits<float>::max())),
    maxdirectionalvelocity_(std::numeric_limits<float>::max()),
    mindirectionalvelocity_(- std::numeric_limits<float>::max()),
    maxrotationvelocity_(std::numeric_limits<float>::max()),
    minrotationvelocity_(- std::numeric_limits<float>::max()),
    rotationvelocity_(0.f),
    rotationacceleration_(0.f),
    directionalvelocity_(0.f),
    directionalacceleration_(0.f),
    angle_(0.f),
    maxangularvelocity_(std::numeric_limits<float>::max()),
    minangularvelocity_(- std::numeric_limits<float>::max()),
    angularvelocity_(0.f),
    angularacceleration_(0.f),
    circularvelocity_(0.f),
    circularacceleration_(0.f)
{
}

const sf::Vector2f& MoveC::velocity() const
{
    return velocity_;
}

const sf::Vector2f& MoveC::maxVelocity() const
{
    return maxvelocity_;
}

const sf::Vector2f& MoveC::minVelocity() const
{
    return minvelocity_;
}

const sf::Vector2f& MoveC::acceleration() const
{
    return acceleration_;
}


void MoveC::setVelocityX(float vx)
{
    if(!vx)
        velocity_.x = 0;
    else if(vx > maxvelocity_.x)
        velocity_.x = maxvelocity_.x;
    else if (vx < minvelocity_.x)
        velocity_.x = minvelocity_.x;
    else
        velocity_.x = vx;
}

void MoveC::setVelocityY(float vy)
{
    if(!vy)
        velocity_.y = 0;
    else if(vy > maxvelocity_.y)
        velocity_.y = maxvelocity_.y;
    else if (vy < minvelocity_.y)
        velocity_.y = minvelocity_.y;
    else
        velocity_.y = vy;
}

void MoveC::setVelocity(const sf::Vector2f& v)
{
    setVelocityX(v.x);
    setVelocityY(v.y);
}

void MoveC::setAccelerationX(float ax)
{
    acceleration_.x = ax;
}

void MoveC::setAccelerationY(float ay)
{
    acceleration_.y = ay;
}

void MoveC::setAcceleration(const sf::Vector2f &acceleration)
{
    acceleration_ = acceleration;
}

void MoveC::setMaxVelocity(const sf::Vector2f &v)
{
    maxvelocity_ = v;
}

void MoveC::setMinVelocity(const sf::Vector2f &v)
{
    minvelocity_ = v;
}

void MoveC::capVelocity(const sf::Vector2f &v)
{
    maxvelocity_ = v;
    minvelocity_ = -v;
}

float MoveC::rotationVelocity() const
{
    return rotationvelocity_;
}

float MoveC::rotationAcceleration() const
{
    return rotationacceleration_;
}

float MoveC::maxRotationVelocity() const
{
    return maxrotationvelocity_;
}

float MoveC::minRotationVelocity() const
{
    return minrotationvelocity_;
}

float MoveC::angle() const
{
    return angle_;
}

void MoveC::setRotationVelocity(float v)
{
    if(!v)
        rotationvelocity_ = 0.f;
    else if(v > maxrotationvelocity_)
        rotationvelocity_ = maxrotationvelocity_;
    else if (v < minrotationvelocity_)
        rotationvelocity_ = minrotationvelocity_;
    else
        rotationvelocity_ = v;
}

void MoveC::setRotationAcceleration(float a)
{
    rotationacceleration_ = a;
}

void MoveC::setMaxRotationVelocity(float v)
{
    maxrotationvelocity_ = v;
}

void MoveC::setMinRotationVelocity(float v)
{
    minrotationvelocity_ = v;
}

void MoveC::capRotationVelocity(float vr)
{
    maxrotationvelocity_ = vr;
    minrotationvelocity_ = -vr;
}

void MoveC::setAngle(float angle)
{
    angle_ = (float)(fmod(angle, 360));
    if (angle_ < 0)
        angle_ += 360.f;
}

float MoveC::directionalVelocity() const
{
    return directionalvelocity_;
}

float MoveC::directionalAcceleration() const
{
    return directionalacceleration_;
}

float MoveC::maxDirectionalVelocity() const
{
    return maxdirectionalvelocity_;
}

float MoveC::minDirectionalVelocity() const
{
    return mindirectionalvelocity_;
}

void MoveC::setDirectionalVelocity(float v)
{
    if(!v)
        directionalvelocity_ = 0.f;
    else if(v > maxdirectionalvelocity_)
        directionalvelocity_ = maxdirectionalvelocity_;
    else if (v < mindirectionalvelocity_)
        directionalvelocity_ = mindirectionalvelocity_;
    else
        directionalvelocity_ = v;
}

void MoveC::setDirectionalAcceleration(float a)
{
    directionalacceleration_ = a;
}

void MoveC::setMaxDirectionalVelocity(float v)
{
    maxdirectionalvelocity_ = v;
}

void MoveC::setMinDirectionalVelocity(float v)
{
    mindirectionalvelocity_ = v;
}

void MoveC::capDirectionalVelocity(float v)
{
    maxdirectionalvelocity_ = v;
    mindirectionalvelocity_ = - v;
}



float MoveC::angularVelocity() const
{
    return angularvelocity_;
}

float MoveC::angularAcceleration() const
{
    return angularacceleration_;
}

float MoveC::maxAngularVelocity() const
{
    return maxangularvelocity_;
}

float MoveC::minAngularVelocity() const
{
    return minangularvelocity_;
}

void MoveC::setAngularVelocity(float v)
{
    if(!v)
        angularvelocity_ = 0.f;
    else if(v > maxangularvelocity_)
        angularvelocity_ = maxangularvelocity_;
    else if (v < minangularvelocity_)
        angularvelocity_ = minangularvelocity_;
    else
        angularvelocity_ = v;
}

void MoveC::setAngularAcceleration(float a)
{
    angularacceleration_ = a;
}

void MoveC::setMaxAngularVelocity(float v)
{
    maxangularvelocity_ = v;
}

void MoveC::setMinAngularVelocity(float v)
{
    minangularvelocity_ = v;
}

void MoveC::capAngularVelocity(float vr)
{
    maxangularvelocity_ = vr;
    minangularvelocity_ = -vr;
}



const sf::Vector2f& MoveC::scaleVelocity() const
{
    return scalevelocity_;
}

const sf::Vector2f& MoveC::scaleAcceleration() const
{
    return scaleacceleration_;
}

const sf::Vector2f& MoveC::maxScaleVelocity() const
{
    return maxscalevelocity_;
}

const sf::Vector2f& MoveC::minScaleVelocity() const
{
    return minscalevelocity_;
}

void MoveC::setScaleVelocityX(float vx)
{
    if(!vx)
        scalevelocity_.x = 0;
    else if(vx > maxscalevelocity_.x)
        scalevelocity_.x = maxscalevelocity_.x;
    else if (vx < minscalevelocity_.x)
        scalevelocity_.x = minscalevelocity_.x;
    else
        scalevelocity_.x = vx;

}

void MoveC::setScaleVelocityY(float vy)
{
    if(!vy)
        scalevelocity_.y = 0;
    else if(vy > maxscalevelocity_.y)
        scalevelocity_.y = maxscalevelocity_.y;
    else if (vy < minscalevelocity_.y)
        scalevelocity_.y = minscalevelocity_.y;
    else
        scalevelocity_.y = vy;
}

void MoveC::setScaleVelocity(const sf::Vector2f& v)
{
    setScaleVelocityX(v.x);
    setScaleVelocityY(v.y);
}

void MoveC::setScaleAccelerationX(float ax)
{
    scaleacceleration_.x = ax;
}

void MoveC::setScaleAccelerationY(float ay)
{
    scaleacceleration_.y = ay;
}

void MoveC::setScaleAcceleration(const sf::Vector2f& sa)
{
    scaleacceleration_ = sa;
}

void MoveC::setMaxScaleVelocity(const sf::Vector2f &v)
{
    maxscalevelocity_ = v;
}

void MoveC::setMinScaleVelocity(const sf::Vector2f &v)
{
    maxscalevelocity_ = v;
}

void MoveC::capScaleVelocity(const sf::Vector2f &v)
{
    maxscalevelocity_ = v;
    minscalevelocity_ = -v;
}


float MoveC::circularVelocity() const
{
    return circularvelocity_;
}

float MoveC::circularAcceleration() const
{
    return circularacceleration_;
}

const sf::Vector2f& MoveC::motionCenter() const
{
    return motion_center_;
}

void MoveC::setCircularVelocity(float vr)
{
    circularvelocity_ = vr;
}

void MoveC::setCircularAcceleration(float ar)
{
    circularacceleration_ = ar;
}

void MoveC::setMotionCenter(const sf::Vector2f &coord)
{
    motion_center_ = coord;
}
