#include "luagame/components/PointC.h"

PointC::PointC() :
    angle_(0.f),
    prevangle_(0.f),
    scale_(sf::Vector2f (1,1)),
    prevscale_(sf::Vector2f (1,1))
{
}

const sf::Vector2f& PointC::coord() const
{
    return coord_;
}

const sf::Vector2f& PointC::prevCoord() const
{    
    return prevcoord_;
}

float PointC::angle() const
{
    return angle_;
}

float PointC::prevAngle() const
{
    return prevangle_;
}

float PointC::rotation() const
{
    float result = angle_ - prevangle_;
    int sign = (result > 0) - (result < 0);
    float absresult = fabs(result);
    if (absresult > 180.f)
        return absresult * (float)sign - 360.f * (float)sign;
    return result;
}

const sf::Vector2f& PointC::scale() const
{
    return scale_;
}

const sf::Vector2f& PointC::prevScale() const
{
    return prevscale_;
}


void PointC::setCoord(float x, float y)
{
    coord_.x = x;
    coord_.y = y;
}

void PointC::setCoord(const sf::Vector2f& coord)
{
    coord_ = coord;
}

void PointC::setPrev(float x, float y)
{
    prevcoord_.x = x;
    prevcoord_.y = y;
}

void PointC::setPrev(const sf::Vector2f& coord)
{
    prevcoord_ = coord;
}

void PointC::savePrevCoord()
{
    prevcoord_ = coord_;
}

void PointC::set(float x , float y)
{
    setCoord(x,y);
    savePrevCoord();
}

void PointC::set(const sf::Vector2f& coord)
{
    setCoord(coord);
    savePrevCoord();
}

void PointC::setX(float x)
{
    coord_.x = x;
    prevcoord_.x = x;
}

void PointC::setY(float y)
{
    coord_.y = y;
    prevcoord_.y = y;
}

float PointC::x() const
{
    return coord_.x;
}

float PointC::y() const
{
    return coord_.y;
}

void PointC::setAngle(float angle)
{
    angle_ = (float)(fmod(angle, 360));
    if (angle_ < 0)
        angle_ += 360.f;
}

void PointC::setPrevAngle(float angle)
{
    prevangle_ = (float)(fmod(angle, 360));
    if (prevangle_ < 0)
        prevangle_ += 360.f;
}

void PointC::savePrevAngle()
{
    prevangle_ = angle_;
}

void PointC::resetAngle(float angle)
{
    angle_ = angle;
    prevangle_ = angle_;
}

void PointC::setScale(const sf::Vector2f &scale)
{
    scale_ = scale;
}

void PointC::setPrevScale(const sf::Vector2f &scale)
{
    prevscale_ = scale;
}

void PointC::savePrevScale()
{
    prevscale_ = scale_;
}

void PointC::resetScale(sf::Vector2f &scale)
{
    scale_ = scale;
    prevscale_ = scale_;
}
