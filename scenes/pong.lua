local this ={}

local pad1move, pad2move, ballmove, ballpoint, ballbody, lunchtimer
FIELD = _v2(640,480)
PAD = _v2(16,96)
BALL = _v2(8,8)
PADSPEED = 500
BALLSPEED = 200
BALLSPEEDSTEP = 5
WINSCORE = 11

local scoreleft = 0 
local scoreright = 0
local balldir = _v2(1,1)
local ballspeed = BALLSPEED

function this.create()  
  _scene:addEntity("bounds", function(e)
    e:add(_c.point):setCoord(_v2(-PAD.x,-16))
    e:add(_c.body):setSize(_v2(FIELD.x + 2 * PAD.x,16))
    e:add(_c.point):setCoord(_v2(-PAD.x,FIELD.y))
    e:add(_c.body):setSize(_v2(FIELD.x + 2 * PAD.x,16))
  end)
  
  _scene:addEntity("middleline", function (e)
    local i
    local chunksize = _v2(4,8)
    e:add(_c.layer)
    for i = 1,28,1 do
      e:add(_c.point):setCoord(FIELD.x / 2 - chunksize.x / 2,  (i-1) * (chunksize.y + 10))
      e:add(_c.quad):setSize(chunksize)
    end
  end)

  _scene:addEntity("pad1", function (e)
    e:add(_c.layer)
    e:add(_c.point):setCoord(_v2(0,285))
    e:add(_c.body):setSize(PAD)
    e:add(_c.quad):setSize(PAD)
    e:add(_c.motion)
  end)
  pad1move = _e.pad1:get(_c.motion)
  
  _scene:addEntity("pad2", function (e)
    e:add(_c.layer)
    e:add(_c.point):setCoord(_v2(FIELD.x - PAD.x,(FIELD.y - PAD.y) / 2))
    e:add(_c.body):setSize(PAD)
    e:add(_c.quad):setSize(PAD)
    e:add(_c.motion)
  end)
  pad2move = _e.pad2:get(_c.motion)
  
  _scene:addEntity("ball", function (e)
    e:add(_c.layer)
    e:add(_c.point):setCoord(_v2((FIELD.x - BALL.x)/2,(FIELD.y - BALL.y)/2))
    e:add(_c.quad):setSize(BALL)
    e:add(_c.body):setSize(BALL) 
    e:add(_c.motion)
  end)
  ballbody = _e.ball:get(_c.body)
  ballmove = _e.ball:get(_c.motion)
  ballpoint = _e.ball:get(_c.point,1)
  
  _scene:addGroup(_c.body, "bounds")
  _group.body.bounds:add(_e.bounds)
  
  _scene:addGroup(_c.body, "pads")
  _group.body.pads:add(_e.pad1)
  _group.body.pads:add(_e.pad2)
  
  _scene:addGroup(_c.body, "ball")
  _group.body.ball:add(_e.ball)
  _scene:addGroup(_c.body,"vs_ball")
  _group.body.vs_ball:addGroup(_group.body.bounds)
  _group.body.vs_ball:addGroup(_group.body.pads)
  
  _scene:addEntity("collisions", function (e)
    e:add(_c.collision_check):setCollisionGroups("pads", "bounds") 
    e:add(_c.collision_check):setCollisionGroups("ball", "vs_ball") 
  end)
  
  _scene:addText("scoreLeft",  "0", 2, "fonts/square.ttf", 75)
  _e.scoreLeft:get(_c.point):setCoord(_v2(160,100))
  _scene:addText("scoreRight", "0", 2, "fonts/square.ttf", 75)
  _e.scoreRight:get(_c.point):setCoord(_v2(440,100))
  
  _scene:addEntity("timers", function (e) 
    e:add(_c.timer)
  end)
  launchtimer = _e.timers:get(_c.timer)
  
  this.respawnBall(math.random() + 1)
end

function this.update()
  pad1move:setVelocity(_v2())
  pad2move:setVelocity(_v2())
  if _input:pressed("up") then pad1move:setVelocityY(-PADSPEED) end
  if _input:pressed("down") then pad1move:setVelocityY(PADSPEED) end
  if _input:pressed("w") then pad2move:setVelocityY(-PADSPEED) end
  if _input:pressed("s") then pad2move:setVelocityY(PADSPEED) end
  
  if ballbody:isTouching(_SIDE.RIGHT) or ballbody:isTouching(_SIDE.LEFT) then
    ballspeed = ballspeed + BALLSPEEDSTEP
    balldir = _v2(balldir.x * -1, balldir.y)
    ballmove:setVelocity(_v2(ballspeed * balldir.x, ballspeed * balldir.y))
  end
  
  if ballbody:isTouching(_SIDE.UP) or ballbody:isTouching(_SIDE.DOWN) then
    ballspeed = ballspeed + BALLSPEEDSTEP
    balldir = _v2(balldir.x, balldir.y * -1)
    ballmove:setVelocity(_v2(ballspeed * balldir.x, ballspeed * balldir.y))
  end
  
  if ballpoint:coord().x < -BALL.x then 
    scoreright = scoreright + 1
    _e.scoreRight:get(_c.text):setText(tostring(scoreright))
    this.respawnBall(math.random() + 1)
  end
  
  if ballpoint:coord().x > FIELD.x + BALL.x then 
    scoreleft = scoreleft + 1
    _e.scoreLeft:get(_c.text):setText(tostring(scoreleft))
    this.respawnBall(-1 - math.random())
  end
end

function this.respawnBall(direction)
  ballspeed = BALLSPEED
  balldir = _v2(direction, 1)
  ballpoint:setCoord(_v2((FIELD.x - BALL.x)/2,(FIELD.y - BALL.y)/2 + math.random(FIELD.y / 2)))
  ballmove:setVelocity(_v2())
  launchtimer:set(1.5, 1, function() 
    this.checkWinner()
    ballmove:setVelocity(_v2(ballspeed * balldir.x, ballspeed * balldir.y))
  end)
end

function this.checkWinner()
  if scoreleft == WINSCORE or scoreright == WINSCORE then
    scoreleft = 0
    scoreright = 0
    _e.scoreRight:get(_c.text):setText("0")
    _e.scoreLeft:get(_c.text):setText("0")
  end
end

return this