#include "luagame/Audio.h"
#include <SFML/Audio/Listener.hpp>

Audio::Audio():
    x_(0.f),
    y_(0.f),
    z_(0.f)
{
}

void Audio::setVolume(float volume)
{
    sf::Listener::setGlobalVolume(volume);
}

void Audio::setCoord(const sf::Vector2f& coord)
{
    x_ = coord.x;
    y_ = coord.y;
    sf::Listener::setPosition(x_,y_,z_);
}

void Audio::setDirection(const sf::Vector3f &direction)
{
    sf::Listener::setDirection(direction);
}

void Audio::bindFocus(Entity *e)
{
    focus_ = e;
}

float Audio::volume()
{
    return sf::Listener::getGlobalVolume();
}

sf::Vector2f Audio::coord()
{
    return sf::Vector2f(x_,y_);
}

sf::Vector3f Audio::direction()
{
    return sf::Listener::getDirection();
}

Entity* Audio::focus()
{
    return focus_;
}
