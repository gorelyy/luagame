 
#include "luagame/FColor.h"

FColor::FColor():r(255), g(255), b(255), a(255) {}

FColor::FColor(float red, float green, float blue, float alpha) :r(red), g(green), b(blue), a(alpha) {}

FColor FColor::operator+(const FColor& right)
{
    return FColor(r + right.r,
                  g + right.g,
                  b + right.b,
                  a + right.a);
}


FColor FColor::operator-(const FColor& right)
{
    return FColor(r - right.r,
                  g - right.g,
                  b - right.b,
                  a - right.a);
}

FColor FColor::operator*(float right)
{
    return FColor(r * right,
                  g * right,
                  b * right,
                  a * right);
}
