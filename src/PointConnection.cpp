#include "luagame/PointConnection.h"
#include "luagame/Group.h"

PointConnection::PointConnection() : need_to_sort_(false)
{
}

void PointConnection::connectFollowing(componentID leader, componentID follower,const FollowData& followdata)
{
    if (leader_.count(follower) && leader_level_[follower])
        return;
    if (!leader_.count(leader)){
        leader_update_order_.push_back(leader);
        leader_level_[leader] = 0;
        leader_level_root_.push_back(leader);
    }
    if (!leader_level_[leader]){
        unsigned short level = 0;
        for (componentID rootid : leader_level_root_){
            level = findFollowingLevel(rootid,leader,0);
            if (level) {
                auto it = std::find(leader_level_root_.begin(),
                                    leader_level_root_.end(),leader);
                if (it != leader_level_root_.end()){
                    std::swap(leader_level_root_.back(), *it);
                    leader_level_root_.pop_back();
                }
                break;
            }
        }
        leader_level_[leader] = level;
    }

    auto& followers = leader_[leader];
    if (std::find(followers.begin(), followers.end(), follower) == followers.end()){
        leader_[leader].push_back(follower);
        follow_data_[follower] = followdata;
    }
    need_to_sort_ = true;
    setFollowingLevel(follower, leader_level_[leader]);

}

void PointConnection::connectFollowing(componentID leader, Group *group, const FollowData& followdata)
{
    group->each([&](ID follower){
        connectFollowing(leader, componentID(follower, 0), followdata);
    });
}

void PointConnection::connectParenting(componentID parent, componentID child)
{
    //Можно присоединять только к нулевой вершине
    if (parent_.count(child) && parent_level_[child])
        return;

    if (!parent_.count(parent)){
        parent_update_order_.push_back(parent);
        parent_level_[parent] = 0;
        parent_level_root_.push_back(parent);
    }
    if (!parent_level_[parent]){
        unsigned short level = 0;
        for (componentID rootid : parent_level_root_){
            level = findParentingLevel(rootid,parent,0);
            if (level) {
                auto it = std::find(parent_level_root_.begin(),
                                    parent_level_root_.end(),parent);
                if (it != parent_level_root_.end()){
                    std::swap(parent_level_root_.back(), *it);
                    parent_level_root_.pop_back();
                }
                break;
            }
        }
        parent_level_[parent] = level;
    }

    auto& children = parent_[parent];
    if (std::find(children.begin(), children.end(), child) == children.end())
        children.push_back(child);
    need_to_sort_ = true;
    setParentingLevel(child, parent_level_[parent]);
}

void PointConnection::connectParenting(componentID parent, Group *group)
{
    group->each([&](ID child)
    {
        connectParenting(parent,componentID(child, 0));
    });
}

void PointConnection::sort()
{
    if (!need_to_sort_)
        return;
    std::sort(leader_update_order_.begin(), leader_update_order_.end(), [&](componentID left, componentID right)
    {
        return leader_level_[left] < leader_level_[right];
    });

    std::sort(parent_update_order_.begin(), parent_update_order_.end(), [&](componentID left, componentID right)
    {
        return parent_level_[left] < parent_level_[right];
    });
    need_to_sort_ = false;
}

const std::vector<componentID>& PointConnection::children(componentID parent) const
{
    return parent_.at(parent);
}

const std::vector<componentID> &PointConnection::followers(componentID leader) const
{
    return leader_.at(leader);
}

const std::vector<componentID>& PointConnection::parents() const
{
    return parent_update_order_;
}

const std::vector<componentID>& PointConnection::leaders() const
{
    return leader_update_order_;
}

void PointConnection::setParentingLevel(componentID id, unsigned short level)
{
    if (!parent_.count(id))
        return;
    parent_level_[id] = level + 1;
    auto& vector = parent_[id];
    for (auto i : vector)
        setParentingLevel(i, level + 1);
}

void PointConnection::setFollowingLevel(componentID id, unsigned short level)
{
    if (!leader_.count(id))
        return;
    leader_level_[id] = level + 1;
    auto& vector = leader_[id];
    for (auto each : vector)
        setFollowingLevel(each, level + 1);
}

unsigned short PointConnection::findParentingLevel(componentID node, componentID id, unsigned short level) const
{
    unsigned short result = 0;
    if (!parent_.count(node))
        return result;
    auto& vector = parent_.at(node);
    for(componentID each : vector){
        if (each == id){
            result = level + 1;
            return result;
        }
        if (parent_.count(each))
            result = findParentingLevel(each,id,level + 1);
    }
    return result;
}

componentID PointConnection::findParent(componentID root, componentID id) const
{
    componentID result = componentID(0,0);
    auto& vector = parent_.at(root);
    for(auto each : vector){
        if (each == id){
            result = root;
            return result;
        }
        if (parent_.count(each))
            result = findParent(each,id);
    }
    return result;
}

void PointConnection::removeParentingNode(componentID node)
{
    if (!parent_.count(node))
        return;
    auto& vector = parent_[node];
    for (auto each : vector)
        removeParentingNode(each);
    parent_update_order_.erase(std::find(parent_update_order_.begin(),
                                         parent_update_order_.end(),node));
    parent_level_.erase(node);
    parent_.erase(node);
    parent_data_offset_.erase(node);
    parent_data_coord_.erase(node);

}

void PointConnection::removeChild(componentID parent, componentID child)
{
    if (!parent_.count(parent))
        return;

    componentID parentnode = findParent(parent, child);
    if (parentnode == componentID(0,0))
        return;
    removeParentingNode(child);
    auto& vector = parent_[parent];
    auto it = std::find(vector.begin(), vector.end(), child);
    if (it != vector.end())
        vector.erase(it);
}

unsigned short PointConnection::findFollowingLevel(componentID node, componentID id, unsigned short level) const
{
    unsigned short result = 0;
    if (!leader_.count(node))
        return result;
    auto& vector = leader_.at(node);
    for(auto each : vector){
        if (each == id){
            result = level + 1;
            return result;
        }
        if (leader_.count(each))
            result = findFollowingLevel(each,id,level + 1);
    }
    return result;
}

componentID PointConnection::findLeader(componentID root, componentID id) const
{
    componentID result = componentID(0,0);
    auto& vector = leader_.at(root);
    for(auto each : vector){
        if (each == id){
            result = root;
            return result;
        }
        if (leader_.count(each))
            result = findLeader(each,id);
    }
    return result;
}

void PointConnection::removeLeadingNode(componentID node)
{
    follow_data_.erase(node);
    if (!leader_.count(node))
        return;
    auto& vector = leader_[node];
    for (auto each : vector)
        removeLeadingNode(each);
    leader_update_order_.erase(std::find(leader_update_order_.begin(),
                                         leader_update_order_.end(),node));
    leader_level_.erase(node);
    leader_.erase(node);

}

void PointConnection::removeFollower(componentID leader, componentID follower)
{
    if (!leader_.count(leader))
        return;

    componentID leadernode = findLeader(leader, follower);
    if (leadernode == componentID(0,0))
        return;
    removeLeadingNode(follower);
    auto& vector = leader_[leader];
    auto it = std::find(vector.begin(), vector.end(), follower);
    if (it!=vector.end())
        vector.erase(it);
}

FollowData& PointConnection::followData(componentID follower)
{
    return follow_data_[follower];
}

void PointConnection::setParentCoord(componentID parent, const sf::Vector2f &coord, const sf::Vector2f &prevcoord)
{
    parent_data_coord_[parent].current = coord;
    parent_data_coord_[parent].prev = prevcoord;
}

void PointConnection::setParentOffset(componentID parent, const sf::Vector2f &offset)
{
    parent_data_offset_[parent] = offset;
}

const ParentCoord& PointConnection::parentCoord(componentID parent)
{
    return parent_data_coord_[parent];
}

const sf::Vector2f& PointConnection::parentOffset(componentID parent)
{
    return parent_data_offset_[parent];
}

bool PointConnection::parentRegistered(componentID parent) const
{
    return (bool)parent_.count(parent);
}

bool PointConnection::leaderRegistered(componentID leader) const
{
    return (bool)leader_.count(leader);
}
