#pragma once
extern "C"
{
    #include <lua.h>
    #include <lualib.h>
    #include <lauxlib.h>
}
#include <luabind/luabind.hpp>
#include <luabind/operator.hpp>


namespace luainterface
{

void initLuaState(lua_State* state)
{
    luabind::open(state);
    luaL_openlibs(state);
    luabind::module(state)
    [
        luabind::class_<sf::Texture>("sfTexture")
        ,
        luabind::class_<sf::Music>("sfMusic")
        ,
        luabind::class_<sf::SoundBuffer>("sfSoundBuffer")
        ,
        //L_Input
        luabind::class_<Input>("Input")
        .def(luabind::constructor<>())
        .def("pressed", &Input::pressed)
        .def("justPressed", &Input::justPressed)
        .def("justReleased", &Input::justReleased)
        .def("mouseCoord", &Input::mouseCoord)
        .def("mouseWindowCoord", &Input::mouseWindowCoord)
        ,
       //L_Core
       luabind::class_<Core>("Core")
       .def(luabind::constructor<>())
       .def("addScene", &Core::addScene)
       .def("buildScene", &Core::buildScene)
       .def("asyncBuildScene", &Core::asyncBuildScene)
       .def("deleteScene",(void(Core::*)(const std::string&))&Core::deleteScene)
       .def("deleteScene",(void(Core::*)(Scene*))&Core::deleteScene)
       .def("resetScene", (void(Core::*)(std::string))&Core::resetScene)
       .def("resetScene", (void(Core::*)(Scene*))&Core::resetScene)
       .def("scene", &Core::scene)
       .def("selectScene", &Core::selectScene)
       .def("exitNow", &Core::exitNow)
       .def("resetLogger", &Core::resetLogger)
       .def("getTexture", &Core::getTexture)
       .def("getSound", &Core::getSound)
       .def("getSoundStream", &Core::getSoundStream)
       .def("getFont", &Core::getFont)
       .def("asyncLoadTexture", &Core::asyncLoadTexture)
       .def("asyncLoadSound", &Core::asyncLoadSound)
       ,
       //L_Audio
       luabind::class_<Audio>("Audio")
       .def(luabind::constructor<>())
       .def("setVolume", &Audio::setVolume)
       .def("setDirection", &Audio::setDirection)
       .def("volume", &Audio::volume)
       .def("coord", &Audio::coord)
       .def("direction", &Audio::direction)
       .def("focus", &Audio::focus)
       ,

       //L_componentID
       luabind::class_<std::pair<ID ,uint> >("componentID")
       .def(luabind::constructor<ID,uint>())
       .def_readonly("entityID", &std::pair<ID,uint>::first)
       .def_readonly("index", &std::pair<ID,uint>::second)
       ,
       //L_color
       luabind::class_<sf::Color>("_color")
       .def(luabind::constructor<uint,uint,uint,uint>())
       //.def_readonly("RED", &sf::Color::Red)
       //.def_readonly("WHITE",&sf::Color::White)
       ,
       //L_v2
       luabind::class_<sf::Vector2f>("_v2")
       .def(luabind::constructor<float, float>())
       .def(luabind::constructor<>())
       .def_readwrite("x", &sf::Vector2f::x)
       .def_readwrite("y", &sf::Vector2f::y)
       .def(luabind::const_self + sf::Vector2f())
       .def(luabind::const_self - sf::Vector2f())
       ,
       //L_v3
       luabind::class_<sf::Vector3f>("_v3")
       .def(luabind::constructor<float, float, float>())
       .def_readwrite("x", &sf::Vector3f::x)
       .def_readwrite("y", &sf::Vector3f::y)
       .def_readwrite("z", &sf::Vector3f::z)
       .def(luabind::const_self + sf::Vector3f())
       ,
       //L_rect
       luabind::class_<sf::IntRect>("_rect")
       .def(luabind::constructor<int,int,int,int>())
       ,
       //L_Component
       luabind::class_<Component>("Component")
       .def(luabind::constructor<>())
       .def("key", &Component::key)
       .def("id", &Component::id)
       .def("componentIndex", &Component::componentIndex)
       .def("entityID", &Component::entityID)
       .def("setEnabled", &Component::setEnabled)
       .def("isEnabled", &Component::enabled)
       .def("pointIndex", &Component::pointIndex)
       ,
       //L_Drawable
       luabind::class_<Drawable>("Drawable")
       .def("setScale", &Drawable::setOffsetScale)
       .def("setAngle", &Drawable::setOffsetAngle)
       .def("setCoord",&Drawable::setOffsetCoord)
       .def("setPivot", &Drawable::setPivot)
       .def("setColor", &Drawable::setColor)
       .def("setVisible", &Drawable::setVisibility)
       .def("coord", &Drawable::offsetCoord)
       ,
       //L_FollowData
       luabind::class_<FollowData>("FollowData")
       .def(luabind::constructor<>())
       .def_readwrite("distance", &FollowData::distance)
       .def_readwrite("easing",&FollowData::easing)
       .def_readwrite("mode", &FollowData::mode)
       ,
       //L_Group
       luabind::class_<Group>("Group")
       .def(luabind::constructor<>())
       .def("add",(void(Group::*)(ID))&Group::add)
       .def("add",(void(Group::*)(Entity*))&Group::add)
       .def("add",(void(Group::*)(ID, size_t))&Group::add)
       .def("add",(void(Group::*)(Entity*, size_t))&Group::add)
       .def("addGroup",(void(Group::*)(Group*))&Group::add)
       .def("addGroup",(void(Group::*)(Group*, size_t))&Group::add)
       .def("remove",(void(Group::*)(ID))&Group::remove)
       .def("remove",(void(Group::*)(Entity*))&Group::remove)
       .def("removeGroup",(void(Group::*)(Group*))&Group::remove)
       .def("setPriority",(void(Group::*)(unsigned char))&Group::setPriority)
       .def("setPriority",(void(Group::*)(ID,unsigned char))&Group::setPriority)
       .def("setPriority",(void(Group::*)(Entity*,unsigned char))&Group::setPriority)
       .def("sort",&Group::sort)
       .def("deepSort",&Group::deepSort)
       .def("has",(bool(Group::*)(ID))&Group::has)
       .def("has",(bool(Group::*)(Entity*))&Group::has)
       .def("hasInRoot",(bool(Group::*)(ID))&Group::hasInRoot)
       .def("hasInRoot",(bool(Group::*)(Entity*))&Group::hasInRoot)
       .def("each",(void(Group::*)(const luabind::object&))&Group::each)
       ,
       //L_Pointc
       luabind::class_<PointC, Component>("PointC")
       .def(luabind::constructor<>())
       .def("setCoord",(void(PointC::*)(float,float))&PointC::set)
       .def("setCoord",(void(PointC::*)(const sf::Vector2f&))&PointC::set)
       .def("setAngle", &PointC::resetAngle)
       .def("setScale", &PointC::resetScale)
       .def("coord", &PointC::coord)
       .def("prevCoord", &PointC::prevCoord)

       //.property("coord", &PointC::coord, (void(PointC::*)(const sf::Vector2f&))&PointC::set)
       //.property("x", &PointC::x, &PointC::setX)
       //.property("y", &PointC::y, &PointC::setY)
       ,
       //L_QuadC
       luabind::class_<QuadC, luabind::bases<Component,Drawable> >("QuadC")
       .def(luabind::constructor<>())
       .def("setTextureRect",&QuadC::setTextureRect)
       .def("setSize", &QuadC::setSize)
       ,
       //L_LineC
       luabind::class_<LineC, luabind::bases<Component,Drawable> >("LineC")
       .def(luabind::constructor<>())
       .def("setTextureLine",&LineC::setTextureLine)
       .def("setLength", &LineC::setLength)
       ,
       //L_AnimC
       luabind::class_<AnimC, Component>("AnimC")
       .def(luabind::constructor<>())
       .def("setFrameSize", &AnimC::setFrameSize)
       .def("addAnimation", (void(AnimC::*)(const std::string&, const sf::IntRect&, const luabind::object&))&AnimC::addAnimation)
       .def("addAnimation", (void(AnimC::*)(const std::string&, const sf::IntRect&, uint, uint))&AnimC::addAnimation)
       .def("addAnimation", (void(AnimC::*)(const std::string&, const luabind::object&))&AnimC::addAnimation)
       .def("addAnimation", (void(AnimC::*)(const std::string&, uint, uint))&AnimC::addAnimation)
       .def("addCallbackOnFinish", &AnimC::addCallbackOnFinish)
       .def("addCallbackOnFrame", &AnimC::addCallbackOnFrame)
       .def("play", (void(AnimC::*)(const std::string&, double, int, AnimC::REPEAT_MODE))&AnimC::play)
       .def("play", (void(AnimC::*)(const std::string&, double, int))&AnimC::play)
       .def("play", (void(AnimC::*)(const std::string&, double))&AnimC::play)
       .def("playNext", (void(AnimC::*)(uint, const std::string&, double, int, AnimC::REPEAT_MODE))&AnimC::playNext)
       .def("playNext", (void(AnimC::*)(uint, const std::string&, double, int))&AnimC::playNext)
       .def("playNext", (void(AnimC::*)(uint, const std::string&, double))&AnimC::playNext)
       .def("playNext", (void(AnimC::*)(const std::string&, double, int, AnimC::REPEAT_MODE))&AnimC::playNext)
       .def("playNext", (void(AnimC::*)(const std::string&, double, int))&AnimC::playNext)
       .def("playNext", (void(AnimC::*)(const std::string&, double))&AnimC::playNext)
       .def("setTextureRect", &AnimC::setTextureBounds)
       ,
       //L_LayerC
       luabind::class_<LayerC, luabind::bases<Component,Drawable> >("LayerC")
       .def(luabind::constructor<>())
       .def("bindTexture", &LayerC::bindTexture)
       .def("setTilemap", &LayerC::setTilemap)
       .def("setType", &LayerC::setType)
       .def("setTilemapCollision", &LayerC::setTilemapCollision)
       .def("setDynamic", &LayerC::setDynamic)
       ,
       //L_CameraC
       luabind::class_<CameraC, Component>("CameraC")
       .def(luabind::constructor<>())
       .def("queue", &CameraC::queue)
       .def("setWindowOffset", &CameraC::setWindowOffset)
       .def("setSize",&CameraC::setSize)
       .def("setBackgroundColor",&CameraC::setBackgroundColor)
       ,
       //L_MoveC
       luabind::class_<MoveC, Component>("MoveC")
       .def(luabind::constructor<>())
       .def("velocity",&MoveC::velocity)
       .def("acceleration",&MoveC::acceleration)
       .def("maxVelocity", &MoveC::maxVelocity)
       .def("minVelocity", &MoveC::minVelocity)
       .def("setVelocity",  &MoveC::setVelocity)
       .def("setVelocityX", &MoveC::setVelocityX)
       .def("setVelocityY", &MoveC::setVelocityY)
       .def("setMaxVelocity", &MoveC::setMaxVelocity)
       .def("setMinVelocity", &MoveC::setMinVelocity)
       .def("capVelocity", &MoveC::capVelocity)
       .def("setAcceleration",(void(MoveC::*)(float,float))&MoveC::setAcceleration)
       .def("setAcceleration",(void(MoveC::*)(const sf::Vector2f&))&MoveC::setAcceleration)
       .def("setAccelerationX", &MoveC::setAccelerationX)
       .def("setAccelerationY", &MoveC::setAccelerationY)

       .def("rotationVelocity", &MoveC::rotationVelocity)
       .def("rotationAcceleration", &MoveC::rotationAcceleration)
       .def("maxRotationVelocity", &MoveC::maxRotationVelocity)
       .def("minRotationVelocity", &MoveC::minRotationVelocity)
       .def("setRotationVelocity", &MoveC::setRotationVelocity)
       .def("setRotationAcceleration", &MoveC::setRotationAcceleration)
       .def("setMaxRotationVelocity", &MoveC::setMaxRotationVelocity)
       .def("setMinRotationVelocity", &MoveC::setMinRotationVelocity)
       .def("capRotationVelocity", &MoveC::capRotationVelocity)
       .def("angle", &MoveC::angle)
       .def("setAngle", &MoveC::setAngle)
       .def("directionalVelocity", &MoveC::directionalVelocity)
       .def("directionalAcceleration", &MoveC::directionalAcceleration)
       .def("setDirectionalVelocity", &MoveC::setDirectionalVelocity)
       .def("setDirectionalAcceleration", &MoveC::setDirectionalAcceleration)

       .def("angularVelocity", &MoveC::angularVelocity)
       .def("angularAcceleration", &MoveC::angularAcceleration)
       .def("maxAngularVelocity", &MoveC::maxAngularVelocity)
       .def("minAngularVelocity", &MoveC::minAngularVelocity)
       .def("setAngularVelocity", &MoveC::setAngularVelocity)
       .def("setAngularAcceleration", &MoveC::setAngularAcceleration)
       .def("setMaxAngularVelocity", &MoveC::setMaxAngularVelocity)
       .def("setMinAngularVelocity", &MoveC::setMinAngularVelocity)
       .def("capAngularVelocity", &MoveC::capAngularVelocity)

       .def("setMotionCenter", &MoveC::setMotionCenter)
       .def("setCircularVelocity", &MoveC::setCircularVelocity)
       .def("setCircularAcceleration", &MoveC::setCircularAcceleration)

       .def("scaleVelocity", &MoveC::scaleVelocity)
       .def("scaleAcceleration", &MoveC::scaleAcceleration)
       .def("setScaleVelocity", &MoveC::setScaleVelocity)
       .def("setScaleVelocityX", &MoveC::setScaleVelocityX)
       .def("setScaleVelocityY", &MoveC::setScaleVelocityY)
       .def("setScaleAcceleration", &MoveC::setScaleAcceleration)
       .def("setMaxScaleVelocity", &MoveC::setMaxScaleVelocity)
       .def("setMinScaleVelocity", &MoveC::setMinScaleVelocity)
       .def("capScaleVelocity", &MoveC::capScaleVelocity)
       ,
       //L_SoundC
       luabind::class_<SoundC,Component>("SoundC")
       .def(luabind::constructor<>())
       .def("bindSound",&SoundC::bindSound)
       .def("bindStream",&SoundC::bindStream)
       .def("play",&SoundC::play)
       .def("pause",&SoundC::pause)
       .def("stop",&SoundC::stop)
       .def("setLoop",&SoundC::setLoop)
       .def("setPlayingOffset",&SoundC::setPlayingOffset)
       .def("setPitch",&SoundC::setPitch)
       .def("setVolume",&SoundC::setVolume)
       .def("setRelativeToListener",&SoundC::setRelativeToListener)
       .def("setMinDistance",&SoundC::setMinDistance)
       .def("setAttenuation",&SoundC::setAttenuation)
       .def("setDepth", &SoundC::setDepth)
       ,
       //L_TextC
       luabind::class_<TextC,Component>("TextC")
       .def(luabind::constructor<>())
       .def("setText", &TextC::setText)
       .def("setFontSize", &TextC::setFontSize)
       .def("setColor", &TextC::setColor)
       .def("setDynamic", &TextC::setDynamic)
       .def("typing", &TextC::type)
       .def("setTypingCallback",&TextC::setTypingCallback)
       .def("hide", &TextC::hide)
       ,
       //L_BoxC
       luabind::class_<BoxC, Component>("BoxC")
       .def(luabind::constructor<>())
       .def("setSize", &BoxC::setSize)
       .def("eachBoxB", &BoxC::eachBoxB)
       .def("isTouching",(bool(BoxC::*)() const)&BoxC::isTouching)
       .def("isTouching",(bool(BoxC::*)(Group*) const)&BoxC::isTouching)
       .def("isTouching",(bool(BoxC::*)(BoxC*) const)&BoxC::isTouching)
       .def("isTouching",(bool(BoxC::*)(Component::SIDE) const)&BoxC::isTouching)
       .def("isTouching",(bool(BoxC::*)(Component::SIDE, Group*) const)&BoxC::isTouching)
       .def("isTouching",(bool(BoxC::*)(Component::SIDE, BoxC*))&BoxC::isTouching)
       .def("startTouching",(bool(BoxC::*)() const)&BoxC::startTouching)
       .def("startTouching",(bool(BoxC::*)(Group*) const)&BoxC::startTouching)
       .def("startTouching",(bool(BoxC::*)(BoxC*) const)&BoxC::startTouching)
       .def("startTouching",(bool(BoxC::*)(Component::SIDE) const)&BoxC::startTouching)
       .def("startTouching",(bool(BoxC::*)(Component::SIDE, Group*) const)&BoxC::startTouching)
       .def("startTouching",(bool(BoxC::*)(Component::SIDE, BoxC*))&BoxC::startTouching)
       .def("stopTouching",(bool(BoxC::*)() const)&BoxC::stopTouching)
       .def("stopTouching",(bool(BoxC::*)(Group*) const)&BoxC::stopTouching)
       .def("stopTouching",(bool(BoxC::*)(BoxC*) const)&BoxC::stopTouching)
       .def("stopTouching",(bool(BoxC::*)(Component::SIDE) const)&BoxC::stopTouching)
       .def("stopTouching",(bool(BoxC::*)(Component::SIDE, Group*) const)&BoxC::stopTouching)
       .def("stopTouching",(bool(BoxC::*)(Component::SIDE, BoxC*))&BoxC::stopTouching)
       .def("setSolidSide",(void(BoxC::*)(Component::SIDE))&BoxC::setSolidSide)
       .def("setSolidSide",(void(BoxC::*)(const luabind::object&))&BoxC::setSolidSide)
       ,
       //L_CollisionCheckC
       luabind::class_<CollisionCheckC, Component>("CollisionCheckC")
       .def(luabind::constructor<>())
       .def("eachBoxA",&CollisionCheckC::eachBoxA)
       .def("setTilemapGroup", &CollisionCheckC::setTilemapGroup)
       .def("setCollisionGroups",&CollisionCheckC::setCollisionGroups)
       .def("setCollisionResponse",&CollisionCheckC::setCollisionResponse)
       .def("setCollisionArea",&CollisionCheckC::setCollisionArea)
       .def("setTreeDepth",&CollisionCheckC::setTreeDepth)
       .def("setGridCellSize", &CollisionCheckC::setGridCellSize)
       .def("setBroadPhase", &CollisionCheckC::setBroadPhaseType)
       .def("setDrawDebug", &CollisionCheckC::setDrawDebugMode)
       .def("setDebugColor",&CollisionCheckC::setDebugColor)
       .def("setAABBDebugColor", &CollisionCheckC::setAABBDebugColor)
       .def("setBroadPhaseDebugColor", &CollisionCheckC::setBroadPhaseDebugColor)
       ,
       //L_ScriptC
       luabind::class_<ScriptC, Component>("ScriptC")
       .def(luabind::constructor<>())
       .def("loadScript", (void(ScriptC::*)(const std::string&))&ScriptC::load)
       .def("loadScript", (void(ScriptC::*)(const std::string&, const luabind::object&))&ScriptC::load)
       .def("build", &ScriptC::create)
       .def("get", &ScriptC::get)
       .def("call",(void(ScriptC::*)(const std::string&))&ScriptC::call)
       .def("call",(void(ScriptC::*)(const std::string&, const luabind::object&))&ScriptC::call)
       ,
       //L_TweenCoordC
       luabind::class_<TweenCoordC, Component>("TweenCoordC")
       .def(luabind::constructor<>())
       .def("set", (void(TweenCoordC::*)(const sf::Vector2f&, float))&TweenCoordC::set)
       .def("set", (void(TweenCoordC::*)(const sf::Vector2f&, float, Ease::FUNCTION, Ease::TYPE))&TweenCoordC::set)
       .def("set", (void(TweenCoordC::*)(const sf::Vector2f&, float, Ease::FUNCTION, Ease::TYPE, int, unsigned char))&TweenCoordC::set)
       .def("onFinish", (void(TweenCoordC::*)(const luabind::object&))&TweenCoordC::addCallbackOnFinish)
       .def("onRepeat", (void(TweenCoordC::*)(const luabind::object&))&TweenCoordC::addCallbackOnRepeat)
       .def("addCustomEasing", (void(TweenCoordC::*)(const luabind::object&))&TweenCoordC::addCustomEasing)
       ,
       //L_TweenScaleC
       luabind::class_<TweenScaleC, Component>("TweenScaleC")
       .def(luabind::constructor<>())
       .def("set", (void(TweenScaleC::*)(const sf::Vector2f&, float))&TweenScaleC::set)
       .def("set", (void(TweenScaleC::*)(const sf::Vector2f&, float, Ease::FUNCTION, Ease::TYPE))&TweenScaleC::set)
       .def("set", (void(TweenScaleC::*)(const sf::Vector2f&, float, Ease::FUNCTION, Ease::TYPE, int, unsigned char))&TweenScaleC::set)
       .def("onFinish", (void(TweenScaleC::*)(const luabind::object&))&TweenScaleC::addCallbackOnFinish)
       .def("onRepeat", (void(TweenScaleC::*)(const luabind::object&))&TweenScaleC::addCallbackOnRepeat)
       .def("addCustomEasing", (void(TweenScaleC::*)(const luabind::object&))&TweenScaleC::addCustomEasing)
       ,
       //L_TweenAngleC
       luabind::class_<TweenAngleC, Component>("TweenAngleC")
       .def(luabind::constructor<>())
       .def("set", (void(TweenAngleC::*)(float, float))&TweenAngleC::set)
       .def("set", (void(TweenAngleC::*)(float, float, Ease::FUNCTION, Ease::TYPE))&TweenAngleC::set)
       .def("set", (void(TweenAngleC::*)(float, float, Ease::FUNCTION, Ease::TYPE, int, unsigned char))&TweenAngleC::set)
       .def("onFinish", (void(TweenAngleC::*)(const luabind::object&))&TweenAngleC::addCallbackOnFinish)
       .def("onRepeat", (void(TweenAngleC::*)(const luabind::object&))&TweenAngleC::addCallbackOnRepeat)
       .def("addCustomEasing", (void(TweenAngleC::*)(const luabind::object&))&TweenAngleC::addCustomEasing)
       ,

       //L_TimerC
       luabind::class_<TimerC, Component>("TimerC")
       .def(luabind::constructor<>())
       .def("set", (void(TimerC::*)(float))&TimerC::set)
       .def("set", (void(TimerC::*)(float, int))&TimerC::set)
       .def("set", (void(TimerC::*)(float, int, const luabind::object&))&TimerC::set)
       .def("remains", &TimerC::remains)
       .def("elapsed", &TimerC::elapsed)
       .def("loopNumber", &TimerC::loopNumber)

       ,
       //L_Scene
       luabind::class_<Scene>("Scene")
       .def(luabind::constructor<>())
       .def("getEntity", &Scene::getEntity)
       .def("addEntity", (Entity*(Scene::*)(const std::string&, const luabind::object&))&Scene::addEntity)
       .def("addEntity", (Entity*(Scene::*)(const std::string&,const sf::Vector2f&, const luabind::object&))&Scene::addEntity)
       .def("deleteEntity", &Scene::deleteEntity)
       .def("addPool", &Scene::addPool)
       .def("eachPointC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachC<PointC>)
       .def("eachQuadC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachC<QuadC>)
       .def("eachLineC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachC<LineC>)
       .def("eachAnimC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachC<AnimC>)
       .def("eachLayerC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachC<LayerC>)
       .def("eachCameraC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachC<CameraC>)
       .def("eachMoveC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachC<MoveC>)
       .def("eachBoxC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachC<BoxC>)
       .def("eachCollisionCheckC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachC<CollisionCheckC>)
       .def("eachScriptC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachC<ScriptC>)
       .def("eachTweenCoordC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachC<TweenCoordC>)
       .def("eachTweenScaleC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachC<TweenScaleC>)
       .def("eachTweenAngleC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachC<TweenAngleC>)
       .def("eachSoundC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachC<SoundC>)
       .def("eachTextC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachC<TextC>)
       .def("eachTimerC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachC<TimerC>)
       .def("eachDisabledPointC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachDisabledC<PointC>)
       .def("eachDisabledQuadC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachDisabledC<QuadC>)
       .def("eachDisabledLineC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachDisabledC<LineC>)
       .def("eachDisabledAnimC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachDisabledC<AnimC>)
       .def("eachDisabledLayerC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachDisabledC<LayerC>)
       .def("eachDisabledCameraC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachDisabledC<CameraC>)
       .def("eachDisabledMoveC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachDisabledC<MoveC>)
       .def("eachDisabledBoxC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachDisabledC<BoxC>)
       .def("eachDisabledCollisionCheckC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachDisabledC<CollisionCheckC>)
       .def("eachDisabledScriptC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachDisabledC<ScriptC>)
       .def("eachDisabledTweenCoordC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachDisabledC<TweenCoordC>)
       .def("eachDisabledTweenScaleC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachDisabledC<TweenScaleC>)
       .def("eachDisabledTweenAngleC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachDisabledC<TweenAngleC>)
       .def("eachDisabledSoundC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachDisabledC<SoundC>)
       .def("eachDisabledTextC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachDisabledC<TextC>)
       .def("eachDisabledTimerC",(void(Scene::*)(const std::string&, const luabind::object&))&Scene::eachDisabledC<TimerC>)
       .def("addGroupPointC",(Group*(Scene::*)(const std::string&, const std::string&))&Scene::addGroup<PointC>)
       .def("addGroupQuadC",(Group*(Scene::*)(const std::string&, const std::string&))&Scene::addGroup<QuadC>)
       .def("addGroupLineC",(Group*(Scene::*)(const std::string&, const std::string&))&Scene::addGroup<LineC>)
       .def("addGroupAnimC",(Group*(Scene::*)(const std::string&, const std::string&))&Scene::addGroup<AnimC>)
       .def("addGroupLayerC",(Group*(Scene::*)(const std::string&, const std::string&))&Scene::addGroup<LayerC>)
       .def("addGroupCameraC",(Group*(Scene::*)(const std::string&, const std::string&))&Scene::addGroup<CameraC>)
       .def("addGroupMoveC",(Group*(Scene::*)(const std::string&, const std::string&))&Scene::addGroup<MoveC>)
       .def("addGroupBoxC",(Group*(Scene::*)(const std::string&, const std::string&))&Scene::addGroup<BoxC>)
       .def("addGroupCollisionCheckC",(Group*(Scene::*)(const std::string&, const std::string&))&Scene::addGroup<CollisionCheckC>)
       .def("addGroupScriptC",(Group*(Scene::*)(const std::string&, const std::string&))&Scene::addGroup<ScriptC>)
       .def("addGroupTweenCoordC",(Group*(Scene::*)(const std::string&, const std::string&))&Scene::addGroup<TweenCoordC>)
       .def("addGroupTweenScaleC",(Group*(Scene::*)(const std::string&, const std::string&))&Scene::addGroup<TweenScaleC>)
       .def("addGroupTweenAngleC",(Group*(Scene::*)(const std::string&, const std::string&))&Scene::addGroup<TweenAngleC>)
       .def("addGroupSoundC",(Group*(Scene::*)(const std::string&, const std::string&))&Scene::addGroup<SoundC>)
       .def("addGroupTextC",(Group*(Scene::*)(const std::string&, const std::string&))&Scene::addGroup<TextC>)
       .def("addGroupTimerC",(Group*(Scene::*)(const std::string&, const std::string&))&Scene::addGroup<TimerC>)
       .def("addCamera", (void(Scene::*)(const std::string&, CameraC::MODE, const sf::Vector2f&, const sf::Vector2f&,
                                         const sf::IntRect&, const sf::Color&))&Scene::addCamera)
       .def("addCamera", (void(Scene::*)(const std::string&, CameraC::MODE, const sf::Vector2f&, const sf::Vector2f&,
                                         const sf::IntRect&))&Scene::addCamera)
       .def("addCamera", (void(Scene::*)(const std::string&, CameraC::MODE, const sf::Vector2f&, const sf::Vector2f&))&Scene::addCamera)
       .def("addCamera", (void(Scene::*)(const std::string&, CameraC::MODE, const sf::Vector2f&))&Scene::addCamera)

       .def("addText", (void(Scene::*)(const std::string&, const std::string&,int, const std::string&,int,
                                       const sf::Color&, TextC::TEXTTWEEN, const sf::IntRect&, const sf::Color&))&Scene::addText)
       .def("addText", (void(Scene::*)(const std::string&, const std::string&,int, const std::string&,int,
                                       const sf::Color&, TextC::TEXTTWEEN, const sf::IntRect&))&Scene::addText)
       .def("addText", (void(Scene::*)(const std::string&, const std::string&,int, const std::string&,int,
                                       const sf::Color&, TextC::TEXTTWEEN))&Scene::addText)
       .def("addText", (void(Scene::*)(const std::string&, const std::string&,int, const std::string&,int,
                                       const sf::Color&))&Scene::addText)
       .def("addText", (void(Scene::*)(const std::string&, const std::string&,int, const std::string&,int))&Scene::addText)


       .def("addText", (void(Scene::*)(const std::string&, const std::string&, const std::string&,int,
                                       const sf::Color&, TextC::TEXTTWEEN, const sf::IntRect&, const sf::Color&))&Scene::addText)
       .def("addText", (void(Scene::*)(const std::string&, const std::string&, const std::string&,int,
                                       const sf::Color&, TextC::TEXTTWEEN, const sf::IntRect&))&Scene::addText)
       .def("addText", (void(Scene::*)(const std::string&, const std::string&, const std::string&,int,
                                       const sf::Color&, TextC::TEXTTWEEN))&Scene::addText)
       .def("addText", (void(Scene::*)(const std::string&, const std::string&, const std::string&,int,
                                       const sf::Color&))&Scene::addText)
       .def("addText", (void(Scene::*)(const std::string&, const std::string&, const std::string&,int))&Scene::addText)


       .def("addText", (void(Scene::*)(const std::string&, int, const std::string&, int,const sf::Color&,
                                       TextC::TEXTTWEEN, const sf::IntRect&, const sf::Color&))&Scene::addText)
       .def("addText", (void(Scene::*)(const std::string&, int, const std::string&, int,const sf::Color&,
                                       TextC::TEXTTWEEN, const sf::IntRect&))&Scene::addText)
       .def("addText", (void(Scene::*)(const std::string&, int, const std::string&, int,const sf::Color&,
                                       TextC::TEXTTWEEN))&Scene::addText)
       .def("addText", (void(Scene::*)(const std::string&, int, const std::string&, int,const sf::Color&))&Scene::addText)
       .def("addText", (void(Scene::*)(const std::string&, int, const std::string&, int))&Scene::addText)
       .def("updateTextureSize",&Scene::updateTextureSize)
       ,
       //L_Pool
       luabind::class_<Pool>("Pool")
       .def("id", &Pool::id)
       .def("layer", &Pool::layer)
       .def("eachPointC",(void(Pool::*)(const luabind::object&))&Pool::each<PointC>)
       .def("eachQuadC",(void(Pool::*)(const luabind::object&))&Pool::each<QuadC>)
       .def("eachLineC",(void(Pool::*)(const luabind::object&))&Pool::each<LineC>)
       .def("eachAnimC",(void(Pool::*)(const luabind::object&))&Pool::each<AnimC>)
       .def("eachLayerC",(void(Pool::*)(const luabind::object&))&Pool::each<LayerC>)
       .def("eachCameraC",(void(Pool::*)(const luabind::object&))&Pool::each<CameraC>)
       .def("eachMoveC",(void(Pool::*)(const luabind::object&))&Pool::each<MoveC>)
       .def("eachBoxC",(void(Pool::*)(const luabind::object&))&Pool::each<BoxC>)
       .def("eachCollisionCheckC",(void(Pool::*)(const luabind::object&))&Pool::each<CollisionCheckC>)
       .def("eachScriptC",(void(Pool::*)(const luabind::object&))&Pool::each<ScriptC>)
       .def("eachTweenCoordC",(void(Pool::*)(const luabind::object&))&Pool::each<TweenCoordC>)
       .def("eachTweenScaleC",(void(Pool::*)(const luabind::object&))&Pool::each<TweenScaleC>)
       .def("eachTweenAngleC",(void(Pool::*)(const luabind::object&))&Pool::each<TweenAngleC>)
       .def("eachSoundC",(void(Pool::*)(const luabind::object&))&Pool::each<SoundC>)
       .def("eachTextC",(void(Pool::*)(const luabind::object&))&Pool::each<TextC>)
       .def("eachTimerC",(void(Pool::*)(const luabind::object&))&Pool::each<TimerC>)
       .def("eachDisabledPointC",(void(Pool::*)(const luabind::object&))&Pool::eachDisabled<PointC>)
       .def("eachDisabledQuadC",(void(Pool::*)(const luabind::object&))&Pool::eachDisabled<QuadC>)
       .def("eachDisabledLineC",(void(Pool::*)(const luabind::object&))&Pool::eachDisabled<LineC>)
       .def("eachDisabledAnimC",(void(Pool::*)(const luabind::object&))&Pool::eachDisabled<AnimC>)
       .def("eachDisabledLayerC",(void(Pool::*)(const luabind::object&))&Pool::eachDisabled<LayerC>)
       .def("eachDisabledCameraC",(void(Pool::*)(const luabind::object&))&Pool::eachDisabled<CameraC>)
       .def("eachDisabledMoveC",(void(Pool::*)(const luabind::object&))&Pool::eachDisabled<MoveC>)
       .def("eachDisabledBoxC",(void(Pool::*)(const luabind::object&))&Pool::eachDisabled<BoxC>)
       .def("eachDisabledCollisionCheckC",(void(Pool::*)(const luabind::object&))&Pool::eachDisabled<CollisionCheckC>)
       .def("eachDisabledScriptC",(void(Pool::*)(const luabind::object&))&Pool::eachDisabled<ScriptC>)
       .def("eachDisabledTweenCoordC",(void(Pool::*)(const luabind::object&))&Pool::eachDisabled<TweenCoordC>)
       .def("eachDisabledTweenScaleC",(void(Pool::*)(const luabind::object&))&Pool::eachDisabled<TweenScaleC>)
       .def("eachDisabledTweenAngleC",(void(Pool::*)(const luabind::object&))&Pool::eachDisabled<TweenAngleC>)
       .def("eachDisabledSoundC",(void(Pool::*)(const luabind::object&))&Pool::eachDisabled<SoundC>)
       .def("eachDisabledTextC",(void(Pool::*)(const luabind::object&))&Pool::eachDisabled<TextC>)
       .def("eachDisabledTimerC",(void(Pool::*)(const luabind::object&))&Pool::eachDisabled<TimerC>)
       .def("capacityPointC",(size_t(Pool::*)())&Pool::capacity<PointC>)
       .def("capacityQuadC",(size_t(Pool::*)())&Pool::capacity<QuadC>)
       .def("capacityLineC",(size_t(Pool::*)())&Pool::capacity<LineC>)
       .def("capacityAnimC",(size_t(Pool::*)())&Pool::capacity<AnimC>)
       .def("capacityLayerC",(size_t(Pool::*)())&Pool::capacity<LayerC>)
       .def("capacityCameraC",(size_t(Pool::*)())&Pool::capacity<CameraC>)
       .def("capacityMoveC",(size_t(Pool::*)())&Pool::capacity<MoveC>)
       .def("capacityBoxC",(size_t(Pool::*)())&Pool::capacity<BoxC>)
       .def("capacityCollisionCheckC",(size_t(Pool::*)())&Pool::capacity<CollisionCheckC>)
       .def("capacityScriptC",(size_t(Pool::*)())&Pool::capacity<ScriptC>)
       .def("capacityTweenCoordC",(size_t(Pool::*)())&Pool::capacity<TweenCoordC>)
       .def("capacityTweenScaleC",(size_t(Pool::*)())&Pool::capacity<TweenScaleC>)
       .def("capacityTweenAngleC",(size_t(Pool::*)())&Pool::capacity<TweenAngleC>)
       .def("capacitySoundC",(size_t(Pool::*)())&Pool::capacity<SoundC>)
       .def("capacityTextC",(size_t(Pool::*)())&Pool::capacity<TextC>)
       .def("capacityTimerC",(size_t(Pool::*)())&Pool::capacity<TimerC>)
       .def("setCapacityPointC",(void(Pool::*)(size_t))&Pool::setCapacity<PointC>)
       .def("setCapacityQuadC",(void(Pool::*)(size_t))&Pool::setCapacity<QuadC>)
       .def("setCapacityLineC",(void(Pool::*)(size_t))&Pool::setCapacity<LineC>)
       .def("setCapacityAnimC",(void(Pool::*)(size_t))&Pool::setCapacity<AnimC>)
       .def("setCapacityLayerC",(void(Pool::*)(size_t))&Pool::setCapacity<LayerC>)
       .def("setCapacityCameraC",(void(Pool::*)(size_t))&Pool::setCapacity<CameraC>)
       .def("setCapacityMoveC",(void(Pool::*)(size_t))&Pool::setCapacity<MoveC>)
       .def("setCapacityBoxC",(void(Pool::*)(size_t))&Pool::setCapacity<BoxC>)
       .def("setCapacityCollisionCheckC",(void(Pool::*)(size_t))&Pool::setCapacity<CollisionCheckC>)
       .def("setCapacityScriptC",(void(Pool::*)(size_t))&Pool::setCapacity<ScriptC>)
       .def("setCapacityTweenCoordC",(void(Pool::*)(size_t))&Pool::setCapacity<TweenCoordC>)
       .def("setCapacityTweenScaleC",(void(Pool::*)(size_t))&Pool::setCapacity<TweenScaleC>)
       .def("setCapacityTweenAngleC",(void(Pool::*)(size_t))&Pool::setCapacity<TweenAngleC>)
       .def("setCapacitySoundC",(void(Pool::*)(size_t))&Pool::setCapacity<SoundC>)
       .def("setCapacityTextC",(void(Pool::*)(size_t))&Pool::setCapacity<TextC>)
       .def("setCapacityTimerC",(void(Pool::*)(size_t))&Pool::setCapacity<TimerC>)
       ,

       //L_Entity
       luabind::class_<Entity>("Entity")
       .def(luabind::constructor<>())
       .def("addPointC", (PointC*(Entity::*)())&Entity::addComponent<PointC>)
       .def("addPointC", (PointC*(Entity::*)(Pool*))&Entity::addComponent<PointC>)
       .def("getPointC", (PointC*(Entity::*)(uint))&Entity::getComponent<PointC>)
       .def("getPointC", (PointC*(Entity::*)())&Entity::getComponent<PointC>)
       .def("getByPointIndexPointC", (PointC*(Entity::*)(uint))&Entity::getComponentByPointIndex<PointC>)

       .def("addQuadC", (QuadC*(Entity::*)())&Entity::addComponent<QuadC>)
       .def("addQuadC", (QuadC*(Entity::*)(Pool*))&Entity::addComponent<QuadC>)
       .def("getQuadC", (QuadC*(Entity::*)(uint))&Entity::getComponent<QuadC>)
       .def("getQuadC", (QuadC*(Entity::*)())&Entity::getComponent<QuadC>)
       .def("getByPointIndexQuadC", (QuadC*(Entity::*)(uint))&Entity::getComponentByPointIndex<QuadC>)

       .def("addLineC", (LineC*(Entity::*)())&Entity::addComponent<LineC>)
       .def("addLineC", (LineC*(Entity::*)(Pool*))&Entity::addComponent<LineC>)
       .def("getLineC", (LineC*(Entity::*)(uint))&Entity::getComponent<LineC>)
       .def("getLineC", (LineC*(Entity::*)())&Entity::getComponent<LineC>)
       .def("getByPointIndexLineC", (LineC*(Entity::*)(uint))&Entity::getComponentByPointIndex<LineC>)

       .def("addAnimC", (AnimC*(Entity::*)())&Entity::addComponent<AnimC>)
       .def("addAnimC", (AnimC*(Entity::*)(Pool*))&Entity::addComponent<AnimC>)
       .def("getAnimC", (AnimC*(Entity::*)(uint))&Entity::getComponent<AnimC>)
       .def("getAnimC", (AnimC*(Entity::*)())&Entity::getComponent<AnimC>)
       .def("getByPointIndexAnimC", (AnimC*(Entity::*)(uint))&Entity::getComponentByPointIndex<AnimC>)

       .def("addLayerC", (LayerC*(Entity::*)())&Entity::addComponent<LayerC>)
       .def("addLayerC", (LayerC*(Entity::*)(Pool*))&Entity::addComponent<LayerC>)
       .def("getLayerC", (LayerC*(Entity::*)(uint))&Entity::getComponent<LayerC>)
       .def("getLayerC", (LayerC*(Entity::*)())&Entity::getComponent<LayerC>)
       .def("getByPointIndexLayerC", (LayerC*(Entity::*)(uint))&Entity::getComponentByPointIndex<LayerC>)

       .def("addCameraC", (CameraC*(Entity::*)())&Entity::addComponent<CameraC>)
       .def("addCameraC", (CameraC*(Entity::*)(Pool*))&Entity::addComponent<CameraC>)
       .def("getCameraC", (CameraC*(Entity::*)(uint))&Entity::getComponent<CameraC>)
       .def("getCameraC", (CameraC*(Entity::*)())&Entity::getComponent<CameraC>)
       .def("getByPointIndexCameraC", (CameraC*(Entity::*)(uint))&Entity::getComponentByPointIndex<CameraC>)

       .def("addMoveC", (MoveC*(Entity::*)())&Entity::addComponent<MoveC>)
       .def("addMoveC", (MoveC*(Entity::*)(Pool*))&Entity::addComponent<MoveC>)
       .def("getMoveC", (MoveC*(Entity::*)())&Entity::getComponent<MoveC>)
       .def("getMoveC", (MoveC*(Entity::*)(uint))&Entity::getComponent<MoveC>)
       .def("getByPointIndexMoveC", (MoveC*(Entity::*)(uint))&Entity::getComponentByPointIndex<MoveC>)

       .def("addBoxC", (BoxC*(Entity::*)())&Entity::addComponent<BoxC>)
       .def("addBoxC", (BoxC*(Entity::*)(Pool*))&Entity::addComponent<BoxC>)
       .def("getBoxC", (BoxC*(Entity::*)(uint))&Entity::getComponent<BoxC>)
       .def("getBoxC", (BoxC*(Entity::*)())&Entity::getComponent<BoxC>)
       .def("getByPointIndexBoxC", (BoxC*(Entity::*)(uint))&Entity::getComponentByPointIndex<BoxC>)

       .def("addCollisionCheckC", (CollisionCheckC*(Entity::*)())&Entity::addComponent<CollisionCheckC>)
       .def("addCollisionCheckC", (CollisionCheckC*(Entity::*)(Pool*))&Entity::addComponent<CollisionCheckC>)
       .def("getCollisionCheckC", (CollisionCheckC*(Entity::*)(uint))&Entity::getComponent<CollisionCheckC>)
       .def("getCollisionCheckC", (CollisionCheckC*(Entity::*)())&Entity::getComponent<CollisionCheckC>)
       .def("getByPointIndexCollisionCheckC", (CollisionCheckC*(Entity::*)(uint))&Entity::getComponentByPointIndex<CollisionCheckC>)

       .def("addScriptC", (ScriptC*(Entity::*)())&Entity::addComponent<ScriptC>)
       .def("addScriptC", (ScriptC*(Entity::*)(Pool*))&Entity::addComponent<ScriptC>)
       .def("getScriptC", (ScriptC*(Entity::*)(uint))&Entity::getComponent<ScriptC>)
       .def("getScriptC", (ScriptC*(Entity::*)())&Entity::getComponent<ScriptC>)
       .def("getByPointIndexScriptC", (ScriptC*(Entity::*)(uint))&Entity::getComponentByPointIndex<ScriptC>)

       .def("addTweenCoordC", (TweenCoordC*(Entity::*)())&Entity::addComponent<TweenCoordC>)
       .def("addTweenCoordC", (TweenCoordC*(Entity::*)(Pool*))&Entity::addComponent<TweenCoordC>)
       .def("getTweenCoordC", (TweenCoordC*(Entity::*)(uint))&Entity::getComponent<TweenCoordC>)
       .def("getTweenCoordC", (TweenCoordC*(Entity::*)())&Entity::getComponent<TweenCoordC>)
       .def("getByPointIndexTweenCoordC", (TweenCoordC*(Entity::*)(uint))&Entity::getComponentByPointIndex<TweenCoordC>)

       .def("addTweenScaleC", (TweenScaleC*(Entity::*)())&Entity::addComponent<TweenScaleC>)
       .def("addTweenScaleC", (TweenScaleC*(Entity::*)(Pool*))&Entity::addComponent<TweenScaleC>)
       .def("getTweenScaleC", (TweenScaleC*(Entity::*)(uint))&Entity::getComponent<TweenScaleC>)
       .def("getTweenScaleC", (TweenScaleC*(Entity::*)())&Entity::getComponent<TweenScaleC>)
       .def("getByPointIndexTweenScaleC", (TweenScaleC*(Entity::*)(uint))&Entity::getComponentByPointIndex<TweenScaleC>)

       .def("addTweenAngleC", (TweenAngleC*(Entity::*)())&Entity::addComponent<TweenAngleC>)
       .def("addTweenAngleC", (TweenAngleC*(Entity::*)(Pool*))&Entity::addComponent<TweenAngleC>)
       .def("getTweenAngleC", (TweenAngleC*(Entity::*)(uint))&Entity::getComponent<TweenAngleC>)
       .def("getTweenAngleC", (TweenAngleC*(Entity::*)())&Entity::getComponent<TweenAngleC>)
       .def("getByPointIndexTweenAngleC", (TweenAngleC*(Entity::*)(uint))&Entity::getComponentByPointIndex<TweenAngleC>)

       .def("addSoundC", (SoundC*(Entity::*)())&Entity::addComponent<SoundC>)
       .def("addSoundC", (SoundC*(Entity::*)(Pool*))&Entity::addComponent<SoundC>)
       .def("getSoundC", (SoundC*(Entity::*)(uint))&Entity::getComponent<SoundC>)
       .def("getSoundC", (SoundC*(Entity::*)())&Entity::getComponent<SoundC>)
       .def("getByPointIndexSoundC", (SoundC*(Entity::*)(uint))&Entity::getComponentByPointIndex<SoundC>)

       .def("addTextC", (TextC*(Entity::*)())&Entity::addComponent<TextC>)
       .def("addTextC", (TextC*(Entity::*)(Pool*))&Entity::addComponent<TextC>)
       .def("getTextC", (TextC*(Entity::*)(uint))&Entity::getComponent<TextC>)
       .def("getTextC", (TextC*(Entity::*)())&Entity::getComponent<TextC>)
       .def("getByPointIndexTextC", (TextC*(Entity::*)(uint))&Entity::getComponentByPointIndex<TextC>)

       .def("addTimerC", (TimerC*(Entity::*)())&Entity::addComponent<TimerC>)
       .def("addTimerC", (TimerC*(Entity::*)(Pool*))&Entity::addComponent<TimerC>)
       .def("getTimerC", (TimerC*(Entity::*)(uint))&Entity::getComponent<TimerC>)
       .def("getTimerC", (TimerC*(Entity::*)())&Entity::getComponent<TimerC>)
       .def("getByPointIndexTimerC", (TimerC*(Entity::*)(uint))&Entity::getComponentByPointIndex<TimerC>)

       .def("id", &Entity::id)

       .def("eachPointC",(void(Entity::*)(const luabind::object&))&Entity::each<PointC>)
       .def("eachQuadC",(void(Entity::*)(const luabind::object&))&Entity::each<QuadC>)
       .def("eachLineC",(void(Entity::*)(const luabind::object&))&Entity::each<LineC>)
       .def("eachAnimC",(void(Entity::*)(const luabind::object&))&Entity::each<AnimC>)
       .def("eachLayerC",(void(Entity::*)(const luabind::object&))&Entity::each<LayerC>)
       .def("eachCameraC",(void(Entity::*)(const luabind::object&))&Entity::each<CameraC>)
       .def("eachMoveC",(void(Entity::*)(const luabind::object&))&Entity::each<MoveC>)
       .def("eachBoxC",(void(Entity::*)(const luabind::object&))&Entity::each<BoxC>)
       .def("eachCollisionCheckC",(void(Entity::*)(const luabind::object&))&Entity::each<CollisionCheckC>)
       .def("eachScriptC",(void(Entity::*)(const luabind::object&))&Entity::each<ScriptC>)
       .def("eachTweenCoordC",(void(Entity::*)(const luabind::object&))&Entity::each<TweenCoordC>)
       .def("eachTweenScaleC",(void(Entity::*)(const luabind::object&))&Entity::each<TweenScaleC>)
       .def("eachTweenAngleC",(void(Entity::*)(const luabind::object&))&Entity::each<TweenAngleC>)
       .def("eachSoundC",(void(Entity::*)(const luabind::object&))&Entity::each<SoundC>)
       .def("eachTextC",(void(Entity::*)(const luabind::object&))&Entity::each<TextC>)
       .def("eachTimerC",(void(Entity::*)(const luabind::object&))&Entity::each<TimerC>)
       .def("eachDisabledPointC",(void(Entity::*)(const luabind::object&))&Entity::eachDisabled<PointC>)
       .def("eachDisabledQuadC",(void(Entity::*)(const luabind::object&))&Entity::eachDisabled<QuadC>)
       .def("eachDisabledLineC",(void(Entity::*)(const luabind::object&))&Entity::eachDisabled<LineC>)
       .def("eachDisabledAnimC",(void(Entity::*)(const luabind::object&))&Entity::eachDisabled<AnimC>)
       .def("eachDisabledLayerC",(void(Entity::*)(const luabind::object&))&Entity::eachDisabled<LayerC>)
       .def("eachDisabledCameraC",(void(Entity::*)(const luabind::object&))&Entity::eachDisabled<CameraC>)
       .def("eachDisabledMoveC",(void(Entity::*)(const luabind::object&))&Entity::eachDisabled<MoveC>)
       .def("eachDisabledBoxC",(void(Entity::*)(const luabind::object&))&Entity::eachDisabled<BoxC>)
       .def("eachDisabledCollisionCheckC",(void(Entity::*)(const luabind::object&))&Entity::eachDisabled<CollisionCheckC>)
       .def("eachDisabledScriptC",(void(Entity::*)(const luabind::object&))&Entity::eachDisabled<ScriptC>)
       .def("eachDisabledTweenCoordC",(void(Entity::*)(const luabind::object&))&Entity::eachDisabled<TweenCoordC>)
       .def("eachDisabledTweenScaleC",(void(Entity::*)(const luabind::object&))&Entity::eachDisabled<TweenScaleC>)
       .def("eachDisabledTweenAngleC",(void(Entity::*)(const luabind::object&))&Entity::eachDisabled<TweenAngleC>)
       .def("eachDisabledSoundC",(void(Entity::*)(const luabind::object&))&Entity::eachDisabled<SoundC>)
       .def("eachDisabledTextC",(void(Entity::*)(const luabind::object&))&Entity::eachDisabled<TextC>)
       .def("eachDisabledTimerC",(void(Entity::*)(const luabind::object&))&Entity::eachDisabled<TimerC>)
       .def("eachByPointIndexPointC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachByPointIndex<PointC>)
       .def("eachByPointIndexQuadC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachByPointIndex<QuadC>)
       .def("eachByPointIndexLineC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachByPointIndex<LineC>)
       .def("eachByPointIndexAnimC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachByPointIndex<AnimC>)
       .def("eachByPointIndexLayerC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachByPointIndex<LayerC>)
       .def("eachByPointIndexCameraC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachByPointIndex<CameraC>)
       .def("eachByPointIndexMoveC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachByPointIndex<MoveC>)
       .def("eachByPointIndexBoxC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachByPointIndex<BoxC>)
       .def("eachByPointIndexCollisionCheckC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachByPointIndex<CollisionCheckC>)
       .def("eachByPointIndexScriptC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachByPointIndex<ScriptC>)
       .def("eachByPointIndexTweenCoordC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachByPointIndex<TweenCoordC>)
       .def("eachByPointIndexTweenScaleC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachByPointIndex<TweenScaleC>)
       .def("eachByPointIndexTweenAngleC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachByPointIndex<TweenAngleC>)
       .def("eachByPointIndexSoundC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachByPointIndex<SoundC>)
       .def("eachByPointIndexTextC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachByPointIndex<TextC>)
       .def("eachByPointIndexTimerC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachByPointIndex<TimerC>)
       .def("eachDisabledByPointIndexPointC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachDisabledByPointIndex<PointC>)
       .def("eachDisabledByPointIndexQuadC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachDisabledByPointIndex<QuadC>)
       .def("eachDisabledByPointIndexLineC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachDisabledByPointIndex<LineC>)
       .def("eachDisabledByPointIndexAnimC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachDisabledByPointIndex<AnimC>)
       .def("eachDisabledByPointIndexLayerC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachDisabledByPointIndex<LayerC>)
       .def("eachDisabledByPointIndexCameraC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachDisabledByPointIndex<CameraC>)
       .def("eachDisabledByPointIndexMoveC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachDisabledByPointIndex<MoveC>)
       .def("eachDisabledByPointIndexBoxC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachDisabledByPointIndex<BoxC>)
       .def("eachDisabledByPointIndexCollisionCheckC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachDisabledByPointIndex<CollisionCheckC>)
       .def("eachDisabledByPointIndexScriptC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachDisabledByPointIndex<ScriptC>)
       .def("eachDisabledByPointIndexTweenCoordC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachDisabledByPointIndex<TweenCoordC>)
       .def("eachDisabledByPointIndexTweenScaleC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachDisabledByPointIndex<TweenScaleC>)
       .def("eachDisabledByPointIndexTweenAngleC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachDisabledByPointIndex<TweenAngleC>)
       .def("eachDisabledByPointIndexSoundC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachDisabledByPointIndex<SoundC>)
       .def("eachDisabledByPointIndexTextC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachDisabledByPointIndex<TextC>)
       .def("eachDisabledByPointIndexTimerC",(void(Entity::*)(size_t, const luabind::object&))&Entity::eachDisabledByPointIndex<TimerC>)

       .def("anyPointC",(PointC*(Entity::*)())&Entity::any<PointC>)
       .def("anyQuadC",(QuadC*(Entity::*)())&Entity::any<QuadC>)
       .def("anyLineC",(LineC*(Entity::*)())&Entity::any<LineC>)
       .def("anyAnimC",(AnimC*(Entity::*)())&Entity::any<AnimC>)
       .def("anyLayerC",(LayerC*(Entity::*)())&Entity::any<LayerC>)
       .def("anyCameraC",(CameraC*(Entity::*)())&Entity::any<CameraC>)
       .def("anyMoveC",(MoveC*(Entity::*)())&Entity::any<MoveC>)
       .def("anyBoxC",(BoxC*(Entity::*)())&Entity::any<BoxC>)
       .def("anyCollisionCheckC",(CollisionCheckC*(Entity::*)())&Entity::any<CollisionCheckC>)
       .def("anyScriptC",(ScriptC*(Entity::*)())&Entity::any<ScriptC>)
       .def("anyTweenCoordC",(TweenCoordC*(Entity::*)())&Entity::any<TweenCoordC>)
       .def("anyTweenScaleC",(TweenScaleC*(Entity::*)())&Entity::any<TweenScaleC>)
       .def("anyTweenAngleC",(TweenAngleC*(Entity::*)())&Entity::any<TweenAngleC>)
       .def("anySoundC",(SoundC*(Entity::*)())&Entity::any<SoundC>)
       .def("anyTextC",(TextC*(Entity::*)())&Entity::any<TextC>)
       .def("anyTimerC",(TimerC*(Entity::*)())&Entity::any<TimerC>)
       .def("anyDisabledPointC",(PointC*(Entity::*)())&Entity::anyDisabled<PointC>)
       .def("anyDisabledQuadC",(QuadC*(Entity::*)())&Entity::anyDisabled<QuadC>)
       .def("anyDisabledLineC",(LineC*(Entity::*)())&Entity::anyDisabled<LineC>)
       .def("anyDisabledAnimC",(AnimC*(Entity::*)())&Entity::anyDisabled<AnimC>)
       .def("anyDisabledLayerC",(LayerC*(Entity::*)())&Entity::anyDisabled<LayerC>)
       .def("anyDisabledCameraC",(CameraC*(Entity::*)())&Entity::anyDisabled<CameraC>)
       .def("anyDisabledMoveC",(MoveC*(Entity::*)())&Entity::anyDisabled<MoveC>)
       .def("anyDisabledBoxC",(BoxC*(Entity::*)())&Entity::anyDisabled<BoxC>)
       .def("anyDisabledCollisionCheckC",(CollisionCheckC*(Entity::*)())&Entity::anyDisabled<CollisionCheckC>)
       .def("anyDisabledScriptC",(ScriptC*(Entity::*)())&Entity::anyDisabled<ScriptC>)
       .def("anyDisabledTweenCoordC",(TweenCoordC*(Entity::*)())&Entity::anyDisabled<TweenCoordC>)
       .def("anyDisabledTweenScaleC",(TweenScaleC*(Entity::*)())&Entity::anyDisabled<TweenScaleC>)
       .def("anyDisabledTweenAngleC",(TweenAngleC*(Entity::*)())&Entity::anyDisabled<TweenAngleC>)
       .def("anyDisabledSoundC",(SoundC*(Entity::*)())&Entity::anyDisabled<SoundC>)
       .def("anyDisabledTextC",(TextC*(Entity::*)())&Entity::anyDisabled<TextC>)
       .def("anyDisabledTimerC",(TimerC*(Entity::*)())&Entity::anyDisabled<TimerC>)
       .def("anyByPointIndexPointC",(PointC*(Entity::*)(size_t))&Entity::anyByPointIndex<PointC>)
       .def("anyByPointIndexQuadC",(QuadC*(Entity::*)(size_t))&Entity::anyByPointIndex<QuadC>)
       .def("anyByPointIndexLineC",(LineC*(Entity::*)(size_t))&Entity::anyByPointIndex<LineC>)
       .def("anyByPointIndexAnimC",(AnimC*(Entity::*)(size_t))&Entity::anyByPointIndex<AnimC>)
       .def("anyByPointIndexLayerC",(LayerC*(Entity::*)(size_t))&Entity::anyByPointIndex<LayerC>)
       .def("anyByPointIndexCameraC",(CameraC*(Entity::*)(size_t))&Entity::anyByPointIndex<CameraC>)
       .def("anyByPointIndexMoveC",(MoveC*(Entity::*)(size_t))&Entity::anyByPointIndex<MoveC>)
       .def("anyByPointIndexBoxC",(BoxC*(Entity::*)(size_t))&Entity::anyByPointIndex<BoxC>)
       .def("anyByPointIndexCollisionCheckC",(CollisionCheckC*(Entity::*)(size_t))&Entity::anyByPointIndex<CollisionCheckC>)
       .def("anyByPointIndexScriptC",(ScriptC*(Entity::*)(size_t))&Entity::anyByPointIndex<ScriptC>)
       .def("anyByPointIndexTweenCoordC",(TweenCoordC*(Entity::*)(size_t))&Entity::anyByPointIndex<TweenCoordC>)
       .def("anyByPointIndexTweenScaleC",(TweenScaleC*(Entity::*)(size_t))&Entity::anyByPointIndex<TweenScaleC>)
       .def("anyByPointIndexTweenAngleC",(TweenAngleC*(Entity::*)(size_t))&Entity::anyByPointIndex<TweenAngleC>)
       .def("anyByPointIndexSoundC",(SoundC*(Entity::*)(size_t))&Entity::anyByPointIndex<SoundC>)
       .def("anyByPointIndexTextC",(TextC*(Entity::*)(size_t))&Entity::anyByPointIndex<TextC>)
       .def("anyByPointIndexTimerC",(TimerC*(Entity::*)(size_t))&Entity::anyByPointIndex<TimerC>)
       .def("anyDisabledByPointIndexPointC",(PointC*(Entity::*)(size_t))&Entity::anyDisabledByPointIndex<PointC>)
       .def("anyDisabledByPointIndexQuadC",(QuadC*(Entity::*)(size_t))&Entity::anyDisabledByPointIndex<QuadC>)
       .def("anyDisabledByPointIndexLineC",(LineC*(Entity::*)(size_t))&Entity::anyDisabledByPointIndex<LineC>)
       .def("anyDisabledByPointIndexAnimC",(AnimC*(Entity::*)(size_t))&Entity::anyDisabledByPointIndex<AnimC>)
       .def("anyDisabledByPointIndexLayerC",(LayerC*(Entity::*)(size_t))&Entity::anyDisabledByPointIndex<LayerC>)
       .def("anyDisabledByPointIndexCameraC",(CameraC*(Entity::*)(size_t))&Entity::anyDisabledByPointIndex<CameraC>)
       .def("anyDisabledByPointIndexMoveC",(MoveC*(Entity::*)(size_t))&Entity::anyDisabledByPointIndex<MoveC>)
       .def("anyDisabledByPointIndexBoxC",(BoxC*(Entity::*)(size_t))&Entity::anyDisabledByPointIndex<BoxC>)
       .def("anyDisabledByPointIndexCollisionCheckC",(CollisionCheckC*(Entity::*)(size_t))&Entity::anyDisabledByPointIndex<CollisionCheckC>)
       .def("anyDisabledByPointIndexScriptC",(ScriptC*(Entity::*)(size_t))&Entity::anyDisabledByPointIndex<ScriptC>)
       .def("anyDisabledByPointIndexTweenCoordC",(TweenCoordC*(Entity::*)(size_t))&Entity::anyDisabledByPointIndex<TweenCoordC>)
       .def("anyDisabledByPointIndexTweenScaleC",(TweenScaleC*(Entity::*)(size_t))&Entity::anyDisabledByPointIndex<TweenScaleC>)
       .def("anyDisabledByPointIndexTweenAngleC",(TweenAngleC*(Entity::*)(size_t))&Entity::anyDisabledByPointIndex<TweenAngleC>)
       .def("anyDisabledByPointIndexSoundC",(SoundC*(Entity::*)(size_t))&Entity::anyDisabledByPointIndex<SoundC>)
       .def("anyDisabledByPointIndexTextC",(TextC*(Entity::*)(size_t))&Entity::anyDisabledByPointIndex<TextC>)
       .def("anyDisabledByPointIndexTimerC",(TimerC*(Entity::*)(size_t))&Entity::anyDisabledByPointIndex<TimerC>)


       .def("countPointC",(uint(Entity::*)())&Entity::count<PointC>)
       .def("countQuadC",(uint(Entity::*)())&Entity::count<QuadC>)
       .def("countLineC",(uint(Entity::*)())&Entity::count<LineC>)
       .def("countAnimC",(uint(Entity::*)())&Entity::count<AnimC>)
       .def("countLayerC",(uint(Entity::*)())&Entity::count<LayerC>)
       .def("countCameraC",(uint(Entity::*)())&Entity::count<CameraC>)
       .def("countMoveC",(uint(Entity::*)())&Entity::count<MoveC>)
       .def("countBoxC",(uint(Entity::*)())&Entity::count<BoxC>)
       .def("countCollisionCheckC",(uint(Entity::*)())&Entity::count<CollisionCheckC>)
       .def("countScriptC",(uint(Entity::*)())&Entity::count<ScriptC>)
       .def("countTweenCoordC",(uint(Entity::*)())&Entity::count<TweenCoordC>)
       .def("countTweenScaleC",(uint(Entity::*)())&Entity::count<TweenScaleC>)
       .def("countTweenAngleC",(uint(Entity::*)())&Entity::count<TweenAngleC>)
       .def("countSoundC",(uint(Entity::*)())&Entity::count<SoundC>)
       .def("countTextC",(uint(Entity::*)())&Entity::count<TextC>)
       .def("countTimerC",(uint(Entity::*)())&Entity::count<TimerC>)

       .def("countExistingPointC",(uint(Entity::*)())&Entity::countExisting<PointC>)
       .def("countExistingQuadC",(uint(Entity::*)())&Entity::countExisting<QuadC>)
       .def("countExistingLineC",(uint(Entity::*)())&Entity::countExisting<LineC>)
       .def("countExistingAnimC",(uint(Entity::*)())&Entity::countExisting<AnimC>)
       .def("countExistingLayerC",(uint(Entity::*)())&Entity::countExisting<LayerC>)
       .def("countExistingCameraC",(uint(Entity::*)())&Entity::countExisting<CameraC>)
       .def("countExistingMoveC",(uint(Entity::*)())&Entity::countExisting<MoveC>)
       .def("countExistingBoxC",(uint(Entity::*)())&Entity::countExisting<BoxC>)
       .def("countExistingCollisionCheckC",(uint(Entity::*)())&Entity::countExisting<CollisionCheckC>)
       .def("countExistingScriptC",(uint(Entity::*)())&Entity::countExisting<ScriptC>)
       .def("countExistingTweenCoordC",(uint(Entity::*)())&Entity::countExisting<TweenCoordC>)
       .def("countExistingTweenScaleC",(uint(Entity::*)())&Entity::countExisting<TweenScaleC>)
       .def("countExistingTweenAngleC",(uint(Entity::*)())&Entity::countExisting<TweenAngleC>)
       .def("countExistingSoundC",(uint(Entity::*)())&Entity::countExisting<SoundC>)
       .def("countExistingTextC",(uint(Entity::*)())&Entity::countExisting<TextC>)
       .def("countExistingTimerC",(uint(Entity::*)())&Entity::countExisting<TimerC>)

       .def("countDisabledPointC",(uint(Entity::*)())&Entity::countDisabled<PointC>)
       .def("countDisabledQuadC",(uint(Entity::*)())&Entity::countDisabled<QuadC>)
       .def("countDisabledLineC",(uint(Entity::*)())&Entity::countDisabled<LineC>)
       .def("countDisabledAnimC",(uint(Entity::*)())&Entity::countDisabled<AnimC>)
       .def("countDisabledLayerC",(uint(Entity::*)())&Entity::countDisabled<LayerC>)
       .def("countDisabledCameraC",(uint(Entity::*)())&Entity::countDisabled<CameraC>)
       .def("countDisabledMoveC",(uint(Entity::*)())&Entity::countDisabled<MoveC>)
       .def("countDisabledBoxC",(uint(Entity::*)())&Entity::countDisabled<BoxC>)
       .def("countDisabledCollisionCheckC",(uint(Entity::*)())&Entity::countDisabled<CollisionCheckC>)
       .def("countDisabledScriptC",(uint(Entity::*)())&Entity::countDisabled<ScriptC>)
       .def("countDisabledTweenCoordC",(uint(Entity::*)())&Entity::countDisabled<TweenCoordC>)
       .def("countDisabledTweenScaleC",(uint(Entity::*)())&Entity::countDisabled<TweenScaleC>)
       .def("countDisabledTweenAngleC",(uint(Entity::*)())&Entity::countDisabled<TweenAngleC>)
       .def("countDisabledSoundC",(uint(Entity::*)())&Entity::countDisabled<SoundC>)
       .def("countDisabledTextC",(uint(Entity::*)())&Entity::countDisabled<TextC>)
       .def("countDisabledTimerC",(uint(Entity::*)())&Entity::countDisabled<TimerC>)

       .def("setParentScript",&Entity::setParentScript)
       .def("parentScript",&Entity::parentScript)

       .def("addFollower",(void(Entity::*)(Entity*,const sf::Vector2f&,FollowData::FollowType)) &Entity::addFollower)
       .def("addFollower",(void(Entity::*)(Entity*,const sf::Vector2f&,FollowData::FollowType, float)) &Entity::addFollower)
       .def("addFollower",(void(Entity::*)(Entity*,const sf::Vector2f&,FollowData::FollowType, const luabind::object&)) &Entity::addFollower)
       .def("addFollower",(void(Entity::*)(Entity*,const sf::Vector2f&,FollowData::FollowType, float,const luabind::object&)) &Entity::addFollower)
       .def("addFollowerGroup",(void(Entity::*)(Group*,const sf::Vector2f&,FollowData::FollowType)) &Entity::addFollower)
       .def("addFollowerGroup",(void(Entity::*)(Group*,const sf::Vector2f&,FollowData::FollowType, float)) &Entity::addFollower)
       .def("addFollowerGroup",(void(Entity::*)(Group*,const sf::Vector2f&,FollowData::FollowType, const luabind::object&)) &Entity::addFollower)
       .def("addFollowerGroup",(void(Entity::*)(Group*,const sf::Vector2f&,FollowData::FollowType, float,const luabind::object&)) &Entity::addFollower)
       .def("setLeader",(void(Entity::*)(Entity*,const sf::Vector2f&,FollowData::FollowType)) &Entity::setLeader)
       .def("setLeader",(void(Entity::*)(Entity*,const sf::Vector2f&,FollowData::FollowType, float)) &Entity::setLeader)
       .def("setLeader",(void(Entity::*)(Entity*,const sf::Vector2f&,FollowData::FollowType, const luabind::object&)) &Entity::setLeader)
       .def("setLeader",(void(Entity::*)(Entity*,const sf::Vector2f&,FollowData::FollowType, float,const luabind::object&)) &Entity::setLeader)
       .def("setLeaderPoint",(void(Entity::*)(PointC*,const sf::Vector2f&,FollowData::FollowType)) &Entity::setLeader)
       .def("setLeaderPoint",(void(Entity::*)(PointC*,const sf::Vector2f&,FollowData::FollowType, float)) &Entity::setLeader)
       .def("setLeaderPoint",(void(Entity::*)(PointC*,const sf::Vector2f&,FollowData::FollowType, const luabind::object&)) &Entity::setLeader)
       .def("setLeaderPoint",(void(Entity::*)(PointC*,const sf::Vector2f&,FollowData::FollowType, float,const luabind::object&)) &Entity::setLeader)
       .def("addChild",(void(Entity::*)(Entity*))&Entity::addChild)
       .def("addChildGroup",(void(Entity::*)(Group*))&Entity::addChild)
       .def("setParent",(void(Entity::*)(Entity*))&Entity::setParent)
       .def("setParentPoint",(void(Entity::*)(PointC*))&Entity::setParent)
       .def("removeChild",(void(Entity::*)(Entity*))&Entity::removeChild)
       .def("removeFollower",(void(Entity::*)(Entity*)) &Entity::removeFollower)

    ];

    //NEW_COMPONENT_TYPE
    luaL_dostring(state,
        "function Entity.add(self,name,...)\n"
        "  return self[\"add\"..name](self,...)\n"
        "end\n"
        "function Entity.get(self,name,...)\n"
        "  return self[\"get\"..name](self,...)\n"
        "end\n"
        "function Entity.getByPointIndex(self,name,...)\n"
        "  return self[\"getByPointIndex\"..name](self,...)\n"
        "end\n"
        "function Entity.each(self,name,...)\n"
        "  self[\"each\"..name](self,...)\n"
        "end\n"
        "function Entity.eachDisabled(self,name,...)\n"
        "  self[\"eachDisabled\"..name](self,...)\n"
        "end\n"
        "function Entity.eachByPointIndex(self,name,...)\n"
        "  self[\"eachByPointIndex\"..name](self,...)\n"
        "end\n"
        "function Entity.eachDisabledByPointIndex(self,name,...)\n"
        "  self[\"eachDisabledByPointIndex\"..name](self,...)\n"
        "end\n"
        "function Entity.any(self,name,...)\n"
        "  return self[\"any\"..name](self,...)\n"
        "end\n"
        "function Entity.anyDisabled(self,name,...)\n"
        "  return self[\"anyDisabled\"..name](self,...)\n"
        "end\n"
        "function Entity.anyByPointIndex(self,name,...)\n"
        "  return self[\"anyByPointIndex\"..name](self,...)\n"
        "end\n"
        "function Entity.anyDisabledByPointIndex(self,name,...)\n"
        "  return self[\"anyDisabledByPointIndex\"..name](self,...)\n"
        "end\n"
        "function Entity.countDisabled(self,name,...)\n"
        "  return self[\"countDisabled\"..name](self,...)\n"
        "end\n"
        "function Entity.count(self,name,...)\n"
        "  return self[\"count\"..name](self,...)\n"
        "end\n"
        "function Entity.countExisting(self,name,...)\n"
        "  return self[\"countExisting\"..name](self,...)\n"
        "end\n"
        "function Pool.each(self,name,...)\n"
        "  self[\"each\"..name](self,...)\n"
        "end\n"
        "function Pool.eachDisabled(self,name,...)\n"
        "  self[\"eachDisabled\"..name](self,...)\n"
        "end\n"
        "function Pool.setCapacity(self,name,...)\n"
        "  self[\"setCapacity\"..name](self,...)\n"
        "end\n"
        "function Pool.capacity(self,name,...)\n"
        "  return self[\"capacity\"..name](self,...)\n"
        "end\n"
        "function Scene.each(self,name,...)\n"
        "  self[\"each\"..name](self,...)\n"
        "end\n"
        "function Scene.eachDisabled(self,name,...)\n"
        "  self[\"eachDisabled\"..name](self,...)\n"
        "end\n"
        "_e = {}\n"
        "_pool = {}\n"
        "_camera = {}\n"
        "_c = {\n"
            "quad = \"QuadC\",\n"
            "line = \"LineC\",\n"
            "body = \"BoxC\",\n"
            "point = \"PointC\",\n"
            "motion = \"MoveC\",\n"
            "script = \"ScriptC\",\n"
            "tweencoord = \"TweenCoordC\",\n"
            "tweenscale = \"TweenScaleC\",\n"
            "tweenangle = \"TweenAngleC\",\n"
            "camera = \"CameraC\",\n"
            "animation = \"AnimC\",\n"
            "collision_check = \"CollisionCheckC\",\n"
            "layer = \"LayerC\",\n"
            "sound = \"SoundC\",\n"
            "text = \"TextC\",\n"
            "timer = \"TimerC\"\n"
        "}\n"
        "_c_invert = {\n"
            "QuadC = \"quad\",\n"
            "LineC = \"line\",\n"
            "BoxC = \"body\",\n"
            "PointC = \"point\",\n"
            "MoveC = \"motion\",\n"
            "ScriptC = \"script\",\n"
            "TweenCoordC = \"tweencoord\",\n"
            "TweenScaleC = \"tweenscale\",\n"
            "TweenAngleC = \"tweenangle\",\n"
            "CameraC = \"camera\",\n"
            "AnimC = \"animation\",\n"
            "CollisionCheckC = \"collision_check\",\n"
            "LayerC = \"layer\",\n"
            "SoundC = \"sound\",\n"
            "TextC = \"text\",\n"
            "TimerC = \"timer\"\n"

        "}\n"
        "_group = {\n"
            "quad = {},\n"
            "line = {},\n"
            "body = {},\n"
            "point = {},\n"
            "motion = {},\n"
            "script = {},\n"
            "tweencoord = {},\n"
            "tweenscale = {},\n"
            "tweenangle = {},\n"
            "camera = {},\n"
            "animation = {},\n"
            "collision_check = {},\n"
            "layer = {},\n"
            "sound = {},\n"
            "text = {},\n"
            "timer = {}\n"
        "}\n"
        "_FOLLOW = {DISTANCE = 0, DEADZONE = 1}\n"
        "_CAMERA = {WINDOW = 0, WINDOWTEXTURE = 1, TEXTURE = 2}\n"
        "_TEXTTWEEN = {NONE = 0, COORD = 1, SCALE = 2, ANGLE = 3, COLOR = 4, ALL = 5}\n"
        "_PLAYMODE = {LOOP = 0, PENDULUM = 1}\n"
        "_EASE = {NONE = 0, LINEAR = 1, QUAD = 2, CUBIC = 3, QUART = 4,"
        " QUINT = 5, SINE = 6, BACK = 7, BOUNCE = 8, CIRC = 9, ELASTIC = 10, EXP = 11,CUSTOM = 12}\n"
        "_EASEMODE = {IN = 0, OUT = 1, IN_OUT = 2}\n"
        "_SIDE = {UP = 0, RIGHT = 1, DOWN = 2, LEFT = 3, ALL = 4, NONE = 5, ANY = 6}\n"
        "_AXIS = {X = 0, Y = 1}\n"
        "_PRIMITIVE = {POINTS = 0, LINES = 1, LINESSTRIP = 2, TRIANGLES = 3,"
        " TRIANGLESSTRIP = 4, TRIANGLESFAN = 5, QUADS = 6}\n"
        "_BROADPHASE = {GRID = 0, STATICGRID = 1, QUADTREE = 2, STATICQUADTREE = 3, NONE = 4}\n"
        "_DEBUG = {AABB = 0, BROADPHASE = 1, ALL = 2, NONE = 3}\n"
        "function Scene.addGroup(self,type,groupname)\n"
        "  return self[\"addGroup\"..type](self,_c_invert[type],groupname)\n"
        "end\n"
        "function LayerC.loadTexture(self,name)\n"
        "  self[\"bindTexture\"](self,_core:getTexture(name))\n"
        "  _scene:updateTextureSize(self:entityID())\n"
        "end\n"
        "function SoundC.load(self,name)\n"
        "  self[\"bindSound\"](self,_core:getSound(name))\n"
        "end\n"
        "function SoundC.loadStream(self,name)\n"
        "  self[\"bindStream\"](self,_core:getSoundStream(name))\n"
        "end\n"
        "function TextC.loadFont(self,name)\n"
        "  self[\"bindFont\"](self,_core:getFont(name))\n"
        "end\n"
    );
}
}
