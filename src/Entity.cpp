 
#include "luagame/Entity.h"

Entity::Entity(): 
    point_index_(-1),
    locked_(false),
    parentscript_(nullptr)
{
}

void Entity::bindLua(lua_State* L)
{
    L_ = L;
}

lua_State * Entity::luaState() const
{
    return L_;
}

void Entity::setID(ID id)
{
    id_ = id;
}

void Entity::setPointIndex(int i)
{
    point_index_ = i;
}

void Entity::bindComponentContainer(ComponentContainer* cc)
{
    component_container_ = cc;
}

int Entity::pointIndex() const
{
    return point_index_;
}

ID Entity::id() const
{
    return id_;
}

void Entity::finalizeAllComponentPools()
{
    //NEW_COMPONENT_TYPE
    finalizeComponentPool<QuadC>();
    finalizeComponentPool<LineC>();
    finalizeComponentPool<MoveC>();
    finalizeComponentPool<ScriptC>();
    finalizeComponentPool<TweenCoordC>();
    finalizeComponentPool<TweenScaleC>();
    finalizeComponentPool<TweenAngleC>();
    finalizeComponentPool<AnimC>();
    finalizeComponentPool<BoxC>();
    finalizeComponentPool<CollisionCheckC>();
    finalizeComponentPool<PointC>();
    finalizeComponentPool<LayerC>();
    finalizeComponentPool<CameraC>();
    finalizeComponentPool<SoundC>();
    finalizeComponentPool<TextC>();
    finalizeComponentPool<TimerC>();
}

void Entity::bindPointConnection(PointConnection *pc)
{
    pointconnection_ = pc;
}

void Entity::addFollower(Entity* follower, const sf::Vector2f &distance, FollowData::FollowType mode)
{
    pointconnection_->connectFollowing(componentID(id_,0),componentID(follower->id(),0), FollowData(distance,mode,1.f,luabind::object()));
}

void Entity::addFollower(Entity* follower, const sf::Vector2f &distance, FollowData::FollowType mode, float easing)
{
    pointconnection_->connectFollowing(componentID(id_,0),componentID(follower->id(),0), FollowData(distance,mode,easing,luabind::object()));
}

void Entity::addFollower(Entity* follower, const sf::Vector2f &distance, FollowData::FollowType mode, const luabind::object& callback)
{
    pointconnection_->connectFollowing(componentID(id_,0),componentID(follower->id(),0), FollowData(distance,mode,1.f,callback));
}

void Entity::addFollower(Entity* follower, const sf::Vector2f &distance, FollowData::FollowType mode, float easing, const luabind::object& callback)
{
    pointconnection_->connectFollowing(componentID(id_,0),componentID(follower->id(),0), FollowData(distance,mode,easing,callback));
}

void Entity::addFollower(Group* group, const sf::Vector2f &distance, FollowData::FollowType mode)
{
    pointconnection_->connectFollowing(componentID(id_,0),group, FollowData(distance,mode,1.f,luabind::object()));
}

void Entity::addFollower(Group* group, const sf::Vector2f &distance, FollowData::FollowType mode, float easing)
{
    pointconnection_->connectFollowing(componentID(id_,0),group, FollowData(distance,mode,easing,luabind::object()));
}

void Entity::addFollower(Group* group, const sf::Vector2f &distance, FollowData::FollowType mode, const luabind::object& callback)
{
    pointconnection_->connectFollowing(componentID(id_,0),group, FollowData(distance,mode,1.f,callback));
}

void Entity::addFollower(Group* group, const sf::Vector2f &distance, FollowData::FollowType mode, float easing, const luabind::object& callback)
{
    pointconnection_->connectFollowing(componentID(id_,0),group, FollowData(distance,mode,easing,callback));
}

void Entity::setLeader(Entity* leader, const sf::Vector2f &distance, FollowData::FollowType mode)
{
    pointconnection_->connectFollowing(componentID(leader->id(),0),componentID(id_,0), FollowData(distance,mode,1.f,luabind::object()));
}

void Entity::setLeader(Entity* leader, const sf::Vector2f &distance, FollowData::FollowType mode, float easing)
{
    pointconnection_->connectFollowing(componentID(leader->id(),0),componentID(id_,0), FollowData(distance,mode,easing,luabind::object()));
}

void Entity::setLeader(Entity* leader, const sf::Vector2f &distance, FollowData::FollowType mode, const luabind::object& callback)
{
    pointconnection_->connectFollowing(componentID(leader->id(),0),componentID(id_,0), FollowData(distance,mode,1.f,callback));
}

void Entity::setLeader(Entity* leader, const sf::Vector2f &distance, FollowData::FollowType mode, float easing, const luabind::object& callback)
{
    pointconnection_->connectFollowing(componentID(leader->id(),0),componentID(id_,0), FollowData(distance,mode,easing,callback));
}

void Entity::setLeader(PointC* leader, const sf::Vector2f &distance, FollowData::FollowType mode)
{
    pointconnection_->connectFollowing(leader->id(),componentID(id_,0), FollowData(distance,mode,1.f,luabind::object()));
}

void Entity::setLeader(PointC* leader, const sf::Vector2f &distance, FollowData::FollowType mode, float easing)
{
    pointconnection_->connectFollowing(leader->id(),componentID(id_,0), FollowData(distance,mode,easing,luabind::object()));
}

void Entity::setLeader(PointC* leader, const sf::Vector2f &distance, FollowData::FollowType mode, const luabind::object& callback)
{
    pointconnection_->connectFollowing(leader->id(),componentID(id_,0), FollowData(distance,mode,1.f,callback));
}

void Entity::setLeader(PointC* leader, const sf::Vector2f &distance, FollowData::FollowType mode, float easing, const luabind::object& callback)
{
    pointconnection_->connectFollowing(leader->id(),componentID(id_,0), FollowData(distance,mode,easing,callback));
}

void Entity::addChild(Entity* child)
{
    pointconnection_->connectParenting(componentID(id_,0),componentID(child->id(),0));
}

void Entity::addChild(Group* group)
{
    pointconnection_->connectParenting(componentID(id_,0),group);
}

void Entity::setParent(Entity* parent)
{
    pointconnection_->connectParenting(componentID(parent->id(),0), componentID(id_,0));
}

void Entity::setParent(PointC* parent)
{
    pointconnection_->connectParenting(parent->id(), componentID(id_,0));
}

void Entity::removeFollower(Entity* follower)
{
    pointconnection_->removeFollower(componentID(id_,0),componentID(follower->id(),0));
}

void Entity::removeChild(Entity* child)
{
    pointconnection_->removeChild(componentID(id_,0),componentID(child->id(),0));
}

bool Entity::isLocked() const
{
    return locked_;
}

void Entity::lock()
{
    locked_ = true;
}

void Entity::unlock()
{
    locked_ = false;
}

void Entity::setParentScript(ScriptC *script)
{
    parentscript_ = script;
}

ScriptC* Entity::parentScript() const
{
    return parentscript_;
}
