#pragma once
#include "Utils.h"
#include <luabind/luabind.hpp>
#include <iostream>
#include <SFML/System/Vector2.hpp>

struct Ease{
    enum FUNCTION {
        NONE,
        LINEAR,
        QUAD,
        CUBIC,
        QUART,
        QUINT,
        SINE,
        BACK,
        BOUNCE,
        CIRC,
        ELASTIC,
        EXPO,
        CUSTOM
    };
    enum TYPE {
        IN,
        OUT,
        IN_OUT
    };
};
template <typename T>
sf::Vector2<T> operator*(const sf::Vector2< T > &left, double right)
{
    return sf::Vector2<T>(left.x*right,left.y*right);
}

template<typename cClass>
class Tweenable
{
public:
    Tweenable():
        current_time_(0.0),
        end_time_(0.0),
        ease_function_(Ease::LINEAR),
        ease_type_(Ease::IN),
        repeat_(0),
        tween_enabled_(false),
        reset_prev_(false),
        repeat_mode_(0)
    {
    }

    cClass nextValue(double time)
    {
        if (tween_enabled_)
            return calculateNext(time);
    }
    cClass value() const
    {
        return current_value_ - prev_value_;
    }
    bool isEnabled() const
    {
        return tween_enabled_;
    }
    void set(const cClass& value, double time, Ease::FUNCTION f, Ease::TYPE type, int repeat, unsigned char mode)
    {
        if (tween_enabled_)
            return;
        current_time_ = 0;
        next_value_ = value;
        end_time_ = time;
        tween_enabled_ = true;
        ease_function_  = f;
        ease_type_ = type;
        current_value_ = cClass();
        prev_value_ = cClass();
        repeat_ = repeat;
        repeat_mode_ = mode;
    }
    void set(const cClass& value, double time, Ease::FUNCTION f, Ease::TYPE type)
    {
        set(value,time,f,type,0,0);
    }
    void set(const cClass& value, double time)
    {
        set(value, time, Ease::LINEAR, Ease::IN, 0, 0);
    }
    void addCallbackOnFinish(const luabind::object& callback)
    {
        callbackonfinish_ = callback;
    }
    void addCallbackOnRepeat(const luabind::object& callback)
    {
        callbackonrepeat_ = callback;
    }
    void addCustomEasing(const luabind::object& f)
    {
        custom_easing_ = f;
    }
    void reset(const cClass& newvalue, double time, Ease::FUNCTION f, Ease::TYPE type)
    {
        set(newvalue,time, f, type);
    }
    void update(double time)
    {
        if(tween_enabled_){
            prev_value_ = current_value_;
            current_value_ = currentValue(time);
        }
    }
    void finish()
    {
        tween_enabled_ = false;
    }
private:
    double current_time_;
    double end_time_;
    unsigned char repeat_;
    unsigned char repeat_mode_; //0-Loop, 1 -Pendulum
    bool tween_enabled_;
    bool reset_prev_;
    Ease::FUNCTION ease_function_;
    Ease::TYPE ease_type_;
    cClass next_value_;
    cClass current_value_;
    cClass prev_value_;
    luabind::object  callbackonfinish_;
    luabind::object  callbackonrepeat_;
    luabind::object  custom_easing_;

    cClass currentValue(double time)
    {
        current_time_ += time;
        //current_time_ = std::min(current_time_,end_time_);
        if (reset_prev_){
            prev_value_ = cClass{};
            reset_prev_ = false;
        }
        if (current_time_ >= end_time_){
            tween_enabled_ = false;
            cClass result = next_value_;
            if (repeat_ > 0){
                current_time_= 0;
                //Loop or Pendulum mode repeat
                next_value_ = next_value_ * (1 - 2 * repeat_mode_);
                reset_prev_ = repeat_mode_;
                //
                tween_enabled_ = true;
                repeat_--;
                luabind::object callback = callbackonrepeat_;
                if (callback.is_valid()){
                    try{
                        callback();
                    }
                    catch(const luabind::error &e){
                        std::cerr<<"luagame_error in tweenable callback: "<<e.what();
                        luabind::object error_msg(luabind::from_stack(e.state(), -1));
                        std::cerr<<error_msg<<std::endl;
                    }
                }
            }
            else{
                luabind::object callback = callbackonfinish_;
                if (callback.is_valid()){
                    try{
                        callback();
                    }
                    catch(const luabind::error &e){
                        std::cerr<<"luagame_error in tweenable callback: "<<e.what();
                        luabind::object error_msg(luabind::from_stack(e.state(), -1));
                        std::cerr<<error_msg<<std::endl;
                    }
                }
            }
            return result;
        }
        return next_value_ * interpolation(current_time_ / end_time_, ease_function_, ease_type_);
    }
/*
    cClass calculateNext(double time)
    {
        return original_value_ + (next_value_ - original_value_) * interpolation(current_time_ + time / end_time_, ease_function_, ease_type_);
    }
*/
    void cancel()
    {
        current_time_ = 0.f;
        next_value_ = cClass();
        end_time_ = 1.f;
        tween_enabled_ = false;
        ease_function_  = 0;
        ease_type_ = 0;
    }

    //See:http://libclaw.sourceforge.net/tweeners.html (Paragraph "Easing Functions")
    double interpolation(double t, Ease::FUNCTION type, Ease::TYPE direction)
    {
        switch(direction)
        {
        case Ease::IN:
            return f(t, type);

        case Ease::OUT:
            return 1 - f(1- t, type);

        case Ease::IN_OUT:
            double result = (t < 0.5f) ? f(2*t,type) / 2 : 0.5 + (1 - f(2 - 2*t,type)) / 2;
            return result;
        }
        return 0;
    }

    double f(double t, int type)
    {
        switch(type)
        {
        case Ease::NONE:
            return 0;

        case Ease::LINEAR:
            return t;

        case Ease::QUAD:
            return t*t;

        case Ease::CUBIC:
            return pow(t,3);

        case Ease::QUART:
            return pow(t,4);

        case Ease::QUINT:
            return pow(t,5);

        case Ease::SINE:
            return 1 - cos(t * utils::constants::PI / 2);

        case Ease::BACK:

            return t*t * (2.70158 * t - 1.70158);

        case Ease::BOUNCE:
        {
            const double x = 1 - t;
            double a, b;
            if (x < 1 / 2.75){
                a = 0;
                b = 0;
            }
            else if(x < 2 / 2.75){
                a = 1.5 / 2.75;
                b = 0.75;
            }
            else if (x < 2.5 / 2.75){
                a = 2.25 /  2.75;
                b = 0.9375;
            }
            else{
                a = 2.625 / 2.75;
                b = 0.984375;
            }
            return 1 - (7.5625 * pow(x-a,2) + b);
        }
        case Ease::CIRC:
            return 1 - sqrt(1- t*t);

        case Ease::ELASTIC:
            return -pow(2,10 * (t-1)) * sin(2 / 0.3 * utils::constants::PI * (t-1 - 0.3/4));

        case Ease::EXPO:
            return (!t)? 0 : pow(2,10 * (t-1));

        case Ease::CUSTOM:
            if (custom_easing_.is_valid()){
                double result = t;
                try{
                    result = luabind::object_cast<double>(custom_easing_(t));
                }
                catch(const luabind::error &e){
                    std::cerr<<"luagame_error in custom easing function: "<<e.what();
                    luabind::object error_msg(luabind::from_stack(e.state(), -1));
                    std::cerr<<error_msg<<std::endl;
                }
                return result;
            }
            else
                return t;
       }
       return 0;
    }

};
