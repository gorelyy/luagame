#pragma once
class CameraC;
class LayerC;
class Entity;
class TweenCoordC;
class TweenScaleC;
class TweenAngleC;
namespace sf{class VertexArray;}
namespace sf{class RenderWindow;}
class Group;
class Scene;
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Color.hpp>

class Renderer{
public:
    Renderer();
    void    update(Scene* scene,float dt, float interpolation, float frame_time);
    void    setWindow(sf::RenderWindow* window, const sf::Color& color);
private:
    void    updateCamera(CameraC* camera);
    void    drawCall(LayerC* component, CameraC *camera);
    void    interpolateCoordinates(LayerC* layer);
    void    updateTweenCoord(TweenCoordC* tween);
    void    updateTweenScale(TweenScaleC* tween);
    void    updateTweenAngle(TweenAngleC* tween);

    sf::RenderWindow* window_;
    sf::Color windowcolor_;
    sf::FloatRect camera_bounds_;
    Scene* scene_;
    float interpolation_;
    float dt_;
    float frame_time_;
    
};

