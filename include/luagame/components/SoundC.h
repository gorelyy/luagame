#pragma once
#include "../Component.h"
#include <SFML/Audio.hpp>

class SoundC: public Component{
public:
    SoundC();
    void    bindSound(const sf::SoundBuffer* sound);
    void    bindStream(sf::Music* music);
    void    play();
    void    pause();
    void    stop();
    void    setLoop(bool loop);
    void    setPlayingOffset(int seconds);
    void    setPitch(float pitch);
    void    setVolume(float volume);
    void    setCoord(const sf::Vector2f& coord);
    void    setDepth(float z);
    void    setRelativeToListener(bool relative);
    void    setMinDistance(float distance);
    void    setAttenuation(float attenuation);
private:
    sf::Sound sound_;
    sf::Music* stream_;
    float x_,y_,z_;
};
