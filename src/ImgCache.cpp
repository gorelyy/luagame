#include "luagame/ImgCache.h"

ImgCache::ImgCache() :
    enabled_(true)
{
    launchLoadingThread();
}

const sf::Texture* ImgCache::getTexture(const std::string& filename)
{
    if (!enabled_)
        return nullptr;
    auto it = tex_container_.find(filename);
    if (it != tex_container_.end())
        return &(it->second);
    tex_container_[filename].loadFromFile(filename);
    return &tex_container_[filename];

}

void ImgCache::asyncLoadTexture(const std::string& filename)
{   
    if (!enabled_){
        if (std::find(loading_queue_.begin(),
                      loading_queue_.end(),filename) == loading_queue_.end())
            loading_queue_.push_back(filename);
        return;
    }
    auto it = tex_container_.find(filename);
    if (it == tex_container_.end()){
        enabled_ = false;
        if (std::find(loading_queue_.begin(),
                      loading_queue_.end(),filename) == loading_queue_.end())
            loading_queue_.push_back(filename);
    }
}

void ImgCache::loadingThread(){
    sf::Context context;
    while(true){
        while(loading_queue_.empty()){
            enabled_ = true;
            sf::sleep(sf::milliseconds(16));
        }
        enabled_ = false;
        std::string& filename = loading_queue_.back();
        tex_container_[filename].loadFromFile(filename);
        loading_queue_.pop_back();
    }

}

void ImgCache::launchLoadingThread()
{
    loading_thread_ = std::thread(&ImgCache::loadingThread, this);
    loading_thread_.detach();
}

uint ImgCache::size() const
{
    return tex_container_.size();
}
