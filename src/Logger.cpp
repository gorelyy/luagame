#include "luagame/Logger.h"

Logger::Logger()
{
}

Logger::~Logger()
{
}

void Logger::log(const std::string &name, const std::string &value)
{
    auto& log = log_container_[name];
    auto counter = counter_container_.find(name);
    if(counter == counter_container_.end() || !(counter->second).second)
        log.push_back(value);
}

void Logger::setLogRate(const std::string &name, int rate)
{
    counter_container_[name].first = rate;
}

void Logger::update()
{
    for (auto& each : counter_container_){
        auto& pair = each.second;
        pair.second++;
        if (pair.second > pair.first)
            pair.second = 0;
    }
}

void Logger::output(const std::string &name)
{
    auto& log = log_container_[name];
    std::cout<<name<<std::endl;
    for(auto& line : log)
        std::cout<<line<<std::endl;
}

void Logger::outputMedian(const std::string &name)
{
    auto& log = log_container_[name];
    std::cout<<name<<std::endl;
    int num = 0;
    float total = 0;
    for(auto& line : log){
        //if (num)
            total += std::stof(line);
        num++;

    }
    std::cout<<(total/(num))<<std::endl;
}

void Logger::output()
{
    for(auto& pair : log_container_)
        output(pair.first);
}

void Logger::outputMedian()
{
    for(auto& pair : log_container_)
        outputMedian(pair.first);
}

void Logger::outputToFile(const std::string &filename, const std::string &name)
{
	std::ofstream file; 
    file.open(filename,std::ios_base::app);
    auto& log = log_container_[name];
    file << name << std::endl;
    for(auto& line : log)
        file<<line<<std::endl;
}

void Logger::outputMedianToFile(const std::string &filename, const std::string &name)
{
    std::ofstream file;
    file.open(filename,std::ios_base::app);
    auto& log = log_container_[name];
    int num = 0;
    float total = 0;
    for(auto& line : log){
        total += std::stof(line);
        num++;
    }
    file<<(total/(num))<<std::endl;
}



void Logger::outputToFile(const std::string &filename)
{
	std::ofstream file;
	file.open(filename);
    for(auto& pair : log_container_){
        std::string name = pair.first;
        auto& log = log_container_[name];
        file << name << std::endl;
        for(auto& line : log)
            file<<line<<std::endl;
        file<<std::endl;
    }
}


void Logger::reset()
{
    outputMedianToFile("logresult/collisiontime.txt", "collisiontime");
    outputMedianToFile("logresult/broadphasetime.txt", "broadphasetime");
    outputMedianToFile("logresult/numofchecks.txt","numofchecks");
    outputMedianToFile("logresult/numofboxesa.txt","numofboxesa");
    outputMedianToFile("logresult/numofboxesb.txt","numofboxesb");
    log_container_.clear();
    counter_container_.clear();
}
