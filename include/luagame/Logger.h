#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>

class Logger {
public:
    Logger();
    ~Logger();
    void    log(const std::string& name, const std::string& value);
    void    output();
    void    output(const std::string& name);
    void    outputMedian();
    void    outputMedian(const std::string& name);
    void    outputToFile(const std::string& filename);
    void    outputToFile(const std::string &filename, const std::string& name);
    void    outputMedianToFile(const std::string& filename, const std::string& name);
    void    setLogRate(const std::string& name, int rate);
    void    reset();
    void    update();
private:
    std::unordered_map<std::string,std::vector<std::string>> log_container_;
    std::unordered_map<std::string,std::pair<int,int> > counter_container_;
};
