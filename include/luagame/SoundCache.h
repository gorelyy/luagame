#pragma once
#include <SFML/Audio.hpp>
#include <map>
#include <string>
#include <thread>

class SoundCache{
public:
    SoundCache();
    ~SoundCache();
    const sf::SoundBuffer*  getSound(const std::string& name);
    sf::Music*       getStream(const std::string& name);
    void    launchLoadingThread();
    void    asyncLoadSound(const std::string& filename);
private:
    void    loadingThread();

    bool enabled_;
    std::map<std::string,sf::SoundBuffer> sound_container_;
    std::map<std::string,sf::Music*> stream_container_;
    std::thread loading_thread_;
    std::vector<std::string> loading_queue_;
};
