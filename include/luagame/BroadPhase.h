#pragma once
class CollisionCheckC;
#include "Grid.h"
#include "QTree.h"
#include <SFML/Graphics/Rect.hpp>
#include <SFML/System/Vector2.hpp>
#include <vector>

template <typename BPType>
class BaseBroadPhase{
public:
    BaseBroadPhase()
    {
    }
    BPType* add()
    {
        container_.resize(container_.size() + 1);
        return &container_.back();
    }
    template <typename Functor>
    void each(const Functor& f)
    {
        for (auto& each :container_)
            f(&each);
    }
private:
    std::vector<BPType> container_;

};

class BroadPhase : private BaseBroadPhase<Grid>,
                   private BaseBroadPhase<QTree>{
public:
    BroadPhase()
    {
    }
    template <typename BPType>
    BPType* add()
    {
        return BaseBroadPhase<BPType>::add();
    }
    template <typename BPType, typename Functor>
    void each(const Functor& f)
    {
        BaseBroadPhase<BPType>::each(f);
    }
};



