#include "luagame/Grid.h"
#include "luagame/components/BoxC.h"

Grid::Grid() : dynamic_(true)
{
}

void Grid::load(const std::string& A, const std::string& B, const sf::IntRect& collision_area, const sf::Vector2f& cell_size, CollisionCheckC* collision_data)
{
    groupA_ = A;
    groupB_ = B;
    collision_area_ = collision_area;
    cell_size_ = cell_size;
    collision_data_ = collision_data;
    for(int y = 0; y < collision_area.height /(int)cell_size.y; ++y)
        for(int x = 0; x < collision_area.width / (int)cell_size.x; ++x)
            cell_rectangles_.push_back(sf::IntRect(x * cell_size.x, y * cell_size.y, cell_size.x, cell_size.y));

}
void Grid::clearCollisions()
{
    collisions_.clear();
}

void Grid::optimizeCollisions()
{
    //Remove dublicate collision pairs
    std::vector<collisionPair>::iterator r, w;
    std::set<collisionPair> tmpset ;
    for(r = collisions_.begin(), w = collisions_.begin(); r != collisions_.end(); ++r){
    if( tmpset.insert(*r).second )
            *w++ = *r ;
    }
    collisions_.erase( w , collisions_.end());
}

void Grid::updateBoxAPosition(BoxC* box){
    box->newCell(collision_area_, cell_size_);
    box_registry_.insert(box);
}

void Grid::updateBoxBPosition(BoxC* box)
{
    std::array<int,4> current_cell;
    auto cell = box->cell();
    for(int i = 0;i != 4; ++i)
        current_cell[i] = cell[i];
    auto new_cell = box->newCell(collision_area_, cell_size_);
    for(int i = 0; i != 4; ++i){
        if (current_cell[i] != new_cell[i]){
            if (current_cell[i] >= 0){
                std::vector<BoxC*>& cell = cells_[current_cell[i]];
                auto it = std::find(cell.begin(), cell.end(), box);
                if(it != cell.end()){
                    std::swap(*it, cell.back());
                    cell.pop_back();
                }
                if (cell.empty())
                    cells_.erase(current_cell[i]);
            }
            if(new_cell[i] >= 0)
                cells_[new_cell[i]].push_back(box);
        } 
    }
    box_registry_.insert(box);
    
}

void Grid::findCollisions(BoxC* box){
    std::vector<int> visited_cells;
    for(int i = 0; i != 4; ++i){
        int cell_index = box->cell()[i];
        if (std::find(visited_cells.begin(), visited_cells.end(),cell_index) == visited_cells.end()){
            std::vector<BoxC*>& cell = cells_[cell_index];
            for(auto each = cell.begin(), end = cell.end(); each != end; ++each)
                collisions_.push_back(std::make_pair(box, *each));
        }
        visited_cells.push_back(cell_index);
    }   
}

void Grid::setDynamic(bool value)
{
    dynamic_ = value;
}

CollisionCheckC* Grid::collisionData() const
{
    return collision_data_;
}

const std::string& Grid::groupA() const
{
    return groupA_;
}


const std::string& Grid::groupB() const
{
    return groupB_;
}

const std::vector<Grid::collisionPair>& Grid::collisions() const
{
    return collisions_;
}

const std::vector<sf::IntRect>& Grid::cells() const
{
    return cell_rectangles_;
}

bool Grid::isDynamic() const
{
    return dynamic_;
}
