#pragma once
#include <string>
#include <sstream>
#include <math.h>
#include <vector>
#include "globals.h"
#include <SFML/System/Vector2.hpp>

namespace utils
{
    template<class T> T const&  constant(T& v){ return v; }
    template <typename T>
    T clip(const T& n, const T& lower, const T& upper) {
      return std::max(lower, std::min(n, upper));
    }
    std::vector<std::string>&   split(const std::string &s, char delim, std::vector<std::string> &elems);
    std::vector<std::string>    split(const std::string &s, char delim);
    double                      round(double number);
    sf::Vector2f                round(const sf::Vector2f& vector);
    bool                        equal(double A, double B);
    bool                        less(double A, double B);
    bool                        greater(double A, double B);
    uint                        highestBit(uint n);

    namespace constants{
        const double PI = std::atan(1)*4;//3.14159265;
        const double RAD = PI / 180;
    }
}
