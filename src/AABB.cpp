
#include "luagame/AABB.h"

AABB::AABB(const sf::Vector2f& coord, const sf::Vector2f& size): 
    p(sf::Vector2f(coord.x + size.x / 2, coord.y + size.y / 2)),
    v(sf::Vector2f(size.x / 2,size.y / 2)),
    min(p - v),
    max(p + v)
{
}

bool AABB::overlaps(const AABB& box) const
{
    sf::Vector2f distance = box.p - p;
    return ( fabs(distance.x) <= v.x + box.v.x
             &&
             fabs(distance.y) <= v.y + box.v.y
           );
}

