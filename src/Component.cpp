 
#include "luagame/Component.h"


Component::Component() :
    point_index_(0),
    disabledcounter_(nullptr),
    enabled_(true)
{
}
std::string Component::key() const
{
    return std::to_string(pool_id_.first) + std::string("#") + std::to_string(pool_id_.second);
}

ID Component::entityID() const
{
    return id_.first;
}

const componentID& Component::id() const
{
    return id_;
}

void Component::setID(ID e)
{
    id_.first = e;
}

uint Component::componentIndex() const
{
    return id_.second;
}

void Component::setIndex(uint index)
{
    id_.second = index;
}

lua_State* Component::luaState() const
{
    return L_;
}

void Component::setLuaState(lua_State* L)
{
    L_= L;
}


int Component::pointIndex() const
{
    return point_index_;
}

void Component::setPointIndex(int index)
{
    point_index_ = index;
}

void Component::setEnabled(bool status)
{
    if (!enabled_ && disabledcounter_)
        (*disabledcounter_)--;
    enabled_ = status;
    if (!status && disabledcounter_)
        (*disabledcounter_)++;
}

bool Component::enabled() const
{
    return enabled_;
}

void Component::setPoolID(ID p)
{
    pool_id_.first = p;
}

void Component::setPoolIndex(uint index)
{
    pool_id_.second = index;
}

ID Component::poolID() const
{
    return pool_id_.first;
}

uint Component::poolIndex() const
{
    return pool_id_.second;
}

void Component::setDisabledCounter(uint *counter)
{
    disabledcounter_ = counter;
}
