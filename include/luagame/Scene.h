#pragma once
class ImgCache;
class SoundCache;
class FontCache;
class Logger;
#include "IdManager.h"
#include "Pool.h"
#include "Entity.h"
#include "ComponentContainer.h"
#include "GroupContainer.h"
#include "VertexArrayContainer.h"
#include "BroadPhase.h"
#include "Utils.h"
#include "PointConnection.h"
#include <SFML/System/Vector2.hpp>
#include <luabind/luabind.hpp>
extern "C"
{
    #include <lua.h>
    #include <lualib.h>
    #include <lauxlib.h>
}
#include <string>
#include <deque>
#include <unordered_map>
//#include <thread>
class SceneBuilder;
class Scene{
public:
    Scene();
    enum QUEUETYPE {
        RENDER_CAMERA,
        COLLISION,
        MOTION,
        SCRIPTING,
        SOUNDMIXER,
        TYPEWRITER
    };
    Entity* getEntity(ID id);
    Entity* addEntity(const std::string& name);
    Entity* addEntity(const std::string& name, const sf::Vector2f& coord);
    Entity* addEntity(const std::string& name, const luabind::object& buildfunction);
    Entity* addEntity(const std::string& name, const sf::Vector2f& coord, const luabind::object& buildfunction);
    Pool*   addPool(const std::string& name);
    BroadPhase& broadPhase();
    std::vector<sf::Vertex>& getVertexArray(ID id);
    Group* queue(QUEUETYPE id);
    lua_State* luaState()                const;
    const std::string& name()            const;
    const std::string& startScriptName() const;
    const ParentCoord& parentCoord(componentID parent);
    const sf::Vector2f& parentOffset(componentID parent);
    void    deleteEntity(Entity* entity);
    void    addCamera(const std::string& name, CameraC::MODE mode, const sf::Vector2f& size);
    void    addCamera(const std::string& name, CameraC::MODE mode, const sf::Vector2f& size, const sf::Vector2f& coord);
    void    addCamera(const std::string& name, CameraC::MODE mode, const sf::Vector2f& size, const sf::Vector2f& coord,
                      const sf::IntRect& scrollborders);
    void    addCamera(const std::string& name, CameraC::MODE mode, const sf::Vector2f& size,
                      const sf::Vector2f& coord, const sf::IntRect& scrollborders, const sf::Color& color);

    void    addText(const std::string& name, int maxsize, const std::string& fontname, int fontsize);
    void    addText(const std::string& name, int maxsize, const std::string& fontname, int fontsize, const sf::Color& textcolor);
    void    addText(const std::string& name, int maxsize, const std::string& fontname, int fontsize, const sf::Color& textcolor,
                        TextC::TEXTTWEEN tweentype);
    void    addText(const std::string& name, int maxsize, const std::string& fontname, int fontsize, const sf::Color& textcolor,
                        TextC::TEXTTWEEN tweentype, const sf::IntRect& textarea);
    void    addText(const std::string& name, int maxsize, const std::string& fontname, int fontsize, const sf::Color& textcolor,
                        TextC::TEXTTWEEN tweentype, const sf::IntRect& textarea, const sf::Color& textareacolor);
    void    addText(const std::string& name, const std::string& textstring,
                    const std::string& fontname, int fontsize);
    void    addText(const std::string& name, const std::string& textstring,
                    const std::string& fontname, int fontsize, const sf::Color& textcolor);
    void    addText(const std::string& name, const std::string& textstring,
                    const std::string& fontname, int fontsize, const sf::Color& textcolor,
                    TextC::TEXTTWEEN tweentype);
    void    addText(const std::string& name, const std::string& textstring,
                    const std::string& fontname, int fontsize, const sf::Color& textcolor,
                    TextC::TEXTTWEEN tweentype, const sf::IntRect& textarea);
    void    addText(const std::string& name, const std::string& textstring,
                    const std::string& fontname, int fontsize, const sf::Color& textcolor,
                    TextC::TEXTTWEEN tweentype, const sf::IntRect& textarea, const sf::Color& textareacolor);
    void    addText(const std::string& name, const std::string& textstring, int maxsize,
                    const std::string& fontname, int fontsize);
    void    addText(const std::string& name, const std::string& textstring, int maxsize,
                    const std::string& fontname, int fontsize, const sf::Color& textcolor);
    void    addText(const std::string& name, const std::string& textstring, int maxsize,
                    const std::string& fontname, int fontsize, const sf::Color& textcolor,
                    TextC::TEXTTWEEN tweentype);
    void    addText(const std::string& name, const std::string& textstring, int maxsize,
                    const std::string& fontname, int fontsize, const sf::Color& textcolor,
                    TextC::TEXTTWEEN tweentype, const sf::IntRect& textarea);
    void    addText(const std::string& name, const std::string& textstring, int maxsize,
                    const std::string& fontname, int fontsize, const sf::Color& textcolor,
                    TextC::TEXTTWEEN tweentype, const sf::IntRect& textarea, const sf::Color& textareacolor);
    void    createDefaultGroups();
    void    updateScript();
    void    setName(const std::string& name);
    void    addStartScript(const std::string& name);
    void    setImageCache(ImgCache* imagecache);
    void    setSoundCache(SoundCache* soundcache);
    void    setFontCache(FontCache* fontcache);
    void    setWindowSize(const sf::Vector2u& window_size);
    void    setUpdateRate(double dt);
    void    bindLuaState(lua_State* state);
    void    bindLogger(Logger* logger);
    void    postUpdate();
    void    sortPointConnections();
    void    storeParentCoord(componentID parent, const sf::Vector2f& coord,const sf::Vector2f& prevcoord);
    void    storeParentOffset(componentID parent, const sf::Vector2f& coord);
    void    updateTextureSize(ID id);
    void    setUpdateLuaFunction(const luabind::object& f);
    void    setPostUpdateLuaFunction(const luabind::object& f);
    void    setConstructed(bool value);
    void    setAsyncBuildStarted(bool value);
    bool    isConstructed();
    bool    isAsyncBuildStarted();

    void    log(const std::string& name, const std::string& value);
    template<typename Functor>
    void eachParentingConnection(const Functor& f)
    {
        for (componentID parent : pointconnection_.parents())
            for(componentID child : pointconnection_.children(parent))
                f(parent,child);
    }
    template<typename Functor>
    void eachFollowingConnection(const Functor& f)
    {
        for (componentID leader : pointconnection_.leaders())
            for(componentID follower : pointconnection_.followers(leader))
                f(leader,follower, pointconnection_.followData(follower));
    }

    template <typename cClass, typename Functor>
    void eachC(const std::string& groupname, const Functor& f)
    {
        group_container_.group<cClass>(groupname)->each([&](ID id)
        {
            component_container_.eachC<cClass>(id, f);
        });
    }
    template <typename cClass, typename Functor>
    void eachExistingC(const std::string& groupname, const Functor& f)
    {
        group_container_.group<cClass>(groupname)->each([&](ID id)
        {
            component_container_.eachExistingC<cClass>(id, f);
        });
    }
    template <typename cClass, typename Functor>
    void eachDisabledC(const std::string& groupname, const Functor& f)
    {
        group_container_.group<cClass>(groupname)->each([&](ID id)
        {
            component_container_.eachDisabledC<cClass>(id, f);
        });
    }
    template <typename cClass, typename Functor>
    void eachC(Group* group, const Functor& f)
    {
        group->each([&](ID id)
        {
            component_container_.eachC<cClass>(id, f);
        });
    }
    template <typename cClass, typename Functor>
    void eachExistingC(Group* group, const Functor& f)
    {
        group->each([&](ID id)
        {
            component_container_.eachExistingC<cClass>(id, f);
        });
    }
    template <typename cClass, typename Functor>
    void eachC(const Functor& f)
    {
        group_container_.defaultGroup<cClass>()->each([&](ID id)
        {
            component_container_.eachC<cClass>(id, f);
        });
    }
    template <typename cClass, typename Functor>
    void eachExistingC(const Functor& f)
    {
        group_container_.defaultGroup<cClass>()->each([&](ID id)
        {
            component_container_.eachExistingC<cClass>(id, f);
        });
    }
    template <typename cClass, typename Functor>
    void eachC(ID id, const Functor& f)
    {
        component_container_.eachC<cClass>(id, f);
    }
    template <typename cClass, typename Functor>
    void eachExistingC(ID id, const Functor& f)
    {
        component_container_.eachExistingC<cClass>(id, f);
    }
    template <typename cClass>
    Group* addGroup(const std::string& type, const std::string& groupname)
    {
        group_container_.addGroup<cClass>(groupname);
        Group* group = group_container_.group<cClass>(groupname);
        group->bindEntityContainer(&entity_container_);
        luabind::globals(scene_lua_state_)["_group"][type][groupname] = group;
        return group;
    }
    template <typename cClass>
    void addDefaultGroup(const std::string& type)
    {
        Group* group = group_container_.defaultGroup<cClass>();
        group->bindEntityContainer(&entity_container_);
        luabind::globals(scene_lua_state_)["_group"][type]["default"] = group;
    }
    template <typename cClass>
    Group* group(const std::string& name)
    {
        return group_container_.group<cClass>(name);
    }
    template <typename cClass>
    Group* defaultGroup()
    {
        return group_container_.defaultGroup<cClass>();
    }
    Group* group(Group::BUILTIN id)
    {
        return group_container_.group(id);
    }

    template <typename cClass>
    cClass* getComponent(const componentID& id)
    {
        return component_container_.getComponent<cClass>(id);
    }

    template <typename cClass>
    size_t componentPoolSize(ID id)
    {
        return component_container_.poolSize<cClass>(id);
    }

private:
    double dt_;
    bool constructed_;
    bool async_build_started_;
    std::string name_;
    std::string start_script_name_;
    sf::Vector2f window_size_;
    sf::Vector2f default_camera_size_;
    sf::Vector2f default_camera_window_offset_;
    IdManager id_manager_;
    ComponentContainer component_container_;
    GroupContainer group_container_;
    EntityGroupRegistry entity_group_registry_;
    VertexArrayContainer vertex_array_container_;
    BroadPhase broadphase_;
    PointConnection pointconnection_;
    Logger* logger_;
    CameraC::MODE default_camera_mode_;
    luabind::object update_function_;
    luabind::object postupdate_function_;
    lua_State* scene_lua_state_;
    Entity* default_camera_;
    ImgCache* image_cache_;
    SoundCache* sound_cache_;
    FontCache* font_cache_;
    SceneBuilder* scenebuilder_;
    std::deque<luabind::object> entity_luafunction_stack_;
    std::array<Group,6> queue_;
    std::unordered_map<ID, Entity> entity_container_;
    std::unordered_map<ID, Pool> pool_container_;
    std::unordered_map<Entity*, std::string> entitynames_;
};



