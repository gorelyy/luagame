#pragma once
#include "SFML/System/Vector2.hpp"
#include "math.h"

class AABB {
public:
    AABB(const sf::Vector2f& coord, const sf::Vector2f& size);
    bool overlaps(const AABB& box) const;
    sf::Vector2f p;
    sf::Vector2f v;
    sf::Vector2f min;
    sf::Vector2f max;
};


