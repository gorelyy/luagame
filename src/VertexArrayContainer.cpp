#include "luagame/VertexArrayContainer.h"

VertexArrayContainer::VertexArrayContainer()
{
}

std::vector<sf::Vertex>& VertexArrayContainer::getVertexArray(ID key)
{
    return layers_[key];
}

void VertexArrayContainer::deleteVertexArray(ID key)
{
    layers_.erase(key);
}

void VertexArrayContainer::deleteVertices(ID key, size_t index, size_t size)
{
    auto it = layers_.find(key);
    if (it == layers_.end())
        return;
    auto& v = it->second;
    if (index >= v.size())
        return;
    size_t last = std::min(index+size,v.size() - 1);
    v.erase(v.begin() + index,v.begin() + last);
}
