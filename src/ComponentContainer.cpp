#include "luagame/ComponentContainer.h"

ComponentContainer::ComponentContainer() :
    scene_constructed_(false)
{
}

void ComponentContainer::bindGroupContainer(GroupContainer* gc)
{
    group_container_ = gc;
}

void ComponentContainer::deleteEntity(ID id)
{
     //NEW_COMPONENT_TYPE
     BaseComponentContainer<QuadC>::deletePool(id);
     BaseComponentContainer<LineC>::deletePool(id);
     BaseComponentContainer<MoveC>::deletePool(id);
     BaseComponentContainer<ScriptC>::deletePool(id);
     BaseComponentContainer<AnimC>::deletePool(id);
     BaseComponentContainer<BoxC>::deletePool(id);
     BaseComponentContainer<CollisionCheckC>::deletePool(id);
     BaseComponentContainer<PointC>::deletePool(id);
     BaseComponentContainer<LayerC>::deletePool(id);
     BaseComponentContainer<CameraC>::deletePool(id);
     BaseComponentContainer<TweenCoordC>::deletePool(id);
     BaseComponentContainer<TweenScaleC>::deletePool(id);
     BaseComponentContainer<TweenAngleC>::deletePool(id);
     BaseComponentContainer<SoundC>::deletePool(id);
     BaseComponentContainer<TextC>::deletePool(id);
     BaseComponentContainer<TimerC>::deletePool(id);
}

bool ComponentContainer::isSceneConstructed() const
{
    return scene_constructed_;
}

void ComponentContainer::setSceneConstructed(bool value)
{
    scene_constructed_ = value;
}
