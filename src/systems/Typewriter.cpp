#include "luagame/systems/Typewriter.h"
#include "luagame/Scene.h"

Typewriter::Typewriter()
{
}

void Typewriter::update(Scene* scene, float time)
{
    scene_ = scene;
    scene->eachC<TextC>(scene->queue(Scene::TYPEWRITER),[&](TextC* text)
    {
        if (text->requiresUpdate())
            updateText(text);

        text->updateTime(time);
        if (text->currentTypingChar() >= 0){
            Entity* entity = scene->getEntity(text->entityID());
            QuadC* quad =  entity->getComponent<QuadC>(text->currentTypingChar());
            quad->setVisibility(true);
        }

        if (text->requiresHide()){
            Entity* entity = scene->getEntity(text->entityID());
            entity->each<QuadC>([](QuadC* quad)
            {
                quad->setVisibility(false);
            });
            text->hidden();
        }

    });
}


void Typewriter::updateText(TextC *text)
{
    const std::wstring& str = text->textString();
    Entity* entity = scene_->getEntity(text->entityID());
    entity->getComponent<LayerC>()->setDynamic(text->isDynamic());
    int limit = std::min(text->capacity(), str.size());
    for (int i = limit; i < text->prevTextSize(); ++i){
         QuadC* quad = entity->getComponent<QuadC>(i);
         quad->setTextureRect(sf::IntRect());
         quad->setSize(sf::Vector2f());
    }
    for(int i = 0; i < limit; ++i){
        PointC* point = entity->getComponent<PointC>(i + 1);
        point->set(text->iterationCharacterCoord(i));
        QuadC* quad = entity->getComponent<QuadC>(i);
        if (!text->characterIsValid(i)){
            quad->setTextureRect(sf::IntRect());
            quad->setSize(sf::Vector2f());
        }
        else{
            quad->setTextureRect(text->characterTextureRect(i));
            quad->setSize(text->characterSize(i));
            quad->setColor(text->color());
        }

    }

    text->updated();
}
