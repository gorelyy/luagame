#pragma once
#include "../Component.h"
#include "../Tweenable.h"
#include <SFML/System/Vector2.hpp>

class TweenCoordC : public Component{
public:
    TweenCoordC();
    bool isEnabled()        const;
    sf::Vector2f offset()   const;
    void    set(const sf::Vector2f& coord, float time);
    void    set(const sf::Vector2f& coord, float time, Ease::FUNCTION f, Ease::TYPE type);
    void    set(const sf::Vector2f& coord, float time, Ease::FUNCTION f, Ease::TYPE type, int repeat, unsigned char mode);
    void    addCallbackOnFinish(const luabind::object& callback);
    void    addCallbackOnRepeat(const luabind::object& callback);
    void    addCustomEasing(const luabind::object& f);
    void    update(double dt);
private:
    Tweenable<sf::Vector2f> tween_vector_;
};
