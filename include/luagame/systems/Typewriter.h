#pragma once
class Scene;
class TextC;
class Typewriter{

public:
    Typewriter();
    void    update(Scene* scene, float time);
private:
    void    updateText(TextC* text);

    float frametime_;
    Scene* scene_;
};
