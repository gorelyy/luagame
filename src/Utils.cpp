 
#include "luagame/Utils.h"

namespace utils{
    
    std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) 
    {
        std::stringstream ss(s);
        std::string item;
        while (std::getline(ss, item, delim)) {
            elems.push_back(item);
        }
        return elems;
    }
    
    std::vector<std::string> split(const std::string &s, char delim) 
    {
        std::vector<std::string> elems;
        split(s, delim, elems);
        return elems;
    }
    
    double round(double number)
    {
        return number < 0.0 ? ceil(number - 0.49999997f) : floor(number + 0.49999997f);
    }

    sf::Vector2f round(const sf::Vector2f &vector)
    {
        return sf::Vector2f(round(vector.x),round(vector.y));
    }

    bool less(double A, double B)
    {
        return (A - B < 1e-2) && (fabs(A-B) > 1e-2);
    }

    bool greater(double A, double B)
    {
        return (A - B > 1e-2) && (fabs(A-B) > 1e-2);
    }

    bool equal(double A, double B)
    {
        return fabs(A-B)<=1e-2;
    }

    uint highestBit(uint n)
    {
        uint r = 0;
        while (n >>= 1)
            r++;
        return r;
    }

}
