#pragma once
#include "../Component.h"
#include "../Group.h"
#include "../Utils.h"
#include <SFML/Graphics.hpp>
#include <SFML/System/Vector2.hpp>
#include <list>
#include <map>

class CameraC:public Component {
public:    
    CameraC();
    enum MODE{
        WINDOW,
        WINDOWTEXTURE,
        TEXTURE
    };
    const sf::View&      view();
    const sf::Vector2f&  coord()            const;
    const sf::Color&     backgroundColor()  const;
    const sf::Vector2f&  scrollOffset()     const;
    const sf::Texture*   texture()          const;
    const sf::BlendMode& blendMode()        const;
    const sf::FloatRect& bounds()           const;
    ID      focusID()                       const;
    MODE    mode()                          const;
    Group*  queue();
    sf::RenderTexture& renderTexture()      const;
    sf::RectangleShape& backgroundRectangle() const;
    void    set(CameraC::MODE mode,const sf::Vector2f& size);
    void    setSize(const sf::Vector2f& size);
    void    setViewCoord(const sf::Vector2f& coord);
    void    setFocus(ID focus_object_id);
    void    setWindowSize(const sf::Vector2f& size);
    void    setWindowOffset(const sf::Vector2f& offset);
    void    setBackgroundColor(const sf::Color& color);
    void    setScrollArea(const sf::IntRect rect);
    void    setBlendMode(const sf::BlendMode& mode);
    void    updateBounds();
private:
    ID focus_id_;
    Group queue_;
    MODE mode_;
    sf::BlendMode blend_mode_;
    sf::View view_;
    sf::Vector2f scroll_offset_coord_;
    sf::Vector2f window_offset_;
    sf::Vector2f window_size_;
    sf::Vector2f coord_;
    sf::Vector2f offset_;
    sf::FloatRect bounds_;
    sf::IntRect scroll_borders_;
    sf::IntRect deadzone_;
    sf::Color background_color_;
    std::shared_ptr<sf::RenderTexture> render_texture_;
    std::shared_ptr<sf::RectangleShape> background_rectangle_;
};

