#pragma once
#include "../Component.h"
#include "../Utils.h"
#include <SFML/System/Vector2.hpp>

class PointC: public Component {
public:
    PointC();
    const sf::Vector2f& coord()     const;
    const sf::Vector2f& prevCoord() const;
    const sf::Vector2f& scale()     const;
    const sf::Vector2f& prevScale() const;
    float angle()                   const;
    float prevAngle()               const;
    float rotation()                const;
    float x()                       const;
    float y()                       const;

    void    setCoord(float x, float y);
    void    setCoord(const sf::Vector2f& coord);
    void    set(float x, float y);
    void    set(const sf::Vector2f& coord);
    void    setX(float x);
    void    setY(float y);
    void    setPrev(float x, float y);
    void    setPrev(const sf::Vector2f& coord);
    void    savePrevCoord();
    void    setAngle(float angle);
    void    setPrevAngle(float angle);
    void    savePrevAngle();
    void    resetAngle(float angle);
    void    setScale(const sf::Vector2f& scale);
    void    setPrevScale(const sf::Vector2f& scale);
    void    savePrevScale();
    void    resetScale(sf::Vector2f& scale);
private:
    float angle_;
    float prevangle_;
    sf::Vector2f coord_;
    sf::Vector2f prevcoord_;
    sf::Vector2f scale_;
    sf::Vector2f prevscale_;
};


