#include "luagame/systems/SoundMixer.h"
#include "luagame/Scene.h"
#include "luagame/Audio.h"

SoundMixer::SoundMixer()
{
}

void SoundMixer::update(Scene* scene, float dt)
{
    scene->eachC<SoundC>(scene->queue(Scene::SOUNDMIXER),[&](SoundC* sound)
    {
        Entity* entity = scene->getEntity(sound->entityID());
        PointC* point = entity->getComponentByPointIndex<PointC>(sound->pointIndex());
        sound->setCoord(point->prevCoord() + (point->coord() - point->prevCoord()) * dt);
    });
    PointC* point = audio_->focus()->getComponent<PointC>();
    audio_->setCoord(point->prevCoord() + (point->coord() - point->prevCoord()) * dt);

}

void SoundMixer::bindAudio(Audio *audio)
{
    audio_ = audio;
}
