#include "luagame/components/CameraC.h"

CameraC::CameraC() :
    background_color_(sf::Color::Transparent),
    window_offset_(sf::Vector2f(-1.f,-1.f)),
    mode_(CameraC::TEXTURE),
    blend_mode_(sf::BlendAlpha)
{
}

ID CameraC::focusID() const
{
    return focus_id_;
}

void CameraC::set(MODE mode, const sf::Vector2f& size)
{   
    mode_ = mode;
    view_.setSize(size);
	if(mode != WINDOW){
        render_texture_.reset(new sf::RenderTexture());
        render_texture_->create(size.x, size.y);
    }
    else{
        background_rectangle_.reset(new sf::RectangleShape(size));
    }
}

void CameraC::setViewCoord(const sf::Vector2f& coord)
{
    sf::Vector2f newcoord = coord;
    if (scroll_borders_ != sf::IntRect()){
        if(newcoord.x - view_.getSize().x / 2 < scroll_borders_.left)
            newcoord.x = scroll_borders_.left + view_.getSize().x / 2;
        if(newcoord.x + view_.getSize().x / 2> scroll_borders_.left + scroll_borders_.width)
            newcoord.x = scroll_borders_.left + scroll_borders_.width - view_.getSize().x / 2;
        if (newcoord.y - view_.getSize().y / 2 < scroll_borders_.top)
            newcoord.y = scroll_borders_.top + view_.getSize().y / 2;
        if (newcoord.y + view_.getSize().y / 2 > scroll_borders_.top + scroll_borders_.height)
            newcoord.y = scroll_borders_.top + scroll_borders_.height - view_.getSize().y / 2;
    }

    scroll_offset_coord_ += (newcoord - view_.getCenter());
    view_.setCenter(newcoord);
    if (background_rectangle_)
        background_rectangle_->setPosition(utils::round(sf::Vector2f(newcoord.x - view_.getSize().x / 2,
                                                                     newcoord.y - view_.getSize().y / 2)));
    
}

const sf::Vector2f& CameraC::coord() const
{
    return coord_;
}

const sf::View& CameraC::view()
{ 
    if(mode_!= WINDOW)
        return view_;

    sf::Vector2f window_offset;
    if(window_offset_.x == -1 && window_offset_.y == -1)
        window_offset = sf::Vector2f((window_size_.x - view_.getSize().x) / 2, (window_size_.y - view_.getSize().y) / 2);
    else
        window_offset = window_offset_;

    view_.setViewport( sf::FloatRect( 1.f * window_offset.x / window_size_.x,
                                      1.f * window_offset.y / window_size_.y,
                                      1.f * view_.getSize().x / window_size_.x,
                                      1.f * view_.getSize().y / window_size_.y
                                    ));
    return view_;
}

const sf::Color& CameraC::backgroundColor() const
{
    return background_color_;
}

void CameraC::setFocus(ID focus_object_id)
{
    focus_id_ = focus_object_id;
}

const sf::Vector2f& CameraC::scrollOffset() const
{
    return scroll_offset_coord_;
}

void CameraC::updateBounds()
{
    bounds_ =  sf::FloatRect(view_.getCenter().x - view_.getSize().x / 2,
                             view_.getCenter().y - view_.getSize().y / 2,
                             view_.getSize().x, 
                             view_.getSize().y);
}

const sf::FloatRect& CameraC::bounds() const
{
    return bounds_;
}

const sf::Texture* CameraC::texture() const
{
    if (!render_texture_)
        return nullptr;
    return &render_texture_->getTexture();
}

sf::RenderTexture& CameraC::renderTexture() const
{
    return *render_texture_;
}

void CameraC::setBackgroundColor(const sf::Color &color)
{
    background_color_ = color;
    if (background_rectangle_)
        background_rectangle_->setFillColor(background_color_);
}

Group* CameraC::queue()
{
    return &queue_;
}

CameraC::MODE CameraC::mode() const
{
    return mode_;
}

void CameraC::setWindowSize(const sf::Vector2f& size)
{
    window_size_= size;
}

void CameraC::setScrollArea(const sf::IntRect rect)
{
    scroll_borders_ = rect;
}

void CameraC::setWindowOffset(const sf::Vector2f &offset)
{
    window_offset_ = offset;
}

void CameraC::setSize(const sf::Vector2f &size)
{
    view_.setSize(size);
}

sf::RectangleShape& CameraC::backgroundRectangle() const
{
    return *background_rectangle_;
}

const sf::BlendMode& CameraC::blendMode() const
{
    return blend_mode_;
}

void CameraC::setBlendMode(const sf::BlendMode &mode)
{
    blend_mode_ = mode;
}

