#pragma once
class QTreeNode;
class Group;
#include "../Component.h"
#include <SFML/Graphics/Rect.hpp>
#include <string>
#include <array>
#include <map>

class BoxC: public Component {
public:
    BoxC();
    uint  collisionSize()               const;
    float    width()                    const;
    float    height()                   const;
    float    friction()                 const;
    BoxC*    anyBoxB()                  const;
    QTreeNode* getQTreeNode()           const;
    const sf::Vector2f& size()          const;
    const sf::Vector2f& coord()         const;
    const sf::Vector2f& prevCoord()     const;
    const sf::Vector2f& scale()         const;
    const sf::Vector2f& prevScale()     const;
    const sf::Vector2f& separation()    const;
    const std::array<int,4>& cell()     const;
    const std::array<int,4>& newCell(const sf::IntRect& area, const sf::Vector2f& cellsize);
    bool    wasTouching()                                       const;
    bool    wasTouching(Group* group)                           const;
    bool    wasTouching(BoxC* box)                              const;
    bool    wasTouching(Component::SIDE side)                   const;
    bool    wasTouching(Component::SIDE side, Group* group)     const;
    bool    wasTouching(Component::SIDE side, BoxC* box);
    bool    isTouching()                                        const;
    bool    isTouching(Group* group)                            const;
    bool    isTouching(BoxC* box)                               const;
    bool    isTouching(Component::SIDE side)                    const;
    bool    isTouching(Component::SIDE side, Group* group)      const;
    bool    isTouching(Component::SIDE side, BoxC* box);
    Component::SIDE touchingSide(BoxC*  box);
    bool    startTouching()                                     const;
    bool    startTouching(Group* group)                         const;
    bool    startTouching(BoxC* box)                            const;
    bool    startTouching(Component::SIDE side)                 const;
    bool    startTouching(Component::SIDE side, Group* group)   const;
    bool    startTouching(Component::SIDE side, BoxC* box);
    bool    stopTouching()                                      const;
    bool    stopTouching(Group* group)                          const;
    bool    stopTouching(BoxC* box)                             const;
    bool    stopTouching(Component::SIDE side)                  const;
    bool    stopTouching(Component::SIDE side, Group* group)    const;
    bool    stopTouching(Component::SIDE side, BoxC* box);
    bool    isSolid (Component::SIDE side)                      const;

    void    noTouching();
    void    noTouching(BoxC*  box);
    void    touch(Component::SIDE side, BoxC* box);
    void    setSolidSide(Component::SIDE side);
    void    setSolidSide(const luabind::object& side_list);
    void    setSize(const sf::Vector2f& size);
    void    eachBoxB(const luabind::object& function);
    void    savePreviousState();
    void    setCoord(const sf::Vector2f& coord);
    void    setPrevCoord(const sf::Vector2f& coord);
    void    saveCoord();
    void    addSeparation(const sf::Vector2f& separation);
    void    resetSeparation();
    void    setScale(const sf::Vector2f& scale);
    void    setPrevScale(const sf::Vector2f& scale);
    void    setQTreeNode(QTreeNode* node);
    void    clearCollisions();
private:    
    float friction_;
    QTreeNode* qtreenode_;
    sf::Vector2f size_;
    sf::Vector2f coord_;
    sf::Vector2f prevcoord_;
    sf::Vector2f separation_;
    sf::Vector2f scale_;
    sf::Vector2f prev_scale_;
    std::array<int8_t,4> openside_;
    std::array<int,4> cell_;
    std::vector<std::pair<BoxC*, SIDE> > collisions_;
    std::vector<std::pair<BoxC*, SIDE> > prev_collisions_;
};

