#pragma once
//NEW_COMPONENT_TYPE
class QuadC;
class LineC;
class MoveC;
class ScriptC;
class AnimC;
class BoxC;
class CollisionCheckC;
class PointC;
class LayerC;
class CameraC;
class TweenCoordC;
class TweenScaleC;
class TweenAngleC;
class SoundC;
class TextC;
class TimerC;
#include "Group.h"
#include "EntityGroupRegistry.h"
#include <unordered_map>
#include <array>
#include <string>

template <typename cClass>
class BaseGroupContainer
{
public:
    BaseGroupContainer()
    {
    }

    Group* getGroup(const std::string& name)
    {
        if (name == "default")
            return &defaultgroup_;
        return &container_[name];
    }

    Group* getDefaultGroup()
    {
        return &defaultgroup_;
    }
private:
    std::unordered_map<std::string, Group> container_;
    Group defaultgroup_;
};

                        //NEW_COMPONENT_TYPE
class GroupContainer :  private BaseGroupContainer<QuadC>,
                        private BaseGroupContainer<LineC>,
                        private BaseGroupContainer<MoveC>,
                        private BaseGroupContainer<ScriptC>,
                        private BaseGroupContainer<AnimC>,
                        private BaseGroupContainer<BoxC>,
                        private BaseGroupContainer<CollisionCheckC>,
                        private BaseGroupContainer<PointC>,
                        private BaseGroupContainer<LayerC>,
                        private BaseGroupContainer<CameraC>,
                        private BaseGroupContainer<TweenCoordC>,
                        private BaseGroupContainer<TweenScaleC>,
                        private BaseGroupContainer<TweenAngleC>,
                        private BaseGroupContainer<SoundC>,
                        private BaseGroupContainer<TextC>,
                        private BaseGroupContainer<TimerC>{
public:
    GroupContainer();

    template <typename cClass>
    Group* group(const std::string& name)
    {
        return BaseGroupContainer<cClass>::getGroup(name);
    }
    template <typename cClass>
    Group* defaultGroup()
    {
        return BaseGroupContainer<cClass>::getDefaultGroup();
    }
    template <typename cClass>
    void addGroup(const std::string& name)
    {
        BaseGroupContainer<cClass>::getGroup(name)->bindEntityGroupRegistry(entitygroupregistry_);
    }
    Group* group(Group::BUILTIN id);
    void   build();
    void bindEntityGroupRegistry(EntityGroupRegistry* egr);
private:
    std::array<Group,2> builtingroups_;
    EntityGroupRegistry* entitygroupregistry_;
};
