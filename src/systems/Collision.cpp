#include "luagame/systems/Collision.h"
#include "luagame/components/CollisionCheckC.h"
#include "luagame/Scene.h"
#include "luagame/Grid.h"
#include "luagame/BroadPhase.h"
#include "luagame/QTree.h"
#include "luagame/AABB.h"

Collision::Collision()
{
}

void Collision::update(Scene* scene)
{
    scene_ = scene;
    scene->eachC<CollisionCheckC>(scene->queue(Scene::COLLISION),[this](CollisionCheckC* check)
    {
        loop(check);
    });
    checked_to_save_state_boxesA_.clear();
    scene->broadPhase().each<Grid>([&](Grid* grid)
    {
        grid->clearCollisions();
        scene->eachC<BoxC>(grid->groupA(), [grid](BoxC* box)
        {
            grid->updateBoxAPosition(box);
        });
        if (grid->isDynamic())
            scene->eachC<BoxC>(grid->groupB(), [grid](BoxC* box)
            {
                grid->updateBoxBPosition(box);
            });
        scene->eachC<BoxC>(grid->groupA(), [grid](BoxC* box)
        {
            grid->findCollisions(box);
        });
        grid->optimizeCollisions();
        auto& collisions = grid->collisions();
        for(auto& pair : collisions)
            collidePair(pair.first,pair.second,grid->collisionData());
    });

    scene->broadPhase().each<QTree>([&](QTree* tree)
    {
        tree->clearCollisions();
        scene->eachC<BoxC>(tree->groupA(),[tree](BoxC* boxA)
        {
            tree->updateBoxA(boxA);
        });

        if (tree->isDynamic())
            scene->eachC<BoxC>(tree->groupB(),[tree](BoxC* boxB)
            {
                tree->updateBoxB(boxB);
            });
        else
            tree->updateStaticBoxesB();

        scene->eachC<BoxC>(tree->groupA(), [tree](BoxC* boxA)
        {
            tree->findCollisions(boxA);
        });

        auto& collisions = tree->collisions();
        for(auto& pair : collisions)
            collidePair(pair.first,pair.second,tree->collisionData());
    });

    //Synchronise coordinates between colliding box and point, set velocity
    for( collisionPair& pair : collisions_){
        BoxC* box = pair.first;
        BoxC* boxB = pair.second;
        Entity* e = scene->getEntity(box->entityID());
        Entity* eB = scene->getEntity(boxB->entityID());
        MoveC* move = e->getComponentByPointIndex<MoveC>(box->pointIndex());
        PointC* point;
        if(!move){
            move = e->getComponent<MoveC>();
            point = e->getComponent<PointC>();
        }
        else{
            point = e->getComponent<PointC>(box->pointIndex());
        }
        point->setCoord(point->coord() - box->separation());
		box->resetSeparation();


        sf::Vector2f velB, accB;
        MoveC* moveB = eB->getComponentByPointIndex<MoveC>(boxB->pointIndex());
        if (moveB){
            velB = moveB->velocity();
            accB = moveB->acceleration();
        }
        moveB = eB->getComponent<MoveC>();
        if (moveB && boxB->pointIndex()){
            velB += moveB->velocity();
            accB += moveB->acceleration();
        }

        velB = velB * box->friction();
        accB = velB * box->friction();
        if(move){
            if(box->isTouching(Component::UP)){
                move->setVelocityY(std::min(move->velocity().y, 0.f));
                move->setAccelerationY(std::min(move->acceleration().y, 0.f));
            }

            if(box->isTouching(Component::DOWN)){
                move->setVelocityY(std::max(move->velocity().y, 0.f));
                move->setAccelerationY(std::max(move->acceleration().y, 0.f));
            }
            if(box->isTouching(Component::LEFT)){
                move->setVelocityX(std::min(move->velocity().x, 0.f));
                move->setAccelerationX(std::min(move->acceleration().x, 0.f));
            }
            if(box->isTouching(Component::RIGHT)){
                move->setVelocityX(std::max(move->velocity().x, 0.f));
                move->setAccelerationX(std::max(move->acceleration().x, 0.f));
            }
        }
    }    
    collisions_.clear();
}

void Collision::loop(CollisionCheckC* component)
{
    scene_->eachC<BoxC>(component->groupA(),[this](BoxC* box){
        if(!checked_to_save_state_boxesA_.count(box->id())){
            box->savePreviousState();
            box->clearCollisions();
        }
        checked_to_save_state_boxesA_.insert(box->id());
        Entity* e = scene_->getEntity(box->entityID());
        PointC* basepoint = e->getComponent<PointC>(0);
        PointC* point = e->getComponentByPointIndex<PointC>(box->pointIndex());
        if(!box->pointIndex()){
            box->setCoord(basepoint->coord());
            box->setPrevCoord(basepoint->prevCoord());
            box->setScale(basepoint->scale());
            box->setPrevScale(basepoint->prevScale());
        }
        else{
            box->setCoord(basepoint->coord() + point->coord() * basepoint->scale());
            box->setPrevCoord(basepoint->prevCoord() + point->prevCoord() * basepoint->prevScale());
            box->setScale(basepoint->scale() * point->scale());
            box->setPrevScale(basepoint->prevScale() * point->prevScale());
        }

    });

    if (!component->isGroupBStatic()){
        scene_->eachC<BoxC>(component->groupB(),[this](BoxC* box){
            Entity* e = scene_->getEntity(box->entityID());
            PointC* basepoint = e->getComponent<PointC>(0);
            PointC* point = e->getComponentByPointIndex<PointC>(box->pointIndex());
            if(!box->pointIndex()){
                box->setCoord(basepoint->coord());
                box->setPrevCoord(basepoint->prevCoord());
                box->setScale(basepoint->scale());
                box->setPrevScale(basepoint->prevScale());
            }
            else{
                box->setCoord(basepoint->coord() + point->coord() * basepoint->scale());
                box->setPrevCoord(basepoint->prevCoord() + point->prevCoord() * basepoint->prevScale());
                box->setScale(basepoint->scale() * point->scale());
                box->setPrevScale(basepoint->prevScale() * point->prevScale());
            }
        });
    }
    component->clearCollisions();

    if (component->broadPhaseType() == CollisionCheckC::BROADPHASE::OFF){
        scene_->eachC<BoxC>(component->groupA(), [&](BoxC* boxA)
        {
            scene_->eachC<BoxC>(component->groupB(), [&](BoxC* boxB)
            {
                collidePair(boxA,boxB,component);
            });
        });
    }

}

void Collision::collideAll(std::vector<collisionPair>::const_iterator pairs_begin, std::vector<collisionPair>::const_iterator pairs_end , CollisionCheckC* collision_data)
{
    for(auto each = pairs_begin; each != pairs_end; ++each)
        collidePair(each->first,each->second, collision_data);
}

void Collision::collidePair(BoxC* boxA, BoxC* boxB, CollisionCheckC* collision_data)
{
    sf::Vector2f separation, translationB;
    if (overlap(boxA,boxB,&separation,&translationB)){
        collision_data->registerCollision(boxA->id());
        if (collision_data->collisionResponseEnabled()){
            boxA->setCoord(boxA->coord() - separation /*+ translationB * boxA->friction()*/);
            boxA->addSeparation(separation);
            collisions_.push_back(std::make_pair(boxA,boxB));
        }
    }
}

bool Collision::overlap(BoxC* boxA, BoxC* boxB, sf::Vector2f* separation, sf::Vector2f* translation)
{
    AABB A(boxA->coord(), boxA->size() * boxA->scale());
    AABB B(boxB->coord(), boxB->size() * boxB->scale());
    AABB prevA(boxA->prevCoord(), boxA->size() * boxA->prevScale());
    AABB prevB(boxB->prevCoord(), boxB->size() * boxB->prevScale());
    sf::Vector2f vA = A.p - prevA.p,
                 vB = B.p - prevB.p,
                 v = vA - vB,
                 in(0.f, 0.f),
                 out(1.f, 1.f );

    if(boxA->scale() != boxA->prevScale() || boxB->scale() != boxB->prevScale()){
        if (v.x > 0){
            vA.x = A.max.x - prevA.max.x;
            vB.x = B.min.x - prevB.min.x;
        }
        else if(v.x < 0){
            vA.x = A.min.x - prevA.min.x;
            vB.x = B.max.x - prevB.max.x;
        }

        if(v.y > 0){
            vA.y = A.max.y - prevA.max.y;
            vB.y = B.min.y - prevB.min.y;
        }
        else if (v.y < 0){
            vA.y = A.min.y - prevA.min.y;
            vB.y = B.max.y - prevB.max.y;
        }
        v = vA - vB;
    }

    int collision_side = 0; // Left or Right < 0, Up or Down > 0, None = 0;
    float inTime = 0.f, outTime = 1.f;

    if( v.x > 0 ){
        in.x =  utils::equal(prevB.min.x, prevA.max.x)? 0 : (prevB.min.x - prevA.max.x) / v.x;
        out.x = utils::equal(prevB.max.x, prevA.min.x)? 0 : (prevB.max.x - prevA.min.x) / v.x;
    }
    else if (v.x < 0){
        in.x  = utils::equal(prevB.max.x, prevA.min.x)? 0 : (prevB.max.x - prevA.min.x) / v.x;
        out.x = utils::equal(prevB.min.x, prevA.max.x)? 0 : (prevB.min.x - prevA.max.x) / v.x;
    }
    else if(prevA.max.x < prevB.min.x || prevA.min.x > prevB.max.x){
        if (A.overlaps(B)){
            boxA->touch(Component::ANY,boxB);
            return true;
        }
        else{
            boxA->noTouching(boxB);
            return false;
        }
    }

    if( v.y > 0 ){
        in.y =  utils::equal(prevB.min.y, prevA.max.y)? 0 : (prevB.min.y - prevA.max.y) / v.y;
        out.y = utils::equal(prevB.max.y, prevA.min.y)? 0 : (prevB.max.y - prevA.min.y) / v.y;
    }
    else if (v.y < 0){
        in.y  = utils::equal(prevB.max.y, prevA.min.y)? 0 : (prevB.max.y - prevA.min.y) / v.y;
        out.y = utils::equal(prevB.min.y, prevA.max.y)? 0 : (prevB.min.y - prevA.max.y) / v.y;
    }
    else if(prevA.max.y < prevB.min.y || prevA.min.y > prevB.max.y){
        if (A.overlaps(B)){
            boxA->touch(Component::ANY,boxB);
            return true;
        }
        else{
            boxA->noTouching(boxB);
            return false;
        }
    }

    inTime = std::max(in.x,in.y);
    outTime = std::min(out.x, out.y);

    if((inTime > outTime) || (in.x < 0 && in.y < 0) || in.x > 1 || in.y > 1) {
        if (A.overlaps(B)){
            boxA->touch(Component::ANY,boxB);
            return true;
        }
        else{
            boxA->noTouching(boxB);
            return false;
        }
    }

    if (A.overlaps(B))
        boxA->touch(Component::ANY,boxB);

    if(utils::greater(in.x,in.y) || (utils::equal(in.x,in.y) && fabs(v.x) >= fabs(v.y))){
        if (v.x > 0){
            //Не застревать на углах
            if (prevA.max.y == B.min.y && boxB->isSolid(Component::UP) && !boxA->wasTouching(Component::LEFT,boxB)){
                boxA->touch(Component::UP, boxB);
                collision_side = 1;
                inTime = 0;
            }
            else if (prevA.min.y == B.max.y && boxB->isSolid(Component::DOWN) && !boxA->wasTouching(Component::LEFT,boxB)){
                boxA->touch(Component::DOWN, boxB);
                collision_side = 1;
                inTime = 0;
            }
            //
            else{
                if (boxB->isSolid(Component::LEFT)){
                    boxA->touch(Component::LEFT, boxB);
                    collision_side = -1;
                }
                else{
                    boxA->touch(Component::ANY, boxB);
                }
            }
        }
        else if (v.x < 0) {
            if (prevA.max.y == B.min.y && boxB->isSolid(Component::UP) && !boxA->wasTouching(Component::RIGHT,boxB)){
                boxA->touch(Component::UP, boxB);
                collision_side = 1;
                inTime = 0;
            }
            else if (prevA.min.y == B.max.y && boxB->isSolid(Component::DOWN) && !boxA->wasTouching(Component::RIGHT,boxB)){
                boxA->touch(Component::DOWN, boxB);
                collision_side = 1;
                inTime = 0;
            }
            else {
                if (boxB->isSolid(Component::RIGHT)){
                    boxA->touch(Component::RIGHT, boxB);
                    collision_side = -1;
                }
                else{
                    boxA->touch(Component::ANY, boxB);
                }
            }
        }
        else{
            if (A.max.x == B.min.x && boxB->isSolid(Component::LEFT)){
                boxA->touch(Component::LEFT, boxB);
                collision_side = -1;
            }
            if (A.min.x == B.max.x && boxB->isSolid(Component::RIGHT)){
                boxA->touch(Component::RIGHT, boxB);
                collision_side = -1;
            }

        }
    }
    if(utils::less(in.x,in.y) || (utils::equal(in.x,in.y) && fabs(v.x) <= fabs(v.y))){
        if (v.y > 0){
            if (prevA.max.x == B.min.x && boxB->isSolid(Component::LEFT) && !boxA->wasTouching(Component::UP,boxB)){
                boxA->touch(Component::LEFT, boxB);
                collision_side = -1;
                inTime = 0;
            }
            else if (prevA.min.x == B.max.x && boxB->isSolid(Component::RIGHT) && !boxA->wasTouching(Component::UP,boxB)){
                boxA->touch(Component::RIGHT, boxB);
                collision_side = -1;
                inTime = 0;
            }
            else {
                if (boxB->isSolid(Component::UP)){
                    boxA->touch(Component::UP, boxB);
                    collision_side = 1;
                }
                else{
                    boxA->touch(Component::ANY, boxB);
                }
            }
        }
        else if (v.y < 0){
            if (prevA.max.x == B.min.x && boxB->isSolid(Component::LEFT) && !boxA->wasTouching(Component::DOWN,boxB)){
                boxA->touch(Component::LEFT, boxB);
                collision_side = -1;
                inTime = 0;
            }
            else if (prevA.min.x == B.max.x && boxB->isSolid(Component::RIGHT) && !boxA->wasTouching(Component::DOWN,boxB)){
                boxA->touch(Component::RIGHT, boxB);
                collision_side = -1;
                inTime = 0;
            }
            else {
                if (boxB->isSolid(Component::DOWN)){
                    boxA->touch(Component::DOWN, boxB);
                    collision_side = 1;
                }
                else{
                    boxA->touch(Component::ANY, boxB);
                }
            }
        }
        else{
            if (A.max.y == B.min.y && boxB->isSolid(Component::UP)){
                boxA->touch(Component::UP, boxB);
                collision_side = 1;
            }
            if (A.min.y == B.max.y && boxB->isSolid(Component::DOWN)){
                boxA->touch(Component::DOWN, boxB);
                collision_side = 1;
            }

        }
    }

    if (collision_side > 0){
        separation->x = 0;
        separation->y = (1.f - inTime) * v.y;
        translation->y = 0;
    }
    if (collision_side < 0){
        separation->x = (1.f - inTime) * v.x;
        separation->y = 0;
        translation->x = 0;
    }
    return true;
}
