local this = {}

function this.create()
  _core:addScene("pong", "scenes/pong")
  _core:selectScene("pong")
  _core:buildScene(_scene)
end

function this.update()
  if _input:justPressed("esc") then
    _core:exitNow()
  end
  if _input:justPressed("f1") then
    _core:resetScene(_scene)
  end    
end

return this
