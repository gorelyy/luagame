#pragma once
//NEW_COMPONENT_TYPE
class QuadC;
class LineC;
class MoveC;
class ScriptC;
class AnimC;
class BoxC;
class CollisionCheckC;
class PointC;
class LayerC;
class CameraC;
class TweenCoordC;
class TweenScaleC;
class TweenAngleC;
class SoundC;
class TextC;
class TimerC;
#include "PointConnection.h"
#include "ComponentContainer.h"
#include "Pool.h"
#include <vector>
#include <deque>
#include <luabind/luabind.hpp>
extern "C"
{
    #include <lua.h>
    #include <lualib.h>
    #include <lauxlib.h>
}

template <typename cClass>
class BaseEntity{
public:
    
    BaseEntity() : disabledcount_(0)
    {
    }

    void registerComponent(const std::pair<ID,size_t>& component_address, uint point_index)
    { 
        address_container_.push_back(component_address);
        size_t size = point_index_container_.size();
        if(size < point_index){
            std::pair<size_t, size_t> point_index_offset {0,0};
            if (size)
                point_index_offset = point_index_container_.back();
            point_index_container_.resize(point_index, std::make_pair(point_index_offset.first + point_index_offset.second,0));
        }
        if (size <= point_index){
            std::pair<size_t, size_t> point_index_offset {0,0};
            for (int i = size - 1; i != -1; i--){
                auto &it = point_index_container_[i];
                if (it.second > 0){
                    point_index_offset = it;
                    break;
                }
            }
            point_index_container_.push_back(std::make_pair(point_index_offset.first + point_index_offset.second, 1));
        }
        if (size > point_index){
            point_index_container_.back().second++;
        }
    }
    void registerComponentWithPointIndex(const std::pair<ID,size_t>& component_address, uint index)
    {
        if (index >= point_index_container_.size())
        {
            std::pair<size_t, size_t> last_point_index {0,0};
            if (point_index_container_.size())
                last_point_index = point_index_container_.back();
            point_index_container_.resize(index + 1,std::make_pair(last_point_index.first + last_point_index.second,0));
        }
        auto &point_index_offset = point_index_container_[index];
        address_container_.insert(address_container_.begin() + point_index_offset.first + point_index_offset.second, component_address);
        point_index_offset.second++;
        for (size_t i = index + 1; i != point_index_container_.size(); ++i)
            point_index_container_[i].first++;
    }

    void registerComponentPointer(cClass* ptr_component)
    {
        ptr_component->setIndex(ptr_container_.size());
        ptr_component->setDisabledCounter(&disabledcount_);
        ptr_container_.push_back(ptr_component);
        if (!ptr_component->enabled())
            disabledcount_++;
    }
    cClass* getC(uint index)
    {
        if (ptr_container_.size() > 0 && index < ptr_container_.size())
            return ptr_container_[index];
        return nullptr;
    }
    template<typename Functor>
    void each(Functor& f, bool enabled)
    {
        for (auto ptr : ptr_container_)
            if(ptr->enabled() == enabled)
                f(ptr);
    }
    template<typename Functor>
    void eachAddress(Functor& f)
    {
        for (auto& address : address_container_)
            f(address);
    }
    template<typename Functor>
    void eachByPointIndex(size_t index, Functor& f, bool enabled)
    {
        if (point_index_container_.size() > 0 && index < point_index_container_.size()){
            auto &point_index_offset = point_index_container_[index];
            if (!point_index_offset.second)
                return;
            for (size_t index = point_index_offset.first, end = point_index_offset.first + point_index_offset.second ; index != end; ++index)
                if(ptr_container_[index]->enabled() == enabled)
                    f(ptr_container_[index]);
        }
    }
    template<typename Functor>
    void eachAddressByPointIndex(size_t index, Functor& f)
    {
        if (point_index_container_.size() > 0 && index < point_index_container_.size()){
            auto &point_index_offset = point_index_container_[index];
            if (!point_index_offset.second)
                return;
            for (int i = point_index_offset.first; i < point_index_offset.first + point_index_offset.second; ++i)
                f(address_container_[i]);
        }
    }

    cClass* any(bool enabled)
    {
        for (auto ptr : ptr_container_)
            if(ptr->enabled() == enabled)
                return ptr;
        return nullptr;
    }
    cClass* anyByPointIndex(size_t index, bool enabled)
    {
        if (point_index_container_.size() > 0 && index < point_index_container_.size()){
            auto &point_index_offset = point_index_container_[index];
            if (!point_index_offset.second)
                return nullptr;
            for (size_t index = point_index_offset.first, end = point_index_offset.first + point_index_offset.second ; index != end; ++index)
                if(ptr_container_[index]->enabled() == enabled)
                    return ptr_container_[index];
        }
        return nullptr;
    }
    cClass* getCByPointIndex(uint index)
    {
		if (point_index_container_.size() > 0 && index < point_index_container_.size()){
			auto &point_index_offset = point_index_container_[index];
			if (!point_index_offset.second)
				return nullptr;
			return ptr_container_[point_index_offset.first];
		}
		return nullptr;
    }
    const std::pair<ID, size_t> getAddress(uint index)
    {
        if (address_container_.size() > 0 && index < address_container_.size())
            return address_container_[index];
		return std::make_pair(0,0);
    }
    const std::pair<ID, size_t> getAddressByPointIndex(uint index)
    {
		if (point_index_container_.size() > 0 && index < point_index_container_.size()){
			auto &point_index_offset = point_index_container_[index];
			if (!point_index_offset.second)
				return std::make_pair(0, 0);
			return  address_container_[point_index_offset.first];
		}
		return std::make_pair(0, 0);
    }
    std::vector<std::pair<ID, size_t> >* getAddressContainer()
    {
        return &address_container_;
    }
    bool pointerContainerEmpty()
    {
        return ptr_container_.empty();
    }
    void clearPointerContainer()
    {
        ptr_container_.clear();
    }

    uint countExistingComponents()
    {
        return ptr_container_.size();
    }

    uint countComponents()
    {
        return ptr_container_.size() - disabledcount_;
    }

    uint countDisabledComponents()
    {
        return disabledcount_;
    }

private:
    std::vector<std::pair<ID, size_t> > address_container_;
    std::vector<std::pair<size_t,size_t> > point_index_container_;
    std::vector<cClass* > ptr_container_;
    uint disabledcount_;
};


template <typename cClass>
class LinkBaseEntity{
public:
    LinkBaseEntity() : pointer_(nullptr)
    {
    }
    
    std::shared_ptr<BaseEntity<cClass> >* getBase(){
        if (pointer_ == nullptr)
            pointer_.reset(new BaseEntity<cClass>());
        return &pointer_;
    }
    private:
    std::shared_ptr<BaseEntity<cClass> > pointer_;
};

                //NEW_COMPONENT_TYPE
class Entity:  private LinkBaseEntity<QuadC>,
               private LinkBaseEntity<LineC>,
               private LinkBaseEntity<MoveC>,
               private LinkBaseEntity<ScriptC>,
               private LinkBaseEntity<AnimC>,
               private LinkBaseEntity<BoxC>,
               private LinkBaseEntity<CollisionCheckC>,
               private LinkBaseEntity<PointC>,
               private LinkBaseEntity<LayerC>,
               private LinkBaseEntity<CameraC>,
               private LinkBaseEntity<TweenCoordC>,
               private LinkBaseEntity<TweenScaleC>,
               private LinkBaseEntity<TweenAngleC>,
               private LinkBaseEntity<SoundC>,
               private LinkBaseEntity<TextC>,
               private LinkBaseEntity<TimerC>{
public:
    Entity();
    
    template<typename cClass>
    cClass* addComponent()
    {
        if (locked_)
            return nullptr;
        cClass component;
        component.setID(id_);
        component.setLuaState(L_);
        component.setPointIndex(point_index_);
        std::pair<ID, size_t> component_address = component_container_->addComponent<cClass>(component, id_);
        (*LinkBaseEntity<cClass>::getBase())->registerComponent(component_address, point_index_);

        return component_container_->getComponent<cClass>(component_address);
    }
    
    template<>
    PointC* addComponent()
    {
        if (locked_)
            return nullptr;
        PointC component;
        component.setID(id_);
        component.setLuaState(L_);
        point_index_++;
        component.setPointIndex(point_index_);
        std::pair<ID, size_t> component_address = component_container_->addComponent<PointC>(component, id_);
        (*LinkBaseEntity<PointC>::getBase())->registerComponent(component_address, point_index_);

        return component_container_->getComponent<PointC>(component_address);
    }

    template<>
    LayerC* addComponent()
    {
        if (locked_)
            return nullptr;
        if (component_container_->poolSize<LayerC>(id_))
            return component_container_->getComponent<LayerC>(std::make_pair(id_,0));
        LayerC component;
        component.setID(id_);
        component.setLuaState(L_);
        component.setPointIndex(point_index_);
        std::pair<ID, size_t> component_address = component_container_->addComponent<LayerC>(component, id_);
        (*LinkBaseEntity<LayerC>::getBase())->registerComponent(component_address, point_index_);

        return component_container_->getComponent<LayerC>(component_address);
    }

    template<typename cClass>
    cClass* addComponent(Pool* pool, bool usermode)
    {
        if (locked_)
            return nullptr;
        if (!pool->hasFreeSpace<cClass>() && usermode)
            return nullptr;
        cClass component;
        component.setID(id_);
        component.setLuaState(L_);
        component.setPointIndex(point_index_);
        std::pair<ID, size_t> component_address = component_container_->addComponent<cClass>(component, pool->id());
        (*LinkBaseEntity<cClass>::getBase())->registerComponent(component_address, point_index_);

        return component_container_->getComponent<cClass>(component_address);
    }

    template<typename cClass>
    cClass* addComponent(Pool* pool)
    {
        return addComponent<cClass>(pool, true);
    }

    template<>
    PointC* addComponent(Pool* pool, bool usermode)
    {
        if (locked_)
            return nullptr;
        if (!pool->hasFreeSpace<PointC>() && usermode)
            return nullptr;
        PointC component;
        component.setID(id_);
        component.setLuaState(L_);
        point_index_++;
        component.setPointIndex(point_index_);
        std::pair<ID, size_t> component_address = component_container_->addComponent<PointC>(component, pool->id());
        (*LinkBaseEntity<PointC>::getBase())->registerComponent(component_address, point_index_);

        return component_container_->getComponent<PointC>(component_address);
    }

    template<>
    PointC* addComponent(Pool* pool)
    {
        return addComponent<PointC>(pool, true);
    }

    template<>
    LayerC* addComponent(Pool* pool)
    {
        return nullptr;
    }

    template<typename cClass>
    cClass* addComponentWithPointIndex(size_t index)
    {
        if (locked_)
            return nullptr;
        cClass component;
        component.setID(id_);
        component.setLuaState(L_);
        component.setPointIndex(index);
        std::pair<ID, size_t> component_address = component_container_->addComponent<cClass>(component, id_);
        (*LinkBaseEntity<cClass>::getBase())->registerComponentWithPointIndex(component_address, index);
        return component_container_->getComponent<cClass>(component_address);
    }

    template<>
    LayerC* addComponentWithPointIndex(size_t index)
    {
        if (locked_)
            return nullptr;
        if (component_container_->poolSize<LayerC>(id_))
            return component_container_->getComponent<LayerC>(std::make_pair(id_,0));
        LayerC component;
        component.setID(id_);
        component.setLuaState(L_);
        component.setPointIndex(index);
        std::pair<ID, size_t> component_address = component_container_->addComponent<LayerC>(component, id_);
        (*LinkBaseEntity<LayerC>::getBase())->registerComponentWithPointIndex(component_address, index);
        return component_container_->getComponent<LayerC>(component_address);
    }

    template<typename cClass>
    cClass* addComponentWithPointIndex(Pool* pool, size_t index, bool usermode)
    {
        if (locked_)
            return nullptr;
        if (!pool->hasFreeSpace<cClass>() && usermode)
            return nullptr;
        cClass component;
        component.setID(id_);
        component.setLuaState(L_);
        component.setPointIndex(index);
        std::pair<ID, size_t> component_address = component_container_->addComponent<cClass>(component, pool->id());
        (*LinkBaseEntity<cClass>::getBase())->registerComponentWithPointIndex(component_address, index);
        return component_container_->getComponent<cClass>(component_address);
    }

    template<typename cClass>
    cClass* addComponentWithPointIndex(Pool* pool, size_t index)
    {
        return addComponentWithPointIndex<cClass>(pool, index, true);
    }

    template<>
    LayerC* addComponentWithPointIndex(Pool* pool, size_t index)
    {
        return nullptr;
    }
    template<typename cClass>
    cClass * getComponent()
    {
        if ((*LinkBaseEntity<cClass>::getBase())->pointerContainerEmpty())
            return component_container_->getComponent<cClass>((*LinkBaseEntity<cClass>::getBase())->getAddress(0));
        else
            return (*LinkBaseEntity<cClass>::getBase())->getC(0);
    }
    template<typename cClass>
    cClass * getComponent(uint index)
    {
        if ((*LinkBaseEntity<cClass>::getBase())->pointerContainerEmpty())
            return component_container_->getComponent<cClass>((*LinkBaseEntity<cClass>::getBase())->getAddress(index));
        else
            return (*LinkBaseEntity<cClass>::getBase())->getC(index);
    }
    template<typename cClass>
    cClass * getComponentByPointIndex(uint index)
    {
        if ((*LinkBaseEntity<cClass>::getBase())->pointerContainerEmpty())
            return component_container_->getComponent<cClass>((*LinkBaseEntity<cClass>::getBase())->getAddressByPointIndex(index));
        else
            return (*LinkBaseEntity<cClass>::getBase())->getCByPointIndex(index);
    }

    template<typename cClass>
    void registerComponentPointer(cClass* ptr_component)
    {
        (*LinkBaseEntity<cClass>::getBase())->registerComponentPointer(ptr_component);
    }
    template<typename cClass,typename Functor>
    void each(const Functor& f)
    {
        if ((*LinkBaseEntity<cClass>::getBase())->pointerContainerEmpty())
            ((*LinkBaseEntity<cClass>::getBase())->eachAddress([&](const std::pair<ID, size_t>& address)
            {
                cClass* component = component_container_->getComponent<cClass>(address);
                if (component && component->enabled())
                    f(component);
            }));
        else
            (*LinkBaseEntity<cClass>::getBase())->each(f, true);
    }
    template<typename cClass>
    void each(const luabind::object& f)
    {
        luafunction_stack_.push_back(f);
        if ((*LinkBaseEntity<cClass>::getBase())->pointerContainerEmpty())
            ((*LinkBaseEntity<cClass>::getBase())->eachAddress([&](const std::pair<ID, size_t>& address)
            {
                cClass* component = component_container_->getComponent<cClass>(address);
                if (component && component->enabled())
                    luafunction_stack_.back()(component);
            }));
        else
            (*LinkBaseEntity<cClass>::getBase())->each(luafunction_stack_.back(), true);
        luafunction_stack_.pop_back();
    }
    template<typename cClass,typename Functor>
    void eachDisabled(const Functor& f)
    {
        if ((*LinkBaseEntity<cClass>::getBase())->pointerContainerEmpty())
            ((*LinkBaseEntity<cClass>::getBase())->eachAddress([&](const std::pair<ID, size_t>& address)
            {
                cClass* component = component_container_->getComponent<cClass>(address);
                if (component && !component->enabled())
                    f(component);
            }));
        else
            (*LinkBaseEntity<cClass>::getBase())->each(f, false);
    }
    template<typename cClass>
    void eachDisabled(const luabind::object& f)
    {
        luafunction_stack_.push_back(f);
        if ((*LinkBaseEntity<cClass>::getBase())->pointerContainerEmpty())
            ((*LinkBaseEntity<cClass>::getBase())->eachAddress([&](const std::pair<ID, size_t>& address)
            {
                cClass* component = component_container_->getComponent<cClass>(address);
                if (component && !component->enabled())
                    luafunction_stack_.back()(component);
            }));
        else
            (*LinkBaseEntity<cClass>::getBase())->each(luafunction_stack_.back(), false);
        luafunction_stack_.pop_back();
    }
    template<typename cClass, typename Functor>
    void eachByPointIndex(size_t index, const Functor& f)
    {
        if ((*LinkBaseEntity<cClass>::getBase())->pointerContainerEmpty())
            ((*LinkBaseEntity<cClass>::getBase())->eachAddressByPointIndex(index,[&](const std::pair<ID, size_t>& address)
            {
                cClass* component = component_container_->getComponent<cClass>(address);
                if (component && component->enabled())
                    f(component);
            }));
        else
            (*LinkBaseEntity<cClass>::getBase())->eachByPointIndex(index,f,true);
    }
    template<typename cClass>
    void eachByPointIndex(size_t index, const luabind::object& f)
    {
        luafunction_stack_.push_back(f);
        if ((*LinkBaseEntity<cClass>::getBase())->pointerContainerEmpty())
            ((*LinkBaseEntity<cClass>::getBase())->eachAddressByPointIndex(index, [&](const std::pair<ID, size_t>& address)
            {
                cClass* component = component_container_->getComponent<cClass>(address);
                if (component && component->enabled())
                    luafunction_stack_.back()(component);
            }));
        else
            (*LinkBaseEntity<cClass>::getBase())->eachByPointIndex(index,luafunction_stack_.back(),true);
        luafunction_stack_.pop_back();
    }
    template<typename cClass, typename Functor>
    void eachDisabledByPointIndex(size_t index, const Functor& f)
    {
        if ((*LinkBaseEntity<cClass>::getBase())->pointerContainerEmpty())
            ((*LinkBaseEntity<cClass>::getBase())->eachAddressByPointIndex(index, [&](const std::pair<ID, size_t>& address)
            {
                cClass* component = component_container_->getComponent<cClass>(address);
                if (component && !component->enabled())
                    f(component);
            }));
        else
            (*LinkBaseEntity<cClass>::getBase())->eachByPointIndex(index,f,false);
    }
    template<typename cClass>
    void eachDisabledByPointIndex(size_t index, const luabind::object& f)
    {
        luafunction_stack_.push_back(f);
        if ((*LinkBaseEntity<cClass>::getBase())->pointerContainerEmpty())
            ((*LinkBaseEntity<cClass>::getBase())->eachAddressByPointIndex(index,[&](const std::pair<ID, size_t>& address)
            {
                cClass* component = component_container_->getComponent<cClass>(address);
                if (component && !component->enabled())
                    luafunction_stack_.back()(component);
            }));
        else
            (*LinkBaseEntity<cClass>::getBase())->eachByPointIndex(index,luafunction_stack_.back(),false);
        luafunction_stack_.pop_back();
    }
    template<typename cClass>
    cClass* any()
    {
        if ((*LinkBaseEntity<cClass>::getBase())->pointerContainerEmpty()){
            cClass* result = nullptr;
            ((*LinkBaseEntity<cClass>::getBase())->eachAddress([&](const std::pair<ID, size_t>& address)
            {
                if (result)
                    return;
                cClass* component = component_container_->getComponent<cClass>(address);
                if (component && component->enabled())
                    result = component;
            }));
            return result;
        }
        else
            return (*LinkBaseEntity<cClass>::getBase())->any(true);
    }
    template<typename cClass>
    cClass* anyDisabled()
    {
        if ((*LinkBaseEntity<cClass>::getBase())->pointerContainerEmpty()){
            cClass* result = nullptr;
            ((*LinkBaseEntity<cClass>::getBase())->eachAddress([&](const std::pair<ID, size_t>& address)
            {
                if (result)
                    return;
                cClass* component = component_container_->getComponent<cClass>(address);
                if (component && !component->enabled())
                    result = component;
            }));
            return result;
        }
        else
            return (*LinkBaseEntity<cClass>::getBase())->any(false);
    }
    template<typename cClass>
    cClass* anyByPointIndex(size_t index)
    {
        if ((*LinkBaseEntity<cClass>::getBase())->pointerContainerEmpty()){
            cClass* result = nullptr;
            ((*LinkBaseEntity<cClass>::getBase())->eachAddressByPointIndex(index, [&](const std::pair<ID, size_t>& address)
            {
                if (result)
                    return;
                cClass* component = component_container_->getComponent<cClass>(address);
                if (component && component->enabled())
                    result = component;
            }));
            return result;
        }
        else
            return (*LinkBaseEntity<cClass>::getBase())->anyByPointIndex(index,true);
    }
    template<typename cClass>
    cClass* anyDisabledByPointIndex(size_t index)
    {
        if ((*LinkBaseEntity<cClass>::getBase())->pointerContainerEmpty()){
            cClass* result = nullptr;
            ((*LinkBaseEntity<cClass>::getBase())->eachAddressByPointIndex(index, [&](const std::pair<ID, size_t>& address)
            {
                if (result)
                    return;
                cClass* component = component_container_->getComponent<cClass>(address);
                if (component && !component->enabled())
                    result = component;
            }));
            return result;
        }
        else
            return (*LinkBaseEntity<cClass>::getBase())->anyByPointIndex(index,false);
    }
    template <typename cClass>
    void clearPointerContainer()
    {
        (*LinkBaseEntity<cClass>::getBase())->clearPointerContainer();
    }

    template <typename cClass>
    uint count()
    {
        return (*LinkBaseEntity<cClass>::getBase())->countComponents();
    }

    template <typename cClass>
    uint countExisting()
    {
        return (*LinkBaseEntity<cClass>::getBase())->countExistingComponents();
    }

    template <typename cClass>
    uint countDisabled()
    {
        return (*LinkBaseEntity<cClass>::getBase())->countDisabledComponents();
    }

    template <typename cClass>
    void finalizeComponentPool()
    {
        if (!LinkBaseEntity<cClass>::getBase())
            return;
        std::vector<std::pair<ID, size_t> >* address_list = (*LinkBaseEntity<cClass>::getBase())->getAddressContainer();
        if(!address_list)
            return;
        clearPointerContainer<cClass>();
        component_container_->shrinkToFitComponentPool<cClass>(id_);
        for(auto address : *address_list)
            registerComponentPointer<cClass>(component_container_->getComponent<cClass>(address));
        address_list->clear();
    }

    ID id()                  const;
    bool isLocked()          const;
    int  pointIndex()        const;
    lua_State*  luaState()   const;
    ScriptC* parentScript()  const;
    const std::vector<Group*>& groupRegistry() const;

    void    setID(ID id);
    void    setPointIndex(int i);
    void    lock();
    void    unlock();
    void    bindLua(lua_State * L);
    void    bindComponentContainer(ComponentContainer* cc);
    void    finalizeAllComponentPools();
    void    bindPointConnection(PointConnection* pc);
    void    setParentScript(ScriptC* script);
    void    addFollower(Entity* follower, const sf::Vector2f& distance, FollowData::FollowType mode);
    void    addFollower(Entity* follower, const sf::Vector2f& distance, FollowData::FollowType mode, float easing);
    void    addFollower(Entity* follower, const sf::Vector2f& distance, FollowData::FollowType mode, const luabind::object& callback);
    void    addFollower(Entity* follower, const sf::Vector2f& distance, FollowData::FollowType mode, float easing, const luabind::object& callback);
    void    addFollower(Group* group, const sf::Vector2f& distance, FollowData::FollowType mode);
    void    addFollower(Group* group, const sf::Vector2f& distance, FollowData::FollowType mode, float easing);
    void    addFollower(Group* group, const sf::Vector2f& distance, FollowData::FollowType mode, const luabind::object& callback);
    void    addFollower(Group* group, const sf::Vector2f& distance, FollowData::FollowType mode, float easing, const luabind::object& callback);
    void    setLeader(Entity* leader, const sf::Vector2f& distance, FollowData::FollowType mode);
    void    setLeader(Entity* leader, const sf::Vector2f& distance, FollowData::FollowType mode, float easing);
    void    setLeader(Entity* leader, const sf::Vector2f& distance, FollowData::FollowType mode, const luabind::object& callback);
    void    setLeader(Entity* leader, const sf::Vector2f& distance, FollowData::FollowType mode, float easing, const luabind::object& callback);
    void    setLeader(PointC* leader, const sf::Vector2f& distance, FollowData::FollowType mode);
    void    setLeader(PointC* leader, const sf::Vector2f& distance, FollowData::FollowType mode, float easing);
    void    setLeader(PointC* leader, const sf::Vector2f& distance, FollowData::FollowType mode, const luabind::object& callback);
    void    setLeader(PointC* leader, const sf::Vector2f& distance, FollowData::FollowType mode, float easing, const luabind::object& callback);
    void    addChild(Entity* child);
    void    addChild(Group* group);
    void    setParent(Entity* parent);
    void    setParent(PointC* parent);
    void    removeChild(Entity* child);
    void    removeFollower(Entity* follower);

private:    
    lua_State* L_;
    std::deque<luabind::object> luafunction_stack_;
    ComponentContainer* component_container_;
    PointConnection* pointconnection_;
    ScriptC* parentscript_;
    int point_index_;
    ID id_;
    bool locked_;
};


