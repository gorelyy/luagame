#include "luagame/components/BoxC.h"
#include "luagame/Group.h"

BoxC::BoxC() : 
    friction_(1.f),
    scale_(sf::Vector2f(1,1)),
    prev_scale_(sf::Vector2f(1,1)),
    qtreenode_(nullptr)
{
    setSolidSide(ALL);
    cell_[0] = -1;
    cell_[1] = -1;
    cell_[2] = -1;
    cell_[3] = -1;
}

float BoxC::width() const
{
    return size_.x;
}

float BoxC::height() const
{
    return size_.y;
}

void BoxC::setSize(const sf::Vector2f& size)
{
    size_ = size;
}

void BoxC::touch(Component::SIDE side, BoxC*  box)
{
    if (side == Component::NONE && !collisions_.empty()){
        auto it = std::find_if(collisions_.begin(), collisions_.end(),[box](std::pair<BoxC*, Component::SIDE>const& pair)
        {
            return pair.first == box;
        });
        if (it != collisions_.end()){
            std::iter_swap(it,collisions_.end() - 1);
            collisions_.pop_back();
        }
    }
    else{
        auto it = std::find_if(collisions_.begin(), collisions_.end(),[box](std::pair<BoxC*, Component::SIDE>const& pair)
        {
            return pair.first == box;
        });
        if (it != collisions_.end())
            it->second = side;
        else
            collisions_.push_back(std::make_pair(box,side));
    }
}

Component::SIDE BoxC::touchingSide(BoxC*  box)
{
    auto it = std::find_if(collisions_.begin(), collisions_.end(),[box](std::pair<BoxC*, Component::SIDE>const& pair)
    {
        return pair.first == box;
    });

    if (it != collisions_.end())
        return it->second;
    return Component::NONE;
}

bool BoxC::isTouching() const
{
    return !collisions_.empty();
}

bool BoxC::wasTouching() const
{
    return  !prev_collisions_.empty();
}

bool BoxC::isTouching(Group *group) const
{    
    for(auto each = collisions_.cbegin(), end = collisions_.cend(); each != end; ++each)
        if(group && group->has(each->first->entityID()))
            return true;
    return false;
}

bool BoxC::wasTouching(Group *group) const
{
    for(auto each = prev_collisions_.cbegin(), end = prev_collisions_.cend(); each != end; ++each)
        if(group && group->has(each->first->entityID()))
            return true;
    return false;
}

bool BoxC::isTouching(BoxC*  box) const
{
    auto it = std::find_if(collisions_.begin(), collisions_.end(),[box](std::pair<BoxC*, Component::SIDE>const& pair)
    {
        return pair.first == box;
    });
    return it != collisions_.end();
}

bool BoxC::wasTouching(BoxC*  box) const
{
    auto it = std::find_if(prev_collisions_.begin(), prev_collisions_.end(),[box](std::pair<BoxC*, Component::SIDE>const& pair)
    {
        return pair.first == box;
    });
    return it != prev_collisions_.end();
}

bool BoxC::isTouching(Component::SIDE side) const
{
    for(auto each = collisions_.cbegin(), end = collisions_.cend(); each != end; ++each)
        if (each->second == side) 
            return true;
    return false;
}

bool BoxC::wasTouching(Component::SIDE side) const
{
    if (!prev_collisions_.empty())
        for(auto each = prev_collisions_.cbegin(), end = prev_collisions_.cend(); each != end; ++each)
            if (each->second == side)
                return true;
    return false;
}

bool BoxC::isTouching(Component::SIDE side, Group *group) const
{
    if (!collisions_.empty())
        for(auto each = collisions_.cbegin(), end = collisions_.cend(); each!= end; ++each)
            if (group && group->has(each->first->entityID()) && each->second == side)
                return true;
    return false;
}

bool BoxC::wasTouching(Component::SIDE side, Group *group) const
{
    if (!prev_collisions_.empty())
        for(auto each = prev_collisions_.cbegin(), end = prev_collisions_.cend(); each != end; ++each)
            if (group && group->has(each->first->entityID()) && each->second == side)
                return true;
    return false;
}
bool BoxC::isTouching(Component::SIDE side, BoxC*  box)
{
    auto it = std::find_if(collisions_.begin(), collisions_.end(),[box](std::pair<BoxC*, Component::SIDE>const& pair)
    {
        return pair.first == box;
    });
    if (it != collisions_.end())
        return it->second == side;
    return false;
}

bool BoxC::wasTouching(Component::SIDE side, BoxC*  box)
{
    auto it = std::find_if(prev_collisions_.begin(), prev_collisions_.end(),[box](std::pair<BoxC*, Component::SIDE>const& pair)
    {
        return pair.first == box;
    });
    if (it != prev_collisions_.end())
        return it->second == side;
    return false;
}

bool BoxC::startTouching() const
{
    return isTouching() && !wasTouching();
}

bool BoxC::stopTouching() const
{
    return !isTouching() && wasTouching();
}

bool BoxC::startTouching(Group *group) const
{
    return isTouching(group) && !wasTouching(group);
}

bool BoxC::stopTouching(Group *group) const
{
    return !isTouching(group) && wasTouching(group);
}

bool BoxC::startTouching(BoxC*  box) const
{
    return isTouching(box) && !wasTouching(box);
}

bool BoxC::stopTouching(BoxC*  box) const
{
    return !isTouching(box) && wasTouching(box);
}

bool BoxC::startTouching(Component::SIDE side) const
{
    return isTouching(side) && !wasTouching(side);
}

bool BoxC::stopTouching(Component::SIDE side) const
{
    return !isTouching(side) && wasTouching(side);
}

bool BoxC::startTouching(Component::SIDE side, Group *group) const
{
    return isTouching(side, group) && !wasTouching(side, group);
}

bool BoxC::stopTouching(Component::SIDE side, Group *group) const
{
    return !isTouching(side, group) && wasTouching(side, group);
}

bool BoxC::startTouching(Component::SIDE side, BoxC*  box)
{
    return isTouching(side, box) && !wasTouching(side,box);
}

bool BoxC::stopTouching(Component::SIDE side, BoxC*  box)
{
    return !isTouching(side, box) && wasTouching(side,box);
}

void BoxC::noTouching(BoxC*  box)
{
    if(collisions_.empty())
        return;
    auto it = std::find_if(collisions_.begin(), collisions_.end(),[box](std::pair<BoxC*, Component::SIDE>const& pair)
    {
        return pair.first == box;
    });
    if (it != collisions_.end()){
        std::iter_swap(it,collisions_.end() - 1);
        collisions_.pop_back();
    }
}

void BoxC::noTouching()
{
    collisions_.clear();
}

uint BoxC::collisionSize() const
{
    return collisions_.size();
}

void BoxC::setSolidSide(Component::SIDE side)
{
    int8_t side_value = (int8_t)side;
    if (side_value > 3){
        for (int i = 0; i != 4; ++i)
            openside_[i] = -1;
        openside_[0] = side_value;
    }
    else if (openside_[0] > 3)
        openside_[0] = side_value;
    else{
        for (int i = 0; i !=4; ++i)
            if (openside_[i] < 0){
                openside_[i] = side_value;
                break;
            }
    }
}

void BoxC::setSolidSide(const luabind::object& side_list)
{
    for(luabind::iterator each_side(side_list),end; each_side != end; ++each_side)
        setSolidSide(luabind::object_cast<Component::SIDE>(*each_side));
}


bool BoxC::isSolid(Component::SIDE side) const
{
    if(side == NONE && openside_[0] == (int8_t)NONE)
        return true;

    if (openside_[0] != (int8_t)NONE){
        if (openside_[0] == ALL)
            return true;
        for (int i = 0; i != 4; ++i)
            if(openside_[i] == (int8_t)side)
                return true;
    }
    return false;
}

const sf::Vector2f& BoxC::size() const
{
    return size_;
}

float BoxC::friction() const
{
    return friction_;
}

void BoxC::eachBoxB(const luabind::object& luafunction)
{
    luabind::object f = luafunction;
    for(auto each = collisions_.begin(); each != collisions_.end(); ++each)
        f(each->first);
}

void BoxC::savePreviousState()
{
    prev_collisions_ = collisions_;
}

const std::array<int,4>&  BoxC::cell() const
{
    return cell_;
}

const std::array<int,4>&  BoxC::newCell(const sf::IntRect& area, const sf::Vector2f& cellsize_f)
{
    sf::Vector2i cellsize((int)cellsize_f.x, (int)cellsize_f.y);
    int width = area.width / cellsize.x;
    sf::Vector2i corner[4];
    corner[0] = sf::Vector2i(coord_.x - area.left, coord_.y - area.top);
    corner[1] = sf::Vector2i(coord_.x + size_.x * scale_.x - area.left, coord_.y - area.top);
    corner[2] = sf::Vector2i(coord_.x + size_.x * scale_.x - area.left, coord_.y + size_.y * scale_.y - area.top);
    corner[3] = sf::Vector2i(coord_.x - area.left, coord_.y + size_.y * scale_.y - area.top);
    
    for(int i = 0; i != 4; ++i){
        if (corner[i].x >= 0 && corner[i].x <= area.left + area.width && corner[i].y >= 0 && corner[i].y <= area.top + area.height)
            cell_[i] = (corner[i].y / cellsize.y) * width + corner[i].x / cellsize.x;
        else
            cell_[i] = -1;
    }
    
    return cell_;
}

const sf::Vector2f& BoxC::coord() const
{
    return coord_;
}

const sf::Vector2f& BoxC::prevCoord() const
{
    return prevcoord_;
}

void BoxC::setCoord(const sf::Vector2f &coord)
{
    coord_ = coord;
}

void BoxC::setPrevCoord(const sf::Vector2f &coord)
{
    prevcoord_ = coord;
}

BoxC* BoxC::anyBoxB() const
{
    if (!collisions_.size())
        return nullptr;
    return collisions_.begin()->first;
}

void BoxC::saveCoord()
{
    prevcoord_ = coord_;
}

void BoxC::addSeparation(const sf::Vector2f &separation)
{
    separation_ += separation;
}

void BoxC::resetSeparation()
{
    separation_.x = 0.f;
    separation_.y = 0.f;
}

const sf::Vector2f& BoxC::separation() const
{
    return separation_;
}

void BoxC::setScale(const sf::Vector2f& scale)
{
    scale_ = scale;
}

const sf::Vector2f& BoxC::scale() const
{
    return scale_;
}

void BoxC::setPrevScale(const sf::Vector2f &scale)
{
    prev_scale_ = scale;
}

const sf::Vector2f& BoxC::prevScale() const
{
    return prev_scale_;
}

void BoxC::setQTreeNode(QTreeNode *node)
{
    qtreenode_ = node;
}

QTreeNode* BoxC::getQTreeNode() const
{
    return qtreenode_;
}

void BoxC::clearCollisions()
{
    collisions_.clear();
}
