#pragma once
#include "../Component.h"
#include <vector>

class ScriptC: public Component {
public:
    ScriptC();
    void    load(const std::string& script_name);
    void    load(const std::string& script_name,const luabind::object &args);
    void    update();
    void    postUpdate();
    void    create();
    void    call(const std::string& name);
    void    call(const std::string& name, const luabind::object& args);
    luabind::object get(const std::string& name);
private:
    luabind::object script_;
    luabind::object create_args_;
    bool created_;
};
