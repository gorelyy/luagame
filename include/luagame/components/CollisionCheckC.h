#pragma once
#include "../Component.h"
#include <luabind/luabind.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/System/Vector2.hpp>
#include <vector>
#include <deque>
#include <string>

class CollisionCheckC: public Component {
public:
    enum BROADPHASE{
        GRID,
        STATICGRID,
        QUADTREE,
        STATICQUADTREE,
        OFF
    };
    enum DRAWDEBUGMODE{
        AABB,
        BROAD,
        ALL,
        NONE
    };
    CollisionCheckC();

    uint    treeDepth()                const;
    bool    collisionResponseEnabled() const;
    const sf::IntRect&  collisionArea()const;
    const sf::Vector2f& cellSize()     const;
    const std::string&  groupA()       const;
    const std::string&  groupB()       const;
    const sf::Color& aabbColor()       const;
    const sf::Color& broadphaseColor() const;
    BROADPHASE   broadPhaseType()      const;
    DRAWDEBUGMODE drawDebugMode()      const;
    bool    isGroupBStatic()           const;
    void    setTilemapGroup(const std::string& group);
    void    registerCollision(const componentID& boxA);
    void    clearCollisions();
    void    eachBoxA(const luabind::object& luafunction);
    void    setCollisionGroups(const std::string& groupA, const std::string& groupB);
    void    setCollisionArea(int left, int top, int width, int height);
    void    setTreeDepth(uint depth);
    void    setCollisionResponse(bool status);
    void    setBroadPhaseType(BROADPHASE type);
    void    setDrawDebugMode(DRAWDEBUGMODE mode);
    void    setGridCellSize(const sf::Vector2f& size);
    void    setAABBDebugColor(const sf::Color& color);
    void    setBroadPhaseDebugColor(const sf::Color& color);
    void    setDebugColor(const sf::Color& color);
private:  
    uint treedepth_;
    bool collision_response_status_;
    bool groupBstatic_;
    std::string groupA_;
    std::string groupB_;
    std::vector<componentID> collisions_list_;
    std::deque<luabind::object> luafunction_stack_;
    sf::IntRect collision_area_;
    sf::Vector2f cell_size_;
    BROADPHASE broad_phase_type_;
    DRAWDEBUGMODE draw_debug_mode_;
    sf::Color aabbdebugcolor_;
    sf::Color broadphasedebugcolor_;
};


