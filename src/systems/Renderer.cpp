#include "luagame/systems/Renderer.h"
#include "luagame/Group.h"
#include "luagame/Scene.h"
#include "luagame/components/TweenCoordC.h"
#include "luagame/components/TweenScaleC.h"
#include "luagame/components/TweenAngleC.h"
Renderer::Renderer() 
{
}

void Renderer::update(Scene* scene, float dt, float interpolation, float frame_time)
{
    scene_ = scene;
    interpolation_ = interpolation;
    frame_time_ = frame_time;
    dt_ = dt;

    scene->eachC<TweenCoordC>([this](TweenCoordC* tween){
        updateTweenCoord(tween);
    });

    scene->eachC<TweenScaleC>([this](TweenScaleC* tween){
        updateTweenScale(tween);
    });

    scene->eachC<TweenAngleC>([this](TweenAngleC* tween){
        updateTweenAngle(tween);
    });

    scene->eachC<LayerC>([this](LayerC* layer){
        interpolateCoordinates(layer);
    });

    //Update animation
    scene->eachC<AnimC>([&](AnimC* animation)
    {
        animation->update(frame_time_);
        Entity* entity = scene->getEntity(animation->entityID());
        QuadC* quad = entity->getComponentByPointIndex<QuadC>(animation->pointIndex());
        quad->setTextureRect(animation->textureRect());
        LayerC* layer = entity->getComponent<LayerC>();
        if (layer && !layer->isDynamic()){
            std::vector<sf::Vertex>& vertex_array = scene->getVertexArray(layer->poolID());
            for(int i = 0; i != 4; ++i){
                sf::Vertex& vertex = vertex_array[quad->poolIndex() * 4 + i];
                vertex.texCoords = quad->getVertexTextureCoord(i);
            }
        }

    });

    //Syncronise offset in parent-child entities
    scene->eachParentingConnection([&](componentID parent, componentID child)
    {
        Entity* parententity = scene->getEntity(parent.first);
        Entity* childentity = scene->getEntity(child.first);

        sf::Vector2f parentoffset;
        LayerC* parentlayer = parententity->getComponent<LayerC>();
        if (parentlayer){
            parentoffset = parentlayer->offsetCoord();
        }

        LayerC* childlayer = childentity->getComponent<LayerC>();
        if (childlayer){
            if (!childlayer->isDynamic()){
                childlayer->setOffsetCoord(childlayer->offsetCoord() + parentoffset - scene->parentOffset(parent));
                childlayer->setOffsetAngle(parentlayer->offsetAngle());
            }
            else{
                childentity->each<QuadC>([&](QuadC* quad)
                {
                    quad->setOffsetCoord(quad->offsetCoord() + parentoffset - scene->parentOffset(parent));
                    quad->setOffsetAngle(parentlayer->offsetAngle());
                });

                childentity->each<LineC>([&](LineC* line)
                {
                    line->setOffsetCoord(line->offsetCoord() + parentoffset - scene->parentOffset(parent));
                    line->setOffsetAngle(parentlayer->offsetAngle());
                });
            }
        }
        scene->storeParentOffset(parent,parentoffset);
    });


    window_->setView(window_->getDefaultView());
    window_->clear(windowcolor_);

    scene->queue(Scene::RENDER_CAMERA)->deepSort();
    scene->eachC<CameraC>(scene->queue(Scene::RENDER_CAMERA), [this](CameraC* camera)
    {
        updateCamera(camera);
    });

    scene->eachC<LayerC>(scene->group(Group::RENDER_TO_WINDOW),[&](LayerC* layer){
        std::vector<sf::Vertex>& vertex_array = scene->getVertexArray(layer->poolID());
        sf::RenderStates render;
        if(!vertex_array.size())
            return;
        scene->eachC<QuadC>(layer->poolID(),[&](QuadC* quad)
        {
            for(int i = 0; i != 4; ++i){
                sf::Vertex& vertex = vertex_array[quad->poolIndex() * 4 + i];
                vertex.position = quad->getVertexCoord(i,sf::Vector2f(),layer->coord(), layer->offsetCoord(),layer->scale() * layer->offsetScale(), layer->angle() + layer->offsetAngle(), layer->pivot());
                vertex.texCoords = quad->getVertexTextureCoord(i);
                vertex.color = quad->isVisible()?quad->color() : sf::Color::Transparent;
            }
        });

        sf::Transform transform;
        transform.translate(layer->coord() + layer->offsetCoord());
        transform.scale(layer->scale());
        transform.rotate(layer->angle() + layer->offsetAngle());
        render.transform = transform;
        render.texture = layer->texture();
        window_->draw(&vertex_array[0],vertex_array.size(),layer->type(), render);
    });
    window_->display();
}

void Renderer::setWindow(sf::RenderWindow* window, const sf::Color& color)
{
    window_ = window;
    windowcolor_ = color;
}

void Renderer::updateCamera(CameraC* camera)
{
    camera->updateBounds();
    camera->setWindowSize(sf::Vector2f(window_->getSize().x,window_->getSize().y));
    camera_bounds_ = camera->bounds();
    Entity* focus_entity = scene_->getEntity(camera->focusID());
    PointC* focus = focus_entity->getComponent<PointC>();
    LayerC* focuslayer = focus_entity->getComponent<LayerC>();
    camera->setViewCoord(focus->prevCoord() +  ((focus->coord() - focus->prevCoord()) * interpolation_) + focuslayer->offsetCoord());

    if (camera->mode() == CameraC::WINDOW){
        window_->setView(camera->view());
        window_->draw(camera->backgroundRectangle());
    }
    else{
        camera->renderTexture().clear(camera->backgroundColor());
        camera->renderTexture().setView(camera->view());
    }

    camera->queue()->deepSort();
    camera->queue()->each([&](ID id){
        scene_->eachC<LayerC>(id,[&](LayerC* layer)
        {
            drawCall(layer,camera);
        });
    });
    if (camera->mode() != CameraC::WINDOW)
        camera->renderTexture().display();
}

void Renderer::drawCall(LayerC* layer, CameraC* camera)
{
    std::vector<sf::Vertex>& vertex_array = scene_->getVertexArray(layer->poolID());
    sf::RenderStates render;
    render.blendMode = camera->blendMode();

    if (layer->isDynamic()){
        scene_->eachC<QuadC>(layer->poolID(),[&](QuadC* quad)
        {
            for(int i = 0; i != 4; ++i){
                sf::Vertex& vertex = vertex_array[quad->poolIndex() * 4 + i];
                vertex.position = quad->getVertexCoord(i,camera->scrollOffset(),layer->coord(), layer->offsetCoord(), layer->scale() * layer->offsetScale(), layer->angle() + layer->offsetAngle(), layer->pivot());
                vertex.texCoords = quad->getVertexTextureCoord(i);
                vertex.color = quad->isVisible()?quad->color() : sf::Color::Transparent;
            }
        });

        scene_->eachC<LineC>(layer->poolID(),[&](LineC* line)
        {
            for(int i = 0; i != 2; ++i){
                sf::Vertex& vertex = vertex_array[line->poolIndex() * 2 + i];
                vertex.position = line->getVertexCoord(i,camera->scrollOffset(),layer->coord(), layer->offsetCoord(), layer->scale() * layer->offsetScale()/*layerscale*/, layer->angle() + layer->offsetAngle(), layer->pivot());
                vertex.texCoords = line->getVertexTextureCoord(i);
                vertex.color = line->isVisible()?line->color() : sf::Color::Transparent;
            }
        });
    }

    else{
        sf::Transform transform;
        transform.translate(layer->coord() + layer->offsetCoord());
        transform.scale(layer->scale() * layer->offsetScale());
        transform.rotate(layer->angle() + layer->offsetAngle());
        render.transform = transform;
    }
    if(!vertex_array.size())
        return;
    const sf::Texture* texture = layer->texture();
    if (texture)
        render.texture = texture;

    if (camera->mode() == CameraC::WINDOW)
        window_->draw(&vertex_array[0],vertex_array.size(),layer->type(), render);
    else{
        camera->renderTexture().draw(&vertex_array[0],vertex_array.size(),layer->type(),render);

    }
}

void Renderer::interpolateCoordinates(LayerC *layer)
{
    if(layer->poolID() != layer->entityID())
        return;
    Entity* e = scene_->getEntity(layer->entityID());
    PointC* basepoint = e->getComponent<PointC>();
    layer->setCoord(basepoint->prevCoord() + (basepoint->coord() - basepoint->prevCoord()) * interpolation_);
    layer->setAngle(basepoint->prevAngle() + basepoint->rotation() * interpolation_);
    layer->setScale(basepoint->prevScale() + (basepoint->scale() - basepoint->prevScale()) * interpolation_);
    if (layer->isDynamic()){
        e->each<QuadC>([&](QuadC* quad)
        {
            if (quad->pointIndex()){
                PointC* point = e->getComponentByPointIndex<PointC>(quad->pointIndex());
                quad->setCoord(point->prevCoord() + (point->coord() - point->prevCoord()) * interpolation_);
                quad->setAngle(point->prevAngle() + point->rotation() * interpolation_);
                quad->setScale(point->prevScale() + (point->scale() - point->prevScale()) * interpolation_);
            }

        });
        e->each<LineC>([&](LineC* line)
        {
            if (line->pointIndex()){
                PointC* point = e->getComponentByPointIndex<PointC>(line->pointIndex());
                line->setCoord(point->prevCoord() + (point->coord() - point->prevCoord()) * interpolation_);
                line->setAngle(point->prevAngle() + point->rotation() * interpolation_);
                line->setScale(point->prevScale() + (point->scale() - point->prevScale()) * interpolation_);
            }
        });

    }
}

void Renderer::updateTweenCoord(TweenCoordC *tween)
{
    if (!tween->isEnabled())
        return;
    tween->update(frame_time_);
    Entity*e = scene_->getEntity(tween->entityID());
    LayerC* layer = e->getComponent<LayerC>();
    if (!layer)
        return;
    if (layer->isDynamic()){ //Absolute coords
        if (!tween->pointIndex())
            layer->setOffsetCoord(layer->offsetCoord() + tween->offset());
        else{
            e->eachByPointIndex<QuadC>(tween->pointIndex(),[&](QuadC* quad)
            {
                quad->setOffsetCoord(quad->offsetCoord() + tween->offset());
            });

            e->eachByPointIndex<LineC>(tween->pointIndex(),[&](LineC* line)
            {
                line->setOffsetCoord(line->offsetCoord() + tween->offset());
            });
        }
    }
    else{ //Relative coords
        if (!tween->pointIndex())
            layer->setOffsetCoord(layer->offsetCoord() + tween->offset());
        else{
            e->eachByPointIndex<QuadC>(tween->pointIndex(),[&](QuadC* quad)
            {
                std::vector<sf::Vertex>& vertex_array = scene_->getVertexArray(quad->poolID());
                quad->setOffsetCoord(quad->offsetCoord() + tween->offset());
                for(int i = 0; i != 4; ++i){
                    sf::Vertex& vertex = vertex_array[quad->poolIndex() * 4 + i];
                    vertex.position = quad->getVertexCoord(i,sf::Vector2f(),sf::Vector2f(),sf::Vector2f(),sf::Vector2f(1,1), 0, sf::Vector2f());
                    vertex.color = quad->isVisible()?quad->color() : sf::Color::Transparent;
                }
            });

            e->eachByPointIndex<LineC>(tween->pointIndex(),[&](LineC* line)
            {
                std::vector<sf::Vertex>& vertex_array = scene_->getVertexArray(line->poolID());
                line->setOffsetCoord(line->offsetCoord() + tween->offset());
                for(int i = 0; i != 2; ++i){
                    sf::Vertex& vertex = vertex_array[line->poolIndex() * 2 + i];
                    vertex.position = line->getVertexCoord(i,sf::Vector2f(),sf::Vector2f(),sf::Vector2f(),sf::Vector2f(1,1), 0, sf::Vector2f());
                    vertex.color = line->isVisible()?line->color() : sf::Color::Transparent;
                }
            });


        }
    }
}

void Renderer::updateTweenScale(TweenScaleC *tween)
{
    if (!tween->isEnabled())
        return;
    tween->update(frame_time_);
    Entity*e = scene_->getEntity(tween->entityID());
    LayerC* layer = e->getComponent<LayerC>();
    if (!layer)
        return;
    if (layer->isDynamic()){ //Absolute
        if (!tween->pointIndex())
            layer->setOffsetScale(layer->offsetScale() + tween->scale());
        else{
            e->eachByPointIndex<QuadC>(tween->pointIndex(),[&](QuadC* quad)
            {
                quad->setOffsetScale(quad->offsetScale() + tween->scale());
            });

            e->eachByPointIndex<LineC>(tween->pointIndex(),[&](LineC* line)
            {
                line->setOffsetScale(line->offsetScale() + tween->scale());
            });
        }

    }
    else{ //Relative
        if (!tween->pointIndex())
            layer->setOffsetScale(layer->offsetScale() + tween->scale());
        else{
            e->eachByPointIndex<QuadC>(tween->pointIndex(),[&](QuadC* quad)
            {
                std::vector<sf::Vertex>& vertex_array = scene_->getVertexArray(quad->poolID());
                quad->setOffsetScale(quad->offsetScale() + tween->scale());
                for(int i = 0; i != 4; ++i){
                    sf::Vertex& vertex = vertex_array[quad->poolIndex() * 4 + i];
                    vertex.position = quad->getVertexCoord(i,sf::Vector2f(),sf::Vector2f(),sf::Vector2f(),sf::Vector2f(1,1), 0, sf::Vector2f());
                    vertex.color = quad->isVisible()?quad->color() : sf::Color::Transparent;
                }
            });

            e->eachByPointIndex<LineC>(tween->pointIndex(),[&](LineC* line)
            {
                std::vector<sf::Vertex>& vertex_array = scene_->getVertexArray(line->poolID());
                line->setOffsetScale(line->offsetScale() + tween->scale());
                for(int i = 0; i != 2; ++i){
                    sf::Vertex& vertex = vertex_array[line->poolIndex() * 2 + i];
                    vertex.position = line->getVertexCoord(i,sf::Vector2f(),sf::Vector2f(),sf::Vector2f(),sf::Vector2f(1,1),0, sf::Vector2f());
                    vertex.color = line->isVisible()?line->color() : sf::Color::Transparent;
                }
            });


        }
    }
}

void Renderer::updateTweenAngle(TweenAngleC* tween)
{
    if (!tween->isEnabled())
        return;
    tween->update(frame_time_);
    Entity*e = scene_->getEntity(tween->entityID());
    LayerC* layer = e->getComponent<LayerC>();
    if (!layer)
        return;
    if (layer->isDynamic()){ //Absolute
        if (!tween->pointIndex()){
            layer->setOffsetAngle(layer->offsetAngle() + tween->angle());
        }
        else{
            e->eachByPointIndex<QuadC>(tween->pointIndex(),[&](QuadC* quad)
            {
                quad->setOffsetAngle(quad->offsetAngle() + tween->angle());
            });

            e->eachByPointIndex<LineC>(tween->pointIndex(),[&](LineC* line)
            {
                line->setOffsetAngle(line->offsetAngle() + tween->angle());
            });
        }
    }
    else{ //Relative
        if (!tween->pointIndex())
              layer->setOffsetAngle(layer->offsetAngle() + tween->angle());
        else{
            e->eachByPointIndex<QuadC>(tween->pointIndex(),[&](QuadC* quad)
            {
               std::vector<sf::Vertex>& vertex_array = scene_->getVertexArray(quad->poolID());
               quad->setOffsetAngle(quad->offsetAngle() + tween->angle());
               for(int i = 0; i != 4; ++i){
                    sf::Vertex& vertex = vertex_array[quad->poolIndex() * 4 + i];
                    vertex.position = quad->getVertexCoord(i,sf::Vector2f(),sf::Vector2f(),sf::Vector2f(),sf::Vector2f(1,1), 0, sf::Vector2f());
                    vertex.color = quad->isVisible()?quad->color() : sf::Color::Transparent;
               }
            });

            e->eachByPointIndex<LineC>(tween->pointIndex(),[&](LineC* line)
            {
                std::vector<sf::Vertex>& vertex_array = scene_->getVertexArray(line->poolID());
                line->setOffsetAngle(line->offsetAngle() + tween->angle());
                for(int i = 0; i != 2; ++i){
                    sf::Vertex& vertex = vertex_array[line->poolIndex() * 2 + i];
                    vertex.position = line->getVertexCoord(i,sf::Vector2f(),sf::Vector2f(),sf::Vector2f(),sf::Vector2f(1,1), 0, sf::Vector2f());
                    vertex.color = line->isVisible()?line->color() : sf::Color::Transparent;
                }
            });


        }
    }
}
