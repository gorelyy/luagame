#pragma once
class Entity;
#include <SFML/System/Vector3.hpp>
#include <SFML/System/Vector2.hpp>

class Audio{
public:
    Audio();
    float volume();
    sf::Vector2f coord();
    sf::Vector3f direction();
    Entity* focus();
    void    setVolume(float volume);
    void    setCoord(const sf::Vector2f& coord);
    void    setDirection(const sf::Vector3f& direction);
    void    bindFocus(Entity* e);
private:
    float x_,y_,z_;
    Entity* focus_;
};
