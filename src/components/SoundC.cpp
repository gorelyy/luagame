#include "luagame/components/SoundC.h"

SoundC::SoundC() :
    stream_(nullptr),
    x_(0.f),
    y_(0.f),
    z_(0.f)
{
}

void SoundC::bindSound(const sf::SoundBuffer* sound)
{
    sound_.setBuffer(*sound);
}

void SoundC::bindStream(sf::Music* music)
{
    stream_ = music;
}

void SoundC::play()
{
    if (stream_)
        stream_->play();
    else
        sound_.play();
}

void SoundC::pause()
{
    if (stream_)
        stream_->pause();
    else
        sound_.pause();
}

void SoundC::stop()
{
    if (stream_)
        stream_->stop();
    else
        sound_.stop();
}

void SoundC::setPlayingOffset(int seconds)
{
    sf::Time time = sf::seconds(seconds);
    if (stream_)
        stream_->setPlayingOffset(time);
    else
        sound_.setPlayingOffset(time);
}

void SoundC::setLoop(bool loop)
{
    if (stream_)
        stream_->setLoop(loop);
    else
        sound_.setLoop(loop);
}

void SoundC::setPitch(float pitch)
{
    if (stream_)
        stream_->setPitch(pitch);
    else
        sound_.setPitch(pitch);
}

void SoundC::setVolume(float volume)
{
    if (stream_)
        stream_->setVolume(volume);
    else
        sound_.setVolume(volume);
}

void SoundC::setCoord(const sf::Vector2f& coord)
{
    x_ = coord.x;
    y_ = coord.y;

    if (stream_)
        stream_->setPosition(x_,y_,z_);
    else
        sound_.setPosition(x_,y_,z_);
}

void SoundC::setDepth(float z)
{
    z_ = z;
    if (stream_)
        stream_->setPosition(x_,y_,z_);
    else
        sound_.setPosition(x_,y_,z_);
}

void SoundC::setRelativeToListener(bool relative)
{
    if (stream_)
        stream_->setRelativeToListener(relative);
    else
        sound_.setRelativeToListener(relative);
}

void SoundC::setMinDistance(float distance)
{
    if (stream_)
        stream_->setMinDistance(distance);
    else
        sound_.setMinDistance(distance);
}

void SoundC::setAttenuation(float attenuation)
{
    if (stream_)
        stream_->setAttenuation(attenuation);
    else
        sound_.setAttenuation(attenuation);
}


