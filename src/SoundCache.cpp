#include "luagame/SoundCache.h"

SoundCache::SoundCache() :
    enabled_(true)
{
    launchLoadingThread();
}

SoundCache::~SoundCache()
{
    for (auto each:stream_container_)
        delete each.second;
}

const sf::SoundBuffer* SoundCache::getSound(const std::string &name)
{
    if(!enabled_)
        return nullptr;
    auto it = sound_container_.find(name);
    if (it != sound_container_.end())
        return &it->second;
    sound_container_[name].loadFromFile(name);
    return &sound_container_[name];
}

sf::Music* SoundCache::getStream(const std::string &name)
{
    auto it = stream_container_.find(name);
    if (it != stream_container_.end())
        return it->second;
    stream_container_[name] = new sf::Music();
    stream_container_[name]->openFromFile(name);
    return stream_container_[name];
}

void SoundCache::asyncLoadSound(const std::string &filename)
{
    if (!enabled_){
        if (std::find(loading_queue_.begin(),
                      loading_queue_.end(),filename) == loading_queue_.end())
            loading_queue_.push_back(filename);
        return;
    }
    auto it = sound_container_.find(filename);
    if (it == sound_container_.end()){
        enabled_ = false;
        if (std::find(loading_queue_.begin(),
                      loading_queue_.end(),filename) == loading_queue_.end())
            loading_queue_.push_back(filename);
        return;
    }
}

void SoundCache::loadingThread()
{
    while(true){
        while(loading_queue_.empty()){
            enabled_ = true;
            sf::sleep(sf::milliseconds(16));
        }
        enabled_ = false;
        std::string& filename = loading_queue_.back();
        sound_container_[filename].loadFromFile(filename);
        loading_queue_.pop_back();
    }
}

void SoundCache::launchLoadingThread()
{
    loading_thread_ = std::thread(&SoundCache::loadingThread, this);
    loading_thread_.detach();
}
