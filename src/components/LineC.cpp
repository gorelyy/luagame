#include "luagame/components/LineC.h"

LineC::LineC()
{
}

void LineC::setLength(float length)
{
    length_ = length;
}

void LineC::setTextureLine(const sf::Vector2f &pointA, const sf::Vector2f &pointB)
{
    texture_line_[0] = pointA;
    texture_line_[1] = pointB;
}

sf::Vector2f LineC::getVertexCoord(uint index, const sf::Vector2f& scrolloffset,const sf::Vector2f& layercoord,const sf::Vector2f& layeroffset, const sf::Vector2f& layerscale,float layerangle, const sf::Vector2f& layerpivot)
{
    float angle = (angle_ + offset_angle_)  *  utils::constants::RAD;
    sf::Vector2f a = layercoord + layeroffset + (coord_ + offset_coord_ * scale_ + scrolloffset * scroll_factor_) * layerscale;
    sf::Vector2f pivot = a + pivot_ * layerscale * scale_;
    switch(index){
        case 0:
            if(angle)
                a =  sf::Vector2f(cosf(angle) * (a.x - pivot.x) - sinf(angle) * (a.y - pivot.y) + pivot.x,
                                  sinf(angle) * (a.x - pivot.x) + cosf(angle) * (a.y - pivot.y) + pivot.y);
            if (layerangle){
                angle = layerangle * utils::constants::RAD;
                pivot = layerpivot + layercoord + layeroffset;
                a = sf::Vector2f(cosf(angle) * (a.x - pivot.x) - sinf(angle) * (a.y - pivot.y) + pivot.x,
                                 sinf(angle) * (a.x - pivot.x) + cosf(angle) * (a.y - pivot.y) + pivot.y);
            }
            return a;

        case 1:
            a = a + (sf::Vector2f(length_,0) * layerscale * scale_);
            if (angle)
                a = sf::Vector2f(cosf(angle) * (a.x - pivot.x) - sinf(angle) * (a.y - pivot.y) + pivot.x,
                                 sinf(angle) * (a.x - pivot.x) + cosf(angle) * (a.y - pivot.y) + pivot.y);
            if (layerangle){
                angle = layerangle * utils::constants::RAD;
                pivot = layerpivot + layercoord + layeroffset;
                a = sf::Vector2f(cosf(angle) * (a.x - pivot.x) - sinf(angle) * (a.y - pivot.y) + pivot.x,
                                 sinf(angle) * (a.x - pivot.x) + cosf(angle) * (a.y - pivot.y) + pivot.y);
            }
            return a;
    }
}

sf::Vector2f LineC::getVertexTextureCoord(uint index)
{
    return texture_line_[index];
}

