#pragma once
class MoveC;
#include "../Scene.h"

class Motion{
public:
    Motion();
    void    update(Scene* scene, float p_dt);
private:
    void    loop(MoveC* component);

    float dt_;
    Scene* scene_;
};



