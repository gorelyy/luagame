#pragma once
#include"Utils.h"
#include <SFML/Graphics.hpp>
#include <map>
#include <vector>
#include <string>
#include <thread>

class ImgCache {
public:
    ImgCache();
    uint size() const;
    const sf::Texture* getTexture(const std::string& filename);
    void    asyncLoadTexture(const std::string& filename);
    void    launchLoadingThread();
private:
    void    loadingThread();

    bool enabled_;
    std::map<std::string, sf::Texture> tex_container_;
    std::thread loading_thread_;
    std::vector<std::string> loading_queue_;
};
