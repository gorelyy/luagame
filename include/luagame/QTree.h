#pragma once
class BoxC;
class CollisionCheckC;
#include "QTreeNode.h"
#include "AABB.h"
#include <SFML/Graphics/Rect.hpp>
#include <unordered_map>
#include <vector>
#include <array>
#include<iostream>
#include "utils.h"
#include "globals.h"

class QTree{
typedef std::pair<BoxC*, BoxC*> collisionPair;
public:
    QTree();
    QTreeNode*    findBoxNode(BoxC* box);
    CollisionCheckC* collisionData();
    const std::vector<collisionPair>& collisions()      const;
    const std::vector<sf::IntRect>& cells()             const;
    const std::string& groupA()                         const;
    const std::string& groupB()                         const;
    const std::vector<QTreeNode*>& boxBNotLeavesNodes() const;
    bool isDynamic()                                    const;
    uint maxLevel()                                     const;
    void    load(const std::string& A, const std::string& B,const sf::IntRect& area, uint maxlevel, CollisionCheckC* collision_data);
    void    findCollisions(BoxC* boxA);
    void    updateBoxA(BoxC* boxA);
    void    updateBoxB(BoxC* boxB);
    void    updateStaticBoxesB();
    void    clearCollisions();
    void    addBoxBNotLeafeNode(QTreeNode* node);
    void    setDynamic(bool value);
private:
    void    lookDownForCollisionsWithB(BoxC*boxB, QTreeNode* node);
    void    lookDownForCollisionsWithA(BoxC*boxA, QTreeNode* node);

    bool dynamic_;
    uint maxlevel_;
    CollisionCheckC* collision_data_;
    std::string groupA_,
                groupB_;
    sf::Vector2f center_;
    sf::Vector2f radius_;
    sf::Vector2f invsize_;
    std::vector<collisionPair> collisions_;
    std::vector<sf::IntRect> cell_rectangles_;
    std::vector<QTreeNode*> boxB_not_leaves_nodes_;
    std::vector<QTreeNode> nodes_;
    std::array<QTreeNode*, 8> levels_;
};
