#include "luagame/QTreeNode.h"
#include "luagame/QTree.h"

QTreeNode::QTreeNode() :
    boxA_inside_(false)
{
    kids_[0] = nullptr;
}

void QTreeNode::set(const sf::Vector2f& coord, const sf::Vector2f& size, unsigned int level, uint index/*, SECTION section,  QTreeNode* parent, QTree* tree*/)
{
    coord_= coord;
    size_ = size;
    level_ = level;
    index_ = index;
}

void QTreeNode::setChild(uint index, QTreeNode *ptr)
{
    kids_[index] = ptr;
}

void QTreeNode::addBoxA(BoxC* box)
{
    boxesA_.push_back(box);
}

void QTreeNode::addBoxB(BoxC* box)
{
    boxesB_.push_back(box);
}

const std::array<QTreeNode*,4>& QTreeNode::kids() const
{
    return kids_;
}

uint QTreeNode::level() const
{
    return level_;
}

size_t QTreeNode::index() const
{
    return index_;
}

const std::vector<BoxC*>& QTreeNode::boxesA() const
{
    return boxesA_;
}

const std::vector<BoxC*>& QTreeNode::boxesB() const
{
    return boxesB_;
}

void QTreeNode::removeBoxA(BoxC* box){
    auto& it = std::find(boxesA_.begin(),boxesA_.end(), box);
    std::swap(*it, boxesA_.back());
    boxesA_.pop_back();
}

void QTreeNode::removeBoxB(BoxC* box){
    auto& it = std::find(boxesB_.begin(),boxesB_.end(), box);
    std::swap(*it, boxesB_.back());
    boxesB_.pop_back();
}

