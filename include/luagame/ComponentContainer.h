#pragma once
//NEW_COMPONENT_TYPE
#include "luagame/components/QuadC.h"
#include "luagame/components/LineC.h"
#include "luagame/components/MoveC.h"
#include "luagame/components/ScriptC.h"
#include "luagame/components/AnimC.h"
#include "luagame/components/BoxC.h"
#include "luagame/components/CollisionCheckC.h"
#include "luagame/components/PointC.h"
#include "luagame/components/LayerC.h"
#include "luagame/components/CameraC.h"
#include "luagame/components/TweenCoordC.h"
#include "luagame/components/TweenScaleC.h"
#include "luagame/components/TweenAngleC.h"
#include "luagame/components/SoundC.h"
#include "luagame/components/TextC.h"
#include "luagame/components/TimerC.h"
#include <set>
#include <vector>
#include <deque>
#include <unordered_map>
#include <string>
#include <luabind/luabind.hpp>
#include "globals.h"
#include "GroupContainer.h"

template <typename cClass>
class BaseComponentContainer{
public:
    
    BaseComponentContainer()
    {
    }
    
    size_t storeComponent(const cClass& component, ID entity_id)
    {
        std::vector<cClass>& pool = component_pool_[entity_id];
        uint pool_index = pool.size();
        pool.push_back(component);
        pool.back().setPoolID(entity_id);
        pool.back().setPoolIndex(pool_index);
        return pool_index;
    }
    template <typename Functor>
    void eachC(ID pool_id, const Functor& f)
    {
        if(!component_pool_.count(pool_id))
            return;
        for(cClass& component : component_pool_[pool_id])
            if (component.enabled())
                f(&component);
    }
    template <typename Functor>
    void eachExistingC(ID pool_id, const Functor& f)
    {
        if(!component_pool_.count(pool_id))
            return;
        for(cClass& component : component_pool_[pool_id])
            f(&component);
    }
    void eachC(ID pool_id, const luabind::object& luafunction)
    {
        if(!component_pool_.count(pool_id))
            return;
        eachC_lua_function_stack_.push_back(luafunction);
        for(cClass& component : component_pool_[pool_id])
            if (component.enabled())
                eachC_lua_function_stack_.back()(&component);
        eachC_lua_function_stack_.pop_back();
    }
    void eachExistingC(ID pool_id, const luabind::object& luafunction)
    {
        if(!component_pool_.count(pool_id))
            return;
        eachC_lua_function_stack_.push_back(luafunction);
        for(cClass& component : component_pool_[pool_id])
            eachC_lua_function_stack_.back()(&component);
        eachC_lua_function_stack_.pop_back();
    }
    void eachDisabledC(ID pool_id, const luabind::object& luafunction)
    {
        if(!component_pool_.count(pool_id))
            return;
        eachC_lua_function_stack_.push_back(luafunction);
        for(cClass& component : component_pool_[pool_id])
            if (!component.enabled())
                eachC_lua_function_stack_.back()(&component);
        eachC_lua_function_stack_.pop_back();
    }
    cClass* getComponent(const std::pair<ID, size_t>& address)
    {
		if (component_pool_.count(address.first) && component_pool_[address.first].size() > address.second)
			return &(component_pool_[address.first][address.second]);
		return nullptr;
    }
    size_t poolSize(ID id)
    {
        if (component_pool_.count(id))
            return component_pool_[id].size();
        return 0;
    }
    void deletePool(ID id)
    {
        component_pool_.erase(id);
    }

    void shrinkToFit(ID id)
    {
        if (component_pool_.count(id))
            return;
        auto& pool = component_pool_[id];
        std::vector<cClass>(pool).swap(pool);
    }

    void reserve(ID id, size_t size)
    {
        if (component_pool_.count(id))
            return;
        auto& pool = component_pool_[id];
        if (!pool.capacity())
            pool.reserve(size);
    }

    size_t poolCapacity(ID id)
    {
        if (component_pool_.count(id))
            return component_pool_[id].capacity();
        return 0;
    }
private:

    std::unordered_map<ID, std::vector<cClass> > component_pool_;
    std::deque<luabind::object> eachC_lua_function_stack_;
    
};
                           //NEW_COMPONENT_TYPE
class ComponentContainer:  private BaseComponentContainer<QuadC>,
                           private BaseComponentContainer<LineC>,
                           private BaseComponentContainer<MoveC>,
                           private BaseComponentContainer<ScriptC>,
                           private BaseComponentContainer<AnimC>,
                           private BaseComponentContainer<BoxC>,
                           private BaseComponentContainer<CollisionCheckC>,
                           private BaseComponentContainer<PointC>,
                           private BaseComponentContainer<LayerC>,
                           private BaseComponentContainer<CameraC>,
                           private BaseComponentContainer<TweenCoordC>,
                           private BaseComponentContainer<TweenScaleC>,
                           private BaseComponentContainer<TweenAngleC>,
                           private BaseComponentContainer<SoundC>,
                           private BaseComponentContainer<TextC>,
                           private BaseComponentContainer<TimerC>{
    
public:
    
    ComponentContainer();

    template <typename cClass, typename Functor>
    void eachC(ID pool_id, const Functor& f)
    {
        BaseComponentContainer<cClass>::eachC(pool_id, f);
    }
    template <typename cClass, typename Functor>
    void eachExistingC(ID pool_id, const Functor& f)
    {
        BaseComponentContainer<cClass>::eachExistingC(pool_id, f);
    }
    template <typename cClass>
    void eachC(ID pool_id, const luabind::object& f)
    {
        BaseComponentContainer<cClass>::eachC(pool_id, f);
    }
    template <typename cClass>
    void eachExistingC(ID pool_id, const luabind::object& f)
    {
        BaseComponentContainer<cClass>::eachExistingC(pool_id, f);
    }
    template <typename cClass>
    void eachDisabledC(ID pool_id, const luabind::object& f)
    {
        BaseComponentContainer<cClass>::eachDisabledC(pool_id, f);
    }
    template <typename cClass>
    std::pair<ID,size_t> addComponent(const cClass& component, ID entity_id)
    {
        group_container_->defaultGroup<cClass>()->add(entity_id);
        size_t component_index = BaseComponentContainer<cClass>::storeComponent(component, entity_id);
        return std::make_pair(entity_id, component_index);
    }
    template <typename cClass>
    cClass* getComponent(const std::pair<ID, size_t>& address)
    {
		if (!address.first) return nullptr;
		return BaseComponentContainer<cClass>::getComponent(address);
    }
    template <typename cClass>
    size_t poolSize(ID id)
    {
        return BaseComponentContainer<cClass>::poolSize(id);
    }

    template <typename cClass>
    void shrinkToFitComponentPool(ID id)
    {
        BaseComponentContainer<cClass>::shrinkToFit(id);
    }

    template <typename cClass>
    void reserveComponentPool(ID id, size_t size)
    {
        BaseComponentContainer<cClass>::reserve(id, size);
    }

    bool    isSceneConstructed() const;
    void    bindGroupContainer(GroupContainer* gc);
    void    deleteEntity(ID id);
    void    setSceneConstructed(bool value);

private:
    GroupContainer* group_container_;
    bool scene_constructed_;
};

