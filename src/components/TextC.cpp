#include "luagame/components/TextC.h"

TextC::TextC():
    bold_(false),
    requires_update_(false),
    requires_hide_(false),
    tweentype_(TextC::NONE),
    prevchar_(0),
    currentline_(0),
    prevtextsize_(0),
    current_typing_char_(-1),
    color_(sf::Color::White),
    dynamic_(true)
{
}

void TextC::bindFont(const sf::Font* font)
{
    font_ = font;
}

void TextC::setFontSize(int size)
{
    if (fontsize_ == size)
        return;
    fontsize_ = size;
    hspace_ = (float)font_->getGlyph(L' ',fontsize_,bold_).advance;
    vspace_ = (float)font_->getLineSpacing(fontsize_);
    requires_update_ = true;
}

void TextC::setText(std::string text)
{
    prevtextsize_ = text_.size();
    if (capacity_ < text.size())
        text[capacity_ - 1] = '\n';
    else if (text.back() != '\n')
        text.push_back('\n');
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    text_ = converter.from_bytes(text);
    requires_update_ = true;
    for(int i = 0, limit = std::min(capacity_, text_.size()); i < limit; ++i){
        calculateLines(i);
    }
}

const sf::Texture* TextC::texture() const
{
    return &font_->getTexture(fontsize_);
}

sf::Vector2f TextC::characterCoord(size_t index) const
{
   sf::Vector2f position;
   uint prevChar = 0;
   for (size_t i = 0; i < index; ++i){
       uint curChar = text_[i];
       position.x += (float)font_->getKerning(prevChar, curChar, fontsize_);
       prevChar = curChar;
       switch (curChar){
           case ' ': position.x += hspace_; continue;
           case '\t': position.x += hspace_ * 4; /*text_[i] = ' '; */continue;
           case '\n': position.y += vspace_; position.x = 0; continue;
       }
       position.x += (float)font_->getGlyph(curChar,fontsize_,bold_).advance;
   }
   const sf::Glyph& glyph = font_->getGlyph(text_[index],fontsize_,bold_);
   position += sf::Vector2f(glyph.bounds.left, glyph.bounds.top);
   return position;
}

sf::Vector2f TextC::iterationCharacterCoord(size_t index)
{
    const sf::Glyph& glyph = font_->getGlyph(text_[index],fontsize_,bold_);

    if(!index)
        return curchar_coord_ + sf::Vector2f(glyph.bounds.left, glyph.bounds.top);

    uint curchar = text_[index - 1];
    curchar_coord_.x += (float)font_->getKerning(prevchar_, curchar, fontsize_);
    prevchar_ = curchar;
    switch (curchar){
        case ' ':
            curchar_coord_.x += hspace_;
            break;
        case '\t':
            curchar_coord_.x += hspace_ * 4;
            break;
        case '\n':
            curchar_coord_.y += vspace_;
            curchar_coord_.x = 0;
            break;
        default:
            curchar_coord_.x += (float)font_->getGlyph(curchar,fontsize_,bold_).advance;
    }

    return curchar_coord_ + sf::Vector2f(glyph.bounds.left, glyph.bounds.top);
}

void TextC::calculateLines(int index)
{
    if (text_[index] != '\n')
        return;

    int lineindex = lines_.size()? lines_.back().first + lines_.back().second : 0;
    int linesize = index - lineindex;
    lines_.push_back(std::make_pair(lineindex,linesize));
}

void TextC::updateTime(float time)
{
    if (current_typing_char_ < 0)
        return;

    time_ += time;
    if (time_ > typing_speed_){
        time_= 0;
        current_typing_char_ ++;

        if (current_typing_char_ > max_typing_char_)
            current_typing_char_ = -1;

        else if (typing_callback_.is_valid()){
            try{
                typing_callback_(current_typing_char_ - 1);
            }
            catch(const luabind::error &e){
                std::cerr<<"luagame_error in typing callback: "<<e.what();
                luabind::object error_msg(luabind::from_stack(e.state(), -1));
                std::cerr<<error_msg<<std::endl;
            }
        }
    }

}

void TextC::type(int lines, float speed)
{
    time_ = 0;
    speed = speed ? speed : 0.001f;
    typing_speed_ = 1 / speed;
    current_typing_char_ = lines_[currentline_].first;
    auto& pair = lines_[std::min(lines_.size(),currentline_ + lines)-1];
    max_typing_char_ = pair.first + pair.second;
}

int TextC::currentTypingChar() const
{
    return current_typing_char_;
}

sf::Vector2f TextC::characterSize(int index) const
{
    const sf::Glyph& glyph = font_->getGlyph(text_[index],fontsize_,bold_);
    return sf::Vector2f(glyph.bounds.width, glyph.bounds.height);
}

sf::Vector2f TextC::characterOffset(int index) const
{
    const sf::Glyph& glyph = font_->getGlyph(text_[index],fontsize_,bold_);
    return sf::Vector2f(glyph.bounds.left, glyph.bounds.top);
}

sf::IntRect TextC::characterTextureRect(int index) const
{
    const sf::Glyph& glyph = font_->getGlyph(text_[index],fontsize_,bold_);
    return glyph.textureRect;
}

bool TextC::characterIsValid(int index) const
{
    char ch = text_[index];
    if (ch == ' ' || ch  == '\t' || ch == '\n')
        return false;
    return true;
}

bool TextC::requiresUpdate() const
{
    return requires_update_;
}

void TextC::updated()
{
    requires_update_ = false;
    curchar_coord_ = sf::Vector2f();
    prevchar_ = 0;
}

TextC::TEXTTWEEN TextC::textTween() const
{
    return tweentype_;
}

void TextC::setTextTween(TEXTTWEEN type)
{
    tweentype_ = type;
}

const std::wstring& TextC::textString() const
{
    return text_;
}

void TextC::setTypingCallback(const luabind::object &callback)
{
    typing_callback_ = callback;
}

void TextC::hide()
{
    requires_hide_ = true;
}

void TextC::hidden()
{
    requires_hide_ = false;
}

bool TextC::requiresHide() const
{
    return requires_hide_;
}

void TextC::setCapacity(size_t size)
{
    capacity_ = size;
}

size_t TextC::capacity() const
{
    return capacity_;
}

size_t TextC::prevTextSize() const
{
    return prevtextsize_;
}

void TextC::setColor(const sf::Color &color)
{
    color_ = color;
    requires_update_ = true;
}

const sf::Color& TextC::color() const
{
    return color_;
}

void TextC::setDynamic(bool value)
{
    dynamic_ = value;
    requires_update_ = true;
}

bool TextC::isDynamic() const
{
    return dynamic_;
}
