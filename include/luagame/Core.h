#pragma once
class Scene;
#include <SFML/Window.hpp>
#include "systems/Renderer.h"
#include "systems/Motion.h"
#include "systems/Scripting.h"
#include "systems/Collision.h"
#include "systems/SoundMixer.h"
#include "systems/Typewriter.h"
#include "systems/PointGraph.h"
#include "Input.h"
#include "ImgCache.h"
#include "SoundCache.h"
#include "FontCache.h"
#include "Audio.h"
#include "Logger.h"
#include "SceneBuilder.h"
extern "C"
{
    #include <lua.h>
    #include <lualib.h>
    #include <lauxlib.h>
}
#include <luabind/luabind.hpp>
#include <luabind/operator.hpp>
#include <map>
#include <deque>

class Core {
public:
    Core();
    Scene*  scene(const std::string& name);
    const sf::Texture*     getTexture(const std::string& name);
    const sf::SoundBuffer* getSound(const std::string& name);
    sf::Music*       getSoundStream(const std::string& name);
    const sf::Font* getFont(const std::string& name);
    void    run();
    void    exitNow();
    void    selectScene(const std::string& name);
    void    addScene(const std::string& name, const std::string& script_name);
    void    buildScene(Scene* scene);
    void    asyncBuildScene(Scene* scene);
    void    deleteScene(const std::string& name);
    void    deleteScene(Scene* scene);
    void    resetScene(std::string name);
    void    resetScene(Scene* scene);
    void    asyncLoadTexture(const std::string& name);
    void    asyncLoadSound(const std::string& name);
    void    resetLogger();

private:
    void    mainLoop();

    lua_State * core_lua_state_;
    std::map<std::string, lua_State*> lua_state_map_;
    luabind::object update_function_;
    bool exit_;
    bool focused_;
	bool firstloop_;
    bool vsync_;
    uint framerate_;
    double      framerate_cap_;
    double      dt_;
    Renderer    renderer_; 
    Motion      motion_; 
    Scripting   scripting_;
    Collision   collision_;
    SoundMixer  sound_mixer_;
    Typewriter  typewriter_;
    PointGraph  pointgraph_;
    Input input_;
    Audio audio_;
    SoundCache sound_cache_;
    ImgCache image_cache_;
    FontCache font_cache_;
    SceneBuilder scenebuilder_;
    Logger logger_;
    sf::Clock clock_;
    sf::RenderWindow window_;
    Scene* current_scene_;
    std::map<std::string, Scene> scene_;
    std::vector<lua_State*> deleted_lua_states_;
};



