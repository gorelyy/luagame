#pragma once
#include "../Component.h"
#include "../Tweenable.h"

class TweenAngleC : public Component{
public:
    TweenAngleC();
    bool isEnabled()    const;
    float angle()       const;
    void    set(float angle, float time);
    void    set(float angle, float time, Ease::FUNCTION f, Ease::TYPE type);
    void    set(float angle, float time, Ease::FUNCTION f, Ease::TYPE type, int repeat, unsigned char mode);
    void    addCallbackOnFinish(const luabind::object& callback);
    void    addCallbackOnRepeat(const luabind::object& callback);
    void    addCustomEasing(const luabind::object& f);
    void    update(double dt);
private:
    Tweenable<float> tween_float_;

};
