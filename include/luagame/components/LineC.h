#pragma once
#include "../Utils.h"
#include "../Component.h"
#include "../Drawable.h"
#include <SFML/Graphics/Color.hpp>
#include <SFML/System/Vector2.hpp>
#include <array>

class LineC : public Component, public Drawable{
public:
    LineC();
    void    setLength(float length);
    void    setTextureLine(const sf::Vector2f& pointA, const sf::Vector2f& pointB);
    sf::Vector2f getVertexTextureCoord(uint index);
    sf::Vector2f getVertexCoord(uint index,
                                const sf::Vector2f& scrolloffset,
                                const sf::Vector2f& layercoord,
                                const sf::Vector2f& layeroffset,
                                const sf::Vector2f& layerscale,
                                float layerangle, const sf::Vector2f& layerpivot);
private:
    float length_;
    std::array<sf::Vector2f,2> texture_line_;
};
